﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Economics
{
    public interface IEconomise
    {
        void Generate(int numberAmount, int amount, bool regenerateOnError);
        void Generate(int numberAmount, int amount, int seed, bool regenerateOnError);
        void Load(TextAsset res);
        void CreateTextAsset(string output);
        int AssignID();
        void OccupyID(int _id);

        // other functions
        T GetData<T>(int id, int index);
        int GetDataAmount();
        bool VerifyData(int id, out string error);
        Vector2 GetPoint(int id, int index);
        Vector2 GetPointViaMapping(int id, int index);
        Vector2 GetTarget(int id, int index, bool requireTrade);

        // gets sorted list
        List<T> GetSortedData<T>(int id);
        List<T> GetSortedData<T>(int id, int amount);
        

        // Get numbers data
        List<Vector2> GetPoints(int id);
        List<List<Vector2>> GetCurves(int id);
        Dictionary<int, float> GetMapping(int id);
        Dictionary<int, Vector2> GetMappedPoints(int id);
        Dictionary<int, Vector2> GetTargetPoints(int id);
        Dictionary<int, List<Vector2>> GetTradeLines(int id);
        Dictionary<int, float> GetTradeRatios(int id);
        float GetTradeRatio(int id, int index);
        string ToString(int id);        

    }

    public interface IEcoData<data>
    {
        data GetInternally(EconomicNumbers<data>.EcoNumbers numbers, int index);
    }
}
