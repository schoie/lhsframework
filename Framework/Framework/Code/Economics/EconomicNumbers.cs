﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Economics
{

    public enum eNumbersMode
    {

    }

    public abstract class EconomicNumbers<DataClass> : IEconomise, IEcoData<DataClass>
    {
        public static char DATA_CHR_Delim = ',';
        public static string DATA_STR_Delim = DATA_CHR_Delim.ToString();
        const float epsilon = 0.0000001f;

        // flags
        private bool mapPointsFlag = true;

        public EconomicNumbers(bool _mapPoints)
        {
            mapPointsFlag = _mapPoints;
        }

        // if we wanted to implemente multiple numbers
        public class EcoNumbers
        {
            public DataClass[] Data;
            public List<Vector2> Points;
            public List<List<Vector2>> Curves;
            public Dictionary<int, float> mappingFloat;
            public Dictionary<int, Vector2> mappedPoints;
            public string errorString = "";

            // add more functionality here for various number systems
            public Dictionary<int, List<Vector2>> TradeLines;
            public Dictionary<int, Vector2> TradeTargets;
            public Dictionary<int, float> TradeRatios;
        }

        public List<EcoNumbers> Numbers = new List<EcoNumbers>();
        public List<int> NumbersToAssign = new List<int>();

        public EconomicNumbers(){}

        // virtual functions for Mapping, Data, Points, 
        // these can be overriden
        public virtual void GenerateMapping(EcoNumbers numbers) { ; }
        public virtual void GenerateData(EcoNumbers numbers, int amount) { ; }
        public virtual void GeneratePoints(EcoNumbers numbers) { ; }
        public virtual List<Vector2> GeneratePoint(DataClass[] data, int numToUse) { return null; }

        // Data functions
        public void Generate(int numberAmount, int amount, bool regenerateOnError)
        {
            EcoNumbers numbers;
            int maxErrors = 100;
            int curErrors = 0;

            for (int i = 0; i < numberAmount; ++i)
            {
                numbers = GetNewEconNumbers(amount); // get a set                

                if (regenerateOnError == true)
                {
                    curErrors = 0;

                    while (numbers.errorString != "" && curErrors < maxErrors)
                    {
                        numbers = GetNewEconNumbers(amount); // new set, errors were found

                        curErrors++;
                    }
                }

                AddNumbers(numbers);
            }
        }
        EcoNumbers GetNewEconNumbers(int amount)
        {
            EcoNumbers numbers = new EcoNumbers();

            if (mapPointsFlag)
            {
                GenerateMapping(numbers);
            }

            GenerateData(numbers, amount);
            GeneratePoints(numbers);

            if (mapPointsFlag)
            {
                MapPoints(numbers);
            }

            return numbers;
        }

        public void Generate(int numberAmount, int amount, int seed, bool regenerateOnError)
        {
            int randSeed = UnityEngine.Random.Range(0, 10000000);
            //int curSeed = ;
            UnityEngine.Random.InitState(seed);
            Generate(numberAmount, amount, regenerateOnError);
            UnityEngine.Random.InitState(randSeed);
        }
        public void Load(TextAsset res)
        {
            LoadData(res.text);
        }
        public void CreateTextAsset(string output)
        {
            File.WriteAllText(Application.dataPath + "/" + output + ".txt", this.GetTextToAssetData());
        }
        public int AssignID()
        {
            // for random assigning
            //return UnityEngine.Random.Range(0, Numbers.Count);

            // incremental assigning
            /*int returnVal = assignCount;
            assignCount++;
            if (assignCount >= Numbers.Count)
                assignCount = 0;
            return returnVal;*/

            if(NumbersToAssign.Count <= 0)
            {
                LHSDebug.LogWarning("All Numbers have been allocated for - Returning Random(0, Count) ");
                return UnityEngine.Random.Range(0, Numbers.Count);
            }

            // grab the first number, remove it then return that number
            int firstNumber = NumbersToAssign[0];
            NumbersToAssign.RemoveAt(0);
            return firstNumber;
        }

        public void OccupyID(int _id)
        {
            if(_id >= 0 && _id < NumbersToAssign.Count)
            {
                NumbersToAssign.Remove(_id);
            }
        }

        private void AddNumbers(EcoNumbers _numbers)
        {
            Numbers.Add(_numbers);
            NumbersToAssign.Add(Numbers.Count - 1);
        }

        // Points Generator
        /*public void GeneratePoints(EcoNumbers numbers)
        {
            numbers.Points = new List<Vector2>();
            numbers.Curves = new List<List<Vector2>>();
            for (int i = 0; i < numbers.Data.Length; i++)
            {
                List<Vector2> points = GeneratePoint(numbers.Data, i);
                numbers.Curves.Add(points);

                if (i == 0 || i == 1) // dont select corner cases
                    numbers.Points.Add(GeneratePointOn(points));
                else if (i == 2 && numbers.Curves[1].Count == 2) // otherwise, drop down to more general case
                    numbers.Points.Add(GeneratePointOn(points));
                else
                    numbers.Points.Add(GeneratePointOnLargerThan(numbers, points, numbers.Points[i - 1]));
            }

        }*/
        protected Vector2 GeneratePointOutside(List<Vector2> points, float percent = 1.25f)
        {
            Vector2 onLine = GeneratePointOn(points);
            Vector2 outside = new Vector2(
                (int)Mathf.Max(onLine.x * percent, onLine.x + 1),
                (int)Mathf.Max(onLine.y * percent, onLine.y + 1));
            return outside;
        }
        protected Vector2 GeneratePointInside(List<Vector2> points, float percent = 0.66f)
        {
            Vector2 onLine = GeneratePointOn(points);
            Vector2 inside = new Vector2(
                (int)Mathf.Min(onLine.x * percent, onLine.x - 1),
                (int)Mathf.Min(onLine.y * percent, onLine.y - 1));
            return inside;
        }
        protected Vector2 GeneratePointOn(List<Vector2> points, bool avoidCornerCases = true)
        {
            if (points.Count > 0)
            {
                if (avoidCornerCases == false || points.Count < 3)
                    return points[UnityEngine.Random.Range(0, points.Count)];
                else
                    return points[UnityEngine.Random.Range(1, points.Count - 1)];
            }
            else
            {
                return new Vector2(0f, 0f);
            }
        }

        protected Vector2 GeneratePointOnLargerThan(EcoNumbers numbers, List<Vector2> points, Vector2 small)
        {
            // create a new list of points
            List<Vector2> availablePoints = new List<Vector2>();
            foreach (Vector2 pS in points)
            {
                //if (pS.x >= (small.x - epsilon) && pS.y >= (small.y - epsilon)) //|| (pS.x > small.x && pS.y >= small.y))
                if ((pS.x - 1f) > small.x && pS.y >= (small.y - epsilon) || pS.x >= (small.x - epsilon) && (pS.y - 1f) > small.y)
                {
                    availablePoints.Add(pS);
                }
            }

            if (availablePoints.Count == 0)
            {
                numbers.errorString += "[Cannot find a point larger than " + small.x + "," + small.y  + " for generating consumption points]";
                return points[UnityEngine.Random.Range(0, points.Count)];
            }

            return GeneratePointOn(availablePoints, false);
        }
        private void MapPoints(EcoNumbers numbers)
        {
            numbers.mappedPoints = new Dictionary<int, Vector2>();

            /*int lastValue = 0;
            foreach(KeyValuePair<int,int> kvp in numbers.mapping)
            {
                int thisValue = kvp.Value;
                int difference = thisValue - lastValue;
                if (difference > 1)
                {
                    // get point at last value
                    Vector2 p1 = this.GetPoint(numbers, lastValue);
                    
                    // get point at this value
                    Vector2 p2 = this.GetPoint(numbers, thisValue);

                    // get difference
                    Vector2 dif = p2 - p1;

                    // get difStep
                    Vector2 step = dif / difference;

                    // fill in the gaps
                    for (int i = 1; i <= difference; ++i)
                    {
                        numbers.mappedPoints.Add(lastValue + i, p1 + (step * i));
                    }
                }
                else
                {
                    numbers.mappedPoints.Add(thisValue, this.GetPoint(numbers, thisValue));
                }
                lastValue = thisValue;
            }*/

            if (numbers.mappingFloat == null)
            {
                LHSDebug.LogError("Mapping float is null");
            }

            foreach (KeyValuePair<int, float> kvp in numbers.mappingFloat)
            {
                int floor = Mathf.FloorToInt(kvp.Value);
                int ceiling = Mathf.CeilToInt(kvp.Value);
                
                if (floor == ceiling)
                {
                    Vector2 targetPoint = this.GetPoint(numbers, floor);
                    numbers.mappedPoints.Add(kvp.Key, targetPoint);
                } 
                else
                {
                    Vector2 targetLow = this.GetPoint(numbers, floor);
                    Vector2 targetHigh = this.GetPoint(numbers, ceiling);

                    Vector2 diff = targetHigh - targetLow;

                    float scale = kvp.Value - (float)floor;
                    Vector2 diffScaled = diff * scale;
                    Vector2 diffRounded = diffScaled;
                    diffRounded.x = Mathf.Round(diffRounded.x);
                    diffRounded.y = Mathf.Round(diffRounded.y);

                    Vector2 targetPoint = targetLow + diffRounded;
                    numbers.mappedPoints.Add(kvp.Key, targetPoint);
                }
            }

            for (int i = 1; i < numbers.mappedPoints.Count; i++)
            {
                if (numbers.mappedPoints[i] == numbers.mappedPoints[i-1])
                {
                    numbers.errorString += "[Point " + i + " is the same as " + (i - 1) + "]";
                }
            }

            /*
            // debug
            int count = 0;
            string s = "";
            foreach (KeyValuePair<int, Vector2> kvp in numbers.mappedPoints)
            {
                s += (String.Format("mappedPoints {0} = ({1},{2})", count, kvp.Key, kvp.Value) + "\n");
                count++;
            }
            LHSDebug.Log(s);
            */
        }


        // other functions
        public DataClass GetInternally(EcoNumbers numbers, int index)
        {
            if (index < 0 || index >= numbers.Data.Length)
                return default(DataClass);

            return numbers.Data[index];
        }
        public T GetData<T>(int id, int index)
        {
            EcoNumbers numbers;
            if (!GetNumbers(id, out numbers))
                return default(T);

            return (T)(object)this.GetInternally(numbers, index);
        }
        public T GetDataSafe<T>(int id, int index)
        {
            EcoNumbers numbers;
            if (!GetNumbers(id, out numbers))
                return default(T);

            return (T)(object)this.GetInternally(numbers, index);
        }
        public virtual bool VerifyData(int id, out string error)
        {
            EcoNumbers numbers;
            if (!GetNumbers(id, out numbers))
            {
                error = "";
            }
            else
            {
                error = numbers.errorString;
            }                        

            return error == "";
        }

        public T GetData<T>(EcoNumbers numbers, int index)
        {
            return (T)(object)this.GetInternally(numbers, index);
        }
        public int GetDataAmount()
        {
            return Numbers.Count;
        }

        public Vector2 GetPoint(int id, int index)
        {
            EcoNumbers numbers;
            if (!GetNumbers(id, out numbers))
                return default(Vector2);

            if (index >= numbers.Points.Count)
                return default(Vector2);

            return numbers.Points[index];
        }
        public Vector2 GetPoint(EcoNumbers numbers, int index)
        {
            if (index >= numbers.Points.Count)
                return default(Vector2);

            return numbers.Points[index];
        }
        public Vector2 GetPointViaMapping(int id, int index)
        {
            EcoNumbers numbers;
            if (!GetNumbers(id, out numbers))
                return default(Vector2);

            return GetPointViaMapping(numbers, index);
        }
        public Vector2 GetPointViaMapping(EcoNumbers numbers, int index)
        {
            Vector2 returnVec;
            if(numbers.mappedPoints.TryGetValue(index, out returnVec))
            {
                return returnVec;
            }

            return default(Vector2);
        }
        public Vector2 GetTarget(int id, int index, bool requireTrade)
        {
            EcoNumbers numbers;
            if (!GetNumbers(id, out numbers))
                return default(Vector2);

            return GetTarget(numbers, index, requireTrade);
        }
        public Vector2 GetTarget(EcoNumbers numbers, int index, bool requireTrade)
        {
            Vector2 returnVec;
            
            if (requireTrade == true)
            {
                if (numbers.TradeTargets.TryGetValue(index, out returnVec))
                {
                    return returnVec;
                }
            }
            else
            {
                return GetPoint(numbers, index);
            }            

            return GetPointViaMapping(numbers, index);
        }

        private bool GetNumbers(int id, out EcoNumbers numbers)
        {
            if(id >= 0 && id < Numbers.Count)
            {
                numbers = Numbers[id];
                return true;
            }
            numbers = default(EcoNumbers);
            return false;
        }

        // Get numbers data
        public List<Vector2> GetPoints(int id)
        {
            EcoNumbers numbers;
            if (!GetNumbers(id, out numbers))
                return null;

            return numbers.Points;
        }
        public List<List<Vector2>> GetCurves(int id)
        {
            EcoNumbers numbers;
            if (!GetNumbers(id, out numbers))
                return null;

            return numbers.Curves;
        }
        public Dictionary<int, float> GetMapping(int id)
        {
            EcoNumbers numbers;
            if (!GetNumbers(id, out numbers))
                return null;

            return numbers.mappingFloat;
        }
        public Dictionary<int, Vector2> GetMappedPoints(int id)
        {
            EcoNumbers numbers;
            if (!GetNumbers(id, out numbers))
                return null;

            return numbers.mappedPoints;
        }
        public Dictionary<int, Vector2> GetTargetPoints(int id)
        {
            EcoNumbers numbers;
            if (!GetNumbers(id, out numbers))
                return null;

            return numbers.TradeTargets;
        }
        public Dictionary<int, List<Vector2>> GetTradeLines(int id)
        {
            EcoNumbers numbers;
            if (!GetNumbers(id, out numbers))
                return null;

            return numbers.TradeLines;
        }
        public Dictionary<int, float> GetTradeRatios(int id)
        {
            EcoNumbers numbers;
            if (!GetNumbers(id, out numbers))
                return null;

            return numbers.TradeRatios;
        }
        public float GetTradeRatio(int id, int index)
        {
            Dictionary<int, float> tradeRatios = GetTradeRatios(id);
            if (tradeRatios == null)
            {
                LHSDebug.LogError("Trade ratios are null for id:" + id);
                return 1f;
            }

            if (tradeRatios.ContainsKey(index) == false)
            {
                LHSDebug.LogError("Cannot find trade ratio for index: " + index);
                return 1f;
            }

            return tradeRatios[index];
        }

        // To String - this can be overloaded
        public override string ToString()
        {
            return GetTextToAssetData();
        }
        public virtual string ToString(int id)
        {
            EcoNumbers numbers;
            if (!GetNumbers(id, out numbers))
                return "";

            return numbers.ToString();
        }

        public abstract string GetOutputString(DataClass data, string delim);

        // This is where the data is specified allowing people to override ToString
        public string GetTextToAssetData()
        {
            string s = "";
            foreach (EcoNumbers numbers in Numbers)
            {
                s += numbers.Data.Length + DATA_STR_Delim;
                foreach (DataClass d in numbers.Data)
                {
                    s += GetOutputString(d, DATA_STR_Delim);
                }
                s += numbers.Points.Count + DATA_STR_Delim;
                for (int i = 0; i < numbers.Points.Count; i++)
                {
                    s += numbers.Points[i].x + DATA_STR_Delim + numbers.Points[i].y + DATA_STR_Delim;
                }
                foreach (KeyValuePair<int, float> kvp in numbers.mappingFloat)
                {
                    s += kvp.Key + DATA_STR_Delim + kvp.Value + DATA_STR_Delim;
                }
                s += "\n";
            }
            return s;
        }

        // load functions
        public abstract void SetDataInClass(ref DataClass data, string[] toLoadFrom, ref int index, int max);

        private void LoadData(string _text)
        {
            EcoNumbers numbers;
            string[] lines = _text.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string text in lines)
            {
                numbers = new EcoNumbers();

                string[] toLoadFrom = text.Split(DATA_CHR_Delim);
                int index = 0;
                int max = toLoadFrom.Length;
                if (max > 0)
                {
                    int DataAmount = int.Parse(toLoadFrom[index]);
                    index++;

                    numbers.Data = new DataClass[DataAmount];

                    // load data first
                    for (int i = 0; i < DataAmount; ++i)
                    {
                        SetDataInClass(ref numbers.Data[i], toLoadFrom, ref index, max);
                        if (index >= max)
                            break;
                    }

                    if (index < max)
                    {
                        numbers.Points = new List<Vector2>();
                        int PointsAmount = int.Parse(toLoadFrom[index]);
                        index++;
                        for (int i = 0; i < PointsAmount; ++i)
                        {
                            if (index < (max - 1))
                            {
                                numbers.Points.Add(new Vector2(int.Parse(toLoadFrom[index]), int.Parse(toLoadFrom[index + 1])));
                                index += 2;
                            }
                        }
                    }

                    if (index < max)
                    {
                        numbers.mappingFloat = new Dictionary<int, float>();
                        while (index < (max - 1))
                        {
                            numbers.mappingFloat.Add(int.Parse(toLoadFrom[index]), float.Parse(toLoadFrom[index + 1]));
                            index += 2;
                        }
                    }

                    // map the laoded points
                    MapPoints(numbers);

                    // Add to the list
                    AddNumbers(numbers);

                }
            }

            LHSDebug.Log("LoadData: " + ToString());
        }

        // Other functions and usages
        // Sorting
        public abstract List<DataClass> SortData(EcoNumbers numbers);
        public List<T> GetSortedData<T>(int id)
        {
            EcoNumbers numbers;
            if (!GetNumbers(id, out numbers))
                return null;

            return (List<T>)(object)SortData(numbers);
        }

        public abstract List<DataClass> SortData(EcoNumbers numbers, int amount);
        public List<T> GetSortedData<T>(int id, int amount)
        {
            EcoNumbers numbers;
            if (!GetNumbers(id, out numbers))
                return null;

            return (List<T>)(object)SortData(numbers, amount);
        }


    }
}
