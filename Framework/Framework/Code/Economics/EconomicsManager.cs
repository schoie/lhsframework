﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LHS.Framework;
using UnityEngine;


namespace LHS.Framework.Economics
{

    public class EconomicsManager : CSingleton<EconomicsManager>
    {
        Dictionary<string, IEconomise> Generators;

        public EconomicsManager()
        {
            Generators = new Dictionary<string, IEconomise>();
        }

        // the register system is on hold for the short term
        public void Register(string id, IEconomise generator)
        {
            IEconomise errorFound;
            if (!Generators.TryGetValue(id, out errorFound))
            {
                Generators.Add(id, generator);
            }
            else
            {
                LHSDebug.LogWarningFormat("EconomicsManager - Register - a IEconomise is already registered with ID {0}", id);
            }
        }

        public IEconomise GetGenerator(string id)
        {
            IEconomise generator;
            if (Generators.TryGetValue(id, out generator))
            {
                return generator;
            }

            LHSDebug.LogErrorFormat("EconomicsManager - GetGenerator - a IEconomise with ID ({0}) could not be found", id);
            return default(IEconomise);
        }

        public Dictionary<string, IEconomise> GetGenerators()
        {
            return Generators;
        }

    }
}
