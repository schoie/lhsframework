﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace LHS.Framework.Sprites
{
    public class SpriteFadeScaleTransition : MonoBehaviour
    {

        public float EntryTime;
        public AnimationCurve ScaleCurve;
        public AnimationCurve FadeCurve;
        public float ExitTime;
        public bool ScaleOnExit = false;
        public bool FadeOnExit = false;
        public bool DestroyOnExit = false;
        public UnityEvent OnDestroy;

        private SpriteRenderer sprite;
        private float scale;
        private bool isEntry = false;
        private bool isExit = false;
        

        public void StartEntryCo(GameObject _obj, float _scale)
        {
            if (isExit)
                return;

            sprite = _obj.GetComponent<SpriteRenderer>();
            if (sprite == null)
            {
                sprite = _obj.GetComponentInChildren<SpriteRenderer>();
                if (sprite == null)
                {
                    LHSDebug.LogError("SpriteFadeScaleTransition requires that the Object has a SpriteRenderer");
                    return;
                }
            }
            
            scale = _scale;
            StartCoroutine(EntryCoroutine(_scale));
        }

        IEnumerator EntryCoroutine(float _scale)
        {
            isEntry = true;

            Color color = sprite.color;
            color.a = 0;
            sprite.color = color;
            Vector3 scale = new Vector3(_scale, _scale, _scale);

            float evaluateTimer;
            float percent_scale;
            float percent_fade;
            float timer = 0;
            bool completed = false;
            while (true)
            {
                timer += Time.deltaTime;
                if (timer > EntryTime)
                {
                    timer = EntryTime;
                    completed = true;
                }

                evaluateTimer = timer / EntryTime;

                percent_scale = ScaleCurve.Evaluate(evaluateTimer);
                percent_fade = FadeCurve.Evaluate(evaluateTimer);

                sprite.transform.localScale = scale * percent_scale;
                color.a = percent_fade;
                sprite.color = color;

                if (completed)
                    break;

                yield return null;
            }

            isEntry = false;

        }

        public void StartExitCo()
        {
            StartCoroutine(ExitCoroutine());
        }

        IEnumerator ExitCoroutine()
        {

            isExit = true;

            //while (isEntry)
             //   yield return null;

            if (FadeOnExit || ScaleOnExit)
            {
                Color color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, sprite.color.a);
                Vector3 scale = new Vector3(sprite.transform.localScale.x, sprite.transform.localScale.y, sprite.transform.localScale.z);

                float evaluateTimer;
                float percent_scale;
                float percent_fade;
                float timer = 0;
                bool completed = false;
                while (true)
                {
                    timer += Time.deltaTime;
                    if (timer > EntryTime)
                    {
                        timer = EntryTime;
                        completed = true;
                    }

                    evaluateTimer = timer / EntryTime;

                    if (ScaleOnExit)
                    {
                        percent_scale = 1 - ScaleCurve.Evaluate(evaluateTimer);
                        sprite.transform.localScale = scale * percent_scale;
                    }

                    if (FadeOnExit)
                    {
                        percent_fade = 1 - FadeCurve.Evaluate(evaluateTimer);
                        color.a = percent_fade;
                        sprite.color = color;
                    }

                    if (completed)
                        break;

                    yield return null;
                }
            }

            if (DestroyOnExit)
                OnDestroy.Invoke();

            isExit = false;
        }

        public void HandleDisabled()
        {
            if (isEntry)
            {
                Color color = sprite.color;
                color.a = 1;
                sprite.color = color;
                Vector3 scale3 = new Vector3(scale, scale, scale);
                sprite.transform.localScale = scale3;
            }
        }

    }
}
