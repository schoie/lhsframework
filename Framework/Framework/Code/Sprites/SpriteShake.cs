﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LHS.Framework.UI;
using UnityEngine;
using System.Collections;

namespace LHS.Framework.Sprites
{
    public class SpriteShake : MonoBehaviour, IShake
    {

        public enum eDirection
        {
            Horizontal,
            Vertical
        }

        public eDirection Direction;
        public float ShakeSize;
        public float ShakeTime;
        public float ShakeSpeed = 1;
        public AnimationCurve ShakeCurve;
        public AnimationCurve ShakeAmount;

        Vector3 position;
        bool isShaking = false;
        Coroutine ShakeCo = null;

        public void Awake()
        {
            position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        }

        public void ProcessShake()
        {
            if (this.gameObject.activeSelf && this.gameObject.activeInHierarchy)
                StartShakeCo();
        }

        public void ProcessShakeOnAll()
        {
            ProcessShake();
            IShake[] shakes = this.GetComponentsInChildren<IShake>();
            foreach (IShake shake in shakes)
            {
                shake.ProcessShake();
            }
        }

        private void StartShakeCo()
        {
            if (!isShaking)
            {
                if (ShakeCo != null)
                    StopCoroutine(ShakeCo);
                ShakeCo = StartCoroutine(ShakeCoroutine());
            }
        }

        IEnumerator ShakeCoroutine()
        {
            isShaking = true;

            float timer = 0;
            float evaluateTimer;
            float percent;
            float heightPercent;
            bool complete = false;

            // pulse out time
            while (!complete)
            {
                timer += Time.deltaTime;
                if (timer >= ShakeTime)
                {
                    timer = ShakeTime;
                    complete = true;
                    evaluateTimer = 1;
                }
                else
                {
                    evaluateTimer = timer / ShakeTime;
                }

                percent = ShakeCurve.Evaluate(ShakeSpeed * timer);
                heightPercent = ShakeAmount.Evaluate(evaluateTimer);

                // apply effect here
                ApplyEffect(percent, heightPercent);

                yield return null;
            }

            isShaking = false;
        }


        private void ApplyEffect(float percent, float heightPercent)
        {
            float movementAmount = (percent * ShakeSize) * heightPercent;

            switch (Direction)
            {
                case eDirection.Horizontal:
                    HandleEffectHorizontal(movementAmount);
                    break;
                case eDirection.Vertical:
                    HandleEffectVertical(movementAmount);
                    break;
            }
        }
        protected void HandleEffectHorizontal(float amount)
        {
            this.transform.position = new Vector3(position.x + amount, position.y, position.z);
        }
        protected void HandleEffectVertical(float amount)
        {
            this.transform.position = new Vector3(position.x, position.y + amount, position.z);
        }


    }
}
