﻿/*
    MultiSpriteAnimator.cs -- Loads sprite array assets into an animation set system and allows us to animate clips

    Name: Steven Ogden
    Date: 22/07/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using LHS.Framework;

public class MultiSpriteAnimator : MonoBehaviour
{

    public class AnimationSet
    {
        Sprite[,] Strips;
        int loadStripIndex = 0;

        public AnimationSet(int strips, int frames)
        {
            Strips = new Sprite[strips, frames];
        }

        public void SetSprite(Sprite spr, int strip, int frame)
        {
            if (strip < Strips.GetLength(0) && frame < Strips.GetLength(1))
                Strips[strip, frame] = spr;
        }

        public Sprite GetSprite(int strip, int frame)
        {
            return Strips[strip, frame];
        }
    }

    public int Views;
    public int Frames;
    public int CurrentStrip;
    public float FPS;

    public string Resource;
    public string Animation;

    private int frame;
    private SpriteRenderer renderer;
    private float timer = 0;
    private float timeCheck;

    private bool LoadedSprites = false;

    Dictionary<string, AnimationSet> AnimationSets = new Dictionary<string, AnimationSet>();
    AnimationSet CurAnimationSet;

    // Use this for initialization
    void Awake()
    {
        renderer = this.GetComponent<SpriteRenderer>();
        if (renderer == null)
        {
            LHSDebug.LogError("Please attach a spriterenderer!");
            this.enabled = false;
        }
    }

    void Start()
    {
        if (!LoadedSprites && Resource != "")
        {
            LoadedSprites = LoadAnimationFromSpriteArrayRes(Resource);
        }

        frame = 0;

        AnimationSet AnimSetTest = null;
        if (AnimationSets.TryGetValue(Animation, out AnimSetTest))
        {
            renderer.sprite = AnimSetTest.GetSprite(CurrentStrip, frame);
            CurAnimationSet = AnimSetTest;
        }
        else
        {
            this.enabled = false;
        }

        timeCheck = 1 / FPS;

    }

    // Update is called once per frame
    void Update()
    {

        timer += Time.deltaTime;
        while (timer >= timeCheck)
        {
            frame++;
            if (frame >= Frames)
                frame = 0;
            renderer.sprite = CurAnimationSet.GetSprite(CurrentStrip, frame);
            timer -= timeCheck;
        }


    }



    public void SetAnimation(string _animation, int direction)
    {
        CurrentStrip = direction;
        CurrentStrip = Mathf.Clamp(CurrentStrip, 0, Views - 1);
        frame = 0;
        AnimationSet AnimSetTest = null;
        if (AnimationSets.TryGetValue(_animation, out AnimSetTest))
        {
            renderer.sprite = AnimSetTest.GetSprite(CurrentStrip, frame);
            CurAnimationSet = AnimSetTest;

            this.enabled = true;
        }
    }

    /*public void SetStrip(int strip)
    {
        this.enabled = true;
        if (CurrentStrip != strip)
        {
            CurrentStrip = strip;
            frame = 0;
            timer = 0;
            renderer.sprite = SpriteStrips[CurrentStrip, frame];
        }
    }
*/
    public void SetIdle(int frameNum = 0)
    {
        if (this.enabled && LoadedSprites)
        {
            this.enabled = false;
            frame = frameNum;
            timer = 0;
            renderer.sprite = CurAnimationSet.GetSprite(CurrentStrip, frame);;
        }
    }

    public void SetFPS(float FPS)
    {
        this.FPS = FPS;
        timeCheck = 1 / FPS;
    }

    public bool LoadAnimationFromSpriteArrayRes(string res)
    {

        Sprite[] Array = ResourceManager.instance.GetAll<Sprite>(res);
        
        if (Array == null || Array.Length <= 1)
        {
            return false;
        }

        // update the resource string
        Resource = res;

        return LoadAnimationFromSpriteArray(Array);
    }

    public bool LoadAnimationFromSpriteArray(Sprite[] sprites)
    {
        // collect sprites
        int stripCount = 0;
        int thisFrame = -1;
        foreach (Sprite spr in sprites)
        {

            string[] data = spr.name.Split(' ');
            if (data.Length < 3)
            {
                LHSDebug.LogError("TestAnimator - requires that the images are broken into 3 parts [name][frame][strip/view]");
                return false;
            }

            string name = data[0];
            thisFrame = int.Parse(data[1]);
            stripCount = int.Parse(data[2]);

            AnimationSet AnimSet = null;
            if (AnimationSets.ContainsKey(name))
            {
                AnimationSets.TryGetValue(name, out AnimSet);
            }
            else
            {
                AnimSet = new AnimationSet(Views, Frames);
                AnimationSets.Add(name, AnimSet);
            }

            AnimSet.SetSprite(spr, stripCount, thisFrame);
        }

        return true;
    }

    public void LoadAnimationFromListOfTextures(List<Texture2D> textures, bool enable = true)
    {

        int stripCount = 0;
        int thisFrame = -1;
        int lastFrame = 0;
        foreach (Texture2D tex in textures)
        {

            Sprite spr = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100);
            spr.name = tex.name;

            string[] data = spr.name.Split(' ');
            if (data.Length < 3)
            {
                LHSDebug.LogError("TestAnimator - requires that the images are broken into 3 parts [name][frame][strip/view]");
            }

            string name = data[0];
            thisFrame = int.Parse(data[1]);
            stripCount = int.Parse(data[2]);
            
            AnimationSet AnimSet = null;
            if (AnimationSets.ContainsKey(name))
            {
                AnimationSets.TryGetValue(name, out AnimSet);
            }
            else
            {
                AnimSet = new AnimationSet(Views, Frames);
                AnimationSets.Add(name, AnimSet);

            }

            LHSDebug.LogFormat("Aniamtion Clip name: {0} - Sprite Name: {1}, Set @ [{2}][{3}]", name, spr.name, stripCount, thisFrame);
            AnimSet.SetSprite(spr, stripCount, thisFrame);

        }

        AnimationSet AnimSetTest = null;
        if (AnimationSets.TryGetValue(Animation, out AnimSetTest))
        {
            renderer.sprite = AnimSetTest.GetSprite(CurrentStrip, frame);
            CurAnimationSet = AnimSetTest;

            this.enabled = enable;
        }
        else
        {
            this.enabled = false;
        }

    }

}

