﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Sprites
{
    public class SpriteAlphaAnimater : MonoBehaviour
    {

        public SpriteRenderer Renderer;
        public AnimationCurve Curve;
        public float AnimationTime;
        [Range(0, 1)]
        public float MaxAlpha;
        private float timer;

        public void Awake()
        {
            if(Renderer == null)
            {
                LHSDebug.LogError("SpriteRenderer cannot be Null");
                enabled = false;
                return;
            }
        }

        public void OnEnable()
        {
            timer = 0;
        }

        public void Update()
        {
            timer += Time.deltaTime;
            if (timer >= AnimationTime)
            {
                timer -= AnimationTime;
            }
            SetColor(Curve.Evaluate(timer / AnimationTime));
        }

        private void SetColor(float alpha)
        {
            Color col = Renderer.color;
            col.a = (alpha* MaxAlpha);
            Renderer.color = col;
        }

    }
}
