﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Sprites
{
    public class SpriteSpecAnimator : MonoBehaviour
    {
        public float FPS;
        public string ResAnim;
        public int min;
        public int max;

        Sprite[] ResArray;
        SpriteRenderer spr;

        private int frame = 0;
        private float timer = 0;
        private float timeCheck;

        public void Awake()
        {
            ResArray = ResourceManager.instance.GetAll<Sprite>(ResAnim);
            if (ResArray == null)
            {
                LHSDebug.LogWarning("SpriteRandomPicker - Resource cannot be NULL!!");
                enabled = false;
                return;
            }

            spr = this.GetComponent<SpriteRenderer>();
            if (spr == null)
            {
                LHSDebug.LogWarning("SpriteRandomPicker - must have a SpriteRenderer");
                enabled = false;
                return;
            }

            timeCheck = 1 / FPS;
            frame = min;
        }

        void Update()
        {

            timer += Time.deltaTime;
            while (timer >= timeCheck)
            {
                frame++;
                if (frame > max)
                    frame = min;
                spr.sprite = ResArray[frame];
                timer -= timeCheck;
            }


        }

        public void SetIdle()
        {
            if (this.enabled)
            {
                this.enabled = false;
                frame = min;
                timer = 0;
                spr.sprite = ResArray[frame];
            }
        }
    }
}
