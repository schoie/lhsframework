﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Sprites
{
    public class SpriteRandomPicker : MonoBehaviour
    {

        public string ResAnim;
        Sprite[] ResArray;
        SpriteRenderer spr;

        public void Awake()
        {
            ResArray = ResourceManager.instance.GetAll<Sprite>(ResAnim);
            if (ResArray == null)
            {
                LHSDebug.LogWarning("SpriteRandomPicker - Resource cannot be NULL!!");
                enabled = false;
                return;
            }

            spr = this.GetComponent<SpriteRenderer>();
            if(spr == null)
            {
                LHSDebug.LogWarning("SpriteRandomPicker - must have a SpriteRenderer");
                enabled = false;
                return;
            }

            PickImage();
        }


        public void PickImage()
        {
            if (!enabled)
                return;

            int spriteIndex = UnityEngine.Random.Range(0, ResArray.Length);
            if (spriteIndex < ResArray.Length)
                spr.sprite = ResArray[spriteIndex];
        }

    }
}
