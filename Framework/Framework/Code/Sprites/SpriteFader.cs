﻿/*
    SpriteFader.cs -- A script to allow us to allocate sprite renderers and fade them logically

    Name: Steven Ogden
    Date: 22/07/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace LHS.Framework.Sprites
{

    public class SpriteFader : MonoBehaviour
    {

        public enum eFadeState
        {
            None,
            FadeIn,
            FadeOut
        }

        public bool StartFadedOut = false;
        public bool DisableFadedOut = true;
        public float FadeTime;
        public AnimationCurve FadeCurve;
        public UnityEvent OnFadedIn;
        public UnityEvent OnFadedOut;
        public List<SpriteRenderer> sprites;
        
        public float Alpha { get; private set; }

        private float inTimer = 0;
        private float outTimer = 0;
        private eFadeState state = eFadeState.None;
        private float EPSILION = 0.05f; // the EPSILIOn is a way to check for 0 or 1 on the alpha and give consideration to floating point precision

        private Coroutine fadeInCo;
        private Coroutine fadeOutCo;


        public delegate void MyAlphaFunction(float _alpha);
        MyAlphaFunction SetAlpha;

        // we use aawake to set the objects we are goign to fade
        public void Awake()
        {
            SetDelegates();
        }

        private void SetDelegates()
        {
            if (DisableFadedOut)
                SetAlpha = SetAlphaWithZeroDisable;
            else
                SetAlpha = SetAlphaNormal;
        }

        public void init()
        {
            SetDelegates();
            SetAlpha((StartFadedOut) ? 0 : 1);
        }

        public void OnDisable()
        {
            switch (state)
            {
                case eFadeState.FadeOut:
                    SetAlpha(0);
                    break;
                case eFadeState.FadeIn:
                    SetAlpha(1);
                    break;
            }
            state = eFadeState.None;
        }

        public void Fadein()
        {
            StartFadeIn();
        }
        public void FadeOut()
        {
            StartFadeOut();
        }

        public bool TryFadein()
        {
            if (state == eFadeState.FadeIn)
                return false;

            if (Alpha > EPSILION)
                return false;
            
            StartFadeIn();
            return true;
        }
        public bool TryFadeOut()
        {
            if (state == eFadeState.FadeOut/* || Group.alpha < (1 - EPSILION)*/)
                return false;
            
            StartFadeOut();
            return true;
        }

        private void StartFadeIn()
        {
            if (!this.gameObject.activeInHierarchy)
                return;

            inTimer = 0;
            if (outTimer > 0)
                inTimer = (FadeTime - outTimer);

            if(fadeOutCo != null)
                StopCoroutine(fadeOutCo);
            state = eFadeState.FadeIn;
            fadeInCo = StartCoroutine(FadeInCoroutine());
        }

        private void StartFadeOut()
        {
            if (!this.gameObject.activeInHierarchy)
                return;

            outTimer = 0;
            if (inTimer > 0)
                outTimer = (FadeTime - inTimer);

            if (fadeInCo != null)
                StopCoroutine(fadeInCo);
            state = eFadeState.FadeOut;
            fadeOutCo = StartCoroutine(FadeOutCoroutine());
        }


        IEnumerator FadeInCoroutine()
        {
            float evaluateTimer;
            float percent;
            bool finished = false;
            while (true)
            {
                if (state == eFadeState.FadeOut)
                    yield break;

                inTimer += Time.deltaTime;
                if (inTimer >= FadeTime)
                {
                    evaluateTimer = 1;
                    finished = true;
                }
                else
                {
                    evaluateTimer = inTimer / FadeTime;
                }

                percent = FadeCurve.Evaluate(evaluateTimer);
                SetAlpha(percent);

                if (finished)
                    break;

                yield return null;
            }

            SetAlpha(1);
            inTimer = 0;
            state = eFadeState.None;
            OnFadedIn.Invoke();
        }

        IEnumerator FadeOutCoroutine()
        {

            float evaluateTimer;
            float percent;
            bool finished = false;
            while (true)
            {
                if (state == eFadeState.FadeIn)
                    yield break;

                outTimer += Time.deltaTime;
                if (outTimer >= FadeTime)
                {
                    evaluateTimer = 1;
                    finished = true;
                }
                else
                {
                    evaluateTimer = outTimer / FadeTime;
                }

                percent = FadeCurve.Evaluate(evaluateTimer);
                SetAlpha(1 - percent);

                if (finished)
                    break;

                yield return null;
            }

            SetAlpha(0);
            outTimer = 0;
            state = eFadeState.None;
            OnFadedOut.Invoke();
        }

        private void SetAlphaNormal(float value)
        {
            Alpha = value;
            foreach (SpriteRenderer spr in sprites)
            {
                Color col = spr.color;
                col.a = value;
                spr.color = col;
            }
        }
        private void SetAlphaWithZeroDisable(float value)
        {
            bool active = value != 0f;

            Alpha = value;
            foreach (SpriteRenderer spr in sprites)
            {
                Color col = spr.color;
                col.a = value;
                spr.color = col;

                spr.enabled = active;
            }
        }
    }
}

