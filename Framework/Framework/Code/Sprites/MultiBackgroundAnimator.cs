﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Sprites
{
    public class MultiBackgroundAnimator : MonoBehaviour
    {
        public enum eFrameType
        {
            Loop,
            Bounce
        }

        public eFrameType Type;
        public float FPS;
        public List<string> MultiSprites;
        List<SpriteRenderer> SpriteRenderers;
        List<Sprite[]> SpriteArrays;

        private int frame;
        private float timer = 0;
        private float timeCheck;
        private int frameDirection = 1;

        public void Awake()
        {
            SpriteRenderer[] sprs = this.GetComponentsInChildren<SpriteRenderer>(true);
            if(sprs.Length <= 0)
            {
                LHSDebug.LogError("MultiBackgroundAnimator - Must have atleast 1 spriterenderer child - Prefereably 2!");
                enabled = false;
                return;
            }

            if (MultiSprites.Count <= 0)
            {
                LHSDebug.LogError("MultiBackgroundAnimator - Requries a multi sprite reference in roder to animate teh background!");
                enabled = false;
                return;
            }

            // get SPRS
            SpriteRenderers = new List<SpriteRenderer>();
            foreach (SpriteRenderer spr in sprs)
            {
                SpriteRenderers.Add(spr);
            }

            // Load Res
            SpriteArrays = new List<Sprite[]>();
            foreach(string str in MultiSprites)
            {
                Sprite[] ResArray = ResourceManager.instance.GetAll<Sprite>(str);
                if (ResArray == null)
                {
                    LHSDebug.LogWarningFormat("MultiBackgroundAnimator - Res: {0} - is NULL", str);
                }
                else
                {
                    if (ResArray.Length == SpriteRenderers.Count)
                    {
                        SpriteArrays.Add(ResArray);
                    }
                    else
                    {
                        LHSDebug.LogWarningFormat("MultiBackgroundAnimator - Res: {0} - Does not have the array size ({1}) to match the amount of SpriteRenderers: {2}",
                            str, ResArray.Length, SpriteRenderers.Count);
                    }
                }
            }


            timeCheck = 1 / FPS;

            SetFrame();
        }

        void Update()
        {

            timer += Time.deltaTime;
            while (timer >= timeCheck)
            {
                // handle the frame
                ManagerFrameIncrement();

                // manage what we now do with the frame
                SetFrame();

                // set next timer
                timer -= timeCheck;
            }


        }

        private void SetFrame()
        {
            int count = 0;
            foreach (SpriteRenderer spr in SpriteRenderers)
            {
                spr.sprite = SpriteArrays[frame][count];
                count++;
            }
        }

        private void ManagerFrameIncrement()
        {
            switch (Type)
            {
                case eFrameType.Loop:

                    frame++;
                    if (frame >= SpriteArrays.Count)
                        frame = 0;

                    break;
                case eFrameType.Bounce:

                    frame += frameDirection;
                    if (frameDirection == 1)
                    {
                        if (frame >= (SpriteArrays.Count - 1))
                        {
                            frameDirection = -frameDirection;
                            frame = (SpriteArrays.Count - 1);
                        }
                    }
                    else
                    {
                        if (frame <= 0)
                        {
                            frameDirection = -frameDirection;
                            frame = 0;
                        }
                    }

                    break;
            }
        }

    }
}
