﻿using UnityEngine;
using System.Collections;

public class AtlasedSpriteRef : ScriptableObject 
{
	public Sprite[] sprites;
}
