﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Sprites
{
    public class SpriteAnimator : MonoBehaviour
    {
        public float FPS;
        public string ResAnim = "";

        Sprite[] ResArray;
        SpriteRenderer spr;

        private int frame;
        private float timer = 0;
        private float timeCheck;

        public void Awake()
        {
            LoadAndPrepare();
        }

        void Update()
        {

            timer += Time.deltaTime;
            while (timer >= timeCheck)
            {
                frame++;
                if (frame >= ResArray.Length)
                    frame = 0;
                spr.sprite = ResArray[frame];
                timer -= timeCheck;
            }


        }

        public void SetIdle()
        {
            if (this.enabled)
            {
                this.enabled = false;
                frame = 0;
                timer = 0;
                spr.sprite = ResArray[frame];
            }
        }

        public void LoadAndPrepare()
        {
            if (ResAnim == "") // without this, we have a latency spike at this time
                return;

            enabled = true;
            ResArray = ResourceManager.instance.GetAll<Sprite>(ResAnim);
            if (ResArray == null)
            {
                LHSDebug.LogWarning("SpriteRandomPicker - Resource cannot be NULL!!");
                enabled = false;
                return;
            }

            spr = this.GetComponent<SpriteRenderer>();
            if (spr == null)
            {
                LHSDebug.LogWarning("SpriteRandomPicker - must have a SpriteRenderer");
                enabled = false;
                return;
            }

            if (ResArray.Length > 0)
                spr.sprite = ResArray[0];
            timeCheck = 1 / FPS;
        }
    }
}
