﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace LHS
{
    namespace Framework
    {
        namespace Particles
        {
            public class AutoDestroyParticleSystem : MonoBehaviour
            {

                public enum eDestroyType
                {
                    Instantiated = 0,
                    Pooled
                };

                public eDestroyType type;
                public delegate void DestroyFunc();
                public DestroyFunc ParticleDestroyFunc;
                public UnityEvent OnParticleDestroy;

                private ParticleSystem _particleSystem = null;

                public void Start()
                {
                    _particleSystem = GetComponent<ParticleSystem>();
                    switch (type)
                    {
                        case eDestroyType.Pooled:
                            ParticleDestroyFunc = new DestroyFunc(ReleaseParticle);
                            break;

                        case eDestroyType.Instantiated:
                        default:
                            ParticleDestroyFunc = new DestroyFunc(DestroyParticle);
                            break;
                    }

                    AutodestroyCo();

                }

                public void OnEnable()
                {
                    if (_particleSystem)
                        AutodestroyCo();
                }

                private void AutodestroyCo()
                {
                    StartCoroutine(Autodestroy());
                }

                IEnumerator Autodestroy()
                {
                    yield return new WaitForSeconds(_particleSystem.duration);
                    OnParticleDestroy.Invoke();
                    ParticleDestroyFunc();
                }

                private void DestroyParticle()
                {
                    Destroy(gameObject);
                }
                private void ReleaseParticle()
                {
                    gameObject.SetActive(false);
                }

                public void AssignOnDestroyFunc(UnityAction call, bool removeall = false)
                {
                    if (removeall)
                        OnParticleDestroy.RemoveAllListeners();

                    OnParticleDestroy.AddListener(call);
                }
            }
        }
    }
}
