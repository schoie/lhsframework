﻿/*
    SaveLoadManager.cs -- A manager for creating and maintaining save data

    Name: Steven Ogden
    Date: 30/06/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace LHS.Framework.FileIO
{

    public class SaveLoadManager : CSingleton<SaveLoadManager>
    {

        public enum eIOResult
        {
            // specific errors
            FileNameAlreadyExists,
            FileNotFound,

            // generic responces
            Success,
            Error,
            None
        }

        List<ISaveLoad> SaveData;

        public SaveLoadManager()
        {
            SaveData = new List<ISaveLoad>();

            LHSDebug.Log("SaveLoadManager - Save Files exist at path: " + Application.persistentDataPath);
        }

        public eIOResult CreateSaveDataObject<T>(string fileName, out T data) where T : new()
        {
            // check filename
            foreach (ISaveLoad ISL in SaveData)
            {
                if (ISL.CheckFileName(fileName))
                {
                    ISaveLoadData<T> dataCast = ISL as ISaveLoadData<T>;
                    data = dataCast.Get();
                    LHSDebug.LogWarning("SaveLoadManager - File Name Already Exists: " + fileName);
                    return eIOResult.FileNameAlreadyExists;
                }
            }

            SaveLoadObject<T> NewSaveData = new SaveLoadObject<T>(fileName);
            if (NewSaveData.Exists())
            {
                NewSaveData.Load();
                LHSDebug.LogFormat("CreateSaveDataObject - {0} - Exists - Loaded", fileName);
            }
            else
            {
                NewSaveData.Create();
                LHSDebug.LogFormat("CreateSaveDataObject - {0} - Not Existing - Created", fileName);
            }

            SaveData.Add(NewSaveData);

            data = NewSaveData.Get();

            return eIOResult.Success;
        }

        public void Save()
        {
            foreach (ISaveLoad ISL in SaveData)
                ISL.Save();
        }

        public void Load()
        {
            foreach (ISaveLoad ISL in SaveData)
                ISL.Load();
        }

        public eIOResult Save(string fileName)
        {
            foreach (ISaveLoad ISL in SaveData)
            {
                if (ISL.CheckFileName(fileName))
                {
                    ISL.Save();
                    return eIOResult.Success;
                }
            }

            LHSDebug.LogWarning("SaveLoadManager - File Name Does Not Exists: " + fileName);
            return eIOResult.FileNotFound;
        }

        public eIOResult Load(string fileName)
        {
            foreach (ISaveLoad ISL in SaveData)
            {
                if (ISL.CheckFileName(fileName))
                {
                    ISL.Load();
                    return eIOResult.Success;
                }
            }

            LHSDebug.LogWarning("SaveLoadManager - File Name Does Not Exists: " + fileName);
            return eIOResult.FileNotFound;
        }

        public eIOResult Get<T>(string fileName, out T data) where T : new()
        {
            foreach (ISaveLoad ISL in SaveData)
            {
                if (ISL.CheckFileName(fileName))
                {
                    ISaveLoadData<T> dataCast = ISL as ISaveLoadData<T>;
                    data = dataCast.Get();
                    return eIOResult.Success;
                }
            }

            Debug.LogWarning("SaveLoadManager - File Name Does Not Exists: " + fileName);
            data = default(T);
            return eIOResult.FileNotFound;
        }

        public void Delete()
        {
            foreach (ISaveLoad ISL in SaveData)
                ISL.Delete();
        }

        public eIOResult Delete(string fileName)
        {
            foreach (ISaveLoad ISL in SaveData)
            {
                if (ISL.CheckFileName(fileName))
                {
                    ISL.Delete();
                    return eIOResult.Success;
                }
            }

            LHSDebug.LogWarning("SaveLoadManager - File Name Does Not Exists: " + fileName);
            return eIOResult.FileNotFound;
        }

        public eIOResult DeleteAndRemove(string fileName)
        {
            int index = 0;
            eIOResult result = eIOResult.FileNotFound;
            bool remove = false;
            foreach (ISaveLoad ISL in SaveData)
            {
                if (ISL.CheckFileName(fileName))
                {
                    ISL.Delete();
                    remove = true;
                    result = eIOResult.Success;
                    break;
                }
                index++;
            }

            if (remove)
            {
                SaveData.RemoveAt(index);
            }

            if(result == eIOResult.FileNotFound)
                LHSDebug.LogWarning("SaveLoadManager - File Name Does Not Exists: " + fileName);

            return result;
        }

        public eIOResult DoesSaveDataExist(string fileName, out bool FileExists)
        {
            foreach (ISaveLoad ISL in SaveData)
            {
                if (ISL.CheckFileName(fileName))
                {
                    FileExists = ISL.Exists();
                    return eIOResult.Success;
                }
            }

            FileExists = false;
            return eIOResult.FileNotFound;
        }

        public void CreateFolder(string pathAddition, string name)
        {
            SaveLoad.CreateFolder(pathAddition, name);
        }
        public FileInfo[] GetFilesInFolder(string pathAddition)
        {
            return SaveLoad.GetFilesInFolder(pathAddition);
        }

        public bool IsResourceAvailableToLoad(string ResourcePath)
        {
            TextAsset asset = ResourceManager.instance.Load(ResourcePath) as TextAsset;
            return (asset != null);
        }

        public eIOResult LoadFromResource<T>(string ResourcePath, out T data) where T : new()
        {

            BinaryFormatter bf = new BinaryFormatter();
            TextAsset asset = ResourceManager.instance.Load(ResourcePath) as TextAsset;
            if (asset != null)
            {
                LHSDebug.LogFormat("CreateSaveDataObject - {0} - Exists - Loaded", ResourcePath);
                Stream s = new MemoryStream(asset.bytes);
                data = (T)bf.Deserialize(s);
                return eIOResult.Success;
            }

            LHSDebug.LogWarning("SaveLoadManager - File Name Does Not Exists: " + ResourcePath);
            data = default(T);
            return eIOResult.FileNotFound;
        }

        static public eIOResult LoadFromTextAsset<T>(TextAsset asset, out T data) where T : new()
        {

            BinaryFormatter bf = new BinaryFormatter();
            if (asset != null)
            {
                LHSDebug.LogFormat("asset {0} - Loading", asset.name);
                Stream s = new MemoryStream(asset.bytes);
                data = (T)bf.Deserialize(s);
                return eIOResult.Success;
            }

            LHSDebug.LogFormat("asset - is null");
            data = default(T);
            return eIOResult.FileNotFound;
        }

        static public void ExportBinraySaveToText<T>(string ResourcePath, string outputpath)
        {
            BinaryFormatter bf = new BinaryFormatter();
            TextAsset asset = ResourceManager.instance.Load(ResourcePath) as TextAsset;
            if (asset != null)
            {
                Stream s = new MemoryStream(asset.bytes);
                T data = (T)bf.Deserialize(s);

                LHSDebug.LogFormat("asset {0} - Exported: \n\n {1}", asset.name, data.ToString());
                File.WriteAllText(Application.dataPath + "/" + outputpath + ".txt", data.ToString());
                
            }
        }

    }
}