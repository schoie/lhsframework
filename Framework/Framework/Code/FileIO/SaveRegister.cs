﻿/*
    SaveRegister.cs -- Allows us to register variables that should be saved and loaded in a class

    Name: Steven Ogden
    Date: 04/07/2016

    Copyright Lionsheart Studios. All rights reserved.
*/


using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;

namespace LHS.Framework.FileIO
{

    [Serializable]
    public class SerialisedData
    {
        public string data;

        public SerialisedData()
        {
            data = "";
        }

        public SerialisedData(string _data)
        {
            data = _data;
        }
    }

    public class SaveRegister<ClassType>
    {

        public List<string> Registers;

        public SaveRegister()
        {
            Registers = new List<string>();
        }

        public void RegisterSaveVariable(string varName)
        {
            if(!Registers.Contains(varName))
                Registers.Add(varName);
        }

        public SerialisedData Save(ClassType obj, string delim = "|")
        {
            SerialisedData Data = new SerialisedData();
            Type T = typeof(ClassType);

            Data.data += (T.Name + delim);

            foreach (string reg in Registers)
            {

                FieldInfo field;
                field = T.GetField(reg);
                if (field != null)
                {
                    Data.data += (field.GetValue(obj).ToString() + delim);
                }
            }
            
            return Data;
        }

        public bool Load(ClassType obj, SerialisedData Data, char delim = '|')
        {
            string[] variables = Data.data.Split(delim);
            Type T = typeof(ClassType);
            int count = 1;

            if (variables.Length <= 0)
                return false;

            if (variables[0] != T.Name)
                return false;

            foreach (string reg in Registers)
            {

                if (count >= variables.Length)
                {
                    LHSDebug.LogWarningFormat("SaveRegister - Load - Type ({0}) - Registers (Size: {1}) - Vars (Size: {2}) - does not match data saved out!!!", 
                        T.Name, Registers.Count, variables.Length);
                    break;
                }

                FieldInfo field;
                field = T.GetField(reg);
                if (field != null)
                {
                    string _val = variables[count];
                    switch (field.FieldType.Name)
                    {
                        case "Int16":
                            short Int16Parse = 0;
                            if(short.TryParse(_val, out Int16Parse))
                                field.SetValue(obj, Int16Parse);
                            break;
                        case "UInt16":
                            ushort UInt16Parse = 0;
                            if (ushort.TryParse(_val, out UInt16Parse))
                                field.SetValue(obj, UInt16Parse);
                            break;
                        case "Int32":
                            int Int32Parse = 0;
                            if (int.TryParse(_val, out Int32Parse))
                                field.SetValue(obj, Int32Parse);
                            break;
                        case "UInt32":
                            uint UInt32Parse = 0;
                            if (uint.TryParse(_val, out UInt32Parse))
                                field.SetValue(obj, UInt32Parse);
                            break;
                        case "Int64":
                            long Int64Parse = 0;
                            if (long.TryParse(_val, out Int64Parse))
                                field.SetValue(obj, Int64Parse);
                            break;
                        case "UInt64":
                            ulong UInt64Parse = 0;
                            if (ulong.TryParse(_val, out UInt64Parse))
                                field.SetValue(obj, UInt64Parse);
                            break;
                        case "Single":
                            float SingleParse = 0;
                            if (float.TryParse(_val, out SingleParse))
                                field.SetValue(obj, SingleParse);
                            break;
                        case "Double":
                            double DoubleParse = 0;
                            if (double.TryParse(_val, out DoubleParse))
                                field.SetValue(obj, DoubleParse);
                            break;
                        case "Boolean":
                            bool BooleanParse = false;
                            if (bool.TryParse(_val, out BooleanParse))
                                field.SetValue(obj, BooleanParse);
                            break;
                        case "String":
                            field.SetValue(obj, _val);
                            break;
                    }

                }

                count++;
            }

            return true;
        }


    }
}
