﻿/*
    SaveLoadObject.cs

    Name: Steven Ogden
    Date: 30/06/2016

    Provided by Steven Ogden copyright free as part of the GNU license.
*/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


namespace LHS.Framework.FileIO
{

    public interface ISaveLoad
    {
        bool Create();
        bool Exists();
        void Save();
        void Load();
        void Delete();
        bool CheckFileName(string _filename);

    }

    public interface ISaveLoadData<T> where T : new()
    {
        T Get();
    }

    public class SaveLoadObject<T> : ISaveLoad, ISaveLoadData<T> where T : new()
    {
        public string fileName { get; private set; }
        public T Data = new T();

        public SaveLoadObject(string _fileName)
        {

            fileName = _fileName;
        }

        // functions
        public bool Create()
        {
            if (!Exists())
            {
                SaveLoad<T>.Save(fileName, ref Data);
                return true;
            }
            return false;
        }

        public bool Exists()
        {
            return SaveLoad<T>.Exists(fileName);
        }
        public void Delete()
        {
            SaveLoad<T>.Delete(fileName);
        }

        public void Save()
        {
            SaveLoad<T>.Save(fileName, ref Data);
        }

        public void Load()
        {
            SaveLoad<T>.Load(fileName, ref Data);
        }

        public T Get()
        {
            return Data;
        }

        public bool CheckFileName(string _filename)
        {
            return fileName == _filename;
        }
    }

    public static class SaveLoad<T> where T : new()
    {

        //public T Data = new T();

        //it's static so we can call it from anywhere
        public static void Save(string fileName, ref T data)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(SaveLoad.defaultPath + "/" + fileName + SaveLoad.defaultExt); //you can call it anything you want
            bf.Serialize(file, data);
            file.Close();
        }

        public static void Load(string fileName, ref T data)
        {
            if (Exists(fileName))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(SaveLoad.defaultPath + "/" + fileName + SaveLoad.defaultExt, FileMode.Open);
                data = (T)bf.Deserialize(file);
                file.Close();
            }
        }

        public static bool Exists(string fileName)
        {
            return File.Exists(SaveLoad.defaultPath + "/" + fileName + SaveLoad.defaultExt);
        }

        public static void Delete(string fileName)
        {
            if (Exists(fileName))
            {
                File.Delete(SaveLoad.defaultPath + "/" + fileName + SaveLoad.defaultExt);
            }
        }
    }

    public static class SaveLoadList<T> where T : new()
    {

        public static List<T> savedGames = new List<T>();

        //it's static so we can call it from anywhere
        public static void Save(string fileName)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(SaveLoad.defaultPath + "/" + fileName + SaveLoad.defaultExt); //you can call it anything you want
            bf.Serialize(file, SaveLoadList<T>.savedGames);
            file.Close();
        }

        public static void Load(string fileName)
        {
            if (Exists(fileName))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(SaveLoad.defaultPath + "/" + fileName + SaveLoad.defaultExt, FileMode.Open);
                SaveLoadList<T>.savedGames = (List<T>)bf.Deserialize(file);
                file.Close();
            }
        }

        public static bool Exists(string fileName)
        {
            return File.Exists(SaveLoad.defaultPath + "/" + fileName + SaveLoad.defaultExt);
        }

        public static void Delete(string fileName)
        {
            if (Exists(fileName))
            {
                File.Delete(SaveLoad.defaultPath + "/" + fileName + SaveLoad.defaultExt);
            }
        }
    }

    public static class SaveLoad
    {
        static public string defaultPath = Application.persistentDataPath;
        static public string defaultExt = ".bytes";

        public static void CreateFolder(string pathAddition, string name)
        {
            string path = defaultPath + "/" + pathAddition + "/" + name;
            if(!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        public static void SetDefaultPath(string path)
        {
            defaultPath = path;
        }
        public static void SetDefaultExtension(string ext)
        {
            defaultExt = ext;
        }

        public static void SetDefaultPathToApplicationPath(string path)
        {
            defaultPath = Application.dataPath + "/" + path;
        }

        public static FileInfo[] GetFilesInFolder(string pathAddition) {

            string path = defaultPath + "/" + pathAddition;
            var info = new DirectoryInfo(path);
            return info.GetFiles("*.*");
        }
    }
}