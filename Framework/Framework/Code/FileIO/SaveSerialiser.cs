﻿/*
    SaveSerialiser.cs -- Allows us to access generic functions for serialising save data at the static level

    Name: Steven Ogden
    Date: 20/07/2016

    Copyright Lionsheart Studios. All rights reserved.
*/


using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;
using LHS.Framework.GameData;

namespace LHS.Framework.FileIO
{
    public abstract class SaveSerialiser
    {

        // secure way of statically defining the string/char deliminators for the save system
        // essentially we need 3
        // 1: JSON conversion packed like a list with a delim to sperate them
        // 2: Dynamic data within the JSON step 1 - Packing components fo data each with a save register - DCP
        // 2: Dynamic data within the JSON step 2 - Used in the SaveRegister System thus defined as SRS
        public static char JSON_CHR_Delim = '|';
        public static string JSON_STR_Delim = JSON_CHR_Delim.ToString();

        public static char DCP_CHR_Delim = '`';
        public static string DCP_STR_Delim = DCP_CHR_Delim.ToString();

        public static char SRS_CHR_Delim = '*';
        public static string SRS_STR_Delim = SRS_CHR_Delim.ToString();


        /// <summary>
        ///  Takes our Packed Serialised String, which represents a list of JSON objects. Breaks them down and creates a list of these objects
        /// </summary>
        /// <typeparam name="ClassData"></typeparam>
        /// <param name="packedSerialisedString"></param>
        /// <returns></returns>
        static public List<ClassData> GetData<ClassData>(string packedSerialisedString) where ClassData : new()
        {
            List<ClassData> Data = new List<ClassData>();
            string[] stringData = packedSerialisedString.Split(JSON_CHR_Delim);

            foreach (string str in stringData)
            {
                if (str != null && str != "")
                {
                    ClassData newData = JsonUtility.FromJson<ClassData>(str);
                    if (newData != null)
                        Data.Add(newData);
                }
            }
            
            return Data;
        }

        /// <summary>
        /// The IGameData interface provides us with a OnSave function. Inside the class in which this is called we can specify saving using the SaveRegister System. This function will loop over all the IGameData components in a Object and call OnSave
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="dataStringToAppendTo"></param>
        static public void SaveIGameData(GameObject obj, ref string dataStringToAppendTo)
        {
            dataStringToAppendTo = "";

            // run on save on the instantiated building
            ISaveData[] comps = obj.GetComponents<ISaveData>();

            // create component string


            foreach (ISaveData comp in comps)
                comp.OnSave(ref dataStringToAppendTo);
        }

        /// <summary>
        /// The IGameData interface provides us with a OnLoad function. Inside the class in which this is called we can specify loading using the SaveRegister System. This function will loop over all the IGameData components in a Object and call OnLoad
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="dataStringToLoadFrom"></param>
        static public void LoadIGameData(GameObject obj, string dataStringToLoadFrom)
        {
            if (dataStringToLoadFrom == null)
                return;

            string[] stringData = dataStringToLoadFrom.Split(DCP_CHR_Delim);
            if (stringData.Length <= 0)
                return;

            /*
            int count = 0;
            ISaveData[] comps = obj.GetComponents<ISaveData>();
            foreach (ISaveData comp in comps)
            {
                if (count >= stringData.Length)
                    break;

                if (stringData[count] != null && stringData[count] != "")
                {
                    comp.OnLoad(stringData[count]);
                }
                count++;
            }
            */

            ISaveData[] comps = obj.GetComponents<ISaveData>();
            int count = 0;
            foreach (ISaveData comp in comps)
            {
                if (count >= stringData.Length)
                    break;

                if (stringData[count] != null && stringData[count] != "")
                {
                    count += (comp.OnLoad(stringData[count])) ? 1 : 0;
                }
                else
                {
                    count++;
                }
            }

        }

        /// <summary>
        /// Will use the Save Register to load data from the string and set it in the class obj
        /// </summary>
        /// <typeparam name="ClassData"></typeparam>
        /// <param name="register"></param>
        /// <param name="stringToLoadFrom"></param>
        /// <param name="obj"></param>
        /// <returns>
        /// bool - To signify if the load was succesful
        /// </returns>
        static public bool LoadSaveRegister<ClassData>(SaveRegister<ClassData> register, string stringToLoadFrom, ClassData obj) where ClassData : new()
        {
            SerialisedData sData = new SerialisedData(stringToLoadFrom);
            return register.Load(obj, sData, SRS_CHR_Delim);
        }

        /// <summary>
        /// Will use the Save Register to save data to a string from the class object and append it to the referenced string
        /// </summary>
        /// <typeparam name="ClassData"></typeparam>
        /// <param name="register"></param>
        /// <param name="stringToAppendTo"></param>
        /// <param name="obj"></param>
        static public void SaveSaveRegister<ClassData>(SaveRegister<ClassData> register, ref string stringToAppendTo, ClassData obj) where ClassData : new()
        {
            SerialisedData save = register.Save(obj, SRS_STR_Delim);
            if (save.data != null && save.data != "")
                stringToAppendTo += (save.data + DCP_STR_Delim);
        }

    }
}
