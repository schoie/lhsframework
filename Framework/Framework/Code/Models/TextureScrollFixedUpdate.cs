﻿using UnityEngine;
using System.Collections;

namespace LHS.Framework.Meshes
{
    public class TextureScrollFixedUpdate : MonoBehaviour
    {

        public enum eDirection
        {
            Forward = 1,
            Backward = -1
        }

        public float scrollSpeed = 0.5F;
        public Renderer rend;
        public eDirection Direction = eDirection.Forward;

        private float offset = 0;

        public void Start()
        {
            rend = GetComponent<Renderer>();
        }
        public void FixedUpdate()
        {
            offset += (Time.fixedDeltaTime * (scrollSpeed * (int)Direction));
            rend.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
        }

        public void SetDirection(eDirection _direction)
        {
            Direction = _direction;
        }
    }
}