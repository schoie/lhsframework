﻿/*
    MeshRotater.cs -- A simple script for rotating a GameObject on some specified access each frame

    Name: Steven Ogden
    Date: 11/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace LHS.Framework.Meshes
{
    public class MeshRotater : MonoBehaviour
    {
        public Vector3 rotation;

        public void Update()
        {
            this.transform.Rotate(rotation);
        }


    }
}
