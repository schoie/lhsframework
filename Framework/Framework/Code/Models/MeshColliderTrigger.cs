﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace LHS.Framework.Meshes
{
    public class MeshColliderTrigger : MonoBehaviour
    {

        public string Tag;
        public UnityEvent OnEnter;
        
        public void OnTriggerEnter(Collider other)
        {
            if(other.tag == Tag)
                OnEnter.Invoke();
        }

    }
}
