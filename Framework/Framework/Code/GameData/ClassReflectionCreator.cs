﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace LHS.Framework.GameData
{
    public class ClassReflectionObjectCreator : CSingleton<ClassReflectionObjectCreator>
    {
        
        public delegate GameObject MyFunctionLink();
        public delegate void MyNetworkSync(GameObject _toSync, string _id);

        public ClassReflectionObjectCreator() { }

        public GameObject Create(string _type, string _name, Transform _parent = null, bool _worldPositionStays = false)
        {

            IFactory Factory = FactoriesManager.instance.MainFactory.GetFactory(_type);
            if (Factory != null)
            {
                GameObject CreationObject = Factory.Create(_name, _parent, _worldPositionStays);
                if (CreationObject != null)
                {
                    return CreationObject;
                }
                else
                {
                   // LHSDebug.LogWarningFormat("ClassReflectionObjectCreator.Create - Name: {0} ERROR!!", _name);
                }
            }
            else
            {
              //  LHSDebug.LogWarningFormat("ClassReflectionObjectCreator.Create - Type: {0} ERROR!!", _type);
            }

            return null;
        }

        public GameObject Create(string _type, string _name, UnityAction<GameObject> _callback, MyNetworkSync _networkSync = null, Transform _parent = null, bool _worldPositionStays = false, string _id = "")
        {

            IFactory Factory = FactoriesManager.instance.MainFactory.GetFactory(_type);
            if (Factory != null)
            {
                GameObject CreationObject = Factory.Create(_name, _parent, _worldPositionStays);
                if (CreationObject != null)
                {
                    if (_callback != null)
                        _callback.Invoke(CreationObject);

                    if (_networkSync != null)
                        _networkSync(CreationObject, _id);

                    return CreationObject;
                }
                else
                {
                  //  LHSDebug.LogWarningFormat("ClassReflectionObjectCreator.Create - Name: {0} ERROR!!", _name);
                }
            }
            else
            {
             //   LHSDebug.LogWarningFormat("ClassReflectionObjectCreator.Create - Type: {0} ERROR!!", _type);
            }

            return null;
        }

        public GameObject CreateViaFunctionLink(MyFunctionLink _functionLink, UnityAction<GameObject> _callback, MyNetworkSync _networkSync = null, string _id = "")
        {
            GameObject CreationObject = _functionLink.Invoke();
            if (CreationObject != null)
            {
                if (_callback != null)
                    _callback.Invoke(CreationObject);

                if (_networkSync != null)
                    _networkSync(CreationObject, _id);

                return CreationObject;
            }
          /*  else
            {
                Debug.LogWarning("ClassReflectionObjectCreator.CreateViaFunctionLink - _functionLink returned NULL - ERROR!!");
            }*/
            return null;
        }

    }
}
