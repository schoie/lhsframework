﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Xml;
using UnityEngine;

namespace LHS.Framework.GameData
{

    using UnityEngine;
    using System.Collections;
    using UnityEngine.Events;

    public interface IFactory
    {

        bool Load();
        void FillContent(Transform content, GameObject prefab, bool worldPositionStays = false);
        void FillAvailableContent(Transform content, GameObject prefab, bool worldPositionStays = false);
        void FillContentWithAppliedFunction(Transform content, GameObject prefab, UnityAction call, bool worldPositionStays = false);
        void FillAvailableContentWithAppliedFunction(Transform content, GameObject prefab, UnityAction call, bool worldPositionStays = false);
        string GetObjectName();
        IReflectionClass GetTemplate(string name);
        GameObject Create(string name, Transform parent = null, bool worldPositionStays = false);

    }

    public interface IFactoryClosed
    {
        bool Load();
    }

    public class FactoriesManager : CSingleton<FactoriesManager>
    {
        Dictionary<int, FactoriesLoader> LoadedFactories;
        Dictionary<Type, int> LoadedIDs;
        public FactoriesLoader MainFactory { get; private set; }

        public FactoriesManager()
        {
            LoadedFactories = new Dictionary<int, FactoriesLoader>();
            LoadedIDs = new Dictionary<Type, int>();
            MainFactory = null;
        }

        // the register system is on hold for the short term
        public bool Register(string factory, FactoriesLoader loader, bool defaultFactory)
        {
            int code = factory.GetHashCode();
            FactoriesLoader checkLoader;
            if (!LoadedFactories.TryGetValue(code, out checkLoader)){

                LoadedFactories.Add(code, loader);

                if (defaultFactory)
                    MainFactory = loader;
                
                return true;
            }
            return false;
        }

        public int GetCurrentIndex(Type type)
        {
            int returnIndex = 0;
            if (LoadedIDs.TryGetValue(type, out returnIndex))
                return returnIndex;
            return 0;
        }
        public void SetCurrentIndex(Type type, int _index)
        {
            if (!LoadedIDs.ContainsKey(type))
            {
                LoadedIDs.Add(type, _index);
            }
            else
            {
                LoadedIDs[type] = _index;
            }
        }
    }

    public class FactoriesLoader : MonoBehaviour
    {

        [Header("Attach Empty GameObjects with Factories", order = 1)]
        [Header("of type IFactory. Each factory can be hidden!", order = 2)]
        [Header("If all are hidden we will hide this GameObejct!!", order = 3)]
        [Header("If you do not want this, set OverrideHide True", order = 4)]
        public bool OverrideHide = false;
        public bool Main = true;

        [Space(20)]
        public GameObject UITypePrefab;
        [Space(0)]
        public GameObject UIObjectPrefab;

        private IFactory[] Factories;

        // Use this for initialization
        public void Awake()
        {

            if (!FactoriesManager.instance.Register(this.gameObject.name, this, Main))
            {
                DestroyImmediate(this.gameObject);
                return;
            }
            
            DontDestroyOnLoad(this.gameObject);

            bool hideFactories = true;

            Factories = this.GetComponentsInChildren<IFactory>();
            foreach (IFactory factory in Factories)
            {
                hideFactories &= factory.Load();
            }

            if (hideFactories && !OverrideHide)
            {
                this.gameObject.SetActive(false);
                this.gameObject.hideFlags = HideFlags.HideInHierarchy;
            }
            else
            {
                this.gameObject.SetActive(false);
            }

            // A section for other factories that we may want to load
            handleOtherFactoryTypes();

        }

        private IFactory[] GetFactoriesArray()
        {
            return this.GetComponentsInChildren<IFactory>();
        }
        public List<IFactory> GetFactories()
        {
            IFactory[] factories = GetFactoriesArray();
            List<IFactory> returnFactories = new List<IFactory>();
            List<string> factoryNames = new List<string>();

            string name;
            foreach (IFactory factory in factories)
            {
                name = factory.GetObjectName();
                if (!factoryNames.Contains(name))
                {
                    factoryNames.Add(name);
                    returnFactories.Add(factory);
                }
            }

            return returnFactories;
        }

        public IFactory GetFactory(string name)
        {
            foreach (IFactory factory in Factories)
            {
                if(factory.GetObjectName() == name)
                {
                    return factory;
                }
            }
            return default(IFactory);
        }

        private void handleOtherFactoryTypes()
        {

            // Handle new factory type closed
            // that is not for the sandbox, but provides objects that we can tempalte and create
            IFactoryClosed[] ClosedFactories = this.GetComponentsInChildren<IFactoryClosed>();
            foreach (IFactoryClosed factory in ClosedFactories)
            {
                factory.Load();
            }


        }

    }

    public abstract class ReflectionFactoryFunctions
    {
        public static void LoadFactory<ReflectionClass>(Dictionary<string, ReflectionClass> Templates, string factoryName, TextAsset _xml, string _xmlRoot, Transform Parent = null)
           where ReflectionClass : IReflection, new()
        {
            if (_xml == null)
                return;

            XmlDocument m_xmlDoc = new XmlDocument();
            m_xmlDoc.LoadXml(_xml.text);
            int index = FactoriesManager.instance.GetCurrentIndex(typeof(ReflectionClass));

            XmlNode nodeObjects = m_xmlDoc.SelectSingleNode(_xmlRoot);
            for (XmlNode node = nodeObjects.FirstChild; node != null; node = node.NextSibling)
            {
                if (node != null && node.Attributes != null)
                {
                    Type ObjectType = Type.GetType(node.Name + ",Assembly-CSharp", false, false);
                    if (ObjectType == null)
                        return;

                    ReflectionClass myObject = new ReflectionClass();

                    myObject.LoadObjectExt(node, myObject);

                    string objectName = "";
                    // we will look for name
                    // we will look for id
                    // if we do not find either it will be type name plus count
                    if (node.Attributes["name"] != null)
                    {
                        objectName = node.Attributes["name"].Value;
                    }
                    else if (node.Attributes["id"] != null)
                    {
                        objectName = node.Attributes["id"].Value;
                    }
                    else
                    {
                        objectName = node.Name + index.ToString();
                    }

                    // set the id
                    // note that we can also use object name ToHash if we wanted to
                    index++;
                    myObject.SetID(index);

                    if (Templates.ContainsKey(objectName))
                        LHSDebug.LogWarningFormat("{0} - Constructed Object {1} with name {2} - This name already exists!!", factoryName, typeof(ReflectionClass).Name, objectName);
                    else
                        Templates.Add(objectName, myObject);

                }
            }

            FactoriesManager.instance.SetCurrentIndex(typeof(ReflectionClass), index);

        }

        public static void LoadGameObjectFactory<ReflectionClass>(Dictionary<string, ReflectionClass> Templates, string factoryName, TextAsset _xml, string _xmlRoot, GameObject Prefab = null, Transform Parent = null, bool worldPositionStays = false) 
            where ReflectionClass : IReflectionGameObject, new()
        {
            if (_xml == null)
                return;

            XmlDocument m_xmlDoc = new XmlDocument();
            m_xmlDoc.LoadXml(_xml.text);
            int index = FactoriesManager.instance.GetCurrentIndex(typeof(ReflectionClass));

            XmlNode nodeObjects = m_xmlDoc.SelectSingleNode(_xmlRoot);
            for (XmlNode node = nodeObjects.FirstChild; node != null; node = node.NextSibling)
            {
                if (node != null && node.Attributes != null)
                {
                    Type ObjectType = Type.GetType(node.Name + ",Assembly-CSharp", false, false);
                    if (ObjectType == null)
                        return;

                    ReflectionClass myObject = new ReflectionClass();

                    if (Prefab == null)
                    {
                        myObject.CreateGameObject();

                        if (Parent != null)
                            myObject.SetParent(Parent, false);
                        else
                            myObject.HideGameObject();

                    }
                    else
                    {
                        myObject.InstantiateGameObject(Prefab, Parent, worldPositionStays);
                        if (Parent == null)
                            myObject.HideGameObject();
                    }

                    myObject.LoadObjectExt(node, myObject);

                    // set the id
                    // note that we can also use object name ToHash if we wanted to
                    index++;
                    myObject.SetID(index);

                    string objectName = myObject.GetName();
                    if (Templates.ContainsKey(objectName))
                        LHSDebug.LogWarningFormat("{0} - Constructed Object {1} with name {2} - This name already exists!!", factoryName, typeof(ReflectionClass).Name, objectName);
                    else
                        Templates.Add(objectName, myObject);

                }
            }

            FactoriesManager.instance.SetCurrentIndex(typeof(ReflectionClass), index);
        }
    }

    public class ClassReflectionFactory<ReflectionClass> : MonoBehaviour
        where ReflectionClass : IReflection, new()
    {

        Dictionary<string, ReflectionClass> Templates = new Dictionary<string, ReflectionClass>();
        List<int> LoadedTemplates = new List<int>();

        public bool isLoaded(string file)
        {
            int code = file.GetHashCode();
            if (LoadedTemplates.Contains(code))
                return true;
            LoadedTemplates.Add(code);
            return false;
        }

        public Dictionary<string, ReflectionClass> GetTemplates()
        {
            return Templates;
        }
        public ReflectionClass GetTemplate(string name)
        {
            ReflectionClass template;
            Templates.TryGetValue(name, out template);
            return template;
        }
        public bool TryGetTemplate(string name, out ReflectionClass template)
        {
            if (Templates.TryGetValue(name, out template))
                return true;
            return false;
        }

        public void Load(TextAsset _xml, string _xmlRoot, Transform Parent = null)
        {
            if (!isLoaded(_xml.name))
                ReflectionFactoryFunctions.LoadFactory(Templates, this.name, _xml, _xmlRoot, Parent);
        }

        public string GetClassName()
        {
            return typeof(ReflectionClass).Name;
        }

    }

    public class ClassReflectionFactorySingleton<Singleton, ReflectionClass> : CSingleton<Singleton>
        where ReflectionClass : IReflection, new()
        where Singleton : new()
    {

        Dictionary<string, ReflectionClass> Templates = new Dictionary<string, ReflectionClass>();
        List<int> LoadedTemplates = new List<int>();

        public bool isLoaded(string file)
        {
            int code = file.GetHashCode();
            if (LoadedTemplates.Contains(code))
                return true;
            LoadedTemplates.Add(code);
            return false;
        }


        public Dictionary<string, ReflectionClass> GetTemplates()
        {
            return Templates;
        }
        public ReflectionClass GetTemplate(string name)
        {
            ReflectionClass template;
            Templates.TryGetValue(name, out template);
            return template;
        }
        public bool TryGetTemplate(string name, out ReflectionClass template)
        {
            if (Templates.TryGetValue(name, out template))
                return true;
            return false;
        }

        public void Load(TextAsset _xml, string _xmlRoot, Transform Parent = null)
        {
            if (!isLoaded(_xml.name))
                ReflectionFactoryFunctions.LoadFactory(Templates, typeof(Singleton).Name, _xml, _xmlRoot, Parent);
        }

        public string GetClassName()
        {
            return typeof(ReflectionClass).Name;
        }

    }

    public class ClassReflectionFactorySingletonMonoBehaiour<Singleton, ReflectionClass> : CSingletonMonoBehaviour<Singleton>
        where ReflectionClass : IReflection, new()
    {

        Dictionary<string, ReflectionClass> Templates = new Dictionary<string, ReflectionClass>();
        List<int> LoadedTemplates = new List<int>();

        public bool isLoaded(string file)
        {
            int code = file.GetHashCode();
            if (LoadedTemplates.Contains(code))
                return true;
            LoadedTemplates.Add(code);
            return false;
        }

        public override void OnAwake() { }


        public Dictionary<string, ReflectionClass> GetTemplates()
        {
            return Templates;
        }
        public ReflectionClass GetTemplate(string name)
        {
            ReflectionClass template;
            Templates.TryGetValue(name, out template);
            return template;
        }
        public bool TryGetTemplate(string name, out ReflectionClass template)
        {
            if (Templates.TryGetValue(name, out template))
                return true;
            return false;
        }

        public void Load(TextAsset _xml, string _xmlRoot, Transform Parent = null)
        {
            if (!isLoaded(_xml.name))
                ReflectionFactoryFunctions.LoadFactory(Templates, this.name, _xml, _xmlRoot, Parent);
        }

        public string GetClassName()
        {
            return typeof(ReflectionClass).Name;
        }

    }


    public class ClassReflectionGameObjectFactory<ReflectionClass> : MonoBehaviour
        where ReflectionClass : IReflectionGameObject, new()
    {

        Dictionary<string, ReflectionClass> Templates = new Dictionary<string, ReflectionClass>();
        List<int> LoadedTemplates = new List<int>();

        public bool isLoaded(string file)
        {
            int code = file.GetHashCode();
            if (LoadedTemplates.Contains(code))
                return true;
            LoadedTemplates.Add(code);
            return false;
        }


        public Dictionary<string, ReflectionClass> GetTemplates()
        {
            return Templates;
        }
        public ReflectionClass GetTemplate(string name)
        {
            ReflectionClass template;
            Templates.TryGetValue(name, out template);
            return template;
        }
        public bool TryGetTemplate(string name, out ReflectionClass template)
        {
            if (Templates.TryGetValue(name, out template))
                return true;
            return false;
        }

        public void Load(TextAsset _xml, string _xmlRoot)
        {
            if (!isLoaded(_xml.name))
                ReflectionFactoryFunctions.LoadGameObjectFactory(Templates, this.name, _xml, _xmlRoot, null, null, false);
        }
        public void Load(TextAsset _xml, string _xmlRoot, GameObject Prefab = null, Transform Parent = null, bool worldPositionStays = false)
        {
            if (!isLoaded(_xml.name))
                ReflectionFactoryFunctions.LoadGameObjectFactory(Templates, this.name, _xml, _xmlRoot, Prefab, Parent, worldPositionStays);
        }

        public string GetClassName()
        {
            return typeof(ReflectionClass).Name;
        }

    }

    public class ClassReflectionGameObjectFactorySingleton<Singleton, ReflectionClass> : CSingleton<Singleton>
        where ReflectionClass : IReflectionGameObject, new()
        where Singleton : new()
    {

        Dictionary<string, ReflectionClass> Templates = new Dictionary<string, ReflectionClass>();
        List<int> LoadedTemplates = new List<int>();

        public bool isLoaded(string file)
        {
            int code = file.GetHashCode();
            if (LoadedTemplates.Contains(code))
                return true;
            LoadedTemplates.Add(code);
            return false;
        }

        public Dictionary<string, ReflectionClass> GetTemplates()
        {
            return Templates;
        }
        public ReflectionClass GetTemplate(string name)
        {
            ReflectionClass template;
            Templates.TryGetValue(name, out template);
            return template;
        }
        public bool TryGetTemplate(string name, out ReflectionClass template)
        {
            if (Templates.TryGetValue(name, out template))
                return true;
            return false;
        }

        public void Load(TextAsset _xml, string _xmlRoot)
        {
            if (!isLoaded(_xml.name))
                ReflectionFactoryFunctions.LoadGameObjectFactory(Templates, typeof(Singleton).Name, _xml, _xmlRoot, null, null, false);
        }
        public void Load(TextAsset _xml, string _xmlRoot, GameObject Prefab = null, Transform Parent = null, bool worldPositionStays = false)
        {
            if (!isLoaded(_xml.name))
                ReflectionFactoryFunctions.LoadGameObjectFactory(Templates, typeof(Singleton).Name, _xml, _xmlRoot, Prefab, Parent, worldPositionStays);
        }

        public string GetClassName()
        {
            return typeof(ReflectionClass).Name;
        }

    }

    public class ClassReflectionGameObjectFactorySingletonMonoBehaviour<Singleton, ReflectionClass> : CSingletonMonoBehaviour<Singleton>
        where ReflectionClass : IReflectionGameObject, new()
    {

        Dictionary<string, ReflectionClass> Templates = new Dictionary<string, ReflectionClass>();
        List<int> LoadedTemplates = new List<int>();

        public bool isLoaded(string file)
        {
            int code = file.GetHashCode();
            if (LoadedTemplates.Contains(code))
                return true;
            LoadedTemplates.Add(code);
            return false;
        }

        public override void OnAwake() {
            LHSDebug.LogFormat("ClassReflectionGameObjectFactorySingletonMonoBehaviour<{0}, {1}>::OnAwake()",
                typeof(Singleton).Name, typeof(ReflectionClass).Name);
        }

        public Dictionary<string, ReflectionClass> GetTemplates()
        {
            return Templates;
        }
        public ReflectionClass GetTemplate(string name)
        {
            ReflectionClass template;
            Templates.TryGetValue(name, out template);
            return template;
        }
        public bool TryGetTemplate(string name, out ReflectionClass template)
        {
            if (Templates.TryGetValue(name, out template))
                return true;
            return false;
        }

        public void Load(TextAsset _xml, string _xmlRoot)
        {
            if(!isLoaded(_xml.name))
                ReflectionFactoryFunctions.LoadGameObjectFactory(Templates, this.name, _xml, _xmlRoot, null, null, false);
        }
        public void Load(TextAsset _xml, string _xmlRoot, GameObject Prefab = null, Transform Parent = null, bool worldPositionStays = false)
        {
            if (!isLoaded(_xml.name))
                ReflectionFactoryFunctions.LoadGameObjectFactory(Templates, this.name, _xml, _xmlRoot, Prefab, Parent, worldPositionStays);
        }

        public string GetClassName()
        {
            return typeof(ReflectionClass).Name;
        }

    }
}
