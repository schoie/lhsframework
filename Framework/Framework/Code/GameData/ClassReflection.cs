﻿/*
    ClassReflection.cs -- Provides classes and abstract functions for creating Objects with reflection support. 
                            Allowing us to build prefabs, load via xml and script Object and component creation

    Name: Steven Ogden
    Date: 31/05/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Xml;
using UnityEngine;
using LHS.Framework.Game;
using System.Text.RegularExpressions;
using LHS.Framework.Tools;

namespace LHS.Framework.GameData
{
    public interface IReflectionData<T> where T : new()
    {
        void DefaultData(T Data);
        void PreparePrefab(GameObject Prefab, GameObject[] resources, int index = 0);
        void SetPrefab(GameObject Prefab, T Data);
        void GetDataFromPrefab(GameObject Prefab, T Data);
        void LoadData(XmlNode Node, T Data);
        void LoadObject(XmlNode Node, T Data, GameObject Obj = null);
        string SetString(string input, T Data);
    }

    public interface IReflection
    {
        void LoadObjectExt<T>(XmlNode Node, T Data) where T : new();
        void SetID(int id);
    }

    public interface IReflectionClass
    {
        GameObject CreateMe();
        GameObject SpawnMe();
        void PurchaseMe();
        void UnlockMe();
        void LevelUpMe(int newLevel);

        void SetMe(GameObject ToSet);
    }

    public interface IReflectionGameObject
    {
        void LoadObjectExt<T>(XmlNode Node, T Data) where T : new();
        void CreateGameObject();
        void HideGameObject();
        void RevealGameObject();
        void InstantiateGameObject(GameObject toInstantiate, Transform Parent = null, bool worldPositionStays = false);
        void InstantiateGameObject(GameObject toInstantiate, Vector3 Position, Quaternion Rotation, Transform Parent = null, bool worldPositionStays = false);
        void SetParent(Transform Parent, bool worldPositionStays);
        string GetName();
        void SetID(int id);
    }

    public abstract class ReflectionFunctions
    {
        ///<summary>
        /// Default the Data in this Class using Reflection
        /// Note it would be possible to pass a different Data to refer to. Alos understand that you can call these functions directly using the abstract class ReflectionFunctions
        ///</summary>
        /// <param name="Data">Data should be the same reference as the ReflectionClass type that you are calling this function with.</param>
        static public void DefaultData<ReflectionClass>(ReflectionClass Data) where ReflectionClass : new()
        {
            Type T = typeof(ReflectionClass);
            FieldInfo[] fields = T.GetFields();
            foreach (FieldInfo field in fields)
            {
                ParseReflectionFunctions.DefaultVariable(field, Data);
            }

        }
        ///<summary>
        /// Build UI and attach to the prefab using resources and Class Reflection
        ///</summary>
        /// <param name="Prefab">Prefab to build UI into.</param>
        /// <param name="resources">resources to build UI from.</param>
        static public void PreparePrefab<ReflectionClass>(GameObject Prefab, GameObject[] resources, int index = 0) where ReflectionClass : new()
        {

            float yRunningPos = 0;
            float startingPos = 0;

            ObjectSpacing spacing = new ObjectSpacing();
            spacing.BetweenElements = 6;

            RectTransform pMainRect = Prefab.GetComponent<RectTransform>();
            startingPos = spacing.header;

            Type T = typeof(ReflectionClass);
            FieldInfo[] fields = T.GetFields();
            foreach (FieldInfo field in fields)
            {

                if (!field.FieldType.IsClass || field.FieldType == typeof(string))
                {
                    GameObject obj = BuildUIReflectionFunctions.ConstructTextResource(ref Prefab, resources[index], field.Name, ref yRunningPos, startingPos, spacing, spacing.TitleIndent);
                    obj.name = field.Name;

                }

            }


        }

        ///<summary>
        /// Set UI in prefab using Class Reflection and Fields of the same name
        /// Note it would be possible to pass a different Data to refer to. Alos understand that you can call these functions directly using the abstract class ReflectionFunctions
        ///</summary>
        /// <param name="Prefab">Prefab to edit UI in into.</param>
        /// <param name="Data">Data should be the same reference as the ReflectionClass type that you are calling this function with.</param>
        static public void SetPrefab<ReflectionClass>(GameObject Prefab, ReflectionClass Data) where ReflectionClass : new()
        {
            Type T = typeof(ReflectionClass);
            for (int i = 0; i < Prefab.transform.childCount; ++i)
            {
                GameObject child = Prefab.transform.GetChild(i).gameObject;

                FieldInfo field;
                string[] variables = child.name.Split('.');
                switch (variables.Length)
                {
                    case 1:
                        field = T.GetField(child.name);
                        if (field != null)
                        {
                            BuildUIReflectionFunctions.ProcessVariable(child, field.GetValue(Data));
                        }
                        break;
                    default:

                        if (variables.Length <= 0)
                            break;

                        field = T.GetField(variables[0]);
                        if (field != null)
                        {
                            for (int f = 1; f < variables.Length; ++f)
                            {
                                var classfield = field.FieldType.GetField(variables[f]);
                                BuildUIReflectionFunctions.ProcessVariable(child, classfield.GetValue(field.GetValue(Data)));
                            }
                        }

                        break;
                }

                // recursion
                SetPrefab(child, Data);

            }
        }

        ///<summary>
        /// Set UI in prefab using Class Reflection and Fields of the same name
        /// Note it would be possible to pass a different Data to refer to. Alos understand that you can call these functions directly using the abstract class ReflectionFunctions
        ///</summary>
        /// <param name="Prefab">Prefab to edit UI in into.</param>
        /// <param name="Data">Data should be the same reference as the ReflectionClass type that you are calling this function with.</param>
        /// <param name="LocalName">LocalName will be used to identify any possible localisation associated with the variable name.</param>
        static public void SetPrefabWithLocal<ReflectionClass>(GameObject Prefab, ReflectionClass Data, string LocalName) where ReflectionClass : new()
        {
            Type T = typeof(ReflectionClass);
            for (int i = 0; i < Prefab.transform.childCount; ++i)
            {
                GameObject child = Prefab.transform.GetChild(i).gameObject;

                FieldInfo field;
                string[] variables = child.name.Split('.');
                switch (variables.Length)
                {
                    case 1:
                        field = T.GetField(child.name);
                        if (field != null)
                        {
                            BuildUIReflectionFunctions.ProcessVariableLocal(child, field.GetValue(Data), LocalName);
                        }
                        break;
                    default:

                        if (variables.Length <= 0)
                            break;

                        field = T.GetField(variables[0]);
                        if (field != null)
                        {
                            for (int f = 1; f < variables.Length; ++f)
                            {
                                var classfield = field.FieldType.GetField(variables[f]);
                                BuildUIReflectionFunctions.ProcessVariableLocal(child, classfield.GetValue(field.GetValue(Data)), LocalName);
                            }
                        }

                        break;
                }

                // recursion
                SetPrefabWithLocal(child, Data, LocalName);

            }
        }

        ///<summary>
        /// Set UI in prefab using Class Reflection and Fields of the same name
        /// Note it would be possible to pass a different Data to refer to. Alos understand that you can call these functions directly using the abstract class ReflectionFunctions
        ///</summary>
        /// <param name="Prefab">Prefab to edit UI in into.</param>
        /// <param name="Data">Data should be the same reference as the ReflectionClass type that you are calling this function with.</param>
        /// <param name="Variable">Specify the variable to target, i.e. variable name and matching Prefab child name.</param>
        static public void SetPrefab<ReflectionClass>(GameObject Prefab, ReflectionClass Data, string Variable) where ReflectionClass : new()
        {
            GameObject child = Prefab.transform.Find(Variable).gameObject;
            if(child != null)
            {
                Type T = typeof(ReflectionClass);
                
                FieldInfo field;
                string[] variables = child.name.Split('.');
                switch (variables.Length)
                {
                    case 1:
                        field = T.GetField(child.name);
                        if (field != null)
                        {
                            BuildUIReflectionFunctions.ProcessVariable(child, field.GetValue(Data));
                        }
                        break;
                    default:

                        if (variables.Length <= 0)
                            break;

                        field = T.GetField(variables[0]);
                        if (field != null)
                        {
                            for (int f = 1; f < variables.Length; ++f)
                            {
                                var classfield = field.FieldType.GetField(variables[f]);
                                BuildUIReflectionFunctions.ProcessVariable(child, classfield.GetValue(field.GetValue(Data)));
                            }
                        }

                        break;
                }

            }
        }

        ///<summary>
        /// Gets the Localised string from the value set in string Variable and the variable name itself
        /// Note it would be possible to pass a different Data to refer to. Alos understand that you can call these functions directly using the abstract class ReflectionFunctions
        ///</summary>
        /// <param name="Data">Data should be the same reference as the ReflectionClass type that you are calling this function with.</param>
        /// <param name="Variable">Specify the variable to target, i.e. variable name and matching Prefab child name.</param>
        static public string GetLocalised<ReflectionClass>(ReflectionClass Data, string Variable) where ReflectionClass : new()
        {
            Type T = typeof(ReflectionClass);

            FieldInfo field;

            field = T.GetField(Variable);
            if (field != null)
            {
                object value = field.GetValue(Data);
                string textValue = value.ToString();

                if (LocalisationReader.instance != null)
                {
                    string localtext;
                    if (LocalisationReader.instance.GetLocalisedString(textValue + "_" + Variable, out localtext))
                        textValue = localtext;
                }

                return textValue;
            }

            return "";
        }

        ///<summary>
        /// Get data from the Prefab using Class Reflection to match the Field names with Object Names in the prefab
        /// Note it would be possible to pass a different Data to refer to. Alos understand that you can call these functions directly using the abstract class ReflectionFunctions
        ///</summary>
        /// <param name="Prefab">Prefab to get data from.</param>
        /// <param name="Data">Data should be the same reference as the ReflectionClass type that you are calling this function with.</param>
        static public void GetDataFromPrefab<ReflectionClass>(GameObject Prefab, ReflectionClass Data) where ReflectionClass : new()
        {
            Type T = typeof(ReflectionClass);
            for (int i = 0; i < Prefab.transform.childCount; ++i)
            {
                GameObject child = Prefab.transform.GetChild(i).gameObject;

                FieldInfo field;
                string[] variables = child.name.Split('.');
                switch (variables.Length)
                {
                    case 1:
                        field = T.GetField(child.name);
                        if (field != null)
                        {
                            BuildUIReflectionFunctions.ReadVariable(child, field, Data);
                        }
                        break;
                    default:

                        if (variables.Length <= 0)
                            break;

                        field = T.GetField(variables[0]);
                        if (field != null)
                        {
                            for (int f = 1; f < variables.Length; ++f)
                            {
                                var classfield = field.FieldType.GetField(variables[f]);
                                BuildUIReflectionFunctions.ReadVariable(child, classfield, field.GetValue(Data));
                            }
                        }

                        break;
                }
            }
        }

        ///<summary>
        /// Get data from the Prefab using Class Reflection to match the Field names with Object Names in the prefab
        /// Note it would be possible to pass a different Data to refer to. Alos understand that you can call these functions directly using the abstract class ReflectionFunctions
        ///</summary>
        /// <param name="Prefab">Prefab to get data from.</param>
        /// <param name="Data">Data should be the same reference as the ReflectionClass type that you are calling this function with.</param>
        /// <param name="Variable">Specify the variable to target, i.e. variable name and matching Prefab child name.</param>
        static public void GetDataFromPrefab<ReflectionClass>(GameObject Prefab, ReflectionClass Data, string Variable) where ReflectionClass : new()
        {
            GameObject child = Prefab.transform.Find(Variable).gameObject;
            if (child != null)
            {
                Type T = typeof(ReflectionClass);

                FieldInfo field;
                string[] variables = child.name.Split('.');
                switch (variables.Length)
                {
                    case 1:
                        field = T.GetField(child.name);
                        if (field != null)
                        {
                            BuildUIReflectionFunctions.ReadVariable(child, field, Data);
                        }
                        break;
                    default:

                        if (variables.Length <= 0)
                            break;

                        field = T.GetField(variables[0]);
                        if (field != null)
                        {
                            for (int f = 1; f < variables.Length; ++f)
                            {
                                var classfield = field.FieldType.GetField(variables[f]);
                                BuildUIReflectionFunctions.ReadVariable(child, classfield, field.GetValue(Data));
                            }
                        }

                        break;
                }
            }
        }

        ///<summary>
        /// Load Data in this Class using Reflection using XML data
        /// Note it would be possible to pass a different Data to refer to. Alos understand that you can call these functions directly using the abstract class ReflectionFunctions
        ///</summary>
        /// <param name="Node">XmlNode to get data from.</param>
        /// <param name="Data">Data should be the same reference as the ReflectionClass type that you are calling this function with.</param>
        /// <param name="Obj">Game Object to load and attach scripts to.</param>
        static public void LoadObject<ReflectionClass>(XmlNode Node, ReflectionClass Data, GameObject Obj = null) where ReflectionClass : new()
        {
            LoadData<ReflectionClass>(Node, Data);
            LoadFunctionData<ReflectionClass>(Node, Data);
            if (Obj != null)
                LoadGameObject(Node, Obj, typeof(ReflectionClass));
        }

        static public void LoadGameObject(XmlNode Node, GameObject Obj, Type objectType = null)
        {
            // set name
            if (Node.Attributes["name"] != null)
            {
                Obj.name = (Node.Attributes["name"] != null) ? Node.Attributes["name"].Value : (objectType != null) ? objectType.Name : "New Object";
            }

            for (XmlNode comp = Node.FirstChild; comp != null; comp = comp.NextSibling)
            {
                if (comp.Name == "component" || comp.Name == "Component")
                {
                    // create and attach component
                    if (comp.Attributes["name"] != null)
                    {
                        Type componentType = Type.GetType(comp.Attributes["name"].Value + ",Assembly-CSharp", false, false);

                        // We support Unity Engine Objects
                        if (componentType == null)
                            componentType = Type.GetType("UnityEngine." + comp.Attributes["name"].Value + ",UnityEngine", false, false);

                        // We support Unity Engine UI Objects - Note that if a script does not load we need to look here and add its assembly reference
                        if (componentType == null)
                            componentType = Type.GetType("UnityEngine.UI." + comp.Attributes["name"].Value + ",UnityEngine.UI", false, false);

                        if (componentType == null)
                        {
                            LHSDebug.LogWarningFormat("Class Reflection - Tyrying to AddComponent Type: ({0}) to Object Type: ({1}). This Component Type does not exist!!",
                                Obj.name, comp.Attributes["name"].Value);
                        }
                        else
                        {
                            var component = Obj.AddComponent(componentType) as MonoBehaviour;
                            for (XmlNode var = comp.FirstChild; var != null; var = var.NextSibling)
                            {
                                if (var.Attributes["name"] != null && var.Attributes["value"] != null)
                                {
                                    const BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.Instance;

                                    FieldInfo field;
                                    PropertyInfo property;
                                    string[] variables = var.Attributes["name"].Value.Split('.');
                                    switch (variables.Length)
                                    {
                                        case 1:
                                            field = componentType.GetField(variables[0], bindingFlags);
                                            if (field != null)
                                            {
                                                ParseReflectionFunctions.ParseVariable(field, component, var.Attributes["value"].Value);
                                            }
                                            property = componentType.GetProperty(variables[0], bindingFlags);
                                            if (property != null)
                                            {
                                                ParseReflectionFunctions.ParseVariable(property, component, var.Attributes["value"].Value);
                                            }
                                            break;

                                        default:

                                            if (variables.Length <= 0)
                                                break;

                                            field = componentType.GetField(variables[0], bindingFlags);
                                            if (field != null)
                                            {
                                                ParseReflectionFunctions.ParseVariables(field, var.Attributes["value"].Value, variables, component);
                                            }
                                            property = componentType.GetProperty(variables[0], bindingFlags);
                                            if (property != null)
                                            {
                                                ParseReflectionFunctions.ParseVariables(property, var.Attributes["value"].Value, variables, component);
                                            }

                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        ///<summary>
        /// Load Data in this Class using Reflection using XML data
        /// Note it would be possible to pass a different Data to refer to. Alos understand that you can call these functions directly using the abstract class ReflectionFunctions
        ///</summary>
        /// <param name="Node">XmlNode to get data from.</param>
        /// <param name="Data">Data should be the same reference as the ReflectionClass type that you are calling this function with.</param>
        static public void LoadData<ReflectionClass>(XmlNode Node, ReflectionClass Data) where ReflectionClass : new()
        {
            Type T = typeof(ReflectionClass);
            FieldInfo[] fields = T.GetFields();
            foreach (FieldInfo field in fields)
            {
                if (field.FieldType.IsClass && field.FieldType.Name != "String")
                {

                    FieldInfo[] classfields = field.FieldType.GetFields();
                    foreach (FieldInfo classfield in classfields)
                    {
                        string varName = field.Name + "." + classfield.Name;
                        if (Node.Attributes[varName] != null)
                            ParseReflectionFunctions.ParseVariable(classfield, field.GetValue(Data), Node.Attributes[varName].Value);
                    }

                    if(classfields == null || classfields.Length <= 0)
                    {
                        if (Node.Attributes[field.Name] != null)
                            ParseReflectionFunctions.ParseVariable(field, Data, Node.Attributes[field.Name].Value);
                    }
                }
                else
                {
                    if (Node.Attributes[field.Name] != null)
                        ParseReflectionFunctions.ParseVariable(field, Data, Node.Attributes[field.Name].Value);
                }
            }

        }


        ///<summary>
        /// Load Function Data in this Class using Reflection using XML data
        /// Will call a function by name in the object class and provide the node in order to load specialised data
        /// Note it would be possible to pass a different Data to refer to. Alos understand that you can call these functions directly using the abstract class ReflectionFunctions
        ///</summary>
        /// <param name="Node">XmlNode to get data from.</param>
        /// <param name="Data">Data should be the same reference as the ReflectionClass type that you are calling this function with.</param>
        static public void LoadFunctionData<ReflectionClass>(XmlNode Node, ReflectionClass Data) where ReflectionClass : new()
        {

            for (XmlNode function = Node.FirstChild; function != null; function = function.NextSibling)
            {
                if (function.Name == "function")
                {
                    if (function.Attributes["name"] != null)
                    {
                        Type T = typeof(ReflectionClass);
                        MethodInfo methodInfo = T.GetMethod(function.Attributes["name"].Value);

                        if (methodInfo != null)
                        {
                            object result = methodInfo.Invoke(Data, new object[] { function });
                        }
                    }
                }
            }

        }

        ///<summary>
        /// Set a formatted string via $variable_name$ using reflection
        /// Will find any $variable_name$ and find if that variable exists using reflection and replace in the string the $variable_name$
        /// with the variable value (ToString) for that ReflectionClass object
        ///</summary>
        /// <param name="input">String to read formatting from and replace.</param>
        /// <param name="Data">Data should be the same reference as the ReflectionClass type that you are calling this function with.</param>
        static public string SetString<ReflectionClass>(string input, ReflectionClass Data) where ReflectionClass : new()
        {
            Type T = typeof(ReflectionClass);
            
            MatchCollection matches = Regex.Matches(input, @"\$(\([^)]*)\)");

            char[] trim = new char[3];
            trim[0] = '$'; trim[1] = '('; trim[2] = ')';

            foreach (Match match in matches)
            {
                string key = match.Value.Trim(trim).ToLower();
                string value = match.Value;

                FieldInfo field;
                string[] variables = key.Split('.');
                switch (variables.Length)
                {
                    case 1:
                        field = T.GetField(key);
                        if (field != null)
                        {
                            input = input.Replace(value, field.GetValue(Data).ToString());
                        }
                        break;
                    default:

                        if (variables.Length <= 0)
                            break;

                        field = T.GetField(variables[0]);
                        if (field != null)
                        {
                            for (int f = 1; f < variables.Length; ++f)
                            {
                                var classfield = field.FieldType.GetField(variables[f]);
                                input = input.Replace(value, classfield.GetValue(field.GetValue(Data)).ToString());
                            }
                        }

                        break;
                }
                
            }


            //input = input.Replace("#CURRENCY#", "$");


            return input;
        }



    }

    // Objects
    public class ClassReflection<ReflectionClass> : IReflectionData<ReflectionClass>, IReflection, IReflectionClass where ReflectionClass : new()
    {
        public int ID;
        public void DefaultData(ReflectionClass Data)
        {
            ReflectionFunctions.DefaultData<ReflectionClass>(Data);
        }
        public void PreparePrefab(GameObject Prefab, GameObject[] resources, int index = 0)
        {
            ReflectionFunctions.PreparePrefab<ReflectionClass>(Prefab, resources, index);
        }
        public void SetPrefab(GameObject Prefab, ReflectionClass Data)
        {
            ReflectionFunctions.SetPrefab<ReflectionClass>(Prefab, Data);
            SetMe(Prefab);
        }
        public void SetPrefab(GameObject Prefab, ReflectionClass Data, string Variable)
        {
            ReflectionFunctions.SetPrefab<ReflectionClass>(Prefab, Data, Variable);
            SetMe(Prefab);
        }
        public void SetPrefabWithLocal(GameObject Prefab, ReflectionClass Data, string LocalName)
        {
            ReflectionFunctions.SetPrefabWithLocal<ReflectionClass>(Prefab, Data, LocalName);
            SetMe(Prefab);
        }
        public void GetDataFromPrefab(GameObject Prefab, ReflectionClass Data)
        {
            ReflectionFunctions.GetDataFromPrefab(Prefab, Data);
        }
        public void GetDataFromPrefab(GameObject Prefab, ReflectionClass Data, string Variable)
        {
            ReflectionFunctions.GetDataFromPrefab(Prefab, Data, Variable);
        }
        public void LoadData(XmlNode Node, ReflectionClass Data)
        {
            ReflectionFunctions.LoadData(Node, Data);
        }
        public void LoadObject(XmlNode Node, ReflectionClass Data, GameObject Obj = null)
        {
            ReflectionFunctions.LoadObject(Node, Data, Obj);
        }

        public void LoadObjectExt<T>(XmlNode Node, T Data) where T : new()
        {
            ReflectionFunctions.LoadObject<T>(Node, Data);
        }
        public string SetString(string input, ReflectionClass Data)
        {
            return ReflectionFunctions.SetString<ReflectionClass>(input, Data);
        }

        public void SetID(int id)
        {
            ID = id;
        }

        public virtual GameObject CreateMe()
        {
            // these are empty, we want the inherited classes to overload these
            return default(GameObject);
        }
        public virtual GameObject SpawnMe()
        {
            // these are empty, we want the inherited classes to overload these
            return CreateMe();
        }
        public virtual void PurchaseMe()
        {
            // these are empty, we want the inherited classes to overload these
        }
        public virtual void UnlockMe()
        {
            // these are empty, we want the inherited classes to overload these
        }
        public virtual void LevelUpMe(int newLevel)
        {
            // these are empty, we want the inherited classes to overload these
        }

        public virtual void SetMe(GameObject ToSet)
        {
            // these are empty, we want the inherited classes to overload these
        }

    }
    
    public class ClassReflectionScriptable<ReflectionClass> : ScriptableObject, IReflectionData<ReflectionClass>, IReflection, IReflectionClass where ReflectionClass : new()
    {
        public int ID;
        public void DefaultData(ReflectionClass Data)
        {
            ReflectionFunctions.DefaultData<ReflectionClass>(Data);
        }
        public void PreparePrefab(GameObject Prefab, GameObject[] resources, int index = 0)
        {
            ReflectionFunctions.PreparePrefab<ReflectionClass>(Prefab, resources, index);
        }
        public void SetPrefab(GameObject Prefab, ReflectionClass Data)
        {
            ReflectionFunctions.SetPrefab<ReflectionClass>(Prefab, Data);
            SetMe(Prefab);
        }
        public void SetPrefab(GameObject Prefab, ReflectionClass Data, string Variable)
        {
            ReflectionFunctions.SetPrefab<ReflectionClass>(Prefab, Data, Variable);
            SetMe(Prefab);
        }
        public void SetPrefabWithLocal(GameObject Prefab, ReflectionClass Data, string LocalName)
        {
            ReflectionFunctions.SetPrefabWithLocal<ReflectionClass>(Prefab, Data, LocalName);
            SetMe(Prefab);
        }
        public void GetDataFromPrefab(GameObject Prefab, ReflectionClass Data)
        {
            ReflectionFunctions.GetDataFromPrefab(Prefab, Data);
        }
        public void GetDataFromPrefab(GameObject Prefab, ReflectionClass Data, string Variable)
        {
            ReflectionFunctions.GetDataFromPrefab(Prefab, Data, Variable);
        }
        public void LoadData(XmlNode Node, ReflectionClass Data)
        {
            ReflectionFunctions.LoadData(Node, Data);
        }
        public void LoadObject(XmlNode Node, ReflectionClass Data, GameObject Obj = null)
        {
            ReflectionFunctions.LoadObject(Node, Data, Obj);
        }

        public void LoadObjectExt<T>(XmlNode Node, T Data) where T : new()
        {
            ReflectionFunctions.LoadObject<T>(Node, Data);
        }
        public string SetString(string input, ReflectionClass Data)
        {
            return ReflectionFunctions.SetString<ReflectionClass>(input, Data);
        }

        public void SetID(int id)
        {
            ID = id;
            LoadedMe();
        }

        protected virtual void LoadedMe()
        {

        }

        public virtual GameObject CreateMe()
        {
            // these are empty, we want the inherited classes to overload these
            return default(GameObject);
        }
        public virtual GameObject SpawnMe()
        {
            // these are empty, we want the inherited classes to overload these
            return CreateMe();
        }
        public virtual void PurchaseMe()
        {
            // these are empty, we want the inherited classes to overload these
        }
        public virtual void UnlockMe()
        {
            // these are empty, we want the inherited classes to overload these
        }
        public virtual void LevelUpMe(int newLevel)
        {
            // these are empty, we want the inherited classes to overload these
        }

        public virtual void SetMe(GameObject ToSet)
        {
            // these are empty, we want the inherited classes to overload these
        }

    }

    public class ClassReflectionMonoBehaviour<ReflectionClass> : MonoBehaviour, IReflectionData<ReflectionClass>, IReflection, IReflectionClass where ReflectionClass : new()
    {
        public int ID;
        public void DefaultData(ReflectionClass Data)
        {
            ReflectionFunctions.DefaultData<ReflectionClass>(Data);
        }
        public void PreparePrefab(GameObject Prefab, GameObject[] resources, int index = 0)
        {
            ReflectionFunctions.PreparePrefab<ReflectionClass>(Prefab, resources, index);
        }
        public void SetPrefab(GameObject Prefab, ReflectionClass Data)
        {
            ReflectionFunctions.SetPrefab<ReflectionClass>(Prefab, Data);
            SetMe(Prefab);
        }
        public void SetPrefab(GameObject Prefab, ReflectionClass Data, string Variable)
        {
            ReflectionFunctions.SetPrefab<ReflectionClass>(Prefab, Data, Variable);
            SetMe(Prefab);
        }
        public void SetPrefabWithLocal(GameObject Prefab, ReflectionClass Data, string LocalName)
        {
            ReflectionFunctions.SetPrefabWithLocal<ReflectionClass>(Prefab, Data, LocalName);
            SetMe(Prefab);
        }
        public void GetDataFromPrefab(GameObject Prefab, ReflectionClass Data)
        {
            ReflectionFunctions.GetDataFromPrefab(Prefab, Data);
        }
        public void GetDataFromPrefab(GameObject Prefab, ReflectionClass Data, string Variable)
        {
            ReflectionFunctions.GetDataFromPrefab(Prefab, Data, Variable);
        }
        public void LoadData(XmlNode Node, ReflectionClass Data)
        {
            ReflectionFunctions.LoadData(Node, Data);
        }
        public void LoadObject(XmlNode Node, ReflectionClass Data, GameObject Obj = null)
        {
            ReflectionFunctions.LoadObject(Node, Data, Obj);
        }

        public void LoadObjectExt<T>(XmlNode Node, T Data) where T : new()
        {
            ReflectionFunctions.LoadObject<T>(Node, Data);
        }
        public string SetString(string input, ReflectionClass Data)
        {
            return ReflectionFunctions.SetString<ReflectionClass>(input, Data);
        }

        public void SetID(int id)
        {
            ID = id;
            LoadedMe();
        }

        protected virtual void LoadedMe()
        {

        }

        public virtual GameObject CreateMe()
        {
            // these are empty, we want the inherited classes to overload these
            return default(GameObject);
        }
        public virtual GameObject SpawnMe()
        {
            // these are empty, we want the inherited classes to overload these
            return CreateMe();
        }
        public virtual void PurchaseMe()
        {
            // these are empty, we want the inherited classes to overload these
        }
        public virtual void UnlockMe()
        {
            // these are empty, we want the inherited classes to overload these
        }
        public virtual void LevelUpMe(int newLevel)
        {
            // these are empty, we want the inherited classes to overload these
        }

        public virtual void SetMe(GameObject ToSet)
        {
            // these are empty, we want the inherited classes to overload these
        }
    }

    public class ClassReflectionGameObject<ReflectionClass> : IReflectionData<ReflectionClass>, IReflectionGameObject, IReflectionClass where ReflectionClass : new()
    {
        public int ID;
        GameObject DataGameObject = null;

        public void DefaultData(ReflectionClass Data)
        {
            ReflectionFunctions.DefaultData<ReflectionClass>(Data);
        }
        public void PreparePrefab(GameObject Prefab, GameObject[] resources, int index = 0)
        {
            ReflectionFunctions.PreparePrefab<ReflectionClass>(Prefab, resources, index);
        }
        public void SetPrefab(GameObject Prefab, ReflectionClass Data)
        {
            ReflectionFunctions.SetPrefab<ReflectionClass>(Prefab, Data);
            SetMe(Prefab);
        }
        public void SetPrefab(GameObject Prefab, ReflectionClass Data, string Variable)
        {
            ReflectionFunctions.SetPrefab<ReflectionClass>(Prefab, Data, Variable);
            SetMe(Prefab);
        }
        public void SetPrefabWithLocal(GameObject Prefab, ReflectionClass Data, string LocalName)
        {
            ReflectionFunctions.SetPrefabWithLocal<ReflectionClass>(Prefab, Data, LocalName);
            SetMe(Prefab);
        }
        public void GetDataFromPrefab(GameObject Prefab, ReflectionClass Data)
        {
            ReflectionFunctions.GetDataFromPrefab(Prefab, Data);
        }
        public void GetDataFromPrefab(GameObject Prefab, ReflectionClass Data, string Variable)
        {
            ReflectionFunctions.GetDataFromPrefab(Prefab, Data, Variable);
        }
        public void LoadData(XmlNode Node, ReflectionClass Data)
        {
            ReflectionFunctions.LoadData(Node, Data);
        }
        public void LoadObject(XmlNode Node, ReflectionClass Data, GameObject Obj = null)
        {
            ReflectionFunctions.LoadObject(Node, Data, (Obj == null) ? DataGameObject : Obj);
        }

        public void LoadObjectExt<T>(XmlNode Node, T Data) where T : new()
        {
            ReflectionFunctions.LoadObject<T>(Node, Data, DataGameObject);
        }
        public string SetString(string input, ReflectionClass Data)
        {
            return ReflectionFunctions.SetString<ReflectionClass>(input, Data);
        }

        public void CreateGameObject()
        {
            DataGameObject = new GameObject();
        }
        public void HideGameObject()
        {
            if (DataGameObject != null)
            {
                DataGameObject.SetActive(false);
                DataGameObject.hideFlags = HideFlags.HideInHierarchy;
            }
        }
        public void RevealGameObject()
        {
            if (DataGameObject != null)
                DataGameObject.hideFlags = HideFlags.None;
        }

        public void InstantiateGameObject(GameObject toInstantiate, Transform Parent = null, bool worldPositionStays = false)
        {
            DataGameObject = GameObject.Instantiate(toInstantiate);
            if(Parent != null)
                DataGameObject.transform.SetParent(Parent, worldPositionStays);
        }

        public void InstantiateGameObject(GameObject toInstantiate, Vector3 Position, Quaternion Rotation, Transform Parent = null, bool worldPositionStays = false)
        {
            DataGameObject = (GameObject)GameObject.Instantiate(toInstantiate, Position, Rotation);
            if (Parent != null)
                DataGameObject.transform.SetParent(Parent, worldPositionStays);
        }
        public void SetParent(Transform Parent, bool worldPositionStays)
        {
            if (Parent != null)
                DataGameObject.transform.SetParent(Parent, worldPositionStays);
        }
        public string GetName()
        {
            return (DataGameObject != null) ? DataGameObject.name : "DataGameObject Error";
        }

        public GameObject Instantiate()
        {
            GameObject obj = GameObject.Instantiate(DataGameObject);
            obj.SetActive(true);
            return obj;
        }

        public T GetComponent<T>()
        {
            if (DataGameObject != null)
                return DataGameObject.GetComponent<T>();
            return default(T);
        }
        public T GetComponentInChildren<T>()
        {
            if (DataGameObject != null)
                return DataGameObject.GetComponentInChildren<T>();
            return default(T);
        }

        public T[] GetComponents<T>()
        {
            if (DataGameObject != null)
                return DataGameObject.GetComponents<T>();
            return default(T[]);
        }
        public T[] GetComponentsInChildren<T>(bool inclusive = false)
        {
            if (DataGameObject != null)
                return DataGameObject.GetComponentsInChildren<T>(inclusive);
            return default(T[]);
        }

        public T AddComponent<T>() where T : UnityEngine.Component
        {
            if (DataGameObject != null)
                return DataGameObject.AddComponent<T>();
            return null;
        }

        public bool RemoveComponent<T>() where T : UnityEngine.Component
        {
            if (DataGameObject != null)
            {
                GameObject.Destroy(DataGameObject.GetComponent<T>());
                return true;
            }
            return false;
        }

        public void SetID(int id)
        {
            ID = id;
            LoadedMe();
        }

        protected virtual void LoadedMe()
        {

        }

        public virtual GameObject CreateMe()
        {
            // these are empty, we want the inherited classes to overload these
            return default(GameObject);
        }
        public virtual GameObject SpawnMe()
        {
            // these are empty, we want the inherited classes to overload these
            return CreateMe();
        }
        public virtual void PurchaseMe()
        {
            // these are empty, we want the inherited classes to overload these
        }
        public virtual void UnlockMe()
        {
            // these are empty, we want the inherited classes to overload these
        }
        public virtual void LevelUpMe(int newLevel)
        {
            // these are empty, we want the inherited classes to overload these
        }

        public virtual void SetMe(GameObject ToSet)
        {
            // these are empty, we want the inherited classes to overload these
        }
    }
}
