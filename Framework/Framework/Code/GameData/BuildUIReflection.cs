﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using LHS.Framework.Game;
using LHS.Framework.Tools;

namespace LHS.Framework.GameData
{
    public abstract class BuildUIReflectionFunctions
    {

        public static void ProcessVariable(GameObject variable, object value)
        {
            if (value == null)
                return;
            
            // find input fields
            InputField[] inputs = variable.GetComponentsInChildren<InputField>(true);
            foreach (InputField input in inputs)
            {
                input.text = value.ToString();
            }

            // find textboxes
            Text[] textboxes = variable.GetComponentsInChildren<Text>(true);
            foreach (Text text in textboxes)
            {
                // we have had problems int eh past here with textboxes
                // and their names, we need to keep an eye on this
                //if (text.gameObject.name != variable.name)
                text.text = value.ToString();
            }

            Image[] Images = variable.GetComponentsInChildren<Image>(true);
            foreach (Image img in Images)
            {
                img.sprite = (Sprite)value;
            }

        }

        public static void ProcessVariableLocal(GameObject variable, object value, string LocalName)
        {
            if (value == null)
                return;

            InputField[] inputs = variable.GetComponentsInChildren<InputField>(true);
            Text[] textboxes = variable.GetComponentsInChildren<Text>(true);

            //if(inputs.Length > 0 || textboxes.Length > 0)
            {
                string textValue = value.ToString();
                if (LocalisationReader.instance != null)
                {
                    string localtext;
                    if(LocalisationReader.instance.GetLocalisedString(LocalName + "_" + variable.name, out localtext))
                        textValue = localtext;
                }

                // find input fields
                foreach (InputField input in inputs)
                {
                    //input.text = value.ToString();
                    input.text = textValue;
                }

                // find textboxes
                foreach (Text text in textboxes)
                {
                    // we have had problems int eh past here with textboxes
                    // and their names, we need to keep an eye on this
                    //if (text.gameObject.name != variable.name)
                    //text.text = value.ToString();
                    text.text = textValue;
                }
            }

            Image[] Images = variable.GetComponentsInChildren<Image>(true);
            foreach (Image img in Images)
            {
                img.sprite = (Sprite)value;
            }

        }

        public static void ReadVariable<ReflectionClass>(GameObject variable, FieldInfo field, ReflectionClass Data) where ReflectionClass : new()
        {

            // find input fields
            InputField[] inputs = variable.GetComponentsInChildren<InputField>(true);
            foreach (InputField input in inputs)
            {
                ParseReflectionFunctions.ParseVariable(field, Data, input.text);
            }

        }

        public static GameObject ConstructTextResource(ref GameObject obj, GameObject res, string text, ref float yPos, float startPos, ObjectSpacing spacing, float xPos = 0)
        {
            GameObject constructed = GameObject.Instantiate(res);
            constructed.GetComponent<Text>().text = text;
            constructed.transform.SetParent(obj.transform, false);
            RectTransform pRect = constructed.GetComponent<RectTransform>();
            pRect.anchoredPosition = new Vector2(xPos, startPos - yPos);
            float textheight = LayoutUtility.GetPreferredHeight(pRect);
            if (textheight != 0)
                pRect.sizeDelta = new Vector2(pRect.sizeDelta.x, textheight);
            yPos += (pRect.sizeDelta.y + spacing.BetweenElements);

            return constructed;
        }

    }
}


