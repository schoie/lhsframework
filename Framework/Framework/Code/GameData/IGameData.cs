﻿
/*
    IGameData.cs -- This interface allows us to group together game data scripts and call functions on them based on context

    Name: Steven Ogden
    Date: 20/07/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.GameData
{

    public abstract class GameDataFunctions
    {
        static public void OnCreated(GameObject obj, string CID)
        {
            IGameData[] GameData = obj.GetComponents<IGameData>();
            foreach (IGameData data in GameData)
                data.OnCreated(CID);
        }
        static public void OnCreatedOffline(GameObject obj)
        {
            IGameData[] GameData = obj.GetComponents<IGameData>();
            foreach (IGameData data in GameData)
                data.OnCreatedOffline();
        }
        static public void OnLoaded(GameObject obj)
        {
            IGameData[] GameData = obj.GetComponents<IGameData>();
            foreach (IGameData data in GameData)
                data.OnLoaded();
        }

    }

    public interface IGameData
    {
        void OnCreated(string CID);
        void OnCreatedOffline();
        void OnLoaded();
    }

    public interface ISaveData
    {
        // These load/save function use the JSON method
        // if we do not like these we can allocate empty peramater versions
        bool OnLoad(string stringToLoadFrom);
        void OnSave(ref string stringToAppendTo);
    }
}
