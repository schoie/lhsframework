﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;


namespace LHS.Framework.GameData
{
    public abstract class ParseReflectionFunctions
    {

        //------------------------------------------------------------------------------------
        // MEMBER
        //------------------------------------------------------------------------------------
        public static void ParseVariable(MemberInfo member, System.Object obj, string _val)
        {
            if (member.MemberType == MemberTypes.Field)
                ParseVariable((FieldInfo)member, obj, _val);
            if (member.MemberType == MemberTypes.Property)
                ParseVariable((PropertyInfo)member, obj, _val);
        }
        public static void ParseVariables(MemberInfo member, string _val, string[] _variables, System.Object obj)
        {
            if (member.MemberType == MemberTypes.Field)
                ParseVariables((FieldInfo)member, _val, _variables, obj);
            if (member.MemberType == MemberTypes.Property)
                ParseVariables((PropertyInfo)member, _val, _variables, obj);
        }
        public static void DefaultVariable(MemberInfo member, System.Object obj)
        {
            if (member.MemberType == MemberTypes.Field)
                DefaultVariable((FieldInfo)member, obj);
            if (member.MemberType == MemberTypes.Property)
                DefaultVariable((PropertyInfo)member, obj);
        }
        //------------------------------------------------------------------------------------
        // FIELD
        //------------------------------------------------------------------------------------
        public static void ParseVariable(FieldInfo field, System.Object obj, string _val)
        {
            if (field.FieldType.IsEnum)
            {
                var enumVar = Enum.Parse(field.FieldType, _val);
                if (enumVar != null)
                    field.SetValue(obj, enumVar);
            }
            else if (field.FieldType.IsClass && field.FieldType.Name != "String")
            {
                switch (field.FieldType.Name)
                {
                    case "Sprite":

                        // consider checking resource manager here
                        // if no resource manager or no asset found then we 
                        // load from resources

                        //field.SetValue(obj, ResourceManager.instance.Load<Sprite>(_val));
                        field.SetValue(obj, ResourceManager.instance.Get<Sprite>(_val));

                        break;
                }
            }
            else
            {
                switch (field.FieldType.Name)
                {
                    case "Int16":
                        field.SetValue(obj, short.Parse(_val));
                        break;
                    case "UInt16":
                        field.SetValue(obj, ushort.Parse(_val));
                        break;
                    case "Int32":
                        field.SetValue(obj, int.Parse(_val));
                        break;
                    case "UInt32":
                        field.SetValue(obj, uint.Parse(_val));
                        break;
                    case "Int64":
                        field.SetValue(obj, long.Parse(_val));
                        break;
                    case "UInt64":
                        field.SetValue(obj, ulong.Parse(_val));
                        break;
                    case "Single":
                        field.SetValue(obj, float.Parse(_val));
                        break;
                    case "Double":
                        field.SetValue(obj, double.Parse(_val));
                        break;
                    case "Boolean":
                        field.SetValue(obj, bool.Parse(_val));
                        break;
                    case "String":
                        field.SetValue(obj, _val);
                        break;
                }
            }
        }

        // allows us to go 2 levels deep and set variables in a class
        // this matches teh serialising level I believe?? (Steven 01/06/16)
        public static void ParseVariables(FieldInfo field, string _val, string[] _variables, System.Object obj)
        {
            if (field.FieldType.IsEnum)
            {
                var enumVar = Enum.Parse(field.FieldType, _val);
                if (enumVar != null)
                    field.SetValue(obj, enumVar);
            }
            else if (field.FieldType.IsClass && field.FieldType.Name != "String")
            {
                // handle class here
                if (_variables != null)
                {
                    for (int i = 1; i < _variables.Length; ++i)
                    {
                        var classfield = field.FieldType.GetField(_variables[i]);
                        ParseVariable(classfield, field.GetValue(obj), _val);
                    }
                }
            }
            else
            {
                switch (field.FieldType.Name)
                {
                    case "Int16":
                        field.SetValue(obj, short.Parse(_val));
                        break;
                    case "UInt16":
                        field.SetValue(obj, ushort.Parse(_val));
                        break;
                    case "Int32":
                        field.SetValue(obj, int.Parse(_val));
                        break;
                    case "UInt32":
                        field.SetValue(obj, uint.Parse(_val));
                        break;
                    case "Int64":
                        field.SetValue(obj, long.Parse(_val));
                        break;
                    case "UInt64":
                        field.SetValue(obj, ulong.Parse(_val));
                        break;
                    case "Single":
                        field.SetValue(obj, float.Parse(_val));
                        break;
                    case "Double":
                        field.SetValue(obj, double.Parse(_val));
                        break;
                    case "Boolean":
                        field.SetValue(obj, bool.Parse(_val));
                        break;
                    case "String":
                        field.SetValue(obj, _val);
                        break;
                }
            }
        }

        public static void DefaultVariable(FieldInfo field, System.Object obj)
        {
            if (field.FieldType.IsEnum)
            {
                field.SetValue(obj, 0);
            }
            else
            {
                switch (field.FieldType.Name)
                {
                    case "Int16":
                    case "UInt16":
                    case "Int32":
                    case "UInt32":
                    case "Int64":
                    case "UInt64":
                        field.SetValue(obj, 0);
                        break;
                    case "Single":
                        field.SetValue(obj, 1.0f);
                        break;
                    case "Double":
                        field.SetValue(obj, 1.0);
                        break;
                    case "Boolean":
                        field.SetValue(obj, "False");
                        break;
                    case "String":
                        field.SetValue(obj, "text");
                        break;
                }
            }
        }
        //------------------------------------------------------------------------------------

        //------------------------------------------------------------------------------------
        // Property
        //------------------------------------------------------------------------------------
        public static void ParseVariable(PropertyInfo property, System.Object obj, string _val)
        {
            if (property.PropertyType.IsEnum)
            {
                var enumVar = Enum.Parse(property.PropertyType, _val);
                if (enumVar != null)
                    property.SetValue(obj, enumVar, null);
            }
            else if (property.PropertyType.IsClass && property.PropertyType.Name != "String")
            {
                switch (property.PropertyType.Name)
                {
                    case "Sprite":

                        // consider checking resource manager here
                        // if no resource manager or no asset found then we 
                        // load from resources

                        property.SetValue(obj, ResourceManager.instance.Get<Sprite>(_val), null);

                        break;
                }
            }
            else
            {
                switch (property.PropertyType.Name)
                {
                    case "Int16":
                        property.SetValue(obj, short.Parse(_val), null);
                        break;
                    case "UInt16":
                        property.SetValue(obj, ushort.Parse(_val), null);
                        break;
                    case "Int32":
                        property.SetValue(obj, int.Parse(_val), null);
                        break;
                    case "UInt32":
                        property.SetValue(obj, uint.Parse(_val), null);
                        break;
                    case "Int64":
                        property.SetValue(obj, long.Parse(_val), null);
                        break;
                    case "UInt64":
                        property.SetValue(obj, ulong.Parse(_val), null);
                        break;
                    case "Single":
                        property.SetValue(obj, float.Parse(_val), null);
                        break;
                    case "Double":
                        property.SetValue(obj, double.Parse(_val), null);
                        break;
                    case "Boolean":
                        property.SetValue(obj, bool.Parse(_val), null);
                        break;
                    case "String":
                        property.SetValue(obj, _val, null);
                        break;
                }
            }
        }

        // allows us to go 2 levels deep and set variables in a class
        // this matches teh serialising level I believe?? (Steven 01/06/16)
        public static void ParseVariables(PropertyInfo property, string _val, string[] _variables, System.Object obj)
        {
            if (property.PropertyType.IsEnum)
            {
                var enumVar = Enum.Parse(property.PropertyType, _val);
                if (enumVar != null)
                    property.SetValue(obj, enumVar, null);
            }
            else if (property.PropertyType.IsClass && property.PropertyType.Name != "String")
            {
                // handle class here
                if (_variables != null)
                {
                    for (int i = 1; i < _variables.Length; ++i)
                    {
                        var classfield = property.PropertyType.GetField(_variables[i]);
                        ParseVariable(classfield, property.GetValue(obj, null), _val);
                    }
                }
            }
            else
            {
                switch (property.PropertyType.Name)
                {
                    case "Int16":
                        property.SetValue(obj, short.Parse(_val), null);
                        break;
                    case "UInt16":
                        property.SetValue(obj, ushort.Parse(_val), null);
                        break;
                    case "Int32":
                        property.SetValue(obj, int.Parse(_val), null);
                        break;
                    case "UInt32":
                        property.SetValue(obj, uint.Parse(_val), null);
                        break;
                    case "Int64":
                        property.SetValue(obj, long.Parse(_val), null);
                        break;
                    case "UInt64":
                        property.SetValue(obj, ulong.Parse(_val), null);
                        break;
                    case "Single":
                        property.SetValue(obj, float.Parse(_val), null);
                        break;
                    case "Double":
                        property.SetValue(obj, double.Parse(_val), null);
                        break;
                    case "Boolean":
                        property.SetValue(obj, bool.Parse(_val), null);
                        break;
                    case "String":
                        property.SetValue(obj, _val, null);
                        break;
                }
            }
        }

        public static void DefaultVariable(PropertyInfo property, System.Object obj)
        {
            if (property.PropertyType.IsEnum)
            {
                property.SetValue(obj, 0, null);
            }
            else
            {
                switch (property.PropertyType.Name)
                {
                    case "Int16":
                    case "UInt16":
                    case "Int32":
                    case "UInt32":
                    case "Int64":
                    case "UInt64":
                        property.SetValue(obj, 0, null);
                        break;
                    case "Single":
                        property.SetValue(obj, 1.0f, null);
                        break;
                    case "Double":
                        property.SetValue(obj, 1.0, null);
                        break;
                    case "Boolean":
                        property.SetValue(obj, "False", null);
                        break;
                    case "String":
                        property.SetValue(obj, "text", null);
                        break;
                }
            }
        }
        //------------------------------------------------------------------------------------
        
    }
}
