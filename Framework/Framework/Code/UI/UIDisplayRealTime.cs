﻿/*
    UIDisplayRealTime.cs -- A generic script for displaying time/date inside a specified Text object

    Name: Steven Ogden
    Date: 04/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace LHS.Framework.UI
{
    public class UIDisplayRealTime : MonoBehaviour
    {

        public enum eFormats
        {
            DateAndTime = 0,
            Time,
            Date,
            NoFormat
        }

        public eFormats Format;
        private Text textbox;

        public void Awake()
        {
            textbox = this.GetComponent<Text>();
            if(textbox == null)
            {
                LHSDebug.LogError("UIDisplayRealTime - GameObject does not contain a Text Object");
                enabled = false;
                return;
            }
        }

        public void Update()
        {
            switch (Format) {
                case eFormats.DateAndTime:
                    textbox.text = System.DateTime.Now.ToString("d/M/yyyy HH:mm:ss tt");
                    break;
                case eFormats.Time:
                    textbox.text = System.DateTime.Now.ToString("HH:mm:ss tt");
                    break;
                case eFormats.Date:
                    textbox.text = System.DateTime.Now.ToString("d/M/yyyy");
                    break;
                case eFormats.NoFormat:
                default:
                    textbox.text = System.DateTime.Now.ToString();
                    break;
            }
        }

    }
}


// FORMAT INFORMATION
/*
String.Format("{0:y yy yyy yyyy}", dt);  // "8 08 008 2008"   year
String.Format("{0:M MM MMM MMMM}", dt);  // "3 03 Mar March"  month
String.Format("{0:d dd ddd dddd}", dt);  // "9 09 Sun Sunday" day
String.Format("{0:h hh H HH}",     dt);  // "4 04 16 16"      hour 12/24
String.Format("{0:m mm}",          dt);  // "5 05"            minute
String.Format("{0:s ss}",          dt);  // "7 07"            second
String.Format("{0:f ff fff ffff}", dt);  // "1 12 123 1230"   sec.fraction
String.Format("{0:F FF FFF FFFF}", dt);  // "1 12 123 123"    without zeroes
String.Format("{0:t tt}",          dt);  // "P PM"            A.M. or P.M.
String.Format("{0:z zz zzz}",      dt);  // "-6 -06 -06:00"   time zone

You can use also date separator / (slash) and time sepatator : (colon). These characters will be rewritten to characters defined in the current DateTimeForma­tInfo.DateSepa­rator and DateTimeForma­tInfo.TimeSepa­rator.

[C#]
// date separator in german culture is "." (so "/" changes to ".")
String.Format("{0:d/M/yyyy HH:mm:ss}", dt); // "9/3/2008 16:05:07" - english (en-US)
String.Format("{0:d/M/yyyy HH:mm:ss}", dt); // "9.3.2008 16:05:07" - german (de-DE)

Here are some examples of custom date and time formatting:

[C#]
// month/day numbers without/with leading zeroes
String.Format("{0:M/d/yyyy}", dt);            // "3/9/2008"
String.Format("{0:MM/dd/yyyy}", dt);          // "03/09/2008"

// day/month names
String.Format("{0:ddd, MMM d, yyyy}", dt);    // "Sun, Mar 9, 2008"
String.Format("{0:dddd, MMMM d, yyyy}", dt);  // "Sunday, March 9, 2008"

// two/four digit year
String.Format("{0:MM/dd/yy}", dt);            // "03/09/08"
String.Format("{0:MM/dd/yyyy}", dt);          // "03/09/2008"
    */
