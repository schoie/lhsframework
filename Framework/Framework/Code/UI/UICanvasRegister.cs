﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using LHS.Framework;

namespace LHS.Framework.UI
{

    public class UICanvasRegister : CRegisterBehaviour<Canvas, UICanvasManager>
    {
        public override void RegisterFunc(Canvas register)
        {
            UICanvasManager.instance.Register(register);
        }
        public override void DeRegisterFunc(Canvas register)
        {
            UICanvasManager.instance.DeRegister(register.name);
        }
    }

    public class UICanvasManager : CRegisterManager<Canvas, UICanvasManager>
    {

        // functions for use with canvases
        public Vector2 WorldToCanvasPosition(string canvas, Vector3 _worldPos)
        {
            return WorldToCanvasPosition(canvas, _worldPos, Camera.main);
        }

        public Vector2 WorldToCanvasPosition(string canvas, Vector3 _worldPos, Camera cam)
        {
            RectTransform CanvasRect = this.Get(canvas).GetComponent<RectTransform>();
            if (CanvasRect)
            {

                Vector2 ViewportPosition = cam.WorldToViewportPoint(_worldPos);
                return new Vector2(
                    ((ViewportPosition.x * CanvasRect.sizeDelta.x) - (CanvasRect.sizeDelta.x * 0.5f)),
                    ((ViewportPosition.y * CanvasRect.sizeDelta.y) - (CanvasRect.sizeDelta.y * 0.5f))
                );

            }
            return new Vector2();
        }

        public Vector2 NestedRectTransformToCanvasPosition(string canvas, RectTransform nested)
        {
            return NestedRectTransformToCanvasPosition(canvas, Camera.main, nested);
        }
        public Vector2 NestedRectTransformToCanvasPosition(string canvas, Camera cam, RectTransform nested)
        {
            Rect rect = RectTransformToScreenSpace(nested);
            Vector3 world = Camera.main.ScreenToWorldPoint(new Vector3(rect.x + (rect.width * 0.5f), rect.y + (rect.height * 0.5f), 0));
            Vector2 pos = WorldToCanvasPosition(canvas, world);
            pos.y *= -1;
            return pos;
        }
        
        public static Rect RectTransformToScreenSpace(RectTransform transform)
        {
            Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
            Rect rect = new Rect(transform.position.x, Screen.height - transform.position.y, size.x, size.y);
            rect.x -= (transform.pivot.x * size.x);
            rect.y -= ((1.0f - transform.pivot.y) * size.y);
            return rect;
        }

    }


}
