﻿/*
    UIPopUp.cs

    Name: Steven Ogden
    Date: Unknown

    Provided by Steven Ogden copyright free as part of the GNU license.
*/

using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.Advertisements;

namespace LHS
{
    namespace Framework
    {
        namespace UI
        {

            public class UIPopUp : MonoBehaviour
            {

                public enum eType
                {
                    timeout,
                    wait
                };

                public eType PopUpType = eType.timeout;
                public float ScaleTime = 0;
                public AnimationCurve ScaleCurve;
                public float Timeout = 0;
                public float Delay = 0;
                public GameObject ToDisable = null;
                public UIPopUp[] LinkedTriggerWaitEvents;
                public UnityEvent OnRevealed;
                public UnityEvent OnCompleted;
                public UnityEvent OnReward;

                private float timer;

                private string adZoneId = null;

                public enum ePhases
                {
                    delay,
                    scale,
                    timeout,
                    scaleDown,
                    finished,
                    wait
                };

                private ePhases phase;

                public void OnEnable()
                {
                    timer = 0;
                    phase = (Delay == 0) ? ePhases.scale : ePhases.delay;
                    this.transform.localScale = new Vector3(0f, 0f);
                    if (ToDisable == null)
                        ToDisable = this.gameObject;

                    if (phase != ePhases.delay)
                    {
                       // SoundManager.Instance.Play("Slide");
                    }
                }

                public void Update()
                {
                    float evaluateTimer;
                    float percent;
                    switch (phase)
                    {
                        case ePhases.delay:
                            timer += Time.deltaTime;
                            if (timer >= Delay)
                            {
                                timer = 0;
                                phase = ePhases.scale;
                                //SoundManager.Instance.Play("Slide");
                            }
                            break;
                        case ePhases.scale:

                            timer += Time.deltaTime;
                            if (timer >= ScaleTime)
                            {
                                timer = 0;
                                evaluateTimer = 1;
                                HandleType();
                                OnRevealed.Invoke();
                            }
                            else
                            {
                                evaluateTimer = timer / ScaleTime;
                            }

                            percent = ScaleCurve.Evaluate(evaluateTimer);
                            this.transform.localScale = new Vector3(percent, percent);

                            break;
                        case ePhases.timeout:

                            timer += Time.deltaTime;
                            if (timer >= Timeout)
                            {
                                timer = 0;
                                phase = ePhases.scaleDown;
                                //SoundManager.Instance.Play("Slide");
                                HandleLinked();
                            }

                            break;
                        case ePhases.scaleDown:

                            timer += Time.deltaTime;
                            if (timer >= ScaleTime)
                            {
                                timer = 0;
                                evaluateTimer = 0;
                                phase = ePhases.finished;
                            }
                            else
                            {
                                evaluateTimer = 1 - (timer / ScaleTime);
                            }

                            percent = ScaleCurve.Evaluate(evaluateTimer);
                            this.transform.localScale = new Vector3(percent, percent);

                            break;
                        case ePhases.finished:

                            OnCompleted.Invoke();
                            ToDisable.SetActive(false);
                            break;
                    }
                }

                private void HandleType()
                {
                    switch (PopUpType)
                    {
                        case eType.timeout:
                            phase = ePhases.timeout;
                            break;
                        case eType.wait:
                            phase = ePhases.wait;
                            break;
                    }
                }

                public void TriggerWaitEndEvent()
                {
                    if (PopUpType == eType.wait && phase == ePhases.wait)
                    {
                        timer = 0;
                        phase = ePhases.scaleDown;
                        //SoundManager.Instance.Play("Slide");
                        HandleLinked();
                    }
                }

                public void TriggerPopUpEvent()
                {
                    this.gameObject.SetActive(true);
                }

                public void HandleLinked()
                {
                    for (int i = 0; i < LinkedTriggerWaitEvents.Length; ++i)
                    {
                        LinkedTriggerWaitEvents[i].TriggerWaitEndEvent();
                    }
                }

                public bool isTransitioningIn()
                {
                    return phase == ePhases.scale;
                }
                public bool isTransitioningOut()
                {
                    return phase == ePhases.scaleDown;
                }
                public bool isTransitioning()
                {
                    return isTransitioningIn() || isTransitioningOut();
                }
                public bool isWaiting()
                {
                    return phase == ePhases.wait;
                }
                public bool isTimeingOut()
                {
                    return phase == ePhases.timeout;
                }

                public void SetDelay(float _delay)
                {
                    Delay = _delay;
                    if(phase == ePhases.scale)
                        phase = (Delay == 0) ? ePhases.scale : ePhases.delay;
                }
                
            }

        }
    }
}
