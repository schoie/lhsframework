﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace LHS.Framework.UI
{
    public class UINotification : MonoBehaviour
    {

        public enum eButtonPosition
        {
            TopLeft,
            TopRight,
            TopMiddle,
            BottomLeft,
            BottomMiddle,
            BottomRight
        }


        public eButtonPosition ButtonPos;
        public GameObject UIPrefab;
        public RectTransform UIToNotify;
        public Vector2 Offset;

        private GameObject InstantiatedUI = null;
        private int amount = 0;

        public void Awake()
        {
            if (UIPrefab == null)
                LHSDebug.LogWarning("UINotification - Requires a Prefab to instantiate the notification artwork from!! - Object Name: " + gameObject.name);

            if (UIToNotify == null)
                LHSDebug.LogWarning("UINotification - Requires a UIToNotify (Transform, in this case Rect) to attach the Notification too!! - Object Name: " + gameObject.name);
        }

        public void IncrementNotifications(int _amount = 1)
        {

            if(InstantiatedUI == null)
            {
                // Create
                InstantiatedUI = GameObject.Instantiate(UIPrefab);

                // position UI
                Position(InstantiatedUI.GetComponent<RectTransform>());

                // AttachUI
                Attach();

            }

            // Set number
            amount += _amount;
            SetAmount(InstantiatedUI.GetComponentInChildren<Text>());
        }

        public void ProcessNotificationInteraction()
        {
            // destroy UI
            DestroyObject(InstantiatedUI);
            InstantiatedUI = null;

            // reset amount
            amount = 0;
        }


        private void Attach()
        {
            if(InstantiatedUI != null)
                InstantiatedUI.transform.SetParent(UIToNotify.transform, false);
        }

        private void Position(RectTransform rect)
        {
            if (rect == null)
                return;

            Vector2 size = UIToNotify.sizeDelta * 0.5f;
            Vector2 pos = Offset;

            switch (ButtonPos)
            {
                case eButtonPosition.BottomLeft:
                    pos.x += -size.x;
                    pos.y += -size.y;
                    break;
                case eButtonPosition.BottomMiddle:
                    pos.y += -size.y;
                    break;
                case eButtonPosition.BottomRight:
                    pos.x += size.x;
                    pos.y += -size.y;
                    break;
                case eButtonPosition.TopLeft:
                    pos.x += -size.x;
                    pos.y += size.y;
                    break;
                case eButtonPosition.TopMiddle:
                    pos.y += size.y;
                    break;
                case eButtonPosition.TopRight:
                    pos.x += size.x;
                    pos.y += size.y;
                    break;
            }

            rect.anchoredPosition = pos;
        }

        private void SetAmount(Text text)
        {
            if(text != null)
                text.text = amount.ToString();
        }

    }
}
