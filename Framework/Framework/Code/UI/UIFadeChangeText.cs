﻿/*
    UIFadeChangeText.cs -- A generic script for fading down and changing text and fading back up

    Name: Steven Ogden
    Date: 19/08/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace LHS.Framework.UI
{
    public class UIFadeChangeText : MonoBehaviour
    {
        
        public float FadeTime;
        public float PauseTime;
        public AnimationCurve FadeCurve;
        public bool AllowInterrupt = false;
        
        private bool isFading = false;
        private bool isFadingDown = false;
        Coroutine FadeCo = null;
        private float textAlpha;

        private Text TextBox;
        private string text;

        public void Awake()
        {
            TextBox = this.GetComponent<Text>();
            if (TextBox == null)
            {
                LHSDebug.LogError("UIFadeChangeText - This object does not have a Text Component");
                enabled = false;
                return;
            }

            textAlpha = TextBox.color.a;

            ApplyEffect(1);
        }

        public void ProcessFade(string newText)
        {
            StartFadeChangeCo(newText);
        }

        public bool IsFading()
        {
            return isFading;
        }

        private void StartFadeChangeCo(string _text)
        {
            if (isFading)
            {
                if (AllowInterrupt)
                {
                    text = _text;
                    if (!isFadingDown)
                    {
                        if (FadeCo != null)
                            StopCoroutine(FadeCo);
                        
                        FadeCo = StartCoroutine(FadeChangeCoroutine());
                    }
                }
            }
            else
            {
                text = _text;
                FadeCo = StartCoroutine(FadeChangeCoroutine());
            }
            
        }

        IEnumerator FadeChangeCoroutine()
        {
            isFading = true;
            isFadingDown = true;

            float timer = 0;
            float evaluateTimer;
            float percent;
            bool complete = false;

            // pulse out time
            while (!complete)
            {
                timer += Time.deltaTime;
                if (timer >= FadeTime)
                {
                    timer = (timer - FadeTime);
                    complete = true;
                    evaluateTimer = 1;
                }
                else
                {
                    evaluateTimer = timer / FadeTime;
                }
                percent = 1 - FadeCurve.Evaluate(evaluateTimer);

                // apply effect here
                ApplyEffect(percent);

                yield return null;
            }
            
            yield return new WaitForSeconds(PauseTime);
            isFadingDown = false;
            TextBox.text = text;

            complete = false;

            // pulse in time
            while (!complete)
            {
                timer += Time.deltaTime;
                if (timer >= FadeTime)
                {
                    timer = (timer - FadeTime);
                    complete = true;
                    evaluateTimer = 1;
                }
                else
                {
                    evaluateTimer = timer / FadeTime;
                }
                percent = FadeCurve.Evaluate(evaluateTimer);

                // apply effect here
                ApplyEffect(percent);

                yield return null;
            }

            isFading = false;
        }

        private void ApplyEffect(float percent)
        {
            Color newCol = TextBox.color;
            newCol.a = textAlpha * percent;
            TextBox.color = newCol;
        }

    }
}
