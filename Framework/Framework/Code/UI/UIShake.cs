﻿/*
    UIShake.cs -- A generic script for shaking any UI unity object

    Name: Steven Ogden
    Date: 19/08/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace LHS.Framework.UI
{

    public interface IShake
    {
        void ProcessShake();
        void ProcessShakeOnAll();
    }

    public class UIShake : MonoBehaviour, IShake
    {

        public enum eDirection
        {
            Horizontal,
            Vertical
        }

        public eDirection Direction;
        public float ShakeSize;
        public float ShakeTime;
        public float ShakeSpeed = 1;
        public AnimationCurve ShakeCurve;
        public AnimationCurve ShakeAmount;

        RectTransform pRect;
        Vector3 position;
        bool isShaking = false;
        Coroutine ShakeCo = null;

        public void Awake()
        {
            pRect = this.GetComponent<RectTransform>();
            if (pRect == null)
            {
                LHSDebug.LogError("UIShake must be placed on a GameObejct with a RectTransform!");
                this.enabled = false;
                return;
            }

            position = pRect.anchoredPosition;
        }

        public void ProcessShake()
        {
            if (this.gameObject.activeSelf && this.gameObject.activeInHierarchy)
                StartShakeCo();
        }

        public void ProcessShakeOnAll()
        {
            ProcessShake();
            IShake[] shakes = this.GetComponentsInChildren<IShake>();
            foreach (IShake shake in shakes)
            {
                shake.ProcessShake();
            }
        }

        private void StartShakeCo()
        {
            if (!isShaking)
            {
                if (ShakeCo != null)
                    StopCoroutine(ShakeCo);
                ShakeCo = StartCoroutine(ShakeCoroutine());
            }
        }

        IEnumerator ShakeCoroutine()
        {
            isShaking = true;

            float timer = 0;
            float evaluateTimer;
            float percent;
            float heightPercent;
            bool complete = false;

            // pulse out time
            while (!complete)
            {
                timer += Time.deltaTime;
                if (timer >= ShakeTime)
                {
                    timer = ShakeTime;
                    complete = true;
                    evaluateTimer = 1;
                }
                else
                {
                    evaluateTimer = timer / ShakeTime;
                }

                percent = ShakeCurve.Evaluate(ShakeSpeed*timer);
                heightPercent = ShakeAmount.Evaluate(evaluateTimer);

                // apply effect here
                ApplyEffect(percent, heightPercent);

                yield return null;
            }
            
            isShaking = false;
        }
        

        private void ApplyEffect(float percent, float heightPercent)
        {
            float movementAmount = (percent * ShakeSize) * heightPercent;

            switch (Direction)
            {
                case eDirection.Horizontal:
                    HandleEffectHorizontal(movementAmount);
                    break;
                case eDirection.Vertical:
                    HandleEffectVertical(movementAmount);
                    break;
            }
        }

        protected void HandleEffectHorizontal(float amount)
        {
            pRect.anchoredPosition = new Vector2(position.x + amount, position.y);
        }
        protected void HandleEffectVertical(float amount)
        {
            pRect.anchoredPosition = new Vector2(position.x, position.y + amount);
        }
    }
    
}

