﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace LHS.Framework.UI
{
    public class MultiImageAnimator : MultiAnimator
    {

        Image image;

        protected override bool InitialiseGraphic()
        {
            image = this.GetComponent<Image>();
            if (image == null)
            {
                LHSDebug.LogError("Please attach a Image!");
                this.enabled = false;
                return false;
            }
            return true;
        }

        protected override void SetSprite(Sprite spr)
        {
            image.sprite = spr;
        }

    }
}
