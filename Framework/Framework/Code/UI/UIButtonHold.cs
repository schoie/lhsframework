﻿/*
    UIButtonHold.cs -- Attach to a gameobject with a button. You can now hold to multi-click

    Name: Timothy Johnson
    Date: 13/07/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace LHS.Framework.UI
{
    public class UIButtonHold : MonoBehaviour
    {
        Button button;
        EventTrigger eventTrigger;

        // Use this for initialization
        void Start()
        {
            button = GetComponent<Button>();
            if (button == null)
            {
                LHSDebug.LogError("A button is required on the gameobject to handle this script");
            }

            eventTrigger = gameObject.AddComponent<EventTrigger>();

            EventTrigger.Entry onDown = new EventTrigger.Entry();
            onDown.eventID = EventTriggerType.PointerDown;
            onDown.callback.AddListener((data) => { OnDown((PointerEventData)data); });
            eventTrigger.triggers.Add(onDown);

            EventTrigger.Entry onUp = new EventTrigger.Entry();
            onUp.eventID = EventTriggerType.PointerUp;
            onUp.callback.AddListener((data) => { OnUp((PointerEventData)data); });
            eventTrigger.triggers.Add(onUp);
        }

        [Header("How long before first click")]
        public float downStart = 0.2f;
        [Header("Gap between clicks")]
        public float clickGap = 0.1f;
        [Header("Gap between clicks")]
        public bool hasSpeedUp = false;
        [Header("How many clicks before it should speed up")]
        public int speedUpFrom = 10;
        public float speedUpFactor = 0.25f;
        float downTime = 0.0f;
        bool down = false;

        int clickCombo = 0;
        // Update is called once per frame
        void Update()
        {
            if (down == true)
            {
                downTime += Time.deltaTime;
                if (downTime > (downStart + clickGap))
                {
                    if (hasSpeedUp && (clickCombo >= speedUpFrom))
                    {
                        downTime -= clickGap * speedUpFactor;
                    }
                    else
                    {
                        downTime -= clickGap;
                    }
                    
                    button.onClick.Invoke();
                    clickCombo++;
                }
            }
        }

        public void OnDown(PointerEventData data)
        {
            downTime = 0f;
            down = true;
            clickCombo = 0;
        }

        public void OnUp(PointerEventData data)
        {
            down = false;
        }
    }
}
