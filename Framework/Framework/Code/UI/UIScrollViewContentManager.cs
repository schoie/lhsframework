﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace LHS.Framework.UI
{

    [Serializable]
    public enum UIScrollViewContentOrderType
    {
        None,
        NewestFirst,
        OldestFirst
    }

    [Serializable]
    public class UIScrollViewContentSpacing
    {
        public float YRunningPosition { get; set; }
        public float HeaderSpace;
        public float ItemSpacing;
        public UIScrollViewContentOrderType OrderType;
    }

    public class UIScrollViewContentManager
    {
        
        private UIScrollViewContentSpacing Spacing = null;
        private GameObject Content= null;
        private RectTransform ContentRect = null;

        private List<GameObject> AddedContent;

        public UIScrollViewContentManager()
        {
            AddedContent = new List<GameObject>();
        }

        public void PrepareContentManager(GameObject content, UIScrollViewContentSpacing spacing)
        {
            if(content == null)
            {
                LHSDebug.LogError("UIScrollViewContentManager - PrepareContentManager: content is null");
            }
            if (spacing == null)
            {
                LHSDebug.LogError("UIScrollViewContentManager - PrepareContentManager: content is spacing");
            }

            if (content.name != "Content")
            {
                LHSDebug.LogErrorFormat("UIScrollViewContentManager - PrepareContentManager: not attached to Content in Scroll view! Attached to: {0}", content.name);
            }

            ContentRect = content.GetComponent<RectTransform>();
            if(ContentRect == null)
            {
                LHSDebug.LogError("UIScrollViewContentManager - PrepareContentManager: content does not have a RectTransform");
            }

            Content = content;
            Spacing = spacing;

            Spacing.YRunningPosition = -Spacing.HeaderSpace;

        }

        public void AddToContent(GameObject obj)
        {
            RectTransform pRect = obj.GetComponent<RectTransform>();
            if (pRect != null)
            {
                obj.transform.SetParent(Content.transform, false);
                pRect.anchoredPosition = new Vector2(pRect.anchoredPosition.x, Spacing.YRunningPosition);
                Spacing.YRunningPosition -= (Spacing.ItemSpacing + pRect.sizeDelta.y);
                if(Mathf.Abs(Spacing.YRunningPosition) > ContentRect.sizeDelta.y)
                {
                    ContentRect.sizeDelta = new Vector2(ContentRect.sizeDelta.x, Mathf.Abs(Spacing.YRunningPosition));
                }
                AddedContent.Add(obj);
            }
            reorderList();
        }

        private void reorderList()
        {
            switch (Spacing.OrderType)
            {
                case UIScrollViewContentOrderType.NewestFirst:

                    Spacing.YRunningPosition = -Spacing.HeaderSpace;
                    for (int i = (AddedContent.Count-1); i >= 0; --i)
                    {
                        GameObject obj = AddedContent[i];
                        RectTransform pRect = obj.GetComponent<RectTransform>();
                        if (pRect != null)
                        {
                            pRect.anchoredPosition = new Vector2(pRect.anchoredPosition.x, Spacing.YRunningPosition);
                            Spacing.YRunningPosition -= (Spacing.ItemSpacing + pRect.sizeDelta.y);
                        }

                    }

                    break;
            }


        }

    }
}
