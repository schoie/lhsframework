﻿/*
    UIMaterialScroll.cs -- A generic script for positioning a group og Type T UIBehaviour's (Unity Ui Objects) in all directions with pivot settings

    Name: Steven Ogden
    Date: 14/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace LHS.Framework.UI
{
    
    public enum eDirection
    {
        Forward = 1,
        Backward = -1,
        None
    }

    public interface IScrollUI
    {
        void ProcessScroll();
    }

    [Serializable]
    public class UIMaterialScroll<T> : MonoBehaviour, IScrollUI where T : Graphic
    {

        public eDirection Verticle = eDirection.None;
        public eDirection Horizontal = eDirection.None;
        public float Speed = 1F;
        public float offsetX = 0;
        public float offsetY = 0;
        private T UIItem;
        public CanvasRenderer rend;

        public void Awake()
        {
            UIItem = this.GetComponent<T>();
            if(UIItem == null)
            {
                LHSDebug.LogErrorFormat("UIMaterialScroll<{0}> - GameObject does not contain a {0}", typeof(T).Name);
                enabled = false;
                return;
            }

            if(UIItem.material == null)
            {
                LHSDebug.LogErrorFormat("UIMaterialScroll<{0}> - Material in {0} is NULL", typeof(T).Name);
                enabled = false;
                return;
            }

            
            rend = GetComponent<CanvasRenderer>();
        }

        public void Update()
        {
            ProcessScroll();
        }
        //YOU ARE SHIT!@@@!@!@!#$!@#$!@#!@ - If you seen this line Steven is SHIT!!! Written by steven 09/06/2016
        public void ProcessScroll() {
            //offsetX += (Speed * (int)Horizontal);
            //offsetY += (Speed * (int)Verticle);
            offsetX = Time.time * (Speed * (int)Horizontal);
            offsetY = Time.time * (Speed * (int)Verticle);
            /* UIItem.material.SetTextureOffset("_MainTex", new Vector2(offsetX, offsetY));
             //UIItem.
             UIItem.materialForRendering.SetTextureOffset("_MainTex", new Vector2(offsetX, offsetY));
             for (int i = 0; i < rend.materialCount; ++i)
             {
                 rend.GetMaterial(i).SetTextureOffset("_MainTex", new Vector2(offsetX, offsetY));
             }*/

            UIItem.materialForRendering.SetTextureOffset("_MainTex", new Vector2(offsetX, offsetY));
            UIItem.materialForRendering.mainTextureOffset = new Vector2(offsetX, offsetY);
            UIItem.materialForRendering.SetTextureScale("_MainTex", new Vector2(offsetX, offsetY));
            UIItem.material.SetTextureOffset("_MainTex", new Vector2(offsetX, offsetY));
            UIItem.defaultMaterial.SetTextureOffset("_MainTex", new Vector2(offsetX, offsetY));
            UIItem.SetMaterialDirty();


            // this.GetComponent<Renderer>().sharedMaterial.SetTextureOffset("_MainTex", new Vector2(offsetX, offsetY));
        }
        
        public void SetDirection(eDirection _horizontal, eDirection _verticle, bool reset = true)
        {
            Horizontal = _horizontal;
            Verticle = _verticle;

            if (reset)
            {
                offsetX = 0;
                offsetY = 0;
            }
        }

    }


    public class UIImageScroll : UIMaterialScroll<Image>
    {
        // UISpacer does all the work
        // this class jsut defines the Button relationship
    }
}
