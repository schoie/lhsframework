﻿/*
    UIFader.cs -- A simple script for fading a single image

    Name: Steven Ogden
    Date: 05/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace LHS.Framework.UI
{
    public class UIFader : MonoBehaviour
    {
        public Image ToFade;
        public float TimeToFade;
        public AnimationCurve FadeCurve;
        public bool DisableParent = false;

        private float timer = 0;

        public void Awake()
        {
            if (ToFade == null)
            {
                LHSDebug.LogError("UIFader - ToFade is NULL");
                return;
            }

        }

        public void Update()
        {
            float evaluateTimer = 0;
            timer += Time.deltaTime;
            if (timer >= TimeToFade)
            {
                timer = 0;
                evaluateTimer = 1;
                enabled = false;
                if (DisableParent)
                    this.transform.parent.gameObject.SetActive(false);
            }
            else
            {
                evaluateTimer = timer / TimeToFade;
            }

            float percent = 1f - FadeCurve.Evaluate(evaluateTimer);
            ToFade.color = new Color(ToFade.color.r, ToFade.color.g, ToFade.color.b, ToFade.color.a * percent);
        }

    }
}

