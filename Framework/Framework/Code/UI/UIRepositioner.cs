﻿/*
    UIRepositioner.cs -- A generic script that will repositon a GameObejct by some vector

    Name: Steven Ogden
    Date: 04/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace LHS.Framework.UI
{
    public class UIRepositioner : MonoBehaviour
    {
        public GameObject ToPosition;
        public Vector2 OffsetPosition; 

        public void Awake()
        {
            if(ToPosition == null)
            {
                LHSDebug.LogError("UIRepositioner - ToPosition is NULL");
                return;
            }

            RectTransform pRect = ToPosition.GetComponent<RectTransform>();
            if (pRect == null)
            {
                LHSDebug.LogError("UIRepositioner - ToPosition has no RectTransform");
                return;
            }

            pRect.anchoredPosition = new Vector2(pRect.anchoredPosition.x + OffsetPosition.x, pRect.anchoredPosition.y + OffsetPosition.y);
        }

    }
}
