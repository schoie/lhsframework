﻿/*
    UIButtonSpriteSwap.cs -- A generic script that will allow us to have more control handeling button presses with sprite swapping

    Name: Steven Ogden
    Date: 29/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;

namespace LHS.Framework.UI
{
    public class UIButtonSpriteSwap : MonoBehaviour
    {
        
        public Sprite TargetSprite;
        public bool ApplyHandleToListener = true;
        
        private Sprite lastTargetSprite = null;
        private Button button;
        private Image image;
        private bool swapped = false;

        public void Start()
        {
            LHSDebug.Log("UIButtonSpriteSwap - Start");
            button = this.GetComponent<Button>();
            if(button == null)
            {
                LHSDebug.LogError("UIButtonSpriteSwap - Please ensure that we attach this script to a Button");
                enabled = false;
                return;
            }

            image = this.GetComponent<Image>();
            if (image == null)
            {
                LHSDebug.LogError("UIButtonSpriteSwap - A Button should have an Image?");
                enabled = false;
                return;
            }

            if (ApplyHandleToListener)
            {
                button.onClick.RemoveAllListeners();
                button.onClick.AddListener(this.HandleSwap);
            }

        }

        public void HandleSwap()
        {
            if (swapped)
            {
                image.sprite = lastTargetSprite;
                swapped = false;
            }
            else
            {
                lastTargetSprite = image.sprite;
                image.sprite = TargetSprite;
                swapped = true;
            }

        }

        public void SetDefault()
        {
            if (swapped)
            {
                image.sprite = lastTargetSprite;
                swapped = false;
            }
        }

        public void SetOption()
        {
            if (!swapped)
            {
                lastTargetSprite = image.sprite;
                image.sprite = TargetSprite;
                swapped = true;
            }
        }


    }
}
