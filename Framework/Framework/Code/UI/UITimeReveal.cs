﻿/*
    UITimeReveal.cs -- A generic script that time reveal a GameObject

    Name: Steven Ogden
    Date: 23/03/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.UI
{
    class UITimeReveal : MonoBehaviour
    {
        public GameObject ObjectToReveal;
        public float time;

        public void Awake()
        {
            if (ObjectToReveal == null)
            {
                LHSDebug.LogErrorFormat("GameObject: {0} - UITimeReveal - ObjectToReveal cannot be null", gameObject.name);
                enabled = false;
            }
            else if(time <= 0)
            {
                LHSDebug.LogErrorFormat("GameObject: {0} - UITimeReveal - Time ({1}) cannot be <= 0", gameObject.name, time);
                enabled = false;
            }
            else
            {
                ObjectToReveal.SetActive(false);
            }
        }

        public void Update()
        {
            if((time -= Time.deltaTime) <= 0)
            {
                ObjectToReveal.SetActive(true);
                enabled = false;
            }
        }

    }
}
