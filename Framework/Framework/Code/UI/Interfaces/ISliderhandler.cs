﻿/*
    ISliderhandler.cs -- A interface to allow the game to manage multiple slider updating types through a single interface

    Name: Steven Ogden
    Date: 28/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;

namespace LHS.Framework.UI
{
    public interface ISliderhandler
    {

        void UpdateSliders(Slider sliderChanged);
        void Reset();
        void SetInteractable(bool set);
        List<Slider> GetSliders();

    }

}
