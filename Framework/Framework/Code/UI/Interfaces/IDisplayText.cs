﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Events;

namespace LHS.Framework.UI
{
    public interface IDisplayText
    {

        void SetString(string _text);
        void SetStringWithCallback(string textT, UnityAction finishCallback);
        void Interrupt();
        bool IsRevealing();

    }
}
