﻿/*
    UIScaleOverTime.cs -- A generic script that will Scale a GameObejct

    Name: Steven Ogden
    Date: 23/03/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.UI
{
    class UIScaleOverTime : MonoBehaviour
    {

        public GameObject ToBeScaled;
        public Vector3 Scale;
        public float TimeToScale;
        public AnimationCurve ScaleCurve;

        private float timer = 0;
        private Vector3 scaleDif;
        private Vector3 scaleBase;
        private RectTransform Rect;

        public void Awake()
        {
            if (ToBeScaled == null)
            {
                LHSDebug.LogErrorFormat("GameObject: {0} - UIScaleOverTime - ToBeScaled cannot be null", gameObject.name);
                enabled = false;
            }
            else if (TimeToScale <= 0)
            {
                LHSDebug.LogErrorFormat("GameObject: {0} - UIScaleOverTime - Time ({1}) cannot be <= 0", gameObject.name, TimeToScale);
                enabled = false;
            }
            else
            {
                Rect = this.GetComponent<RectTransform>();
                if (Rect == null)
                {
                    LHSDebug.LogErrorFormat("GameObject: {0} - Has no RectTransform", gameObject.name);
                    enabled = false;
                    return;
                }
                scaleBase = new Vector3();
                scaleBase = Rect.transform.localScale;
                scaleDif = new Vector3();
                scaleDif = Scale - scaleBase;
            }

        }

        public void Update()
        {

            float evaluateTimer = 0;
            float percent;

            timer += Time.deltaTime;
            if (timer >= TimeToScale)
            {
                evaluateTimer = 1;
                enabled = false;
            }
            else
            {
                evaluateTimer = timer / TimeToScale;
            }

            percent = ScaleCurve.Evaluate(evaluateTimer);
            Rect.transform.localScale = scaleBase + (scaleDif * percent);

        }

    }
}
