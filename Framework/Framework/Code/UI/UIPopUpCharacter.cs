﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using LHS.Framework;
using UnityEngine.Events;
using LHS.Framework.Game;

namespace LHS.Framework.UI
{
    //public class UIPopUpCharacter : CSingletonMonoBehaviour<UIPopUpCharacter>
    public class UIPopUpCharacter : MonoBehaviour
    {
        //public GameObject bac
        public CanvasGroup characterCanvasGroup;
        public CanvasGroup textCanvasGroup;
        public Text messageText;
        public bool dontDoFade = false;

        [Space(10)]
        [Header("This object must have a type IDisplayText component")]
        [Header("It will work in sync with the messageText")]
        public GameObject TextOptionObject = null;
        [Space(10)]

        [Header("We support buttons now")]
        public bool ResizeForButton = false;
        public float TextWidth;
        public float TextWidthWithButton;

        bool playingForward = false;
        bool playingFade = false;
        bool fadingHelper = false;
        bool displayingHelper = false;

        const float maxFadeDuration = 1f;
        float halfFadeDuration;
        float currentFadeTime = 0f;

        private IDisplayText styledText = null;
        private List<CharacterPopUpManager.CharacterMultiData> MessageList;

        private RectTransform messageTextRect = null;

        bool buttonActionActive = false;
        bool buttonMoveActive = false;

       // public override void OnAwake()
        public void Awake()
        {
            
            halfFadeDuration = maxFadeDuration / 2f;

            Button[] buttons = this.GetComponentsInChildren<Button>(true);
            foreach (Button but in buttons)
            {
                switch (but.name)
                {
                    case "CloseButton":
                        but.onClick.RemoveAllListeners();
                        //but.onClick.AddListener(delegate { SoundManager.instance.Play("BUTTON_BACK"); });
                        break;
                }
            }

            if (TextOptionObject != null)
                styledText = TextOptionObject.GetComponent<IDisplayText>();

            if(messageText != null)
            {
                messageTextRect = messageText.GetComponent<RectTransform>();
                if(messageTextRect != null)
                {
                    TextWidth = messageTextRect.sizeDelta.x;
                }
            }
        }

        void Update()
        {
            if (playingFade == true)
            {
                currentFadeTime -= Time.deltaTime;

                if (playingForward == true)
                {
                    // are we fading the character or the speech bubble?
                    if (fadingHelper == true)
                    {
                        float alpha = ((halfFadeDuration - currentFadeTime) / halfFadeDuration);
                        if (dontDoFade == false)
                            characterCanvasGroup.alpha = alpha;

                        if (currentFadeTime < 0f)
                        {
                            fadingHelper = false;
                            displayingHelper = true;
                            ResetFadeTime();
                        }
                    }
                    else
                    {
                        float alpha = ((halfFadeDuration - currentFadeTime) / halfFadeDuration);
                        if (dontDoFade == false)
                            textCanvasGroup.alpha = alpha;

                        if (currentFadeTime < 0f)
                        {
                            playingFade = false;
                        }
                        // finished fading
                    }
                }
                else
                {
                    if (fadingHelper == true)
                    {
                        float alpha = currentFadeTime / halfFadeDuration;
                        if (dontDoFade == false)
                            characterCanvasGroup.alpha = alpha;

                        if (currentFadeTime < 0f)
                        {
                            // finished fading
                            playingFade = false;
                            gameObject.SetActive(false);
                            Reset();
                        }
                    }
                    else
                    {
                        float alpha = currentFadeTime / halfFadeDuration;
                        if (dontDoFade == false)
                            textCanvasGroup.alpha = alpha;

                        if (currentFadeTime < 0f)
                        {
                            ResetFadeTime();
                            displayingHelper = false;
                            fadingHelper = true;
                        }
                    }
                }
            }
        }

        void ResetFadeTime()
        {
            currentFadeTime = halfFadeDuration;
        }

        public void Reset()
        {
            DisableButtons();
        }

        /*
        public static void ShowMessage(string message)
        {
            if (instance != null)
            {
                instance.ShowHelperMessage(message);
            }
        }*/

        public void ShowHelperMessage(List<CharacterPopUpManager.CharacterMultiData> messages)
        {
            MessageList = messages;
            if (ProcessHelperMessageComplete())
                MessageList = null;
        }

        public bool IsHelperMessageComplete()
        {
            if (MessageList != null)
            {
                return MessageList.Count <= 0;
            }
            return true;
        }

        public bool ProcessHelperMessageComplete()
        {
            if (MessageList != null)
            {
                return !TriggerNextMessageFromList();
            }
            return true;
        }

        private bool TriggerNextMessageFromList()
        {
            if (MessageList.Count > 0) {
                ShowHelperMessage(GetNextMessageFromList(true));
                return true;
            }
            MessageList = null;
            return false;
        }

        public string GetNextMessageFromList(bool remove = true)
        {
            if (MessageList != null && MessageList.Count > 0)
            {
                string nextString = MessageList[0].message;
                MessageList[0].TriggerEvent();
                if (remove)
                    MessageList.RemoveAt(0);
                return nextString;
            }
            return "";
        }

        public void ShowHelperMessage(string message)
        {
            SetString(message);

            // enable the object and start the alpha tween
            if (displayingHelper == true)
            {
                this.gameObject.SetActive(true);
                playingForward = true;
                playingFade = true;
                fadingHelper = false;
                if (dontDoFade == false)
                {
                    textCanvasGroup.alpha = 0f;
                }
                ResetFadeTime();                
            }
            else
            {
                this.gameObject.SetActive(true);
                playingForward = true;
                playingFade = true;
                fadingHelper = true;
                if (dontDoFade == false)
                {
                    characterCanvasGroup.alpha = 0f;
                    textCanvasGroup.alpha = 0f;
                }
                ResetFadeTime();
            }
        }

        private void SetString(string _message)
        {
            if (styledText != null)
            {
                styledText.SetString(_message);
            }
            else
            {
                messageText.text = _message;
            }
        }

        public void HideHelperMessage()
        {
            // tween out
            playingForward = false;
            playingFade = true;
            fadingHelper = false;
            ResetFadeTime();
        }

        public void OnClickClose()
        {
            if (playingFade == true && playingForward == false)
                return; // we're alraedy closing
            HideHelperMessage();
        }

        public void SetHelperImage(Sprite _image)
        {
            Image img = characterCanvasGroup.GetComponent<Image>();
            if (img != null)
            {
                img.sprite = _image;
            }
        }

        public void DisableButtons()
        {
            Button[] buttons = this.GetComponentsInChildren<Button>(true);
            foreach (Button but in buttons)
            {
                switch (but.name)
                {
                    case "CloseButton":
                        break;
                    default:
                        but.onClick.RemoveAllListeners();
                        but.gameObject.SetActive(false);
                        HandleTextUI(false);
                        break;
                }
            }
        }

        public void SetPopUpButton(string buttonName, UnityAction function, string sprite)
        {
            Button[] buttons = this.GetComponentsInChildren<Button>(true);
            foreach (Button but in buttons)
            {
                if (but.name == buttonName)
                {
                    but.onClick.RemoveAllListeners();
                    but.onClick.AddListener(function);
                    but.onClick.AddListener(OnClickClose);
                    but.gameObject.SetActive(true);

                    // handle text UI
                    HandleTextUI(true);

                    Sprite spr = ResourceManager.instance.Get<Sprite>(sprite);
                    if(spr != null)
                    {
                        Image img = but.GetComponent<Image>();
                        if(img != null)
                        {
                            img.sprite = spr;
                        }
                    }

                    break;
                }
            }
        }

        private void HandleTextUI(bool withButtons)
        {
            if (ResizeForButton)
            {
                messageTextRect.sizeDelta = new Vector2((withButtons) ? TextWidthWithButton : TextWidth, messageTextRect.sizeDelta.y);
            }

        }
    }
}
