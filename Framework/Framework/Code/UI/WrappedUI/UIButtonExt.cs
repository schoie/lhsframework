﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class UIButtonExt : Button
{
    public ButtonClickedEvent OnDown;
    public ButtonClickedEvent OnHeld;
    public ButtonClickedEvent OnUp;

    private bool down = false;

    public void Update()
    {
        //A public function in the selectable class which button inherits from.
        if (IsPressed())
        {
            if (!down)
            {
                down = true;
                OnDown.Invoke();
            }
            else
                OnHeld.Invoke();
        }
        else if (down)
        {
            down = false;
            OnUp.Invoke();
        }
        
    }
    
}