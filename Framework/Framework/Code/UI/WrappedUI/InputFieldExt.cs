﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine.Events;
using System;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

public class InputFieldExt : InputField
{
    public bool Done { get { return m_Keyboard != null && m_Keyboard.done && !m_Keyboard.wasCanceled; } }

}
