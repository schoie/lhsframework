﻿/*
    UISpacers.cs -- A generic script for positioning a group og Type T UIBehaviour's (Unity Ui Objects) in all directions with pivot settings

    Name: Steven Ogden
    Date: 14/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace LHS.Framework.UI
{

    [Serializable]
    public enum eSpacingType
    {
        Vertical_up = 0,
        Vertical_down,
        Horizontal_left,
        Horizontal_right
    }
    [Serializable]
    public enum eSpacingPivot
    {
        Positive = 0,
        Zero,
        Negative
    }

    public interface ISpaceUI
    {
        void ProcessSpace();
    }

    [Serializable]
    public class UISpacer<T> : MonoBehaviour, ISpaceUI where T : UIBehaviour
    {

        public eSpacingType Type = eSpacingType.Vertical_up;
        public eSpacingPivot PivotFrom = eSpacingPivot.Zero;
        public Vector2 StartPosition;
        public float Spacing;
        public bool SpaceInactive = false;

        public void Awake()
        {
            HandleUISpacing();
        }

        public void ProcessSpace()
        {
            HandleUISpacing();
        }

        private void HandleUISpacing()
        {
            T[] UIItems = this.GetComponentsInChildren<T>(SpaceInactive);
            if (UIItems.Length > 0)
            {
                Vector2 direction = new Vector2();
                Vector2 runningPosition = DetermineStartingPos(UIItems.Length - 1, ref direction);
                LHSDebug.LogFormat("UISpacer - runningPosition: {0}", runningPosition);

                // Determine the starting pos using the size and the pivot

                foreach (T item in UIItems)
                {
                    RectTransform pRect = item.GetComponent<RectTransform>();
                    pRect.anchoredPosition = StartPosition + runningPosition;
                    runningPosition += (direction * Spacing);
                }
            }
        }

        private Vector2 DetermineStartingPos(int itemSize, ref Vector2 direction)
        {
            Vector2 pos = new Vector2(0,0);
            switch (PivotFrom)
            {
                case eSpacingPivot.Zero:

                    switch (Type)
                    {
                        case eSpacingType.Vertical_up:
                            pos.y = -(itemSize * (Spacing / 2f));
                            break;
                        case eSpacingType.Vertical_down:
                            pos.y = (itemSize * (Spacing / 2f));
                            break;
                        case eSpacingType.Horizontal_left:
                            pos.x = -(itemSize * (Spacing / 2f));
                            break;
                        case eSpacingType.Horizontal_right:
                            pos.x = (itemSize * (Spacing / 2f));
                            break;
                    }

                    break;

            }

            // determine direction
            switch (Type)
            {
                case eSpacingType.Vertical_up:
                    direction.y = 1;
                    break;
                case eSpacingType.Vertical_down:
                    direction.y = -1;
                    break;
                case eSpacingType.Horizontal_left:
                    direction.x = -1;
                    break;
                case eSpacingType.Horizontal_right:
                    direction.x = 1;
                    break;
            }

            return pos;
        }

    }
}
