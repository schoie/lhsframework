﻿/*
    UITextRevealRPG.cs -- Will organise the RPG style text scrolling of a string on a particular text field

    Name: Daniel Schofiled
    Date: Unknown (moved from specific project into framework) 18/10/2016
    
    Copyright Lionsheart Studios. All rights reserved.

    // Edited by Steven Ogden 5/12/2016 - Changed Update to a Coroutine
*/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

namespace LHS.Framework.UI
{

    public class UITextRevealRPG : MonoBehaviour, IDisplayText
    {
        public float ScrollTime = 1f;
        public float Delay = 0f;
        public bool ApplyDelay = false;
        public Text text;

        bool isRevealing = false;

        int curWord = 0;
        string[] wordArr;

        string fullText = "";

        Coroutine ScrollTextCo = null;

        public void OnDisable()
        {
            isRevealing = false;
        }

        public bool IsRevealing()
        {
            return isRevealing;
        }

        public void SetDelay(float delay, bool applyDelay)
        {
            Delay = delay;
            ApplyDelay = applyDelay;
        }

        public void SetString(string textT)
        {
            curWord = 0;
            wordArr = textT.Split(' ');
            this.text.text = "";
            fullText = textT;
            StartScrollTextCo();
        }

        public void SetStringWithCallback(string textT, UnityAction finishCallback)
        {
            curWord = 0;
            wordArr = textT.Split(' ');
            this.text.text = "";
            fullText = textT;
            StartScrollTextCo(finishCallback);
        }

        public bool CheckForStillWriting()
        {
            return curWord < fullText.Length;
        }

        public void Interrupt()
        {
            wordArr = null;
            text.text = fullText;
            curWord = fullText.Length;

            if(ScrollTextCo != null)
            {
                StopCoroutine(ScrollTextCo);
                ScrollTextCo = null;
            }
        }

        private void StartScrollTextCo(UnityAction finishCallback = null)
        {
            if (wordArr == null || wordArr.Length == 0)
                return;
            
            if (this.gameObject.activeInHierarchy && this.gameObject.activeSelf)
            {
                if (ScrollTextCo != null)
                {
                    StopCoroutine(ScrollTextCo);
                }
                
                ScrollTextCo = StartCoroutine(ScrollTextCoroutine(ScrollTime / fullText.Length, finishCallback));
            }
        }

        IEnumerator ScrollTextCoroutine(float charSpeed, UnityAction finishCallback = null)
        {
            isRevealing = true;
            float timer = 0f;

            // do delay if we need to
            if (ApplyDelay)
            {
                while (true)
                {
                    timer += Time.deltaTime;

                    if (timer >= Delay)
                    {
                        timer -= Delay;
                        break;
                    }

                    yield return null;
                }
            }
            
            yield return null;

            // now scroll text
            while (fullText.Length > curWord)
            {
                timer += Time.deltaTime;

                while (timer >= charSpeed)
                {
                    curWord++;
                    if (curWord <= fullText.Length)
                    {
                        string newS = "";
                        newS = fullText.Substring(0, curWord);
                        text.text = newS;
                    }
                    else
                    {
                        break;
                    }

                    timer -= charSpeed;
                }

                yield return null;
            }

            if (finishCallback != null)
                finishCallback.Invoke();

            isRevealing = false;
        }

        public float GetFullTextPreferredWidth()
        {
            if (text == null || string.IsNullOrEmpty(fullText))
                return 0;

            TextGenerator textGen = new TextGenerator();
            TextGenerationSettings generationSettings = text.GetGenerationSettings(text.rectTransform.rect.size);
            return textGen.GetPreferredWidth(fullText, generationSettings);
        }

        public float GetFullHeightPreferredHeight()
        {
            if (text == null || string.IsNullOrEmpty(fullText))
                return 0;

            TextGenerator textGen = new TextGenerator();
            TextGenerationSettings generationSettings = text.GetGenerationSettings(text.rectTransform.rect.size);
            return textGen.GetPreferredHeight(fullText, generationSettings);
        }

        // static functions
        #region Static Functions

        public static bool SetRPGText(GameObject obj, string _text)
        {
            if (obj.activeSelf && obj.activeInHierarchy)
            {
                IDisplayText revealComp = obj.GetComponent<IDisplayText>();
                if (revealComp != null)
                {
                    if (!revealComp.IsRevealing())
                    {
                        revealComp.SetString(_text);
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool SetRPGText(Text obj, string _text)
        {
            if (obj.gameObject.activeSelf && obj.gameObject.activeInHierarchy)
            {
                IDisplayText revealComp = obj.GetComponent<IDisplayText>();
                if (revealComp != null)
                {
                    if (!revealComp.IsRevealing())
                    {
                        revealComp.SetString(_text);
                        return true;
                    }
                }
                else
                {
                    obj.text = _text;
                }
            }
            return false;
        }

        public static bool SetRPGText(GameObject obj, string _text, UnityAction _callback)
        {
            if (obj.activeSelf && obj.activeInHierarchy)
            {
                IDisplayText revealComp = obj.GetComponent<IDisplayText>();
                if (revealComp != null)
                {
                    if (!revealComp.IsRevealing())
                    {
                        revealComp.SetStringWithCallback(_text, _callback);
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool SetRPGText(Text obj, string _text, UnityAction _callback)
        {
            if (obj.gameObject.activeSelf && obj.gameObject.activeInHierarchy)
            {
                IDisplayText revealComp = obj.GetComponent<IDisplayText>();
                if (revealComp != null)
                {
                    if (!revealComp.IsRevealing())
                    {
                        revealComp.SetStringWithCallback(_text, _callback);
                        return true;
                    }
                }
                else
                {
                    obj.text = _text;
                }
            }
            return false;
        }
        #endregion
    }
}


