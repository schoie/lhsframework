﻿/*
    UIButtonSpacer.cs -- Utilises the UISpacer script to geenrate X amount of Spacing scripts for UI elements

    Name: Steven Ogden
    Date: 14/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace LHS.Framework.UI
{
    public class UIButtonSpacer : UISpacer<Button>
    {
        // UISpacer does all the work
        // this class jsut defines the Button relationship
    }
    public class UIImageSpacer : UISpacer<Image>
    {
        // UISpacer does all the work
        // this class jsut defines the Image relationship
    }
    public class UITextSpacer : UISpacer<Text>
    {
        // UISpacer does all the work
        // this class jsut defines the Text relationship
    }
    public class UISliderSpacer : UISpacer<Slider>
    {
        // UISpacer does all the work
        // this class jsut defines the Text relationship
    }
}
