﻿/*
    UITextFileToText.cs -- A generic script to read in a text file and apply it to the Text objects text field

    Name: Steven Ogden
    Date: 20/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace LHS.Framework.UI
{
    public class UITextFileToText : MonoBehaviour
    {

        public TextAsset TextFile;
        public GameObject[] ToResize;

        private Text text;

        public void Awake()
        {
            text = this.GetComponent<Text>();
            if (text == null)
            {
                LHSDebug.LogError("UITextFileToText - This object does not have a Text Component");
            }

            if (TextFile != null)
            {
                // load text file
                text.text = TextFile.text;
            }
        }

        public void Start()
        {
            if (TextFile != null)
            {
                HandleResizing();
            }
        }

        private void HandleResizing()
        {
            RectTransform pRect = this.GetComponent<RectTransform>();
            if(pRect != null)
            {
                float textheight = LayoutUtility.GetPreferredHeight(pRect);
                pRect.sizeDelta = new Vector2(pRect.sizeDelta.x, textheight);

                foreach (GameObject obj in ToResize)
                {
                    RectTransform resizeRect = obj.GetComponent<RectTransform>();
                    if (resizeRect != null)
                    {
                        resizeRect.sizeDelta = new Vector2(resizeRect.sizeDelta.x, textheight);
                    }
                }
            }
        }

        public void SetTextDirect(TextAsset textAsset)
        {
            text.text = textAsset.text;
            HandleResizing();
        }


    }
}
