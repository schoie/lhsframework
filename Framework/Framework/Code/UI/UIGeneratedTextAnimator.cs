﻿/*
    UIGeneratedTextAnimator.cs -- A generic script to generate text that will animate 

    Name: Steven Ogden
    Date: 22/08/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace LHS.Framework.UI
{
    public class UIGeneratedTextAnimator : MonoBehaviour
    {

        public enum eAnimatedTextType
        {
            RPGFadeUp,

            OthersToCome
        }

        public class AnimatedText
        {
            public Text text;
            public float timer;
            public float data;

            private RectTransform pRect;
            private Vector2 Position;
            private float alpha;
            public delegate void MyAnimater(float percent);
            MyAnimater Animate;

            public AnimatedText(eAnimatedTextType _type, Text _text, Vector2 _pos, float _data)
            {
                text = _text;
                data = _data;

                switch (_type)
                {
                    case eAnimatedTextType.RPGFadeUp:
                        Animate = AnimateRPGStyle;
                        break;
                }

                pRect = text.GetComponent<RectTransform>();
                if (pRect == null)
                {
                    LHSDebug.LogError("You cannot create a AnimatedText object specifying a Text object that does not have a RectTransform!!");
                    Animate = ErrorAnimator;
                    return;
                }
                
                Position = _pos;
                alpha = text.color.a;

            }

            public bool isProcessed(float time, AnimationCurve curve)
            {
                bool returnVal = false;
                float evaluateTimer;
                float percent;
                timer += UnityEngine.Time.deltaTime;
                if (timer >= time)
                {
                    timer = (timer - time);
                    evaluateTimer = 1;
                    returnVal = true;
                    DestroyObject(text.gameObject);
                }
                else
                {
                    evaluateTimer = timer / time;
                }
                percent = curve.Evaluate(evaluateTimer);

                Animate(percent);

                return returnVal;
            }

            public void AnimateRPGStyle(float percent)
            {
                pRect.anchoredPosition = new Vector2(Position.x, Position.y + (data * percent));
                Color newCol = text.color;
                newCol.a = alpha * (1 - percent);
                text.color = newCol;
            }

            public void ErrorAnimator(float percent)
            {
                LHSDebug.LogWarning("Error in AnimatedText object. As a result we are running ErrorAnimator delegate!!");
            }

        }

        public enum eGeneratedType
        {
            FromTextComponent,
            GameObjectReference,
            Resource
        }

        [Header("Animated text creation")]
        public eAnimatedTextType TextType;
        public float Data;
        public Vector2 Offset;
        public float ScaleFactor = 1;

        [Header("Text object options")]
        public eGeneratedType GeneratedType;
        public GameObject ObjectResource;
        public String ResourcePath;

        [Header("Aninmated Text Update")]
        public float Time;
        public AnimationCurve Curve;

        private List<AnimatedText> GeneratedText;
        private bool isUpdating = false;

        public void Awake()
        {
            GeneratedText = new List<AnimatedText>();
        }
        
        public void Generate(string text)
        {
            Text newText;
            if (GetTextObject(out newText)) {

                PrepNewText(newText, text);
                ProcessGeneratedTextCo(new AnimatedText(TextType, newText, Offset, Data));
            }
        }

        public void Generate(string text, Color color)
        {
            Text newText;
            if (GetTextObject(out newText))
            {
                newText.color = color;
                PrepNewText(newText, text);
                ProcessGeneratedTextCo(new AnimatedText(TextType, newText, Offset, Data));
            }
        }

        private void PrepNewText(Text newText, string text)
        {
            newText.gameObject.transform.localScale *= ScaleFactor;
            newText.gameObject.transform.SetParent(this.transform, false);
            newText.text = text;
        }

        private bool GetTextObject(out Text text)
        {
            text = null;
            GameObject obj = null;
            // create here
            switch (GeneratedType)
            {
                case eGeneratedType.FromTextComponent:

                    Text comp = this.GetComponent<Text>();
                    if (comp != null)
                    {
                        obj = GameObject.Instantiate(comp.gameObject);
                        // if this works we need to remove this component from itself
                        UIGeneratedTextAnimator toDel = obj.GetComponent<UIGeneratedTextAnimator>();
                        if (toDel != null)
                            DestroyImmediate(toDel);
                    }

                    break;
                case eGeneratedType.GameObjectReference:

                    if (ObjectResource == null)
                    {
                        LHSDebug.LogWarning("UIGeneratedTextAnimator::Generate - the ObjectResource must not be null when generating via Type: " + GeneratedType);
                        return false;
                    }

                    obj = GameObject.Instantiate(ObjectResource);

                    break;
                case eGeneratedType.Resource:

                    obj = ResourceManager.instance.Load(ResourcePath) as GameObject;

                    break;
            }

            if (obj == null)
            {
                LHSDebug.LogWarning("UIGeneratedTextAnimator::Generate - Could not find a object via the Generate Type: " + GeneratedType);
                return false;
            }

            text = obj.GetComponent<Text>();
            if (text == null)
            {
                LHSDebug.LogWarning("UIGeneratedTextAnimator::Generate - Could not find a Text object on the object generated via the Generate Type: " + GeneratedType);
                return false;
            }

            return true;
        }

        private void ProcessGeneratedTextCo(AnimatedText _text)
        {
            GeneratedText.Add(_text);

            if(!isUpdating)
                StartCoroutine(ProcessGeneratedTextCoroutine());

        }

        IEnumerator ProcessGeneratedTextCoroutine()
        {
            isUpdating = true;

            // pulse in time
            while (GeneratedText.Count > 0)
            {
                GeneratedText.RemoveAll(delegate (AnimatedText gt) {
                    return gt.isProcessed(Time, Curve);
                });

                yield return null;
            }
            
            isUpdating = false;
        }

    }
}
