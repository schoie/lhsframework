﻿/*
    UIPairedSliders.cs -- A generic script managing 2 Sliders, so that if one is changed the other reflects its inverse value

    Name: Steven Ogden
    Date: 30/03/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;

namespace LHS.Framework.UI
{
    public class UIPairedSliders : MonoBehaviour, ISliderhandler
    {

        public Slider SliderOne;
        public Slider SliderTwo;
        public bool InteractableOnAwake = true;
        public bool SetStartingValueOnStart = true;
        public bool SyncSliderMinAndMax = true;
        public bool locked { get; set; }

        [Range(0, 1)]
        public float StartingPercentSliderOne;

        public UnityEvent OnUpdate;

        public void Awake()
        {
            if (SliderOne == null || SliderTwo == null)
            {
                enabled = false;
                return;
            }

            if (!InteractableOnAwake)
            {
                SetInteractable(false);
            }

            if (SyncSliderMinAndMax)
            {
                SliderTwo.minValue = SliderOne.minValue;
                SliderTwo.maxValue = SliderOne.maxValue;
            }

            locked = false;
        }

        public void Start()
        {
            if (SetStartingValueOnStart)
            {
                Reset();
            }
        }

        public void UpdateSliders(Slider sliderChanged)
        {
            if (locked)
                return;

            if (sliderChanged.name == SliderOne.name)
            {
                SliderTwo.value = SliderOne.maxValue - (SliderOne.value - SliderOne.minValue);
            }
            else
            {
                SliderOne.value = SliderTwo.maxValue - (SliderTwo.value - SliderTwo.minValue);
            }
            OnUpdate.Invoke();
        }

        public void Reset()
        {
            if (locked)
                return;

            SliderOne.value = SliderOne.minValue + (StartingPercentSliderOne * (SliderOne.maxValue - SliderOne.minValue));
            SliderTwo.value = SliderOne.maxValue - (SliderOne.value - SliderOne.minValue);

            UISliderValueHandle[] handles;
            handles = SliderOne.GetComponentsInChildren<UISliderValueHandle>(true);
            foreach (UISliderValueHandle handle in handles)
            {
                handle.handleValueChange(SliderOne.value);
            }
            handles = SliderTwo.GetComponentsInChildren<UISliderValueHandle>(true);
            foreach (UISliderValueHandle handle in handles)
            {
                handle.handleValueChange(SliderTwo.value);
            }
        }

        public void SetInteractable(bool set)
        {
            SliderOne.interactable = set;
            SliderTwo.interactable = set;
        }

        public List<Slider> GetSliders()
        {
            List<Slider> Sliders = new List<Slider>();
            Sliders.Add(SliderOne);
            Sliders.Add(SliderTwo);
            return Sliders;
        }

    }
}
