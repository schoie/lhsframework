﻿/*
    UITextAnimator.cs -- A generic script that will animate text with features

    Name: Steven Ogden
    Date: 08/09/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace LHS.Framework.UI
{
    public class UITextAnimator : MonoBehaviour
    {

        public float AnimationTime;

        private Text text;
        private string textToAnimate;
        private float timePerCharacter;
        private float timer = 0;
        private int index = 0;

        public void Awake()
        {
            text = this.GetComponent<Text>();
            if (text == null)
            {
                LHSDebug.LogError("UITextFileToText - This object does not have a Text Component");
                enabled = false;
                return;
            }

            if (text.text == "")
            {
                LHSDebug.LogError("UITextFileToText - The text component must have some text in it to animate");
                enabled = false;
                return;
            }

            textToAnimate = text.text;
            timePerCharacter = AnimationTime / textToAnimate.Length;
            text.text = "";
        }

        public void Update()
        {
            timer += Time.deltaTime;
            while(timer >= timePerCharacter)
            {
                index++;
                if(index > textToAnimate.Length)
                {
                    index = 0;
                    text.text = "";
                }
                else
                {
                    text.text = textToAnimate.Substring(0, index);
                }
                
                timer -= timePerCharacter;
            }

        }

        public void Stop()
        {
            text.text = textToAnimate;
            enabled = false;
        }


    }
}

