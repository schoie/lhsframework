﻿/*
    UILinkedSliders.cs -- A generic script managing X Sliders, so that if one is changed the others reflect its inverse value shared amongst X

    Name: Steven Ogden
    Date: 28/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace LHS.Framework.UI
{
    public class UILinkedSliders : MonoBehaviour, ISliderhandler
    {
        [Header("Manually add Sliders or allow the script", order = 1)]
        [Header("to automatically grab the children and use those", order = 2)]
        public bool LocateAndAllocateSliderChildren = false;
        public List<Slider> Sliders;
        public bool InteractableOnAwake = true;
        public bool SetStartingValueOnStart = true;
        public bool SyncSliderMinAndMax = true;

        public int StartingSliderIndex = 0;
        [Range(0, 1)]
        public float StartingPercent;

        public UnityEvent OnUpdate;

        /*
        // we have dropped the colour idea
        // The colour stuff for now is a bit of a hack because we are not using white/grey assets
        public Color LockColor;
        public Color NormalColor;
        public Color FillColor;
        */

        private bool updateCoroutineisRunning = false;


        // Currently supports a single lock
        // because we are using it for a linked size of 3 it works
        // Moving forward a class that has the slider and a boolean for the locked state should be implemented
        // Then the locking function below should be designed to allow there to be always 2 sliders unlocked
        // so it would be Acceptable Locked Amount = Slider Count - 2
        private int lockIndex = -1;

        public void Awake()
        {
            if (LocateAndAllocateSliderChildren)
            {
                Sliders.RemoveAll(ListClearAllSliders);

                Slider[] ChildSliders = this.GetComponentsInChildren<Slider>(true);
                foreach (Slider slider in ChildSliders)
                {
                    Sliders.Add(slider);
                }
            }

            if (Sliders.Count <= 0)
            {
                LHSDebug.LogError("UILinkedSliders - Sliders is empty?");
                enabled = false;
                return;
            }

            if (Sliders.Count == 1)
            {
                LHSDebug.LogError("UILinkedSliders - Requires more then 1 Slider");
                enabled = false;
                return;
            }

            if (!InteractableOnAwake)
            {
                SetInteractable(false);
            }
        }

        public void Start()
        {
            if (SetStartingValueOnStart)
            {

                float value = 1f - StartingPercent;
                float valueOn2 = value / 2;
                for (int i = 0; i < Sliders.Count; ++i)
                {
                    if (SyncSliderMinAndMax && i != 0)
                    {
                        Sliders[i].minValue = Sliders[0].minValue;
                        Sliders[i].maxValue = Sliders[0].maxValue;
                    }

                    if (i == StartingSliderIndex)
                    {
                        Sliders[i].value = Sliders[i].minValue + (StartingPercent * (Sliders[i].maxValue - Sliders[i].minValue));
                    }
                    else
                    {
                        Sliders[i].value = Sliders[i].minValue + (valueOn2 * (Sliders[i].maxValue - Sliders[i].minValue));
                    }
                }

                NormaliseSliders(null);
            }
        }

        public void UpdateSliders(Slider sliderChanged)
        {
            if (!updateCoroutineisRunning)
                StartCoroutine(UpdateSlidersCoroutine(sliderChanged));
        }

        IEnumerator UpdateSlidersCoroutine(Slider sliderChanged)
        {
            updateCoroutineisRunning = true;
            int newLockindex = 0;

            Slider foundSlider = Sliders.Where(obj => obj == sliderChanged).SingleOrDefault();
            if (foundSlider != null)
            {
                // create list of available sliders

                int Count = (Sliders.Count - 1);
                //float value = 1f - foundSlider.value;
                float value = foundSlider.maxValue - (foundSlider.value - foundSlider.minValue);

                /*
                // we have dropped the colour idea
                // handle colours
                for (int i = 0; i < Sliders.Count; ++i)
                {
                    if (Sliders[i] != foundSlider)
                    {
                        ColourSlider(Sliders[i], NormalColor, FillColor);
                    }
                    else
                    {
                        ColourSlider(Sliders[i], LockColor, LockColor);
                        newLockindex = i;
                    }
                }
                */

                if (value == 0 && lockIndex < 0)
                {
                    for (int i = 0; i < Sliders.Count; ++i)
                    {
                        if (Sliders[i] != foundSlider)
                        {
                            Sliders[i].value = 0;
                        }
                    }
                }
                else
                {

                    // float valueOnCount = value / Count;

                    // determine what the value is from the other slider inverses
                    // and then dividing that by Count-1
                    // this value should be +/- different from the found sliders value
                    // and thus this different is the change
                    // we then divide the difference by Count-1 and apply it to each other slider
                    // check bounds!!
                    float otherValue = 0;
                    for (int i = 0; i < Sliders.Count; ++i)
                    {
                        if (Sliders[i] != foundSlider)
                        {
                            otherValue += Sliders[i].value;
                        }
                    }

                    List<Slider> ActiveSliders = new List<Slider>();
                    // handle lock here
                    if (lockIndex >= 0)
                    {
                        for (int i = 0; i < Sliders.Count; ++i)
                        {
                            if (i != lockIndex)
                            {
                                ActiveSliders.Add(Sliders[i]);
                            }
                        }
                        Count = ActiveSliders.Count - 1;
                    }
                    else
                    {
                        ActiveSliders = Sliders;
                    }

                    float otherValue2 = otherValue / Count;
                    float otherValueonCount = otherValue2 / Count;

                    float newValue = value - otherValue;
                    float newValueOnCount = newValue / Count;

                    bool weHaveClamped = false;
                    if (lockIndex >= 0)
                    {
                        // if something is locked we need to ensure that we clamp
                        // the left over sliders
                        float checkVal = Sliders[lockIndex].maxValue - Sliders[lockIndex].value;
                        if (foundSlider.value >= checkVal)
                        {
                            foundSlider.value = checkVal;
                            weHaveClamped = true;
                        }
                    }

                    // handeling the spill
                    // If a slider reaches 100 or 0 we need to lock it as spill
                    // then take the spilt amount anf even apply it to the remaining sliders
                    List<Slider> NonSpiltSliders = new List<Slider>();
                    float spiltAmount = 0;

                    for (int i = 0; i < ActiveSliders.Count; ++i)
                    {
                        if (ActiveSliders[i] != foundSlider)
                        {
                            if (weHaveClamped)
                            {
                                ActiveSliders[i].value = 0f;
                            }
                            else
                            {
                                float sliderValue = ActiveSliders[i].value + newValueOnCount;
                                if (sliderValue < ActiveSliders[i].minValue)
                                {
                                    spiltAmount += sliderValue;
                                    sliderValue = ActiveSliders[i].minValue;
                                }
                                else if (sliderValue > ActiveSliders[i].maxValue)
                                {
                                    spiltAmount += (sliderValue - ActiveSliders[i].maxValue);
                                    sliderValue = ActiveSliders[i].maxValue;
                                }
                                else
                                {
                                    NonSpiltSliders.Add(ActiveSliders[i]);
                                }
                                ActiveSliders[i].value = sliderValue;
                            }
                        }
                    }

                    // apply spilt amount to remaining sliders
                    if (NonSpiltSliders.Count == 1)
                    {
                        NonSpiltSliders[0].value += spiltAmount;
                    }
                    else
                    {
                        float sliderAddition = (spiltAmount / (NonSpiltSliders.Count - 1));
                        for (int i = 0; i < NonSpiltSliders.Count; ++i)
                        {
                            NonSpiltSliders[i].value += sliderAddition;
                        }
                    }
                }

                NormaliseSliders(foundSlider);

                OnUpdate.Invoke();
            }

            updateCoroutineisRunning = false;

            yield return null;
        }

        private void NormaliseSliders(Slider lastChanged)
        {
            int total = 0;
            List<Slider> ActiveSliders = new List<Slider>();

            LHSDebug.LogFormat("lockIndex: {0}", lockIndex);
            for (int i = 0; i < Sliders.Count; ++i)
            {

                total += Mathf.FloorToInt(Sliders[i].value);
                LHSDebug.LogFormat("Sliders[i].value: {0} - total: {1}", Sliders[i].value, total);


                if (i != lockIndex)
                {
                    if(lastChanged == null)
                        ActiveSliders.Add(Sliders[i]);
                    else if(lastChanged != Sliders[i])
                        ActiveSliders.Add(Sliders[i]);
                }
            }


            LHSDebug.LogFormat("Size: {0} - total: {1}", ActiveSliders.Count, total);

            if(total != 100)
            {
                int dif = 100 - total;

                foreach(Slider slide in ActiveSliders)
                {
                    int value = (int)slide.value + dif;
                    if (value < 0)
                    {
                        slide.value = 0;
                        dif = value;
                    }
                    else
                    {
                        slide.value = value;
                        break;
                    }
                }

                
            }
        }


        public void Reset()
        {
            /*LHSDebug.Log("UIPairedSliders - Reset");
            SliderOne.value = StartingPercentSliderOne;
            SliderTwo.value = 1f - SliderOne.value;

            UISliderValueHandle[] handles;
            handles = SliderOne.GetComponentsInChildren<UISliderValueHandle>(true);
            foreach (UISliderValueHandle handle in handles)
            {
                handle.handleValueChange(SliderOne.value);
            }
            handles = SliderTwo.GetComponentsInChildren<UISliderValueHandle>(true);
            foreach (UISliderValueHandle handle in handles)
            {
                handle.handleValueChange(SliderTwo.value);
            }*/
        }

        public void SetInteractable(bool set)
        {
            foreach (Slider slider in Sliders)
                slider.interactable = set;
        }

        private static bool ListClearAllSliders(Slider s)
        {
            return true;
        }

        public List<Slider> GetSliders()
        {
            return Sliders;
        }

        public void LockSlider(Slider slider)
        {
            Slider foundSlider = Sliders.Where(obj => obj == slider).SingleOrDefault();
            if (foundSlider != null)
            {
                for (int i = 0; i < Sliders.Count; ++i)
                {
                    if (Sliders[i] == foundSlider)
                    {
                        if (lockIndex == i)
                        {
                            lockIndex = -1;
                            Sliders[i].interactable = true;
                            HandlePossibleUnlockEvents(Sliders[i]);
                        }
                        else
                        {
                            lockIndex = i;
                            Sliders[i].interactable = false;
                            HandlePossibleLockEvents(Sliders[i]);
                        }
                    }
                    else
                    {
                        Sliders[i].interactable = true;
                        HandlePossibleUnlockEvents(Sliders[i]);
                    }
                }
            }
        }

        // we have some cross referencing here, but only from the the Framework itself
        // They will only work if the appropriate UI relative script from the framework is attached
        // to the slider in some way
        private void HandlePossibleLockEvents(Slider slider)
        {
            UIButtonSpriteSwap[] components = slider.GetComponentsInChildren<UIButtonSpriteSwap>(true);
            foreach (UIButtonSpriteSwap comp in components)
            {
                comp.SetOption();
            }

        }

        private void HandlePossibleUnlockEvents(Slider slider)
        {
            UIButtonSpriteSwap[] components = slider.GetComponentsInChildren<UIButtonSpriteSwap>(true);
            foreach (UIButtonSpriteSwap comp in components)
            {
                comp.SetDefault();
            }
        }

        /*
        // we have dropped the colour idea
        private void ColourSlider(Slider slider, Color bg, Color fill)
        {
            Image[] images = slider.GetComponentsInChildren<Image>(true);
            foreach (Image img in images)
            {
                switch (img.name) {
                    case "Background":
                        img.color = bg;
                        break;
                    case "Fill":
                    case "Handle":
                        img.color = fill;
                        break;
                }
            }
        }
        */

    }
}

