﻿/*
    UITableBuilder.cs -- A generic script to build tables of data from prefabs

    Name: Steven Ogden
    Date: 18/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace LHS.Framework.UI
{

    public class UITableBuilder : MonoBehaviour
    {
        public class Cell
        {
            public GameObject cell;
            public Text text = null;
        }

        public GameObject DataCell;
        public GameObject TitleCell;
        public GameObject LockedCell;

        public Color HighLightColour;

        public int rows = 1;
        public int columns = 1;

        private Cell[,] table;

        public void Awake()
        {

            if (DataCell == null || TitleCell == null || LockedCell == null)
            {
                LHSDebug.LogError("UITableBuilder - All GameObejct cell data must be set");
                enabled = false;
                return;
            }

            if (rows <= 0 || columns <= 0)
            {
                LHSDebug.LogError("UITableBuilder - Both rows and columsn must be > 0");
                enabled = false;
                return;
            }


            BuildTable();

        }

        private void BuildTable()
        {
            Vector2 TitleSize = new Vector2();
            Vector2 DataSize = new Vector2();

            // get cell size data
            RectTransform pRect;
            pRect = TitleCell.GetComponent<RectTransform>();
            if (pRect != null)
            {
                TitleSize = pRect.sizeDelta;
            }
            pRect = DataCell.GetComponent<RectTransform>();
            if (pRect != null)
            {
                DataSize = pRect.sizeDelta;
            }


            // build table
            table = new Cell[rows, columns];
            float yPos = 0;
            float xPos = 0;

            Vector2 size = TitleSize;
            for (int r = 0; r < rows; ++r)
            {
                xPos = 0;
                for (int c = 0; c < columns; ++c)
                {
                    Cell cell = new Cell();
                    if (r == 0 || c == 0)
                    {
                        cell.cell = GameObject.Instantiate(TitleCell);
                        size = TitleSize;
                    }
                    else
                    {
                        cell.cell = GameObject.Instantiate(DataCell);
                        size = DataSize;
                    }

                    cell.text = cell.cell.GetComponentInChildren<Text>();
                    if (cell.text != null)
                    {
                        cell.text.text = string.Format("R:{0}, C:{1}", r, c);
                    }

                    pRect = cell.cell.GetComponent<RectTransform>();
                    if (pRect != null)
                    {
                        pRect.anchoredPosition = new Vector2(xPos, yPos);
                    }

                    cell.cell.transform.SetParent(this.transform, false);

                    table[r, c] = cell;

                    xPos += size.x;
                }

                yPos -= size.y;
            }
        }

        public Cell GetCell(uint row, uint col)
        {
            if (row < rows && col < columns)
            {
                return table[row, col];
            }
            return default(Cell);
        }

        public void SetText(uint row, uint col, string text)
        {
            Cell cell = GetCell(row, col);
            if (cell.text != null)
            {
                cell.text.text = text;
            }
        }

        public void SetLocked(uint row, uint col)
        {
            Cell cell = GetCell(row, col);
            if (cell.cell != null)
            {
                //cell.cell.
                RectTransform pRect = cell.cell.GetComponent<RectTransform>();
                if (pRect != null)
                {
                    Vector2 cellPos = new Vector2(pRect.anchoredPosition.x, pRect.anchoredPosition.y);
                    int sIndex = cell.cell.transform.GetSiblingIndex();
                    DestroyImmediate(cell.cell);
                    cell.cell = GameObject.Instantiate(LockedCell);
                    cell.cell.transform.SetParent(this.transform, false);
                    cell.cell.transform.SetSiblingIndex(sIndex);

                    pRect = cell.cell.GetComponent<RectTransform>();
                    if (pRect != null)
                    {
                        pRect.anchoredPosition = cellPos;
                    }
                }
            }
        }

        // this needs work to handle different sized objects
        // could be managed by the heirarchy
        public void SetPrefab(uint row, uint col, GameObject obj)
        {
            Cell cell = GetCell(row, col);
            if (cell.cell != null)
            {
                //cell.cell.
                RectTransform pRect = cell.cell.GetComponent<RectTransform>();
                if (pRect != null)
                {
                    Vector2 cellPos = new Vector2(pRect.anchoredPosition.x, pRect.anchoredPosition.y);
                    int sIndex = cell.cell.transform.GetSiblingIndex();
                    DestroyImmediate(cell.cell);
                    cell.cell = GameObject.Instantiate(obj);
                    cell.cell.transform.SetParent(this.transform, false);
                    cell.cell.transform.SetSiblingIndex(sIndex);

                    pRect = cell.cell.GetComponent<RectTransform>();
                    if (pRect != null)
                    {
                        pRect.anchoredPosition = cellPos;
                    }

                    cell.text = cell.cell.GetComponentInChildren<Text>();
                    if (cell.text != null)
                    {
                        cell.text.text = string.Format("R:{0}, C:{1}", row, col);
                    }
                }
            }
        }


        public void HighlightRow(uint row)
        {
            if (row < rows)
            {
                for (int i = 1; i < columns; ++i)
                {
                    if(table[row, i].cell != null)
                    {
                        if (table[row, i].cell.name != (LockedCell.name + "(Clone)"))
                        {
                            Image img = table[row, i].cell.GetComponent<Image>();
                            if(img != null)
                            {
                                img.color = HighLightColour;
                            }
                        }
                    }
                }
            }
        }

        public void HighlightColumn(uint col)
        {
            if (col < columns)
            {
                for (int i = 1; i < rows; ++i)
                {
                    if (table[i, col].cell != null)
                    {
                        if (table[i, col].cell.name != (LockedCell.name + "(Clone)"))
                        {
                            Image img = table[i, col].cell.GetComponent<Image>();
                            if (img != null)
                            {
                                img.color = HighLightColour;
                            }
                        }
                    }
                }
            }
        }

        public void HighlightCell(uint row, uint col)
        {
            Cell cell = GetCell(row, col);
            if (cell.cell != null)
            {
                if (cell.cell.name != (LockedCell.name + "(Clone)"))
                {
                    Image img = cell.cell.GetComponent<Image>();
                    if (img != null)
                    {
                        img.color = HighLightColour;
                    }
                }
            }
        }

        public void Removehighlights()
        {
            for (int r = 1; r < rows; ++r)
            {
                for (int c = 1; c < columns; ++c)
                {
                    if (table[r, c].cell != null)
                    {
                        if (table[r, c].cell.name != (LockedCell.name + "(Clone)"))
                        {
                            Image img = table[r, c].cell.GetComponent<Image>();
                            if (img != null)
                            {
                                Image DataImg = DataCell.GetComponent<Image>();
                                if (DataImg != null)
                                {
                                    img.color = DataImg.color;
                                }
                            }
                        }
                    }
                }
            }
        }

    }

}
