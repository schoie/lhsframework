﻿/*
    UIGroupFader.cs -- A generic script to control the fading in/out of a Canvas Group

    Name: Steven Ogden
    Date: 18/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace LHS.Framework.UI
{
    [Serializable]
    public class UIGroupFader : MonoBehaviour
    {

        public enum eFadeState
        {
            None,
            FadeIn,
            FadeOut
        }

        public CanvasGroup Group;
        public bool StartFadedOut = false;
        public float FadeTime;
        public AnimationCurve FadeCurve;
        public UnityEvent OnFadedIn;
        public UnityEvent OnFadedOut;

        private float inTimer = 0;
        private float outTimer = 0;
        private eFadeState state = eFadeState.None;

        private float EPSILION = 0.05f; // the EPSILIOn is a way to check for 0 or 1 on the alpha and give consideration to floating point precision

        private Coroutine fadeInCo;
        private Coroutine fadeOutCo;

        public void Awake()
        {
            Group.alpha = (StartFadedOut) ? 0 : 1;
        }

        public void Reset()
        {
            Group.alpha = (StartFadedOut) ? 0 : 1;
            outTimer = 0;
            inTimer = 0;
            state = eFadeState.None;
            Group.gameObject.SetActive(!StartFadedOut);
        }

        public void OnDisable()
        {
            switch (state)
            {
                case eFadeState.FadeOut:
                    Group.alpha = 0;
                    break;
                case eFadeState.FadeIn:
                    Group.alpha = 1;
                    break;
            }
            state = eFadeState.None;
        }

        public void Fadein()
        {
            if (!Group.gameObject.activeSelf)
                Group.gameObject.SetActive(true);
            if (!this.gameObject.activeSelf)
                this.gameObject.SetActive(true);


            StartFadeIn();
        }
        public void FadeOut()
        {
            if(Group.gameObject.activeSelf)
                StartFadeOut();
        }

        public bool TryFadein()
        {
            if (state == eFadeState.FadeIn)
            {
                return false;
            }

            if (Group.alpha > EPSILION)
            {
                return false;
            }

            if (!Group.gameObject.activeSelf)
            {
                Group.gameObject.SetActive(true);
            }
            if (!this.gameObject.activeSelf)
            {
                this.gameObject.SetActive(true);
            }
            
            StartFadeIn();
            return true;
        }
        public bool TryFadeOut()
        {
            if (state == eFadeState.FadeOut/* || Group.alpha < (1 - EPSILION)*/)
            {
                return false;
            }

            if (Group.gameObject.activeSelf && this.gameObject.activeSelf)
            {
                StartFadeOut();
                return true;
            }
            return false;
        }

        private void StartFadeIn()
        {
            inTimer = 0;
            if (outTimer > 0)
                inTimer = (FadeTime - outTimer);

            if (fadeOutCo != null)
                StopCoroutine(fadeOutCo);

            state = eFadeState.FadeIn;

            if (fadeInCo != null)
                StopCoroutine(fadeInCo);

            fadeInCo = StartCoroutine(FadeInCoroutine());
        }

        private void StartFadeOut()
        {
            outTimer = 0;
            if (inTimer > 0)
                outTimer = (FadeTime - inTimer);

            if (fadeInCo != null)
                StopCoroutine(fadeInCo);

            state = eFadeState.FadeOut;

            if (fadeOutCo != null)
                StopCoroutine(fadeOutCo);

            fadeOutCo = StartCoroutine(FadeOutCoroutine());
        }


        IEnumerator FadeInCoroutine()
        {
            float evaluateTimer;
            float percent;
            bool finished = false;
            while (true)
            {
                if (state == eFadeState.FadeOut)
                    yield break;

                inTimer += Time.deltaTime;
                if(inTimer >= FadeTime)
                {
                    evaluateTimer = 1;
                    finished = true;
                }
                else
                {
                    evaluateTimer = inTimer / FadeTime;
                }

                percent = FadeCurve.Evaluate(evaluateTimer);
                Group.alpha = percent;

                if (finished)
                    break;

                yield return null;
            }

            Group.alpha = 1;
            inTimer = 0;
            state = eFadeState.None;
            OnFadedIn.Invoke();
        }

        IEnumerator FadeOutCoroutine()
        {
            
            float evaluateTimer;
            float percent;
            bool finished = false;
            while (true)
            {
                if (state == eFadeState.FadeIn)
                    yield break;

                outTimer += Time.deltaTime;
                if (outTimer >= FadeTime)
                {
                    evaluateTimer = 1;
                    finished = true;
                }
                else
                {
                    evaluateTimer = outTimer / FadeTime;
                }

                percent = FadeCurve.Evaluate(evaluateTimer);
                Group.alpha = 1 - percent;

                if (finished)
                    break;

                yield return null;
            }

            Group.alpha = 0;
            outTimer = 0;
            state = eFadeState.None;
            Group.gameObject.SetActive(false);
            OnFadedOut.Invoke();
        }
    }
}
