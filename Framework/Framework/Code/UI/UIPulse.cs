﻿/*
    UIPulse.cs -- A generic script for pulsing any UI unity object and possibly sprites

    Name: Steven Ogden
    Date: 19/08/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace LHS.Framework.UI
{

    public interface IPulseUI
    {
        void ProcessPulse();
        void ProcessPulseOnAll();
        void ProcessPulseLoop();
        void StopPulse();
        void StopPulseOnAll();
        void ForceFinished();
        void ForceFinishedAll();
    }

    public class UIPulse : MonoBehaviour, IPulseUI
    {

        public float PulseSize;
        public float PulseTime;
        public float PulsePauseTime;
        public Color color;
        public AnimationCurve PulseCurve;
        public bool AllowInterruptPulse = false;
        public bool Loop = false;

        public bool isPulsing { get; private set; }
        private Outline outline = null;
        Coroutine PulseCo = null;
        private bool isLooping = false;

        public void Awake()
        {
            isPulsing = false;
        }

        public void OnDisable()
        {
            ForceFinished();
        }

        public void ProcessPulse()
        {
            if(this.gameObject.activeSelf && this.gameObject.activeInHierarchy)
                StartPulseCo(Loop);
        }

        public void ProcessPulseOnAll()
        {
            ProcessPulse();
            IPulseUI[] pulses = this.GetComponentsInChildren<IPulseUI>();
            foreach (IPulseUI pulse in pulses)
            {
                pulse.ProcessPulse();
            }
        }

        public void ProcessPulseLoop()
        {
            if (this.gameObject.activeSelf && this.gameObject.activeInHierarchy)
                StartPulseCo(true);
        }

        public void StopPulse()
        {
            isLooping = false;
        }

        public void StopPulseOnAll()
        {
            isLooping = false;
            IPulseUI[] pulses = this.GetComponentsInChildren<IPulseUI>();
            foreach (IPulseUI pulse in pulses)
            {
                pulse.StopPulseOnAll();
            }
        }

        public void ForceFinished()
        {
            if (isPulsing)
            {
                if(PulseCo != null)
                {
                    StopCoroutine(PulseCo);
                    PulseCo = null;
                }
                ApplyEffect(0f);
                isPulsing = false;
            }
        }

        public void ForceFinishedAll()
        {
            ForceFinished();
            IPulseUI[] pulses = this.GetComponentsInChildren<IPulseUI>();
            foreach (IPulseUI pulse in pulses)
            {
                pulse.StopPulseOnAll();
            }

        }

        private void StartPulseCo(bool loop = false)
        {
            if (!isPulsing || AllowInterruptPulse)
            {
                if (PulseCo != null)
                    StopCoroutine(PulseCo);
                PulseCo = StartCoroutine(PulseCoroutine(loop));
            }
        }

        IEnumerator PulseCoroutine(bool loop)
        {
            isLooping = loop;
            isPulsing = true;
            AttachComponents();

            float timer = 0;
            float evaluateTimer;
            float percent;
            bool complete = false;

            // pulse out time
            while (!complete)
            {
                timer += Time.deltaTime;
                if (timer >= PulseTime)
                {
                    timer = (timer - PulseTime);

                    if (isLooping)
                    {
                        timer -= PulseTime;
                    }
                    else
                    {
                        complete = true;
                    }

                    evaluateTimer = 1;
                }
                else
                {
                    evaluateTimer = timer / PulseTime;
                }
                percent = PulseCurve.Evaluate(evaluateTimer);

                // apply effect here
                ApplyEffect(percent);

                yield return null;
            }

            yield return new WaitForSeconds(PulsePauseTime);

            complete = false;

            // pulse in time
            while (!complete)
            {
                timer += Time.deltaTime;
                if (timer >= PulseTime)
                {
                    timer = (timer - PulseTime);
                    complete = true;
                    evaluateTimer = 1;
                }
                else
                {
                    evaluateTimer = timer / PulseTime;
                }
                percent = 1 - PulseCurve.Evaluate(evaluateTimer);

                // apply effect here
                ApplyEffect(percent);

                yield return null;
            }

            RemoveComponents();
            isPulsing = false;
        }

        private void AttachComponents()
        {
            RemoveComponents();

            outline = this.gameObject.AddComponent<Outline>();
            outline.effectDistance = new Vector2(-PulseSize, PulseSize);
            outline.effectColor = color;
        }
        private void RemoveComponents()
        {
            if (outline != null)
            {
                Destroy(outline);
                outline = null;
            }
        }

        private void ApplyEffect(float percent)
        {
            Color newColor = outline.effectColor;
            newColor.a = percent * color.a;
            outline.effectColor = newColor;
        }

    }
}
