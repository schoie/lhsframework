﻿/*
    UISlideIn.cs

    Name: Steven Ogden
    Date: Unknown

    Provided by Steven Ogden copyright free as part of the GNU license.
*/

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Advertisements;

namespace LHS
{
    namespace Framework
    {
        namespace UI
        {
            public class UISlideIn : MonoBehaviour
            {

                public enum eType
                {
                    timeout,
                    wait
                };
                public enum eSlideType
                {
                    Up,
                    Down,
                    Left,
                    Right,
                    Reverse,
                    Same,
                    None
                };

                public eType Type = eType.timeout;
                public eSlideType SlideType = eSlideType.Up;
                public eSlideType SlideOutType = eSlideType.Reverse;
                public float SlideTime = 0;
                public AnimationCurve SlideCurve;
                public AnimationCurve SlideOutCurve;
                public float Timeout = 0;
                public float Delay = 0;
                public GameObject ToDisable = null;
                public UISlideIn[] LinkedTriggerWaitEvents;
                public UnityEvent OnRevealed;
                public UnityEvent OnCompleted;
                public UnityEvent OnReward;

                private float timer;
                private Vector2 Difference;
                private Vector2 Position;
                private RectTransform Transform;
                private bool StartPosSet = false;
                private Vector2 DifferenceOut;
                private Vector2 PositionOut;

                public enum ePhases
                {
                    delay,
                    slideIn,
                    timeout,
                    setSlideOut,
                    slideOut,
                    finished,
                    wait
                };

                private ePhases phase;
                private ePhases outPhase = ePhases.setSlideOut;

                public void Start()
                {
                    if (!StartPosSet)
                        SetSlideStartPosition();

                    if (SlideType == eSlideType.Reverse || SlideType == eSlideType.Same)
                    {
                        SlideType = eSlideType.Up;
                        LHSDebug.LogErrorFormat("SlideType must be a direction: {0}", SlideType.ToString());
                    }
                }

                public void OnEnable()
                {
                    if (Type == eType.wait && this.isWaiting())
                    {
                        // do nothing
                    }
                    else
                    {

                        timer = 0;
                        phase = (Delay == 0) ? ePhases.slideIn : ePhases.delay;

                        if (ToDisable == null)
                            ToDisable = this.gameObject;

                        /*if (phase != ePhases.delay)
                        {
                            // SoundManager.Instance.Play("Slide");
                        }*/
                    }

                }

                public void Update()
                {
                    float evaluateTimer;
                    float percent;
                    Vector2 NewPos;

                    switch (phase)
                    {
                        case ePhases.delay:
                            timer += Time.deltaTime;
                            if (timer >= Delay)
                            {
                                timer = 0;
                                phase = ePhases.slideIn;
                               // SoundManager.Instance.Play("Slide");
                            }
                            break;
                        case ePhases.slideIn:

                            timer += Time.deltaTime;
                            if (timer >= SlideTime)
                            {
                                timer = 0;
                                evaluateTimer = 1;
                                HandleType();
                                OnRevealed.Invoke();
                            }
                            else
                            {
                                evaluateTimer = timer / SlideTime;
                            }

                            percent = SlideCurve.Evaluate(evaluateTimer);
                            NewPos = Difference * percent;
                            NewPos = Position + NewPos;

                            Transform.anchoredPosition = new Vector3(NewPos.x, NewPos.y, 0);

                            break;
                        case ePhases.timeout:

                            timer += Time.deltaTime;
                            if (timer >= Timeout)
                            {
                                timer = 0;
                                phase = ePhases.setSlideOut;
                               // SoundManager.Instance.Play("Slide");
                                HandleLinked();
                            }

                            break;
                        case ePhases.setSlideOut:

                            SetSlideOutPosition();
                            phase = ePhases.slideOut;

                            break;
                        case ePhases.slideOut:

                            timer += Time.deltaTime;
                            if (timer >= SlideTime)
                            {
                                timer = 0;
                                evaluateTimer = 1;
                                phase = ePhases.finished;
                            }
                            else
                            {
                                //evaluateTimer = 1 - (timer / SlideTime);
                                evaluateTimer = timer / SlideTime;
                            }


                            percent = SlideOutCurve.Evaluate(evaluateTimer);
                            NewPos = DifferenceOut * percent;
                            NewPos = PositionOut + NewPos;

                            Transform.anchoredPosition = new Vector3(NewPos.x, NewPos.y, 0);

                            break;
                        case ePhases.finished:
                            
                            OnCompleted.Invoke();
                            ToDisable.SetActive(false);

                            break;
                    }
                }

                private void HandleType()
                {
                    switch (Type)
                    {
                        case eType.timeout:
                            phase = ePhases.timeout;
                            break;
                        case eType.wait:
                            phase = ePhases.wait;
                            break;
                    }
                }

                public void TriggerWaitEndEvent()
                {
                    if (Type == eType.wait && phase == ePhases.wait)
                    {
                        timer = 0;
                        phase = ePhases.setSlideOut;
                       // SoundManager.Instance.Play("Slide");
                        HandleLinked();
                    }
                }



                public void TriggerPopUpEvent()
                {
                    this.gameObject.SetActive(true);
                }

                public void HandleLinked()
                {
                    for (int i = 0; i < LinkedTriggerWaitEvents.Length; ++i)
                    {
                        LinkedTriggerWaitEvents[i].TriggerWaitEndEvent();
                    }
                }

                private void SetSlideStartPosition()
                {
                    Transform = this.GetComponent<RectTransform>();

                    switch (SlideType)
                    {
                        case eSlideType.Up:
                            Difference = new Vector2(0, Transform.sizeDelta.y);
                            break;
                        case eSlideType.Down:
                            Difference = new Vector2(0, -Transform.sizeDelta.y);
                            break;
                        case eSlideType.Left:
                            Difference = new Vector2(-Transform.sizeDelta.x, 0);
                            break;
                        case eSlideType.Right:
                            Difference = new Vector2(Transform.sizeDelta.x, 0);
                            break;
                    }
                    Position = new Vector2(Transform.anchoredPosition.x - Difference.x, Transform.anchoredPosition.y - Difference.y);
                    Transform.anchoredPosition = Position;

                    StartPosSet = true;
                }

                private void SetSlideOutPosition()
                {

                    PositionOut = new Vector2(Transform.anchoredPosition.x, Transform.anchoredPosition.y);

                    switch (SlideOutType)
                    {
                        case eSlideType.Up:
                            DifferenceOut = new Vector2(0, Transform.sizeDelta.y);
                            break;
                        case eSlideType.Down:
                            DifferenceOut = new Vector2(0, -Transform.sizeDelta.y);
                            break;
                        case eSlideType.Left:
                            DifferenceOut = new Vector2(-Transform.sizeDelta.x, 0);
                            break;
                        case eSlideType.Right:
                            DifferenceOut = new Vector2(Transform.sizeDelta.x, 0);
                            break;
                        case eSlideType.Reverse:
                            DifferenceOut = -Difference;
                            break;
                        case eSlideType.Same:
                            DifferenceOut = Difference;
                            break;
                    }

                    StartPosSet = true;
                }

                public void ResetSlideStartPositionY(float yPos)
                {
                    if (Transform == null)
                        Transform = this.GetComponent<RectTransform>();
                    if (Transform != null)
                        Transform.anchoredPosition = new Vector2(Transform.anchoredPosition.x, yPos);
                    SetSlideStartPosition();
                }
                public void ResetSlideStartPositionX(float xPos)
                {
                    if (Transform == null)
                        Transform = this.GetComponent<RectTransform>();
                    if (Transform != null)
                        Transform.anchoredPosition = new Vector2(xPos, Transform.anchoredPosition.y);
                    SetSlideStartPosition();
                }
                public void ResetSlideStartPosition(float xPos, float yPos)
                {
                    if (Transform == null)
                        Transform = this.GetComponent<RectTransform>();
                    if (Transform != null)
                        Transform.anchoredPosition = new Vector2(xPos, yPos);
                    SetSlideStartPosition();
                }

                public bool isTransitioningIn()
                {
                    return phase == ePhases.slideIn;
                }
                public bool isTransitioningOut()
                {
                    return phase == ePhases.slideOut;
                }
                public bool isTransitioning()
                {
                    return isTransitioningIn() || isTransitioningOut();
                }
                public bool isWaiting()
                {
                    return phase == ePhases.wait;
                }
                public bool isTimeingOut()
                {
                    return phase == ePhases.timeout;
                }

                public void SetDelay(float _delay)
                {
                    Delay = _delay;
                    if (phase == ePhases.slideIn)
                        phase = (Delay == 0) ? ePhases.slideIn : ePhases.delay;
                }
            }

        }
    }
}
