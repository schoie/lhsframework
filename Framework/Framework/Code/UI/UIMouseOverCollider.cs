﻿/*
    UIMouseOverCollider.cs -- A generic script managing mouse over logic and enabling disabling a gameobject relative to this logic

    Name: Steven Ogden
    Date: 05/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using LHS.Framework;

public class UIMouseOverCollider : MonoBehaviour
    , IPointerEnterHandler
    , IPointerExitHandler
{
    
    public enum eMouseOverOptions
    {
        ActivateObject = 0,
        InstantiateObject,

        Size
    }

    public eMouseOverOptions Option;
    public GameObject ObjectData;
    public GameObject ObjectParent;

    private GameObject createdOBJ;

    public void OnPointerEnter(PointerEventData eventData)
    {
        switch (Option)
        {
            case eMouseOverOptions.ActivateObject:
                if (ObjectData != null)
                    ObjectData.SetActive(true);
                break;
            case eMouseOverOptions.InstantiateObject:
                if (ObjectData != null) {
                    createdOBJ = (GameObject)Instantiate(ObjectData);
                    createdOBJ.transform.SetParent(ObjectParent.transform, false);
                }
                break;
        }

        LHSDebug.LogFormat("OnPointerEnter - Name: {0}", this.name);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        switch (Option)
        {
            case eMouseOverOptions.ActivateObject:
                if (ObjectData != null)
                    ObjectData.SetActive(false);
                break;
        }

        LHSDebug.LogFormat("OnPointerExit - Name: {0}", this.name);
    }
}

