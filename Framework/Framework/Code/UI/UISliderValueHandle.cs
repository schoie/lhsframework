﻿/*
    UISliderValueHandle.cs -- A script to gain access to a slider and set its value text

    Name: Steven Ogden
    Date: 30/03/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace LHS.Framework.UI
{
    public class UISliderValueHandle : MonoBehaviour
    {

        public class SliderValueRange
        {
            public float min;
            public float max;
            public float difference;
        }

        public float value;
        public Text TextBox;
        public string Format = "{0}";

        private SliderValueRange range = new SliderValueRange();

        // Use this for initialization
        public void Awake()
        {

            if (TextBox == null)
            {
                Debug.LogError("UISliderValueHandle - Please ensure that we referecne the TextBox");
                enabled = false;
                return;
            }

            // apply the updater func to the on value changed unity event
            Slider slider = this.GetComponent<Slider>();
            if (slider == null)
            {
                LHSDebug.LogError("UISliderValueHandle - Please ensure this script is attached to a UnityEngine.UI.Slider");
                enabled = false;
                return;
            }

            slider.onValueChanged.AddListener(handleValueChange);
            
            range.min = slider.minValue;
            range.max = slider.maxValue;
            range.difference = slider.maxValue - slider.minValue;

        }

        public void handleValueChange(float change)
        {
            // if this is misused in order of operations
            // in the game then it is possible to have this called before awake
            // which is a bit of an issue
            // We may want to look into a possibly more elegant fix here
            if (range.difference == 0)
            {
                Awake();
            }

            // the below checks will ensure that we cannot get 0 / x or x / 0
            // if this occurs we will  see "infinity" in the textbox
            float changeMinusMin = (change - range.min);
            if (range.difference != 0 && changeMinusMin != 0)
            {
                double percent = changeMinusMin / range.difference;
                TextBox.text = string.Format(Format, value * percent);

            }
            else
            {
                TextBox.text = string.Format(Format, 0);
            }
        }
    }


}