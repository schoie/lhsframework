﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace LHS.Framework.Events
{

    public interface IEventHolder
    {
        T GetParam<T>();
        T GetParam<T>(int index);
    }

    public class EventHolder : IEventHolder
    {
        public System.Object[] Parameters;

        public EventHolder(params System.Object[] _params)
        {
            Parameters = _params;
        }

        public virtual T GetParam<T>()
        {
            return (T)Parameters[0];
        }
        public virtual T GetParam<T>(int index)
        {
            return (T)Parameters[index];
        }
    }

    public class EventHolderVar<Type> : EventHolder, IEventHolder
    {
        public Type Parameter;

        public EventHolderVar(Type _param) : base(null)
        {
            Parameter = _param;
        }
        public EventHolderVar(Type _param, params System.Object[] _params) : base(_params)
        {
            Parameter = _param;
        }

        public override T GetParam<T>()
        {
            return (T)(System.Object)Parameter;
        }
    }

    public class EventsManager : CSingleton<EventsManager>
    {

        public interface IEvent
        {
            void MyInvoke(EventHolder holder);
        }

        public class LHSEvent : UnityEvent<IEventHolder> { }

        public class EventCategory
        {
            public Dictionary<string, LHSEvent> Events;

            public EventCategory()
            {
                Events = new Dictionary<string, LHSEvent>();
            }

            public void CreateEvent(string _event)
            {
                if (!Events.ContainsKey(_event))
                {
                    Events.Add(_event, new LHSEvent());
                }
            }
            public void DestroyEvent(string _event)
            {
                if (Events.ContainsKey(_event))
                {
                    Events.Remove(_event);
                }
            }


            public void RegisterEvent(string _event, UnityAction<IEventHolder> call)
            {
                LHSEvent Event;
                if (Events.TryGetValue(_event, out Event))
                {
                    Event.RemoveListener(call);
                    Event.AddListener(call);
                }
            }
            public void DeregisterEvent(string _event, UnityAction<IEventHolder> call)
            {
                LHSEvent Event;
                if (Events.TryGetValue(_event, out Event))
                {
                    Event.RemoveListener(call);
                }
            }

            public void OnEvent(string _event, EventHolder _params = null)
            {
                LHSEvent Event;
                if (Events.TryGetValue(_event, out Event))
                {
                    Event.Invoke(_params);
                }
            }

            public void Clear(string _event)
            {
                LHSEvent Event;
                if (Events.TryGetValue(_event, out Event))
                {
                    Event.RemoveAllListeners();
                }
            }

            public void ClearAll()
            {
                Events.Clear();
            }

        }

        public Dictionary<string, EventCategory> Categories;

        public EventsManager()
        {
            Categories = new Dictionary<string, EventCategory>();
        }

        // Categories
        public void CreateCategory(string _category)
        {
            if (!Categories.ContainsKey(_category))
            {
                Categories.Add(_category, new EventCategory());
            }
            else
            {
                LHSDebug.LogWarningFormat("EventsManager.CreateCategory({0}) - Category already exists!!", _category);
            }
        }
        public void DestroyCategory(string _category)
        {
            if (Categories.ContainsKey(_category))
            {
                Categories.Remove(_category);
            }
            else
            {
                LHSDebug.LogWarningFormat("EventsManager.DestroyCategory({0}) - Category does not exists!!", _category);
            }
        }
        public void Clear()
        {
            Categories.Clear();
        }

        // events
        public void CreateEvent(string _category, string _event)
        {
            EventCategory category;
            if(Categories.TryGetValue(_category, out category))
            {
                category.CreateEvent(_event);
            }
            else
            {
                LHSDebug.LogWarningFormat("EventsManager.CreateEvent({0}, {1}) - Category does not exists!!", _category, _event);
            }
        }
        public void DestroyEvent(string _category, string _event)
        {
            EventCategory category;
            if (Categories.TryGetValue(_category, out category))
            {
                category.DestroyEvent(_event);
            }
            else
            {
                LHSDebug.LogWarningFormat("EventsManager.DestroyEvent({0}, {1}) - Category does not exists!!", _category, _event);
            }
        }

        public void RegisterEvent(string _category, string _event, UnityAction<IEventHolder> call)
        {
            EventCategory category;
            if (Categories.TryGetValue(_category, out category))
            {
                category.RegisterEvent(_event, call);
            }
            else
            {
                LHSDebug.LogWarningFormat("EventsManager.RegisterEvent({0}, {1}, {2}) - Category does not exists!!", _category, _event, call);
            }
        }
        public void DeregisterEvent(string _category, string _event, UnityAction<IEventHolder> call)
        {
            EventCategory category;
            if (Categories.TryGetValue(_category, out category))
            {
                category.DeregisterEvent(_event, call);
            }
            else
            {
                LHSDebug.LogWarningFormat("EventsManager.DeregisterEvent({0}, {1}, {2}) - Category does not exists!!", _category, _event, call);
            }
        }
        public void OnEvent(string _category, string _event, EventHolder _params = null)
        {
            EventCategory category;
            if (Categories.TryGetValue(_category, out category))
            {
                category.OnEvent(_event, _params);
            }
            else
            {
                LHSDebug.LogWarningFormat("EventsManager.OnEvent({0}, {1}, {2}) - Category does not exists!!", _category, _event, _params);
            }
        }
        public void ClearEvent(string _category, string _event)
        {
            EventCategory category;
            if (Categories.TryGetValue(_category, out category))
            {
                category.Clear(_event);
            }
            else
            {
                LHSDebug.LogWarningFormat("EventsManager.ClearEvent({0}, {1}) - Category does not exists!!", _category, _event);
            }
        }
        public void ClearAllEvents(string _category)
        {
            EventCategory category;
            if (Categories.TryGetValue(_category, out category))
            {
                category.ClearAll();
            }
            else
            {
                LHSDebug.LogWarningFormat("EventsManager.ClearAllEvents({0}) - Category does not exists!!", _category);
            }
        }


    }
}
