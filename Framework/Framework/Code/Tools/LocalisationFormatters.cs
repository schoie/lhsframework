﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LHS.Framework.Tools
{
    public delegate void MySetter(LocalisationVar var, object data);
    public delegate string MyFormatter(LocalisationVar var, string inString);

    public class LocalFormatter
    {
        public MySetter Set;
        public MyFormatter Format;

        public LocalFormatter(MySetter _set, MyFormatter _format)
        {
            Set = _set;
            Format = _format;
        }
    }

    public class LocalFormatters : CSingleton<LocalFormatters>
    {
        private Dictionary<int, LocalFormatter> Formatters;

        public LocalFormatters()
        {
            Formatters = new Dictionary<int, LocalFormatter>();

            RegisterFormatter("STRING", DefaultSetter, null);
            RegisterFormatter("INT", DefaultSetter, null);
            RegisterFormatter("FLOAT", DefaultSetter, null);
            RegisterFormatter("LABEL", null, LabelFormatter);
        }

        public void RegisterFormatter(string id, MySetter _set, MyFormatter _format)
        {
            RegisterFormatter(id.GetHashCode(), _set, _format);
        }

        public void RegisterFormatter(int id, MySetter _set, MyFormatter _format)
        {
            LocalFormatter formatter;
            if (!Formatters.TryGetValue(id, out formatter))
            {
                Formatters.Add(id, new LocalFormatter(_set, _format));
            }
            else
            {
                formatter.Format = _format;
            }
        }

        public bool Get(string id, out LocalFormatter formatter)
        {
            return Get(id.GetHashCode(), out formatter);
        }
        public bool Get(int id, out LocalFormatter formatter)
        {
            return Formatters.TryGetValue(id, out formatter);
        }

        // default setters
        public static void DefaultSetter(LocalisationVar var, object data)
        {
            var.result = data.ToString();
        }

        // default formatter
        public static string LabelFormatter(LocalisationVar var, string inString)
        {
            return inString.Replace(var.variable, LocalisationReader.instance.GetLocalisedString(var.var));
        }

    }


}
