﻿/*
    LocalisationManager.cs -- Part of the Local system

    Name: Steven Ogden
    Date: 15/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LHS.Framework;

namespace LHS.Framework.Tools
{
    public class LocalisationManager : CSingletonMonoBehaviour<LocalisationManager>
    {

        public LocalisationObject Language;
        public List<LocalisationObject> OptionalLanguages;

        public override void OnAwake()
        {

        }

        public void LoadLanguage()
        {
            Language.Load();
        }

        public string GetLocalisedString(string label)
        {
            string getString = "";
            Language.getLocalisationString(label, out getString);
            return getString;
        }

        public string GetLocalisedString(int labelHash)
        {
            string getString = "";
            Language.getLocalisationString(labelHash, out getString);
            return getString;
        }

    }
}
