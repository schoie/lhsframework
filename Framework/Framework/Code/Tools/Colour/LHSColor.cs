﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Tools
{
    public abstract class LHSColor
    {

        private static byte SKIN_COLOUR_SIZE = 15;
        private static byte HAIR_COLOUR_SIZE = 24;
        private static byte EYE_COLOUR_SIZE = 8;
        private static byte PASTEL_COLOUR_SIZE = 12;

        public static Color GetRandomSkinColour(out int index)
        {
            index = UnityEngine.Random.Range(0, SKIN_COLOUR_SIZE-1);
            return GetSkinColour(index);
        }

        public static Color GetSkinColour(int index)
        {
            switch (index)
            {
                case 0: return GetColor(255, 224, 189);
                case 1: return GetColor(255, 205, 148);
                case 2: return GetColor(234, 192, 134);
                case 3: return GetColor(255, 173, 96);
                case 4: return GetColor(255, 227, 159);
                case 5: return GetColor(165, 114, 87);
                case 6: return GetColor(240, 200, 201);
                case 7: return GetColor(221, 168, 160);
                case 8: return GetColor(185, 124, 109);
                case 9: return GetColor(168, 117, 108);
                case 10: return GetColor(173, 100, 82);
                case 11: return GetColor(92, 56, 54);
                case 12: return GetColor(203, 132, 66);
                case 13: return GetColor(189, 114, 60);
                case 14: return GetColor(112, 65, 57);
            }

            return GetColor(255.0f, 224.0f, 189.0f);
        }

        public static Color GetRandomHairColour(out int index)
        {
            index = UnityEngine.Random.Range(0, HAIR_COLOUR_SIZE-1);
            return GetHairColour(index);
        }

        public static Color GetHairColour(int index)
        {
            switch (index)
            {
                case 0: return GetColor(009, 008, 006);
                case 1: return GetColor(044, 034, 043);
                case 2: return GetColor(113, 099, 090);
                case 3: return GetColor(183, 166, 158);
                case 4: return GetColor(214, 196, 194);
                case 5: return GetColor(202, 191, 177);
                case 6: return GetColor(220, 208, 186);
                case 7: return GetColor(255, 245, 225);
                case 8: return GetColor(230, 206, 168);
                case 9: return GetColor(229, 200, 168);
                case 10: return GetColor(222, 188, 153);
                case 11: return GetColor(184, 151, 120);
                case 12: return GetColor(165, 107, 070);
                case 13: return GetColor(181, 082, 057);
                case 14: return GetColor(141, 074, 067);
                case 15: return GetColor(145, 085, 061);
                case 16: return GetColor(083, 061, 050);
                case 17: return GetColor(059, 048, 036);
                case 18: return GetColor(085, 072, 056);
                case 19: return GetColor(078, 067, 063);
                case 20: return GetColor(080, 068, 068);
                case 21: return GetColor(106, 078, 066);
                case 22: return GetColor(167, 133, 106);
                case 23: return GetColor(151, 121, 097);
            }

            return GetColor(145, 085, 061);
        }

        public static Color GetRandomEyeColour(out int index)
        {
            index = UnityEngine.Random.Range(0, EYE_COLOUR_SIZE-1);
            return GetEyeColour(index);
        }

        public static Color GetEyeColour(int index)
        {
            switch (index)
            {
                case 0: return GetColor(53, 143, 64);
                case 1: return GetColor(41, 119, 245);
                case 2: return GetColor(24, 112, 173);
                case 3: return GetColor(15, 169, 155);
                case 4: return GetColor(48, 119, 169);
                case 5: return GetColor(191, 184, 169);
                case 6: return GetColor(191, 213, 175);
                case 7: return GetColor(239, 217, 27);
            }

            return GetColor(239, 217, 27);
        }

        public static Color GetRandomPastelColour(out int index)
        {
            index = UnityEngine.Random.Range(0, PASTEL_COLOUR_SIZE-1);
            return GetPastelColour(index);
        }

        public static Color GetPastelColour(int index)
        {
            switch (index)
            {
                case 0: return GetColor(227, 227, 227);
                case 1: return GetColor(223, 238, 216);
                case 2: return GetColor(237, 218, 218);
                case 3: return GetColor(214, 230, 241);
                case 4: return GetColor(248, 233, 207);
                case 5: return GetColor(229, 198, 175);
                case 6: return GetColor(196, 199, 162);
                case 7: return GetColor(140, 142, 143);
                case 8: return GetColor(190, 163, 147);
                case 9: return GetColor(147, 119, 114);
                case 10: return GetColor(132, 150, 182);
                case 11: return GetColor(143, 181, 164);
            }

            return GetColor(239, 217, 27);
        }

        // just a quick way to pass numbers ranging from 0-255 and get the Color expected range of 0 - 1
        private static Color GetColor(float R, float G, float B)
        {
            return new Color(R / 255.0f, G / 255.0f, B / 255.0f);
        }

    }
}
