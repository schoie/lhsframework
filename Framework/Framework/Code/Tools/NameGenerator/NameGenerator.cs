﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

namespace LHS.Framework.Tools
{
    public class NameGenerator : CSingleton<NameGenerator>
    {
        public enum eNamesType
        {
            Generic,
            Male,
            Female
        }

        private Dictionary<int, LoadViaTextAsset> LoadedNames;
        private Dictionary<string, NameColumn> Columns;

        NameColumn FirstNames = null;
        NameColumn FemaleNames = null;
        NameColumn MaleNames = null;
        NameColumn LastNames = null;

        public class NameColumn
        {
            public List<string> names;
            public NameColumn SubNamesList = null;

            public NameColumn()
            {
                names = new List<string>();
            }

            public string GetRandom()
            {
                int rand_index;
                return GetRandom(out rand_index);
            }
            public string GetRandom(out int index)
            {
                index = 0;
                if (names.Count == 0)
                    return "";
                index = UnityEngine.Random.Range(0, names.Count - 1);
                if (index < names.Count)
                    return names[index];
                return "";
            }
            public string GetName(int index)
            {
                if (index >= 0 && index < names.Count)
                    return names[index];
                return "";
            }

            public void AddName(string name)
            {
                if (name != null && name != "")
                {
                    names.Add(name);
                    if (SubNamesList != null)
                        SubNamesList.AddName(name);
                }
            }

        }

        public NameGenerator()
        {
            LoadedNames = new Dictionary<int, LoadViaTextAsset>();
            Columns = new Dictionary<string, NameColumn>();
        }
        
        public void Load(string filename, eDeliminators delim)
        {
            int hash = filename.GetHashCode();
            LoadViaTextAsset ToLoad;
            if (!LoadedNames.TryGetValue(hash, out ToLoad))
            {
                ToLoad = new LoadViaTextAsset();
                ToLoad.File = ResourceManager.instance.Load(filename) as TextAsset;
                if (ToLoad.File == null)
                    LHSDebug.LogErrorFormat("NameGenerator - Resources failed to load - {0}", filename);
                ToLoad.deliminator = delim;
                LoadedNames.Add(hash, ToLoad);
                LoadTextAsset(ToLoad.File.text, ToLoad.getDeliminator());
            }
        }

        public string GetName(string column)
        {
            NameColumn Col;
            if (Columns.TryGetValue(column, out Col))
            {
                return Col.GetRandom();
            }
            return "";
        }
        public string GetFirstName()
        {
            if(FirstNames != null)
            {
                return FirstNames.GetRandom();
            }
            return "";
        }
        public string GetMaleName()
        {
            if (MaleNames != null)
            {
                return MaleNames.GetRandom();
            }
            return "";
        }
        public string GetFemaleName()
        {
            if (FemaleNames != null)
            {
                return FemaleNames.GetRandom();
            }
            return "";
        }

        public string GetMaleName(out int index)
        {
            index = -1;
            if (MaleNames != null)
            {
                return MaleNames.GetRandom(out index);
            }
            return "";
        }
        public string GetFemaleName(out int index)
        {
            index = -1;
            if (FemaleNames != null)
            {
                return FemaleNames.GetRandom(out index);
            }
            return "";
        }

        public string GetFirstName(eNamesType type = eNamesType.Generic)
        {
            switch (type)
            {
                case eNamesType.Generic:
                    return GetFirstName();
                case eNamesType.Male:
                    return GetMaleName();
                case eNamesType.Female:
                    return GetFemaleName();
            }
            return "";
        }
        public string GetFirstName(out int index, eNamesType type = eNamesType.Generic)
        {
            index = -1;
            switch (type)
            {
                case eNamesType.Generic:
                    return GetFirstName(out index);
                case eNamesType.Male:
                    return GetMaleName(out index);
                case eNamesType.Female:
                    return GetFemaleName(out index);
            }
            return "";
        }
        public string GetFirstName(int index, eNamesType type = eNamesType.Generic)
        {
            switch (type)
            {
                case eNamesType.Generic:
                    return GetIndexedFirstName(index);
                case eNamesType.Male:
                    return GetIndexedMaleName(index);
                case eNamesType.Female:
                    return GetIndexedFemaleName(index);
            }
            return "";
        }

        public string GetLastName()
        {
            if (LastNames != null)
            {
                return LastNames.GetRandom();
            }
            return "";
        }
        public string GetFirstLastName(eNamesType type = eNamesType.Generic)
        {
            switch (type)
            {
                case eNamesType.Generic:
                    return GetGenericFullName();
                case eNamesType.Male:
                    return GetMaleFullName();
                case eNamesType.Female:
                    return GetFemaleFullName();
            }
            
            return "";
        }

        private string GetGenericFullName()
        {
            if (FirstNames != null && LastNames != null)
            {
                return FirstNames.GetRandom() + " " + LastNames.GetRandom();
            }
            return "";
        }

        private string GetMaleFullName()
        {
            if (MaleNames != null && LastNames != null)
            {
                return MaleNames.GetRandom() + " " + LastNames.GetRandom();
            }
            return "";
        }

        private string GetFemaleFullName()
        {
            if (FemaleNames != null && LastNames != null)
            {
                return FemaleNames.GetRandom() + " " + LastNames.GetRandom();
            }
            return "";
        }

        public string GetName(string column, out int index)
        {
            NameColumn Col;
            if (Columns.TryGetValue(column, out Col))
            {
                return Col.GetRandom(out index);
            }
            index = 0;
            return "";
        }
        public string GetFirstName(out int index)
        {
            if (FirstNames != null)
            {
                return FirstNames.GetRandom(out index);
            }
            index = 0;
            return "";
        }
        public string GetLastNamex(out int index)
        {
            if (LastNames != null)
            {
                return LastNames.GetRandom(out index);
            }
            index = 0;
            return "";
        }

        public string GetFirstLastNameWithIndecies(out int first, out int last)
        {
            if (FirstNames != null && LastNames != null)
            {
                return FirstNames.GetRandom(out first) + " " + LastNames.GetRandom(out last);
            }
            first = 0; last = 0;
            return "";
        }

        private void LoadTextAsset(string text, char delim)
        {

            List<NameColumn> TempColumns = new List<NameColumn>();

            int index = 0;
            int entriesIndex = 0;

            string[] lines = text.Split('\n');
            foreach (string line in lines)
            {

                string[] entries = line.Split(delim);

                if (entries.Length > 0)
                {
                    switch (entriesIndex)
                    {
                        case 0: // titles
                            
                            for (int i = 0; i < entries.Length; ++i)
                            {
                                NameColumn getCol;
                                if (!Columns.TryGetValue(entries[i], out getCol))
                                {
                                    getCol = new NameColumn();
                                    Columns.Add(entries[i], getCol);

                                    if (CultureInfo.CurrentCulture.CompareInfo.IndexOf(entries[i], "Male", CompareOptions.IgnoreCase) == 0)
                                    {
                                        MaleNames = getCol;
                                        SetFirstNameAsSubNameList(MaleNames);
                                    }
                                    else if (CultureInfo.CurrentCulture.CompareInfo.IndexOf(entries[i], "Female", CompareOptions.IgnoreCase) == 0)
                                    {
                                        FemaleNames = getCol;
                                        SetFirstNameAsSubNameList(FemaleNames);
                                    }
                                    else if (CultureInfo.CurrentCulture.CompareInfo.IndexOf(entries[i], "Surname", CompareOptions.IgnoreCase) == 0)
                                    {
                                        LastNames = getCol;
                                    }

                                   /* if (entries[i].Contains("Given"))
                                    {
                                        FirstNames = getCol;
                                    }
                                    else if (entries[i].Contains("Male"))
                                    {
                                        LastNames = getCol;
                                    }
                                    else if (entries[i].Contains("Female"))
                                    {
                                        LastNames = getCol;
                                    }
                                    else if (entries[i].Contains("Surname"))
                                    {
                                        LastNames = getCol;
                                    }*/
                                }

                                TempColumns.Add(getCol);
                            }

                            entriesIndex++;
                            break;
                        default:

                            // index 0 is label

                            for (int i = 0; i < entries.Length; ++i)
                            {
                                if (i < TempColumns.Count)
                                {
                                    TempColumns[i].AddName(Regex.Replace(entries[i], @"\s+", ""));
                                }
                            }

                            break;
                    }

                }

            }


        }

        private void SetFirstNameAsSubNameList(NameColumn nameList)
        {
            if (FirstNames == null)
                FirstNames = new NameColumn();

            nameList.SubNamesList = FirstNames;
        }



        public string GetIndexedFirstName(int index)
        {
            if (FirstNames != null)
            {
                return FirstNames.GetName(index);
            }
            return "";
        }
        public string GetIndexedMaleName(int index)
        {
            if (MaleNames != null)
            {
                return MaleNames.GetName(index);
            }
            return "";
        }
        public string GetIndexedFemaleName(int index)
        {
            if (FemaleNames != null)
            {
                return FemaleNames.GetName(index);
            }
            return "";
        }

    }
}
