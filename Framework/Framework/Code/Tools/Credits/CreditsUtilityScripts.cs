﻿using UnityEngine;
namespace LHS.Framework.Tools
{
    public static class CreditsUtilityScripts
    {


        /// <summary>
        /// Return a Vector3 based on the current vector with Z set to zero
        /// </summary>
        public static Vector3 ToVector3(this Vector2 vector)
        {
            return new Vector3(vector.x, vector.y, 0);
        }

    }
}