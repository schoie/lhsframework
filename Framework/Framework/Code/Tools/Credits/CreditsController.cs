﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System; 
using LHS.Framework.Tools;

namespace LHS.Framework.Tools
{
    /*
     * Controls the credits UI, spawning and deleting credit objects.
     */


    public delegate void CreditsCompleteEventHandler(object sender);

    public class CreditsController : MonoBehaviour
    {

        public class BaseCredits
        {

            public virtual GameObject CreatePrefab(CreditsController controller, Transform transform)
            {
                return null;
            }
        }

        public class TextCredits : BaseCredits
        {
            public string title;
            public string name;

            public TextCredits(string title, string name) : base()
            {
                this.title = title;
                this.name = name;
            }

            public override GameObject CreatePrefab(CreditsController controller, Transform transform)
            {
                if (controller.creditObjectPrefab != null)
                {
                    GameObject obj;
                    if (this.name != "")
                    {
                        if (controller.creditObjectPrefab == null)
                            return default(GameObject);

                        obj = GameObject.Instantiate(controller.creditObjectPrefab);
                    }
                    else
                    {
                        if (controller.creditMessageObjectPrefab == null)
                            return default(GameObject);

                        obj = GameObject.Instantiate(controller.creditMessageObjectPrefab);
                    }
                    obj.transform.SetParent(transform, false);
                    return obj;
                }
                return default(GameObject);
            }
        }

        public class ImageCredits : BaseCredits
        {
            public Sprite image;

            public ImageCredits(string resPath) : base()
            {
                image = ResourceManager.instance.Load<Sprite>(resPath) as Sprite;
            }

            public ImageCredits(Sprite _image) : base()
            {
                image = _image;
            }

            public override GameObject CreatePrefab(CreditsController controller, Transform transform)
            {
                if (controller.creditImageObjectPrefab != null)
                {
                    GameObject obj = GameObject.Instantiate(controller.creditImageObjectPrefab);
                    obj.transform.SetParent(transform, false);
                    return obj;
                }
                return default(GameObject);
            }
        }


        #region Variables
        public GameObject creditObjectPrefab;
        public GameObject creditImageObjectPrefab;
        public GameObject creditMessageObjectPrefab;
        List<BaseCredits> credits = new List<BaseCredits>();//replace with real credits later

        public float beginDelay = 0;
        public bool StopLastCredit = false;

        //mode variables
        public CreditType creditType = CreditType.Scrolling;
        public CreditGrouping creditGrouping = CreditGrouping.Multiple;

        //variabled for scroll mode
        public Vector2 scrollDirection = Vector2.up;
        public float scrollGroupCreditGap = 1;//Increases the gap between individual credits in group mode, scaled with speed.
        public float scrollSpeed = 10;

        //variables for the static mode
        public float staticFadeTime = 0.4f;//How long it takes to fade in and out a credit
        public float staticCreditDuration = 3f;//how long a single credit is shown on screen.
        public float staticCreditDelay = 0.6f;//how long the delay is between static credits.

        private Coroutine CreditsCo = null;
        bool nextCreditReady = false;//used by single group credits to decide when the next credit will appear
        public event CreditsCompleteEventHandler CreditsComplete;
        #endregion



        void Awake()
        {
            scrollDirection.Normalize();
        }

        public void CreateCreditsList()
        {
            int Title_Count = 0;
            while (true)
            {
                string title;
                if (!LocalisationReader.instance.GetLocalisedString("title_" + Title_Count.ToString("00"), out title))
                    break;

                int Name_Count = 0;
                string name = "";
                while (true)
                {
                    string newName;
                    if (!LocalisationReader.instance.GetLocalisedString("title_" + Title_Count.ToString("00") + "_name_" + Name_Count.ToString("00"), out newName))
                        break;

                    name += (newName + "\n");

                    Name_Count++;
                }

                if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(title))
                    break;

                credits.Add(new TextCredits(title, name));

                Title_Count++;
            }


        }

        public void AddImageCredit(string resPath)
        {
            credits.Add(new ImageCredits(resPath));
        }
        public void AddImageCredit(Sprite _image)
        {
            credits.Add(new ImageCredits(_image));
        }

        public void AddTextCredit(string title, string name)
        {
            credits.Add(new TextCredits(title, name));
        }

        public void AddTextCredit(string text)
        {
            credits.Add(new TextCredits(text, ""));
        }

        public void BeginCredits()
        {
            if (CreditsCo != null)
            {
                StopCoroutine(CreditsCo);
                CreditsCo = null;

                for (int i = 0; i < this.transform.childCount; ++i)
                {
                    GameObject child = this.transform.GetChild(i).gameObject;
                    DestroyObject(child);
                }
                this.transform.DetachChildren();
            }

            switch (creditType)
            {
                case CreditType.Scrolling:
                    CreditsCo = StartCoroutine(ScrollCredits());//scroll mode uses one coroutine
                    break;

                case CreditType.Static://static mode has two different coroutines
                    switch (creditGrouping)
                    {
                        case CreditGrouping.Single:
                            CreditsCo = StartCoroutine(StaticCreditsSingle());
                            break;

                        case CreditGrouping.Multiple:
                            CreditsCo = StartCoroutine(StaticCreditsMultiple());
                            break;
                    }
                    break;

            }

        }


        #region ModeCoroutines
        IEnumerator StaticCreditsSingle()
        {
            yield return new WaitForSeconds(beginDelay);


            foreach (var credit in credits)
            {
                float height;
                var creditObject = CreateCreditObject(credit, out height);
                SetupCreditObjectForStatic(creditObject);

                yield return new WaitUntil(() => nextCreditReady);//wait until the previous credit is off screen
                nextCreditReady = false;
                yield return new WaitForSeconds(staticCreditDelay);
                
            }

            CallEndEvent();
        }

        IEnumerator StaticCreditsMultiple()
        {
            yield return new WaitForSeconds(beginDelay);


            var screenRect = GetComponent<RectTransform>().rect;

            float yOffset = screenRect.height / (credits.Count + 1);//make all credits equaly distant from each other
            float yPos = screenRect.height / 2 - yOffset;



            foreach (var credit in credits)
            {
                float height;
                var creditObject = CreateCreditObject(credit, out height);
                SetupCreditObjectForStatic(creditObject);
                var creditPos = creditObject.GetComponent<RectTransform>().localPosition;

                creditPos.y = yPos;
                creditObject.GetComponent<RectTransform>().localPosition = creditPos;
                yPos -= yOffset;
            }

            yield return new WaitUntil(() => nextCreditReady);//wait until the previous credit is off screen
            CallEndEvent();//remove this if you want to add your own button 
        }

        /// Scroll Credits uses one function, with a delay added for single mode
        IEnumerator ScrollCredits()
        {
            yield return new WaitForSeconds(beginDelay);

            for (int i = 0; i < credits.Count; ++i)
            {
                float height;
                var creditObject = CreateCreditObject(credits[i], out height);
                SetupCreditObjectForScroll(creditObject, i == (credits.Count-1) && this.StopLastCredit);

                float gap = scrollGroupCreditGap + height;

                switch (creditGrouping)
                {
                    case CreditGrouping.Multiple:
                        yield return new WaitForSeconds(gap / scrollSpeed);//wait for the gap time.
                        break;

                    case CreditGrouping.Single:
                        yield return new WaitUntil(() => nextCreditReady);//wait til the currnet credit is offscreen
                        nextCreditReady = false;
                        break;
                }

            }
            CallEndEvent();

        }
        #endregion

        #region Setup Functions
        GameObject CreateCreditObject(BaseCredits credit, out float height)
        {
            GameObject creditObject = credit.CreatePrefab(this, this.transform);

            height = 0;
            ICredit CreditComp = creditObject.GetComponent<ICredit>();
            if (CreditComp != null)
            {
                height = CreditComp.Init(credit);//setup the text values
            }

            return creditObject;
        }

        void SetupCreditObjectForScroll(GameObject creditObject, bool lastCredit)
        {
            creditObject.GetComponent<RectTransform>().localPosition = getScrollCreditStartPosition();
            creditObject.GetComponent<ICredit>().InitScroll(scrollDirection, scrollSpeed, this, lastCredit);//setup the text values
        }

        void SetupCreditObjectForStatic(GameObject creditObject)
        {
            creditObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
            creditObject.GetComponent<ICredit>().InitStatic(staticFadeTime, staticCreditDuration, this);//setup the text values
        }

        Vector2 getScrollCreditStartPosition()
        {
            var creditObjectRect = creditObjectPrefab.GetComponent<RectTransform>().rect;

            var screenRect = GetComponent<RectTransform>().rect;

            Vector2 startPosition = new Vector2
                (
                    (screenRect.width / 2 + creditObjectRect.width / 2) * -scrollDirection.x,
                    (screenRect.height / 2 + creditObjectRect.height / 2) * -scrollDirection.y
                );

            return startPosition;
        }
        #endregion


        public void CreditComplete()
        {
            nextCreditReady = true;
        }


        /// <summary>
        /// Calls the end event
        /// </summary>
        public void CallEndEvent()
        {

            if (CreditsComplete != null)//stops the exception if nothing is connected to this.
            {
                CreditsComplete(this);
            }

        }


    }


    public enum CreditType { Scrolling, Static }
    public enum CreditGrouping { Single, Multiple }

}