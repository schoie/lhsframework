﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


namespace LHS.Framework.Tools
{
    public class OneCreditScript : MonoBehaviour, ICredit
    {

        float sizeFudge = 120f;//add a little bit of size to the test rect to make sure we never delete a credit object just appearing


        CreditsController creditsController;

        public Text CreditTitle;
        public Text CreditName;
        

        public float Init(CreditsController.BaseCredits credit)
        {
            RectTransform pRect;
            float height = 0;

            CreditsController.TextCredits textCredit = credit as CreditsController.TextCredits;
            if (textCredit != null)
            {

                if (CreditTitle != null)
                {
                    CreditTitle.text = textCredit.title;
                    pRect = CreditTitle.GetComponent<RectTransform>();
                    if (pRect != null)
                    {
                        float textheight = LayoutUtility.GetPreferredHeight(pRect);
                        if (textheight != 0)
                        {
                            pRect.sizeDelta = new Vector2(pRect.sizeDelta.x, textheight);
                            height += textheight;
                        }
                        else
                        {
                            height += pRect.sizeDelta.y;
                        }
                    }
                }

                if (CreditName != null)
                {
                    CreditName.text = textCredit.name;
                    pRect = CreditName.GetComponent<RectTransform>();
                    if (pRect != null)
                    {
                        float textheight = LayoutUtility.GetPreferredHeight(pRect);
                        if (textheight != 0)
                        {
                            pRect.sizeDelta = new Vector2(pRect.sizeDelta.x, textheight);
                            height += textheight;
                        }
                        else
                        {
                            height += pRect.sizeDelta.y;
                        }
                    }
                }
            }

            return height;
        }

        public void InitScroll(Vector2 direction, float speed, CreditsController controller, bool lastCredit)
        {
            creditsController = controller;
            StartCoroutine(CreditFunctions.Scroll(this.gameObject, controller, direction * speed, sizeFudge, lastCredit));
        }

        

        public void InitStatic(float fadeTime, float duration, CreditsController controller)
        {
            creditsController = controller;
            StartCoroutine(CreditFunctions.Fade(this, controller, duration, fadeTime));
        }
        

        public void SetFade(float fadeValue)
        {
            var c1 = CreditTitle.color;
            var c2 = CreditName.color;
            c1.a = fadeValue;
            c2.a = fadeValue;
            CreditTitle.color = c1;
            CreditName.color = c2;
        }
    }
}
