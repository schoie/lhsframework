﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


namespace LHS.Framework.Tools
{
    public class ImageCreditScript : MonoBehaviour, ICredit
    {

        float sizeFudge = 120f;//add a little bit of size to the test rect to make sure we never delete a credit object just appearing


        CreditsController creditsController;

        public Image image;


        public float Init(CreditsController.BaseCredits credit)
        {
            RectTransform pRect;
            float height = 0;

            CreditsController.ImageCredits imageCredit = credit as CreditsController.ImageCredits;
            if (imageCredit != null)
            {
                if (image != null && imageCredit.image != null)
                    image.sprite = imageCredit.image;

                pRect = image.GetComponent<RectTransform>();
                if (pRect != null)
                {
                    height = LayoutUtility.GetPreferredHeight(pRect);
                }
            }

            return height;
        }

        public void InitScroll(Vector2 direction, float speed, CreditsController controller, bool lastCredit)
        {
            creditsController = controller;
            StartCoroutine(CreditFunctions.Scroll(this.gameObject, controller, direction * speed, sizeFudge, lastCredit));
        }

        public void InitStatic(float fadeTime, float duration, CreditsController controller)
        {
            creditsController = controller;
            StartCoroutine(CreditFunctions.Fade(this, controller, duration, fadeTime));
        }


        public void SetFade(float fadeValue)
        {
            var c1 = image.color;
            c1.a = fadeValue;
            image.color = c1;
        }
    }
}
