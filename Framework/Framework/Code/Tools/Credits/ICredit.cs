﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Tools
{
    
    public interface ICredit
    {
        float Init(CreditsController.BaseCredits credit);

        void InitScroll(Vector2 direction, float speed, CreditsController controller, bool lastCredit);
        void InitStatic(float fadeTime, float duration, CreditsController controller);

        void SetFade(float fadeValue);
    }

    public abstract class CreditFunctions
    {

        static public IEnumerator Scroll(GameObject obj, CreditsController creditsController, Vector2 movementPerSecond, float sizeFudge, bool lastCredit = false)
        {
            if (obj != null && creditsController != null)
            {
                var rectTransform = obj.GetComponent<RectTransform>();
                var rectParentTransform = obj.transform.parent.GetComponent<RectTransform>();

                if (rectTransform != null && rectParentTransform != null)
                {
                    while (true)
                    {
                        rectTransform.localPosition += (movementPerSecond * Time.deltaTime).ToVector3();

                        if (lastCredit)
                        {
                            if (rectTransform.localPosition.y > (rectParentTransform.sizeDelta.y / 2))
                            {
                                Vector3 pos = rectTransform.localPosition;
                                pos.y = rectParentTransform.sizeDelta.y / 2;
                                rectTransform.localPosition = pos;

                                yield break;
                            }
                        }

                        //setup the rect for testing if we're still on screen
                        Rect testRect = new Rect(
                            rectTransform.localPosition.x - rectTransform.rect.width / 2 - sizeFudge / 2,
                            rectTransform.localPosition.y - rectTransform.rect.height / 2 - sizeFudge / 2,
                            rectTransform.rect.width + sizeFudge / 2,
                            rectTransform.rect.height + sizeFudge / 2
                                        );


                        //code for destroying the object
                        if (!rectParentTransform.rect.Overlaps(testRect))
                        {
                            GameObject.Destroy(obj);

                            creditsController.CreditComplete();//send a message back to the credits controller to ready the next credit.
                            break;
                        }
                        yield return null;
                    }
                }
            }

        }


        static public IEnumerator Fade(ICredit credit, CreditsController creditsController, float duration, float fadeTime)
        {
            if (credit != null && creditsController != null)
            {
                float fadeValue = 0;

                credit.SetFade(0);

                //fade in all the way
                while (fadeValue != 1)
                {
                    fadeValue = Mathf.Clamp01(fadeValue + Time.deltaTime / fadeTime);
                    credit.SetFade(fadeValue);
                    yield return null;
                }

                //keep the text there for the duration
                yield return new WaitForSeconds(duration);


                //fade out all the way
                while (fadeValue != 0)
                {
                    fadeValue = Mathf.Clamp01(fadeValue - Time.deltaTime / fadeTime);
                    credit.SetFade(fadeValue);
                    yield return null;
                }

                creditsController.CreditComplete();//send a message back to the credits controller to ready the next credit.
            }
        }

    }
}
