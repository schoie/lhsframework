﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace LHS.Framework.Tools.Texture
{

    public class TextureFromCanvas : MonoBehaviour
    {

        public Canvas canvas;

        public void Awake()
        {
            Renderer comp = this.GetComponent<Renderer>();

            Set();
            

        }

        public void Set()
        {

            //RenderTexture.active = myRenderTexture;

          /*  Texture2D myTexture2D = new Texture2D(200, 200);

            Color[] colors = new Color[10];
            for (int i = 0; i < 10; ++i)
            {
                colors[i] = new Color();
                colors[i] = Color.red;
            }
            myTexture2D.SetPixels(10, 10, 100, 100, colors);
            myTexture2D.Apply();
            */
            var myTexture2D = new Texture2D(2, 2, TextureFormat.RGBA32, false);

            // set the pixel values
            myTexture2D.SetPixel(0, 0, Color.red);
            myTexture2D.SetPixel(1, 0, Color.clear);
            myTexture2D.SetPixel(0, 1, Color.white);
            myTexture2D.SetPixel(1, 1, Color.black);

            // Apply all SetPixel calls
            myTexture2D.Apply();


            Renderer rend;
            rend = GetComponent<Renderer>();
            if(rend != null)
            {
                Renderer compcanvas = canvas.GetComponent<Renderer>();
                if (compcanvas != null)
                {
                    rend.material.SetTexture("_MainTex", compcanvas.material.GetTexture("_MainTex"));
                }
                else
                {
                    rend.material.SetTexture("_MainTex", myTexture2D);
                }
            }

        }


    }
}
