﻿/*
    LocalisationObject.cs -- Part of the Local system

    Name: Steven Ogden
    Date: 15/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LHS.Framework;
using LHS.Framework.Game;
using System.Text.RegularExpressions;

namespace LHS.Framework.Tools
{
    public class LocalisationVar
    {
        LocalFormatter formatter;
        public string variable;
        public string var;
        public string result;

        public LocalisationVar(string _variable, string _type, string _var)
        {
            variable = _variable;
            var = _var;
            result = "";
            
            formatter = null;
            LocalFormatters.instance.Get(_type, out formatter);
        }

        public string Get(string inString)
        {
            if (formatter != null && formatter.Format != null)
                return formatter.Format.Invoke(this, inString);

            return inString.Replace(variable, result);
        }

        public void Set(object data)
        {
            if (formatter != null && formatter.Set != null)
                formatter.Set.Invoke(this, data);
            else
                result = data.ToString();
        }
    }

    public class LocalisationString
    {
        public delegate string MyLocalGet();
        MyLocalGet MyGetter;

        string local;
        public Dictionary<int, LocalisationVar> Variables;

        public LocalisationString(string _local)
        {
            local = _local;

            MyGetter = GetBase;

            Variables = new Dictionary<int, LocalisationVar>();
            ProcessVariables();

            if (Variables.Count > 0)
                MyGetter = GetVars;
        }

        private void ProcessVariables()
        {

            MatchCollection matches = Regex.Matches(local, @"\$(\([^)]*)\)");

            char[] trim = new char[3];
            trim[0] = '$'; trim[1] = '('; trim[2] = ')';

            foreach (Match match in matches)
            {
                string possibleVariable = match.Value.Trim(trim);
                string[] varData = possibleVariable.Split(':');

                if(varData.Length == 2) // means we have a $(VARKEY:VARNAME) case
                {
                    Variables.Add(varData[1].GetHashCode(), new LocalisationVar(match.Value, varData[0], varData[1]));
                }

            }
        }

        public string Get()
        {
            return MyGetter.Invoke();
        }

        public string GetVars()
        {
            string outString = local;
            foreach (KeyValuePair<int, LocalisationVar> kvp in Variables)
            {
                outString = kvp.Value.Get(outString);
            }

            return Regex.Replace(outString, @"\s+", " ").Trim();
        }
        public string GetBase()
        {
            return local;
        }

        public void Set(int var_hash, object var)
        {
            LocalisationVar variable;
            if (Variables.TryGetValue(var_hash, out variable))
            {
                variable.Set(var);
            }
        }
    }

    public class LocalisationObject : CObject
    {
        public const string MISSING_TEXT_KEY = "Missing";

        public string Filename;
        public string Language;
        public int LabelAmount;

        public List<string> Labels;
        public List<string> Words;
        
        private Dictionary<int, LocalisationString> LocalisationStrings;

        public LocalisationObject()
        {
            LocalisationStrings = new Dictionary<int, LocalisationString>();

            Labels = new List<string>();
            Words = new List<string>();
        }

        public bool getLocalisationString(string label, out string localString)
        {
            return getLocalisationString(label.GetHashCode(), out localString);
        }
        public bool getLocalisationString(int label_Hash, out string localString)
        {
            LocalisationString local;
            if (LocalisationStrings.TryGetValue(label_Hash, out local))
            {
                localString = local.Get();
                return true;
            }
            localString = "";
            return false;
        }

        public void setLocalisationString(string label, string localString)
        {
            if (!Labels.Contains(label))
            {
                Labels.Add(label);
                if (localString == null || localString == "")
                    localString = MISSING_TEXT_KEY;
                Words.Add(localString);
            }
        }
        
        public void SetLocalisationStringVariable<type>(string label, string var, type value)
        {
            SetLocalisationStringVariable<type>(label.GetHashCode(), var.GetHashCode(), value);
        }
        public void SetLocalisationStringVariable<type>(int label_Hash, int var_hash, type value)
        {
            LocalisationString local;
            if (LocalisationStrings.TryGetValue(label_Hash, out local))
            {
                local.Set(var_hash, value);
            }
        }

        public void removeLocalisationString(string label, string localString, out bool removedLabel, out bool removedWord)
        {
            removedWord = false;
            removedLabel = Labels.Remove(label);
            if (removedLabel)
            {
                if (localString == null || localString == "")
                    localString = MISSING_TEXT_KEY;
                removedWord = Words.Remove(localString);
            }
        }

        public void Load()
        {
            // clear teh old load
            LocalisationStrings.Clear();

            // load latest set of labels and words
            for (int i = 0; (i < Labels.Count && i < Words.Count); ++i)
            {
                LocalisationStrings.Add(Labels[i].GetHashCode(), new LocalisationString(Words[i]));
            }
        }

        private string GetDebugOutStr()
        {
            return string.Format("DebugOut - Language: {0} - Labels: {1}, Words: {2}", this.Name, Labels.Count, Words.Count);
        }

        public void DebugOut()
        {
            LHSDebug.LogFormat(GetDebugOutStr());
        }

        public override string ToString()
        {
            string returnString = GetDebugOutStr();
            returnString += "\n";
            foreach (KeyValuePair<int, LocalisationString> pair in LocalisationStrings)
            {
                returnString += pair.Key.ToString();
                returnString += ": ";
                returnString += pair.Value.GetBase();
                returnString += ": ";
                returnString += pair.Value.Get();
                returnString += "\n";
            }
            return returnString;
        }
    }
}
