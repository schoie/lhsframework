﻿/*
    LocalisationObject.cs -- Part of the Local system

    Name: Steven Ogden
    Date: 15/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using LHS.Framework;

namespace LHS.Framework.Tools
{
    public enum eDeliminators
    {
        Comma = 0,
        Tab
    }

    [Serializable]
    public class LoadViaTextAsset
    {
        public TextAsset File;
        public eDeliminators deliminator = eDeliminators.Comma;

        public char getDeliminator()
        {
            switch (deliminator)
            {
                case eDeliminators.Comma:
                    return ',';
                case eDeliminators.Tab:
                    return '\t';
            }
            return ',';
        }
    }

    public class LocalHashFormatters
    {
        public string Hash;
        public string Text;

        public LocalHashFormatters(string _hash, string _text)
        {
            Hash = _hash;
            Text = _text;
        }

        public string ProcessString(string localString)
        {
            return localString.Replace(Hash, Text);
        }
    }

    public class LocalisationReader : CSingleton<LocalisationReader>
    {
        
        public LocalisationObject LoadedLocal { get; private set; }

        private List<LocalisationObject> Objects;
        private List<LocalHashFormatters> HashFormatters;
        private Dictionary<int, LoadViaTextAsset> LoadedLocalisation;

        public LocalisationReader()
        {
            Objects = new List<LocalisationObject>();
            HashFormatters = new List<LocalHashFormatters>();
            LoadedLocalisation = new Dictionary<int, LoadViaTextAsset>();

            LoadedLocal = null;
        }

        public void Load(string filename, eDeliminators delim)
        {
            int hash = filename.GetHashCode();
            LoadViaTextAsset loadedLocal;
            if(!LoadedLocalisation.TryGetValue(hash, out loadedLocal))
            {
                loadedLocal = new LoadViaTextAsset();
                loadedLocal.File = ResourceManager.instance.Load(filename) as TextAsset;
                if (loadedLocal.File == null)
                    LHSDebug.LogErrorFormat("Resources failed to load - {0}", filename);
                loadedLocal.deliminator = delim;
                LoadedLocalisation.Add(hash, loadedLocal);
                LoadTextAsset(loadedLocal.File.text, loadedLocal.getDeliminator());
            }

        }

        public void UnLoad(string filename)
        {
            int hash = filename.GetHashCode();
            LoadViaTextAsset loadedLocal;
            if (LoadedLocalisation.TryGetValue(hash, out loadedLocal))
            {
                UnLoadTextAsset(loadedLocal.File.text, loadedLocal.getDeliminator());
                LoadedLocalisation.Remove(hash);
                
                if (LoadedLocal != null)
                    LoadedLocal.Load();
            }

        }

        public string GetLocalisedString(string label)
        {
            string getString = "";
            LoadedLocal.getLocalisationString(label, out getString);
            return getString;
        }

        public string GetLocalisedString(int labelHash)
        {
            string getString = "";
            LoadedLocal.getLocalisationString(labelHash, out getString);
            return getString;
        }

        public bool GetLocalisedString(string label, out string local)
        {
            return LoadedLocal.getLocalisationString(label, out local);
        }

        public bool GetLocalisedString(int labelHash, out string local)
        {
            return LoadedLocal.getLocalisationString(labelHash, out local);
        }

        public void SetLocalisationStringVariable<T>(string label, string var, T value)
        {
            SetLocalisationStringVariable<T>(label.GetHashCode(), var.GetHashCode(), value);
        }
        public void SetLocalisationStringVariable<T>(int label_Hash, int var_hash, T value)
        {
            LoadedLocal.SetLocalisationStringVariable<T>(label_Hash, var_hash, value);
        }

        public void LoadLanguage(string name)
        {
            for (int i = 0; i < Objects.Count; ++i)
            {
                if (Objects[i].Language == name)
                {
                    LoadedLocal = Objects[i];
                    LoadedLocal.Load();
                    return;
                }
            }
        }

        public void OutputLanguage(string name)
        {
            for (int i = 0; i < Objects.Count; ++i)
            {
                if (Objects[i].Language == name)
                {
                    Objects[i].DebugOut();
                    return;
                }
            }
        }

        private void LoadTextAsset(string text, char delim)
        {

            int index = 0;
            int entriesIndex = 0;

            string[] lines = text.Split('\n');
            foreach (string line in lines)
            {

                string[] entries = line.Split(delim);

                // use these for debugging
                /*LHSDebug.LogFormat("Line: {0}", line);

                for (int i = 0; i < entries.Length; ++i)
                {
                    LHSDebug.LogFormat("Strings: {0}", entries[i]);
                }*/

                if (entries.Length > 0)
                {
                    switch (entriesIndex)
                    {
                        case 0: // languages

                            for (int i = 1; i < entries.Length; ++i)
                            {
                                if (!entries[i].Contains("Notes"))
                                {
                                    LocalisationObject objFind = Objects.Find(f => f.Language == entries[i]);
                                    if (objFind == null)
                                    {
                                        LocalisationObject obj = ScriptableObject.CreateInstance<LocalisationObject>();
                                        obj.Language = entries[i];
                                        Objects.Add(obj);
                                        //LHSDebug.LogFormat("ScriptableObject.CreateInstance<LocalisationObject>: {0}", obj.Language);
                                    }
                                }
                            }

                            entriesIndex++;
                            break;
                        default:

                            // index 0 is label
                            // LHSDebug.LogFormat("Label: {0}", entries[0]);

                            index = 0;
                            // following are language strings
                            for (int i = 1; i < entries.Length; ++i, index++)
                            {
                                if (index < Objects.Count && !entries[0].Contains("//"))
                                {
                                    //LHSDebug.LogFormat("i = {0}, index: {1}, label: {2} - Strings: {3} - ", i, index, entries[0], entries[i]);
                                    Objects[index].LabelAmount++;
                                    Objects[index].setLocalisationString(entries[0], ProcessString(entries[i]));
                                }
                            }

                            break;
                    }

                }

            }


        }

        private void UnLoadTextAsset(string text, char delim)
        {

            int index = 0;
            int entriesIndex = 0;

            string[] lines = text.Split('\n');
            foreach (string line in lines)
            {

                string[] entries = line.Split(delim);
                

                if (entries.Length > 0)
                {
                    switch (entriesIndex)
                    {
                        case 0: // languages
                            // do nothing
                            entriesIndex++;
                            break;
                        default:
                            
                            index = 0;
                            // following are language strings
                            for (int i = 1; i < entries.Length; ++i, index++)
                            {
                                if (index < Objects.Count && !entries[0].Contains("//"))
                                {
                                    bool removedLabel; bool removedWord;
                                    Objects[index].removeLocalisationString(entries[0], ProcessString(entries[i]), out removedLabel, out removedWord);
                                    if (removedLabel)
                                    {
                                        if(Objects[index].LabelAmount > 0)
                                            Objects[index].LabelAmount--;
                                    }
                                }
                            }

                            break;
                    }

                }

            }

        }

        public void RegisterHashFormatter(string _hash, string _text)
        {
            this.HashFormatters.Add(new LocalHashFormatters(_hash, _text));
        }

        private string ProcessString(string localString)
        {
            for (int i = 0; i < this.HashFormatters.Count; ++i)
                localString = this.HashFormatters[i].ProcessString(localString);

            return localString.Replace("#NL#", "\n");
        }

        private bool Load(string fileName)
        {
            // Handle any problems that might arise when reading the text
            try
            {
                string line;
                StreamReader theReader = new StreamReader(fileName, Encoding.Default);

                using (theReader)
                {
                    // While there's lines left in the text file, do this:
                    do
                    {
                        line = theReader.ReadLine();

                        if (line != null)
                        {
                            // Do whatever you need to do with the text line
                            string[] entries = line.Split(',');
                            if (entries.Length > 0)
                            {
                                // index 0 is label
                                LHSDebug.LogFormat("Label: {0}", entries[0]);

                                // following are language strings
                                for (int i = 1; i < entries.Length; ++i)
                                {
                                    LHSDebug.LogFormat("Strings: {0}", entries[i]);
                                }
                            }
                        }
                    }
                    while (line != null);
                    // Done reading, close the reader and return true 
                    theReader.Close();
                    return true;
                }
            }
            // If anything went wrong throw an exception
            catch (Exception e)
            {
                LHSDebug.LogErrorFormat("{0}\n", e.Message);
                return false;
            }
        }
    }
}
