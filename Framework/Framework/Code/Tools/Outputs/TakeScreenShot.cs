﻿/*
    TakeScreenShot.cs -- A generic script that will allow you to take a screenshot in game

    Name: Steven Ogden
    Date: 29/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Tools
{
    public class TakeScreenShot : MonoBehaviour
    {

        public KeyCode ScreenShotKey;
        public KeyCode PauseKey;
        public string Path = "Assets/";
        public string ScreenshotName = "Screenshot";

        [Header("This is used to control the resolution")]
        [Header("0 is default.")]
        public int SuperSize = 0;

        private int nameIncrementor = 0;

        public void Update()
        {
            if (Input.GetKeyUp(ScreenShotKey))
            {
                DoScreenShot();
            }

            if (Input.GetKeyUp(PauseKey))
            {
                if (Time.timeScale != 0f)
                    Time.timeScale = 0f;
                else
                    Time.timeScale = 1f;
            }
        }

        public void DoScreenShot()
        {
            Application.CaptureScreenshot(Path + ScreenshotName + "_" + nameIncrementor.ToString() + ".png", SuperSize);
            nameIncrementor++;
        }

    }
}
