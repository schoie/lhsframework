﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Tools
{
    public class RandomActivator : MonoBehaviour
    {
        [Range(0, 100)]
        public int chance;
        
        public void RunActivator()
        {
            int roll = UnityEngine.Random.Range(0, 100);
            this.gameObject.SetActive(roll <= chance);
        }

    }
}
