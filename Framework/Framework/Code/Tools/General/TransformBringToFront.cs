﻿/*
    TransformBringToFront.cs -- Set the transform to the last sibling to allow it to be rendered on top (bring to front)

    Date: 31/03/2016

    Provided as part of the Multiplayer Demo - Developed by Richard
*/

using UnityEngine;
using System.Collections;

namespace LHS.Framework.Tools
{
    public class TransformBringToFront : MonoBehaviour
    {

        void OnEnable()
        {
            transform.SetAsLastSibling();
        }
    }
}
