﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace LHS.Framework.Tools
{
    public enum eFadeObjectType
    {
        Image,
        Text,
        Sprite,
        Mesh,

        None,

        Error
    }

    [Serializable]
    public class FadeObject
    {
        float alpha;
        public eFadeObjectType Type = eFadeObjectType.None;
        public GameObject Object;
        [Range(0, 1)]
        public float FadeAmount;
        [Range(0, 1)]
        public float StartFade;
        public bool SetStartFade = false;
        public bool FadeChildren = false;

        private float scalableAlpha = 0;

        public void Init()
        {
            // locate scripts that must exist that we can fade
            switch (Type)
            {
                case eFadeObjectType.Image:
                    alpha = (SetStartFade) ? StartFade : GetGraphicAlpha<Image>();
                    break;
                case eFadeObjectType.Text:
                    alpha = (SetStartFade) ? StartFade : GetGraphicAlpha<Text>();
                    break;
                case eFadeObjectType.Sprite:
                    alpha = (SetStartFade) ? StartFade : GetSpriteAlpha();
                    break;
                case eFadeObjectType.Mesh:
                    alpha = (SetStartFade) ? StartFade : GetMeshAlpha();
                    break;
            }

            scalableAlpha = alpha * FadeAmount;
            alpha -= scalableAlpha;

            Fade(1);
        }
        public void Fade(float percent)
        {
            float newAlpha = alpha + (scalableAlpha * percent);
            // locate scripts that must exist that we can fade
            switch (Type)
            {
                case eFadeObjectType.Image:
                    FadeGraphic<Image>(newAlpha);
                    break;
                case eFadeObjectType.Text:
                    FadeGraphic<Text>(newAlpha);
                    break;
                case eFadeObjectType.Sprite:
                    FadeSprite(newAlpha);
                    break;
                case eFadeObjectType.Mesh:
                    FadeMesh(newAlpha);
                    break;
            }
        }


        //-------------------------------------------------------------------------------
        // INIT Functions
        //-------------------------------------------------------------------------------
        private float GetGraphicAlpha<TGraphic>() where TGraphic : Graphic
        {
            TGraphic grp = Object.GetComponent<TGraphic>();
            if (grp != null)
                return grp.color.a;
            else if (!FadeChildren)
                Type = eFadeObjectType.Error;
            return 0;
        }

        private float GetSpriteAlpha()
        {
            SpriteRenderer spr = Object.GetComponent<SpriteRenderer>();
            if (spr != null)
                return spr.color.a;
            else if (!FadeChildren)
                Type = eFadeObjectType.Error;
            return 0;
        }

        private float GetMeshAlpha()
        {
            Renderer rdr = Object.GetComponent<Renderer>();
            if (rdr != null)
                return rdr.material.color.a;
            else if(!FadeChildren)
                Type = eFadeObjectType.Error;
            return 0;
        }
        //-------------------------------------------------------------------------------


        //-------------------------------------------------------------------------------
        // FADE Functions
        //-------------------------------------------------------------------------------
        private void FadeGraphic<TGraphic>(float _alpha) where TGraphic : Graphic
        {
            TGraphic grp = Object.GetComponent<TGraphic>();
            if (grp != null)
            {
                Color newCol = grp.color;
                newCol.a = _alpha;
                grp.color = newCol;
            }

            if (FadeChildren)
            {
                TGraphic[] grps = Object.GetComponentsInChildren<TGraphic>(true);
                foreach(TGraphic child in grps)
                {
                    Color newCol = child.color;
                    newCol.a = _alpha;
                    child.color = newCol;
                }
            }
        }

        private void FadeSprite(float _alpha)
        {
            SpriteRenderer spr = Object.GetComponent<SpriteRenderer>();
            if (spr != null)
            {
                Color newCol = spr.color;
                newCol.a = _alpha;
                spr.color = newCol;
            }

            if (FadeChildren)
            {
                SpriteRenderer[] sprs = Object.GetComponentsInChildren<SpriteRenderer>(true);
                foreach (SpriteRenderer child in sprs)
                {
                    Color newCol = child.color;
                    newCol.a = _alpha;
                    child.color = newCol;
                }
            }
        }

        private void FadeMesh(float _alpha)
        {
            Renderer rdr = Object.GetComponent<Renderer>();
            if (rdr != null)
            {
                Color newCol = rdr.material.color;
                newCol.a = _alpha;
                rdr.material.color = newCol;
            }

            if (FadeChildren)
            {
                Renderer[] rdrs = Object.GetComponentsInChildren<Renderer>(true);
                foreach (Renderer child in rdrs)
                {
                    Color newCol = child.material.color;
                    newCol.a = _alpha;
                    child.material.color = newCol;
                }
            }
        }
        //-------------------------------------------------------------------------------

    }
}
