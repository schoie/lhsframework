﻿/*
    MeshBounds.cs -- This script will draw Mesh Bounds for a model in the scene view

    Name: Steven Ogden
    Date: 24/03/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Tools.Mesh
{
    
    public class MeshBounds : MonoBehaviour
    {
        public Renderer rend;
        void Start()
        {
            rend = GetComponent<Renderer>();
        }
        void OnDrawGizmosSelected()
        {
            Vector3 center = rend.bounds.center;
            float radius = rend.bounds.extents.magnitude;
            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(center, radius);
        }
    }
}
