﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using LHS.Framework;

namespace LHS.Framework.Cameras
{

    public class CameraRegister : CRegisterBehaviour<Camera, CameraManager>
    {
        public override void RegisterFunc(Camera register)
        {
            CameraManager.instance.Register(register);
        }

    }

    public class CameraManager : CRegisterManager<Camera, CameraManager>
    {

        // functions for use with canvases
        

    }

}
