﻿/*
    CameraFacingBillboard.cs -- Makes GameObject camera facing

    Name: Daniel Schofield 
    Date: 09/06/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System.Collections;

namespace LHS.Framework.Cameras
{
    public class CameraFacingBillboard : MonoBehaviour
    {
        public void Update()
        {
            CameraFace();
        }

        public void CameraFace()
        {
            transform.LookAt(transform.position + Camera.main.transform.rotation * Vector3.forward,
                Camera.main.transform.rotation * Vector3.up);
        }
    }
}