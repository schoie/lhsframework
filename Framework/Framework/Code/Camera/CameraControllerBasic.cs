﻿/*
    CameraControllerBasic.cs -- A specific yet basic camera script providing moveTo and rotateTo features

    Name: Steven Ogden
    Date: 15/04/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace LHS.Framework.Cameras
{
    public class CameraControllerBasic : MonoBehaviour
    {
        public float MovementTime;
        [Header("1 will snap")]
        [Header("0 will not move - Min is 0.1f")]
        [Range(0.1f, 1)]
        public float RotationSpeed = 0.1f;

        public UnityEvent OnMoveCompleted;
        public UnityEvent OnRotateCompleted;

        private Camera thisCamera;
        
        private Vector3 NewTargetPosition;
        private Quaternion NewTargetRotation;

        private float curMovementTime;

        bool updateMove = false;
        bool updateRotate = false;

        //    public void Awake()
        public void OnEnable()
        {
            // apply the updater func to the on value changed unity event
            thisCamera = this.GetComponent<Camera>();
            if (thisCamera == null)
            {
                LHSDebug.LogError("CameraControllerBasic - Please ensure this script is attached to a UnityEngine.UI.Camera");
                enabled = false;
                return;
            }
            
            NewTargetPosition = new Vector3();
            NewTargetRotation = new Quaternion();

            NewTargetPosition = thisCamera.gameObject.transform.position;
            NewTargetRotation = thisCamera.gameObject.transform.rotation;

            curMovementTime = MovementTime;
        }

        public void Update()
        {
            if(updateMove)
                DoMove();

            if (updateRotate)
                DoRotate();
        }

        public void TransformTo(GameObject obj)
        {
            MoveTo(obj.transform.position);
            RotateTo(obj.transform.rotation);
            curMovementTime = MovementTime;
        }
        public void TransformTo(Vector3 position, Quaternion rotation)
        {
            MoveTo(position);
            RotateTo(rotation);
            curMovementTime = MovementTime;
        }

        public void TransformTo(GameObject obj, out Vector3 oldPosition, out Quaternion oldRotation, float newMovementTime = 0f, bool setLastmovementComplete = false)
        {
            if (setLastmovementComplete)
                setLastMovementCompleted();

            setTransformSpeed(newMovementTime);

            getCurrentPositionRotation(out oldPosition, out oldRotation);

            MoveTo(obj.transform.position);
            RotateTo(obj.transform.rotation);
        }
        public void TransformTo(Vector3 position, Quaternion rotation, out Vector3 oldPosition, out Quaternion oldRotation, float newMovementTime = 0f, bool setLastmovementComplete = false)
        {
            if (setLastmovementComplete)
                setLastMovementCompleted();

            setTransformSpeed(newMovementTime);

            getCurrentPositionRotation(out oldPosition, out oldRotation);

            MoveTo(position);
            RotateTo(rotation);
        }

        public void MoveTo(Vector3 position)
        {
            NewTargetPosition = position;
            updateMove = true;
        }
        public void RotateTo(Quaternion rotation)
        {
            NewTargetRotation = rotation;
            updateRotate = true;
        }

        private void getCurrentPositionRotation(out Vector3 oldPosition, out Quaternion oldRotation)
        {
            // if we are still moving or rotating grab the target in which we are trying to get to
            oldPosition = (updateMove) ? NewTargetPosition : thisCamera.transform.position;
            oldRotation = (updateRotate) ? NewTargetRotation : thisCamera.transform.rotation;
        }

        private void setLastMovementCompleted()
        {
            thisCamera.transform.position = NewTargetPosition;
            thisCamera.transform.rotation = NewTargetRotation;
        }

        private void setTransformSpeed(float newMovementTime)
        {
            if (newMovementTime != 0)
                curMovementTime = newMovementTime;
            else
                curMovementTime = MovementTime;
        }

        private void DoMove()
        {
            thisCamera.transform.position = Vector3.MoveTowards(thisCamera.transform.position, NewTargetPosition, curMovementTime);

            if (Mathf.Abs(Vector3.Distance(thisCamera.transform.position, NewTargetPosition)) < 0.5f)
            {
                thisCamera.transform.position = NewTargetPosition;
                updateMove = false;
                OnMoveCompleted.Invoke();
            }
            else if (thisCamera.transform.position == NewTargetPosition)
            {
                updateMove = false;
                OnMoveCompleted.Invoke();
            }
        }
        private void DoRotate()
        {
            thisCamera.transform.rotation = Quaternion.Slerp(thisCamera.transform.rotation, NewTargetRotation, RotationSpeed);

            
            if (Mathf.Abs((thisCamera.transform.rotation.eulerAngles.magnitude - NewTargetRotation.eulerAngles.magnitude)) < 0.5f)
            {
                thisCamera.transform.rotation = NewTargetRotation;
                updateRotate = false;
                OnRotateCompleted.Invoke();
            }
            else if (thisCamera.transform.rotation == NewTargetRotation)
            {
                updateRotate = false;
                OnRotateCompleted.Invoke();
            }

        }
    }
}
