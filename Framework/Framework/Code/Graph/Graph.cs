﻿/*
    Graph.cs -- Holds data for a graph object

    Name: Tim Johnson
    Date: 02/05/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LHS.Framework.Graph
{
    public class Graph
    {
        public string graphTitle;

        public string xAxisTitle;
        public string yAxisTitle;

        public float xLowerBound;
        public float yLowerBound;

        public float xUpperBound;
        public float yUpperBound;

        public float xGridSteps;
        public float yGridSteps;

        public Graph()
        {
            graphTitle = "New Graph";

            xAxisTitle = "X Axis";
            yAxisTitle = "Y Axis";

            xLowerBound = 0f;
            yLowerBound = 0f;

            xUpperBound = 10f;
            yUpperBound = 10f;

            xGridSteps = 10f;
            yGridSteps = 10f;
        }
    }

    public class GraphElement
    {
        public string labelName;
    }
    public class Line : GraphElement
    {
        public List<Vector2> points = new List<Vector2>();
        public Color pointColour;
        public Color lineColour;

        public Line()
        {
            labelName = "Unknown";
            pointColour = Color.red;
            lineColour = Color.red;
        }
        public Line(string labelName, List<Vector2> points, Color pointColour, Color lineColour)
        {
            this.labelName = labelName;
            this.points = points;
            this.pointColour = pointColour;
            this.lineColour = lineColour;
        }
    }
    public class Points : Line // points is just like a line only with a transparent line
    {
        public Points()
        {
            labelName = "Unknown";
            this.pointColour = Color.red;
            this.lineColour = new Color(0f, 0f, 0f, 0f);
        }
        public Points(string labelName, List<Vector2> points, Color pointColour)
        {
            this.labelName = labelName;
            this.points = points;
            this.pointColour = pointColour;
            this.lineColour = new Color(0f, 0f, 0f, 0f);
        }
    }
}
