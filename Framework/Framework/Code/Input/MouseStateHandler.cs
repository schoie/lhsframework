﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Inputs
{

    public enum eMouseButtons
    {
        Left,
        Right,
        Middle,

        Size
    }

    public class MouseData : IInputData
    {

        public eMouseButtons id;
        public float duration;
        public Vector2 initialPosition;
        public Vector2 lastPosition;
        public Vector2 currentPosition;
        public Vector2 deltaPosition;
        public InputState State;

        public MouseData(eMouseButtons _id)
        {
            id = _id;
            this.initialPosition = Input.mousePosition;
            this.currentPosition = this.initialPosition;
            lastPosition = this.currentPosition;
            //deltaPosition.x = Input.GetAxis("Mouse X");
            //deltaPosition.y = Input.GetAxis("Mouse Y");

            this.duration = 0f;
            this.State = InputState.DownSingle;

        }

        public bool Tick()
        {
            switch (State)
            {
                case InputState.DownSingle:
                    State = InputState.Down;
                    goto case InputState.Down;
                case InputState.Down:

                    duration += Time.deltaTime;

                    if (duration > 0.5f)
                    {
                        State = InputState.Held;
                    }

                    goto case InputState.Held;

                case InputState.Held:

                    currentPosition = Input.mousePosition;
                    deltaPosition = currentPosition - lastPosition;
                    lastPosition = currentPosition;
                    //deltaPosition.x = Input.GetAxis("Mouse X");
                    //deltaPosition.y = Input.GetAxis("Mouse Y");

                    if (!Input.GetMouseButton((int)id))
                        State = (State == InputState.Held) ? InputState.Up: InputState.Tap;

                    break;
                default:
                    // nothing
                    return false;
                    break;
            }
            return true;
        }

        public int GetID()
        {
            return (int)id;
        }
        public InputState GetState()
        {
            return State;
        }

        public bool isUpState()
        {
            return (State == InputState.Up || State == InputState.Tap);
        }
        public bool isDownState()
        {
            return (State == InputState.DownSingle || State == InputState.Down || State == InputState.Held);
        }

        public Vector2 GetPosition()
        {
            return currentPosition;
        }
        public Vector2 GetInitialPosition()
        {
            return initialPosition;
        }
        public Vector2 GetDeltaPosition()
        {
            return deltaPosition;
        }

        public Vector2 GetVelocity()
        {
            return (duration == 0) ? new Vector2() : (currentPosition - initialPosition) / duration;
        }
        public Vector2 GetVelocityDifference(Vector2 last)
        {
            return (duration == 0) ? new Vector2() : (currentPosition - last) / duration;
        }
        public Vector2 GetPositionChange()
        {
            return (currentPosition - initialPosition);
        }
        public Vector2 GetPositionChange(Vector2 last)
        {
            return (currentPosition - last);
        }
        public bool HasMoved()
        {
            return initialPosition != currentPosition;
        }
        public bool HasMoved(float threshold)
        {
            Vector2 change = currentPosition - initialPosition;
            return change.magnitude > threshold;
        }
    }

    public class MouseStateHandler : CInputStateSystem<MouseData>
    {
        public MouseStateHandler() : base()
        {

        }

        public override void Tick()
        {
            base.Tick();

            // update mouse here
            MouseCheck();

        }

        public void MouseCheck()
        {
            // 3 because unity by default supports 3 moyuse buttons - Left, Right, Middle
            for (eMouseButtons i = 0; i < eMouseButtons.Size; ++i) 
            {
                // currently defaulting to left click
                if (Input.GetMouseButtonDown((int)i))
                {
                    this.InputData.Add(new MouseData(i));
                }
            }
        }

        public override IInputData GetInputData()
        {
            MouseData rM = GetData((int)eMouseButtons.Left);
            return rM;
        }

        public override eInputSystems GetType()
        {
            return eInputSystems.Mouse;
        }
    }
}
