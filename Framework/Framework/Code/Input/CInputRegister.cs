﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace LHS.Framework.Inputs
{
    public enum ButtonState
    {
        Down,
        Held,
        Up,

        Combo,

        None,
        Error

    }

    public interface IbuttonHandler
    {
        bool Check();
        void Process();
        void Clear();
        void AddEvent(UnityAction _action);
    }
    public class ButtonHandler : IbuttonHandler
    {
        UnityEvent Event;

        public ButtonHandler(UnityAction _action)
        {
            Event = new UnityEvent();
            AddEvent(_action);
        }

        public void AddEvent(UnityAction _action)
        {
            Event.AddListener(_action);
        }

        // must be overloaded
        virtual public bool Check() { return false; }
        
        public void Process()
        {
            Event.Invoke();
        }

        public void Clear()
        {
            Event.RemoveAllListeners();
        }
    }

    public interface IInputRegister
    {
        void Tick();

        void Register(ButtonState state, KeyCode key, UnityAction _action);
        void Register(ButtonState state, KeyCode key1, KeyCode key2, UnityAction _action);
        void Register(ButtonState state, string key, UnityAction _action);
        void Register(ButtonState state, string key1, string key2, UnityAction _action);
        void DeRegister(ButtonState state, KeyCode key);
        void DeRegister(ButtonState state, string key);
        void Clear(ButtonState state, KeyCode key);
        void Clear(ButtonState state, string key);
        eInputRegisters GetType();
    }

    

    // use for button management - Keyboard, controller
    public class CInputRegisterSystem : IInputRegister
    {

        public Dictionary<int, IbuttonHandler> handlers;

        public CInputRegisterSystem()
        {
            handlers = new Dictionary<int, IbuttonHandler>();
        }

        public void Tick()
        {
            foreach(KeyValuePair<int, IbuttonHandler> kvp in handlers)
            {
                if (kvp.Value.Check())
                    kvp.Value.Process();
            }
        }

        virtual public eInputRegisters GetType()
        {
            return eInputRegisters.Other;
        }

        virtual public void Register(ButtonState state, KeyCode key, UnityAction _action)
        {

        }
        virtual public void Register(ButtonState state, string key, UnityAction _action)
        {

        }

        virtual public void Register(ButtonState state, KeyCode key1, KeyCode key2, UnityAction _action)
        {

        }
        virtual public void Register(ButtonState state, string key1, string key2, UnityAction _action)
        {

        }

        public void DeRegister(ButtonState state, KeyCode key)
        {
            DeRegister(state, key.ToString());
        }
        public void DeRegister(ButtonState state, string key)
        {
            int Dickey = GetDictionaryKey(state, key);
            handlers.Remove(Dickey);
        }

        public void Clear(ButtonState state, KeyCode key)
        {
            Clear(state, key.ToString());
        }
        public void Clear(ButtonState state, string key)
        {
            int Dickey = GetDictionaryKey(state, key);
            IbuttonHandler bHandler;
            if (handlers.TryGetValue(Dickey, out bHandler))
            {
                bHandler.Clear();
            }
        }

        protected int GetDictionaryKey(ButtonState state, KeyCode key)
        {
            return GetDictionaryKey(state, key.ToString());
        }

        protected int GetDictionaryKey(ButtonState state, KeyCode key1, KeyCode key2)
        {
            return GetDictionaryKey(state, key1.ToString()+ key2.ToString());
        }

        protected int GetDictionaryKey(ButtonState state, string key)
        {
            return (state.ToString() + key).GetHashCode();
        }

    }
}
