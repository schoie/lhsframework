﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Inputs
{

    public class TouchData : IInputData
    {
        public int id;
        public Touch touch;
        public float duration;
        public Vector2 initialPosition;
        public Vector2 currentPosition;
        public Vector2 deltaPosition;
        public InputState State;

        public TouchData(int id, Touch touch)
        {
            this.id = id;
            this.touch = touch;
            
            this.initialPosition = touch.position;
            this.currentPosition = this.initialPosition;

            this.duration = 0f;
            this.State = InputState.DownSingle;

        }

        public bool Tick()
        {
            switch (State)
            {
                case InputState.DownSingle:
                    State = InputState.Down;
                    goto case InputState.Down;
                case InputState.Down:

                    currentPosition = touch.position;
                    duration += Time.deltaTime;

                    if (duration > /*heldThreshold*/ 0.5f)
                    {
                        State = InputState.Held;
                    }
                    break;

                case InputState.Held:

                    currentPosition = touch.position;

                    break;
                default:
                    // nothing
                    return false;
                    break;
            }
            return true;
        }

        public void HandleReleased()
        {
            switch (State)
            {
                case InputState.DownSingle:
                case InputState.Down:
                    State = InputState.Tap;
                    break;
                case InputState.Held:
                    State = InputState.Up;
                    break;
                default:
                    State = InputState.Error;
                    break;
            }
        }
        
        public int GetID()
        {
            return id;
        }
        public InputState GetState()
        {
            return State;
        }

        public bool isUpState()
        {
            return (State == InputState.Up || State == InputState.Tap);
        }
        public bool isDownState()
        {
            return (State == InputState.DownSingle || State == InputState.Down || State == InputState.Held);
        }

        public Vector2 GetPosition()
        {
            return currentPosition;
        }
        public Vector2 GetInitialPosition()
        {
            return initialPosition;
        }
        public Vector2 GetDeltaPosition()
        {
            return deltaPosition;
        }

        public Vector2 GetVelocity()
        {
            return (duration == 0) ? new Vector2() : (currentPosition - initialPosition) / duration;
        }
        public Vector2 GetVelocityDifference(Vector2 last)
        {
            return (duration == 0) ? new Vector2() : (currentPosition - last) / duration;
        }
        public Vector2 GetPositionChange()
        {
            return (currentPosition - initialPosition);
        }
        public Vector2 GetPositionChange(Vector2 last)
        {
            return (currentPosition - last);
        }
        public bool HasMoved()
        {
            return initialPosition != currentPosition;
        }
        public bool HasMoved(float threshold)
        {
            Vector2 change = currentPosition - initialPosition;
            return change.magnitude > threshold;
        }

    }

    public class TouchStateHandler : CInputStateSystem<TouchData>
    {


        public TouchStateHandler() : base()
        {

        }

        public override void Tick()
        {
            base.Tick();

            // update touchese here
            TouchesCheck();

        }


        public void TouchesCheck()
        {
            
            Touch[] touches = Input.touches;

            foreach (Touch touch in touches)
            {
                switch (touch.phase)
                {
                    case TouchPhase.Began:

                        CreateTouch(touch.fingerId, touch);
                        break;
                    case TouchPhase.Moved:
                    case TouchPhase.Stationary:
                        UpdateTouchesMovement(touch);
                        break;
                    case TouchPhase.Canceled:
                    case TouchPhase.Ended:
                        UpdateTouchesReleased(touch);
                        break;

                }
            }
        }

        public void CreateTouch(int id, Touch touch)
        {
            TouchData tData = new TouchData(id, touch);
            this.InputData.Add(tData);
        }

        public void UpdateTouchesMovement(Touch touch)
        {
            TouchData rT = GetData(touch.fingerId);

            rT.currentPosition = touch.position;
            rT.deltaPosition = touch.deltaPosition;
        }

        public void UpdateTouchesReleased(Touch touch)
        {
            TouchData rT = GetData(touch.fingerId);

            rT.HandleReleased();
        }

        public override eInputSystems GetType()
        {
            return eInputSystems.Touch;
        }

    }
}
