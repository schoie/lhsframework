﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Inputs
{
    public interface IHandleRayInput
    {
        void HandleRayInput(RaycastHit hit, ref LayerMask curMask);
        void HandleNoInput(ref LayerMask curMask);
    }
}
