﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Inputs
{
    public enum InputState
    {
        DownSingle,
        Down,
        Held,
        Tap,
        Up,

        DoubleTap,

        None, // if there is no data we will return None

        Error // if something goes wron in the update we will set the state as error and it will be cleaned up next Tick
    }

    public interface IInputData
    {
        bool Tick();

        int GetID();
        InputState GetState();

        bool isUpState();
        bool isDownState();

        Vector2 GetPosition();
        Vector2 GetInitialPosition();
        Vector2 GetDeltaPosition();

        Vector2 GetVelocity();
        Vector2 GetVelocityDifference(Vector2 last);
        Vector2 GetPositionChange();
        Vector2 GetPositionChange(Vector2 last);

        bool HasMoved();
        bool HasMoved(float threshold);
    }

    public interface IInputSystem
    {
        void Tick();

        IInputData GetInputData();
        IInputData GetInputData(int id);
        IInputData[] GetDoubleInputData();
        IInputData[] GetDoubleInputDataSafe();
        IInputData[] GetInputDataRange(int range);
        IInputData[] GetInputDataRangeSafe(int range);
        IInputData[] GetAllInputData();

        bool GetSingleInput();
        bool GetDoubleInput();
        bool GetMultiInput(int min);
        bool GetNoInput();
        int GetInputAmount();

        void Reset();

        eInputSystems GetType();
    }

    // use for state management - Mouse, touch
    public class CInputStateSystem<Data> : IInputSystem where Data : IInputData
    {

        public List<Data> InputData;

        public CInputStateSystem()
        {
            InputData = new List<Data>();
        }

        virtual public void Tick()
        {
            InputData.RemoveAll(delegate (Data data) {
                return !data.Tick();
            });
        }

        virtual public IInputData GetInputData()
        {
            if (InputData.Count > 0)
                return InputData[0] as IInputData;
            return null;
        }
        public IInputData GetInputData(int id)
        {
            if (InputData.Count > id)
                return InputData[id] as IInputData;
            return null;
        }
        public IInputData[] GetDoubleInputData()
        {
            return InputData.GetRange(0, 2).ToArray() as IInputData[];
        }
        public IInputData[] GetDoubleInputDataSafe()
        {
            if (InputData.Count > 0)
                return InputData.GetRange(0, 2).ToArray() as IInputData[];
            else
                return null;
        }
        public IInputData[] GetInputDataRange(int range)
        {
            return InputData.GetRange(0, range).ToArray() as IInputData[];
        }
        public IInputData[] GetInputDataRangeSafe(int range)
        {
            if (InputData.Count > range)
                return InputData.GetRange(0, range).ToArray() as IInputData[];
            else
                return null;
        }
        public IInputData[] GetAllInputData()
        {
            return InputData.ToArray() as IInputData[];
        }

        public Data GetData(int id)
        {
            foreach (Data data in InputData)
            {
                if (data.GetID() == id)
                {
                    return data;
                }
            }
            return default(Data);
        }

        // interface functions
        public bool GetSingleInput()
        {
            return InputData.Count == 1;
        }

        public bool GetDoubleInput()
        {
            return InputData.Count == 2;
        }

        public bool GetMultiInput(int min)
        {
            return InputData.Count >= min;
        }

        public bool GetNoInput()
        {
            return InputData.Count <= 0;
        }

        public int GetInputAmount()
        {
            return InputData.Count;
        }

        virtual public eInputSystems GetType()
        {
            return eInputSystems.Other;
        }

        public void Reset()
        {
            InputData.Clear();
        }

    }
    
}
