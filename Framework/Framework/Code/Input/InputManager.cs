﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Inputs
{
    public enum eInputSystems
    {
        Touch,
        Mouse,

        Other
    }

    public enum eInputRegisters
    {
        Pad,
        KeyBoard,

        Other
    }

    public class InputManager : CSingleton<InputManager>
    {
        // Register IInputSystems
        Dictionary<eInputSystems, IInputSystem> Systems;
        
        // Register IInputRegisters
        Dictionary<eInputRegisters, IInputRegister> Registers;

        // Handle Ray Input System
        Dictionary<int, List<IHandleRayInput>> Handles;
        //LayerMask InactiveLayers = new LayerMask();

        private bool SystemsOperatingFlag = true;
        private bool RegistersOperatingFlag = true;

        public InputManager()
        {
            Handles = new Dictionary<int, List<IHandleRayInput>>();
            Systems = new Dictionary<eInputSystems, IInputSystem>();
            Registers = new Dictionary<eInputRegisters, IInputRegister>();
        }


        //------------------------------------------------------------------------
        // IHandleRayInput Functions
        //------------------------------------------------------------------------
        public void Register(LayerMask mask, IHandleRayInput value)
        {
            for(int i = 0; i < 32; ++i)
            {
                if (mask.value == (mask.value | (1 << i)))
                {
                    Register(i, value);
                }
            }
        }
        public void Register(string layer, IHandleRayInput value)
        {
            Register(LayerMask.NameToLayer(layer), value);
        }

        public void Register(int layer, IHandleRayInput value)
        {
            if (Handles.ContainsKey(layer))
            {
                List<IHandleRayInput> addedList;
                Handles.TryGetValue(layer, out addedList);
                if (addedList != null)
                {
                    addedList.Add(value);
                }
            }
            else
            {
                List<IHandleRayInput> newList = new List<IHandleRayInput>();
                newList.Add(value);
                Handles.Add(layer, newList);
            }
        }

        public void DeRegister(LayerMask mask, IHandleRayInput value)
        {
            for (int i = 0; i < 32; ++i)
            {
                if (mask.value == (mask.value | (1 << i)))
                {
                    DeRegister(i, value);
                }
            }
        }
        public void DeRegister(string layer, IHandleRayInput value)
        {
            DeRegister(LayerMask.NameToLayer(layer), value);
        }

        public void DeRegister(int layer, IHandleRayInput value)
        {
            if (Handles.ContainsKey(layer))
            {
                List<IHandleRayInput> removeList;
                Handles.TryGetValue(layer, out removeList);
                if (removeList != null)
                {
                    if (removeList.Contains(value))
                    {
                        removeList.Remove(value);
                    }
                }
            }
        }

        public List<IHandleRayInput> Get(int key)
        {
            List<IHandleRayInput> toGet;
            Handles.TryGetValue(key, out toGet);
            return toGet;
        }
        public List<IHandleRayInput> GetSafe(int key)
        {
            List<IHandleRayInput> toGet;
            if (Handles.TryGetValue(key, out toGet))
                return toGet;
            return default(List<IHandleRayInput>);
        }
        public bool Get(int key, out List<IHandleRayInput> toGet)
        {
            if (Handles.TryGetValue(key, out toGet))
                return true;
            return false;
        }

        public List<IHandleRayInput> Get(string layer)
        {
            int key = LayerMask.NameToLayer(layer);
            List<IHandleRayInput> toGet;
            Handles.TryGetValue(key, out toGet);
            return toGet;
        }
        public List<IHandleRayInput> GetSafe(string layer)
        {
            int key = LayerMask.NameToLayer(layer);
            List<IHandleRayInput> toGet;
            if (Handles.TryGetValue(key, out toGet))
                return toGet;
            return default(List<IHandleRayInput>);
        }
        public bool Get(string layer, out List<IHandleRayInput> toGet)
        {
            int key = LayerMask.NameToLayer(layer);
            if (Handles.TryGetValue(key, out toGet))
                return true;
            return false;
        }

        // handle actual input
        public bool Handle(Camera cam, ref LayerMask layerMask)
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            bool hitDetected = Physics.Raycast(ray, out hit, cam.farClipPlane, layerMask.value);

            // FOund Grid Unit
            if (hitDetected)
            {
                List<IHandleRayInput> hitHandlers;
                if (Get(hit.collider.gameObject.layer, out hitHandlers))
                {
                    foreach(IHandleRayInput handle in hitHandlers)
                    {
                        handle.HandleRayInput(hit, ref layerMask);
                    }
                }
            }
            else
            {
                foreach (KeyValuePair<int, List<IHandleRayInput>> kvp in Handles)
                {
                    if (layerMask.value == (layerMask.value | (1 << kvp.Key)))
                    {
                        foreach (IHandleRayInput handle in kvp.Value)
                        {
                            handle.HandleNoInput(ref layerMask);
                        }
                    }
                }
            }

            return hitDetected;
        }

        public bool HandleNoRef(Camera cam, ref LayerMask layerMask)
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            bool hitDetected = Physics.Raycast(ray, out hit, cam.farClipPlane, layerMask.value);
            LayerMask layerMaskLocal = new LayerMask();

            // FOund Grid Unit
            if (hitDetected)
            {
                List<IHandleRayInput> hitHandlers;
                if (Get(hit.collider.gameObject.layer, out hitHandlers))
                {
                    foreach (IHandleRayInput handle in hitHandlers)
                    {
                        handle.HandleRayInput(hit, ref layerMaskLocal);
                    }
                }
            }
            else
            {
                foreach (KeyValuePair<int, List<IHandleRayInput>> kvp in Handles)
                {
                    if (layerMask.value == (layerMask.value | (1 << kvp.Key)))
                    {
                        foreach (IHandleRayInput handle in kvp.Value)
                        {
                            handle.HandleNoInput(ref layerMaskLocal);
                        }
                    }
                }
            }

            return hitDetected;
        }


        public void HandleInactive(ref LayerMask layerMask)
        {
            foreach (KeyValuePair<int, List<IHandleRayInput>> kvp in Handles)
            {
                foreach (IHandleRayInput handle in kvp.Value)
                {
                    handle.HandleNoInput(ref layerMask);
                }
            }
        }

        // helper functions
        public void SetLayerMask(ref LayerMask curMask, int layer)
        {
            curMask |= (1 << layer);
        }
        public void SetInactiveLayers(ref LayerMask curMask, LayerMask inactiveLayerMask)
        {
           // this.InactiveLayers = inactiveLayerMask;
            SetLayerMaskAllOn(ref curMask);
        }
        public void SetLayerMask(ref LayerMask curMask, string layer)
        {
            curMask |= (1 << LayerMask.NameToLayer(layer));
        }
        public void SetLayerMask(ref LayerMask curMask, int[] layers)
        {
            foreach(int layer in layers)
                curMask |= (1 << layer);
        }
        public void SetLayerMask(ref LayerMask curMask, string[] layers)
        {
            foreach (string layer in layers)
                curMask |= (1 << LayerMask.NameToLayer(layer));
        }
        public void SetLayerMaskAllOn(ref LayerMask curMask)
        {
            curMask = int.MaxValue;
            //curMask ^= InactiveLayers;
        }
        public void SetLayerMaskAllOff(ref LayerMask curMask)
        {
            curMask = 0;
        }
        public void ClearInactiveLayers(ref LayerMask curMask, LayerMask inactiveLayerMask)
        {
            // find teh bytes that are on in inactive layers
            byte[] bytes = BitConverter.GetBytes(inactiveLayerMask.value);

            int bitPos = 0;
            while (bitPos < 8 * bytes.Length)
            {
                int byteIndex = bitPos / 8;
                int offset = bitPos % 8;
                bool isSet = (bytes[byteIndex] & (1 << offset)) != 0;

                // isSet = [True] if the bit at bitPos is set, false otherwise
                if (isSet)
                {
                    // if we find one use the index to see if curmask aslo has this on
                    // if so turn it off without affecting any other bits
                    int bitIndex = (byteIndex * 8) + offset;
                    if ((curMask.value & (1 << bitIndex)) != 0)
                    {
                        curMask ^= (1 << bitIndex);
                    }
                    
                }

                bitPos++;
            }

        }
        //------------------------------------------------------------------------

        //------------------------------------------------------------------------
        // IInputSystem Functions
        //------------------------------------------------------------------------

        public IInputSystem Register(eInputSystems system)
        {
            IInputSystem returnSystem = default(IInputSystem);
            if (!Systems.ContainsKey(system))
            {
                switch (system)
                {
                    case eInputSystems.Touch:
                        returnSystem = new TouchStateHandler();
                        Systems.Add(system, returnSystem);
                        break;
                    case eInputSystems.Mouse:
                        returnSystem = new MouseStateHandler();
                        Systems.Add(system, returnSystem);
                        break;
                }
                return returnSystem;
            }
            LHSDebug.LogWarning("System already Registered - Default(IInputSystem) has been returned for eInputSystems type: " + system);
            return GetSystem(system);
        }

        public void DeRegister(eInputSystems system)
        {
            if (Systems.ContainsKey(system))
                Systems.Remove(system);
        }

        public void EnableSystems()
        {
            SystemsOperatingFlag = true;
        }
        public void DisableSystems()
        {
            SystemsOperatingFlag = false;
        }

        public void TickSystems()
        {
            if (SystemsOperatingFlag)
                foreach (KeyValuePair<eInputSystems, IInputSystem> kvp in Systems)
                    kvp.Value.Tick();
        }

        public IInputSystem GetSystem(eInputSystems system)
        {
            IInputSystem isystem;
            Systems.TryGetValue(system, out isystem);
            return isystem;
        }
        public bool GetSystem(eInputSystems system, out IInputSystem isystem)
        {
            if (Systems.TryGetValue(system, out isystem))
                return true;
            return false;
        }

        //------------------------------------------------------------------------

        //------------------------------------------------------------------------
        // IInputSystem Functions
        //------------------------------------------------------------------------

        public IInputRegister Register(eInputRegisters register)
        {
            IInputRegister returnRegister = default(IInputRegister);
            if (!Registers.ContainsKey(register))
            {
                switch (register)
                {
                    case eInputRegisters.Pad:
                        returnRegister = new PadRegisterHandler();
                        Registers.Add(register, returnRegister);
                        break;
                    case eInputRegisters.KeyBoard:
                        returnRegister = new KeyboardRegisterHandler();
                        Registers.Add(register, returnRegister);
                        break;
                }
                return returnRegister;
            }
            LHSDebug.LogWarning("Register already Registered - Default(IInputRegister) has been returned for eInputRegisters type: " + register);
            return GetRegister(register);
        }

        public void DeRegister(eInputRegisters register)
        {
            if (Registers.ContainsKey(register))
                Registers.Remove(register);
        }

        public void EnableRegisters()
        {
            RegistersOperatingFlag = true;
        }
        public void DisableRegisters()
        {
            RegistersOperatingFlag = false;
        }

        public void TickRegisters()
        {
            if(RegistersOperatingFlag)
                foreach (KeyValuePair<eInputRegisters, IInputRegister> kvp in Registers)
                    kvp.Value.Tick();
        }

        public IInputRegister GetRegister(eInputRegisters register)
        {
            IInputRegister iregister;
            Registers.TryGetValue(register, out iregister);
            return iregister;
        }
        public bool GetRegister(eInputRegisters register, out IInputRegister iregister)
        {
            if (Registers.TryGetValue(register, out iregister))
                return true;
            return false;
        }
        //------------------------------------------------------------------------

    }


}
