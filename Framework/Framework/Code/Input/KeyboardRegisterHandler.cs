﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace LHS.Framework.Inputs
{
    public class KeyboardRegisterHandler : CInputRegisterSystem
    {
        public class DownHandler : ButtonHandler
        {
            KeyCode Key;

            public DownHandler(KeyCode _key, UnityAction _action) : base(_action)
            {
                Key = _key;
            }

            public override bool Check()
            {
                return Input.GetKeyDown(Key);
            }

        }
        public class HeldHandler : ButtonHandler
        {
            KeyCode Key;

            public HeldHandler(KeyCode _key, UnityAction _action) : base(_action)
            {
                Key = _key;
            }

            public override bool Check()
            {
                return Input.GetKey(Key);
            }

        }
        public class UpHandler : ButtonHandler
        {
            KeyCode Key;

            public UpHandler(KeyCode _key, UnityAction _action) : base(_action)
            {
                Key = _key;
            }

            public override bool Check()
            {
                return Input.GetKeyUp(Key);
            }

        }

        public class ComboHandler : ButtonHandler
        {
            KeyCode KeyHeld;
            KeyCode KeyCombo;

            public ComboHandler(KeyCode _keyHeld, KeyCode _keyCombo, UnityAction _action) : base(_action)
            {
                KeyHeld = _keyHeld;
                KeyCombo = _keyCombo;
            }

            public override bool Check()
            {
                return Input.GetKey(KeyHeld) && Input.GetKeyDown(KeyCombo);
            }

        }

        public override void Register(ButtonState state, KeyCode key, UnityAction _action)
        {
            int Dickey = GetDictionaryKey(state, key);
            IbuttonHandler bHandler;
            if (handlers.TryGetValue(Dickey, out bHandler))
            {
                bHandler.AddEvent(_action);
            }
            else
            {

                switch (state)
                {
                    case ButtonState.Down:
                        handlers.Add(Dickey, new DownHandler(key, _action));
                        break;
                    case ButtonState.Held:
                        handlers.Add(Dickey, new HeldHandler(key, _action));
                        break;
                    case ButtonState.Up:
                        handlers.Add(Dickey, new UpHandler(key, _action));
                        break;
                }
            }
        }

        public override void Register(ButtonState state, KeyCode key1, KeyCode key2, UnityAction _action)
        {
            int Dickey = GetDictionaryKey(state, key1, key2);
            IbuttonHandler bHandler;
            if (handlers.TryGetValue(Dickey, out bHandler))
            {
                bHandler.AddEvent(_action);
            }
            else
            {

                switch (state)
                {
                    case ButtonState.Combo:
                        handlers.Add(Dickey, new ComboHandler(key1, key2, _action));
                        break;
                }
            }
        }

    }
}
