﻿/*
    CSingleton.cs

    Name: Steven Ogden
    Date: Unknown

    Provided by Steven Ogden copyright free as part of the GNU license.
*/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine.UI;

namespace LHS
{
    namespace Framework
    {
        namespace Audio
        {
            public enum eSoundTypes
            {
                SFX,
                Music,

                Size
            }

            public class SoundManager : CSingleton<SoundManager>
            {

                public class AudioObject
                {
                    protected AudioSource Source;
                    GameObject Obj;

                    public AudioObject(GameObject _obj)
                    {
                        Obj = _obj;
                        Source = Obj.GetComponent<AudioSource>();
                        SoundManager.LoadedAudioSources.Add(Source);
                        
                    }

                    public virtual void Play() { }
                    public virtual void Stop() { }
                    public virtual bool IsLoop() { return false; }

                    public void Destroy()
                    {
                        if(Obj != null)
                        {
                            GameObject.DestroyObject(Obj);
                            Obj = null;
                        }
                    }

                    public void SetVolume(float _volume)
                    {
                        Source.volume = _volume;
                    }

                    public void Mute(bool mute)
                    {
                        Source.mute = mute;
                    }

                    public bool CompareSource(AudioSource toCompare)
                    {
                        return Source == toCompare;
                    }
                }

                public class AudiObjectOneshot : AudioObject
                {
                    AudioClip Clip;

                    public AudiObjectOneshot(string _clip, GameObject _obj)
                        : base(_obj)
                    {
                        Clip = (AudioClip)ResourceManager.instance.Load(_clip, typeof(AudioClip));
                        if (Clip == null)
                            LHSDebug.LogErrorFormat("AudiObjectOneshot - Clip ({0}) is null", _clip);
                    }

                    public override void Play()
                    {
                        Source.PlayOneShot(Clip);
                    }
                    public override bool IsLoop()
                    {
                        return false;
                    }
                }
                public class AudiObjectLoop : AudioObject
                {
                    public AudiObjectLoop(string _clip, GameObject _obj)
                        : base(_obj)
                    {
                        AudioClip clip = (AudioClip)ResourceManager.instance.Load(_clip, typeof(AudioClip));
                        if (clip != null)
                        {
                            Source.clip = clip;
                        }
                        if (clip == null)
                            LHSDebug.LogErrorFormat("AudiObjectLoop - Clip ({0}) is null", _clip);

                    }

                    public override void Play()
                    {
                        if (!Source.isPlaying)
                            Source.Play();
                    }
                    public override void Stop()
                    {
                        Source.Stop();
                    }
                    public override bool IsLoop()
                    {
                        return true;
                    }
                }

                public static List<AudioSource> LoadedAudioSources = new List<AudioSource>();
                public static List<string> LoadedPackages = new List<string>();
                public static Dictionary<string, AudioObject> AudioList = new Dictionary<string, AudioObject>();
                public static float MasterVolume = AudioListener.volume;

                public bool MasterMute { get; private set; }
                public bool[] Muted { get; private set; }
                public AudioObject CurrentSong = null;

                public SoundManager()
                {
                    MasterMute = false;
                    Muted = new bool[(int)eSoundTypes.Size];
                    for(int i = 0; i < (int)eSoundTypes.Size; ++i)
                    {
                        Muted[i] = false;
                    }
                }

                public void SetCurrentSong(AudioObject _song)
                {
                    CurrentSong = _song;
                    CurrentSong.Mute(Muted[(int)eSoundTypes.Music]);
                }

                public AudioObject Get(string trigger)
                {
                    AudioObject returnObj = null;
                    AudioList.TryGetValue(trigger, out returnObj);
                    return returnObj;
                }

                public void Play(string trigger)
                {
                    AudioObject outObj = null;
                    AudioList.TryGetValue(trigger, out outObj);
                    if (outObj != null)
                        outObj.Play();
                }
                public void Stop(string trigger)
                {
                    AudioObject outObj = null;
                    AudioList.TryGetValue(trigger, out outObj);
                    if (outObj != null)
                        outObj.Stop();
                }

                public void Mute()
                {
                    MasterMute = !MasterMute;
                    for (int i = 0; i < (int)eSoundTypes.Size; ++i)
                    {
                        if (Muted[i] != MasterMute)
                        {
                            Mute((eSoundTypes)i);
                        }
                    }
                }
                public void Mute(eSoundTypes type)
                {
                    Muted[(int)type] = !Muted[(int)type];
                    LHSDebug.LogFormat("Muted[{0}]: {1}", type, Muted[(int)type]);
                    switch (type) {
                        case eSoundTypes.SFX:
                            foreach (AudioSource src in LoadedAudioSources)
                            {
                                if (CurrentSong != null)
                                {
                                    if(!CurrentSong.CompareSource(src))
                                        src.mute = Muted[(int)type];
                                }
                                else
                                    src.mute = Muted[(int)type];
                            }
                            break;
                        case eSoundTypes.Music:
                            if (CurrentSong != null)
                                CurrentSong.Mute(Muted[(int)type]);
                            break;
                    }
                }

                public void SetMasterVolume(float _volume)
                {
                    MasterVolume = AudioListener.volume = Mathf.Clamp(_volume, 0, 1);
                }

                public void ClearAll()
                {
                    foreach (KeyValuePair<string, AudioObject> kvp in AudioList)
                        kvp.Value.Destroy();
                    AudioList.Clear();
                    LoadedAudioSources.Clear();
                    LoadedPackages.Clear();

                }

                static public void LoadAudio(TextAsset _xml, bool clear = false)
                {
                    if (_xml == null)
                        return;

                    if (clear)
                    {
                        SoundManager.AudioList.Clear();
                    }

                    XmlDocument m_xmlDoc = new XmlDocument();
                    m_xmlDoc.LoadXml(_xml.text);

                    XmlNode nodeObjects = m_xmlDoc.SelectSingleNode("/Audio");
                    for (XmlNode node = nodeObjects.FirstChild; node != null; node = node.NextSibling)
                    {
                        if (node != null && node.Attributes != null)
                        {
                            if (node.Attributes["trigger"].Value != null && node.Attributes["clip"].Value != null)
                            {

                                string trigger = node.Attributes["trigger"].Value;
                                string clip = node.Attributes["clip"].Value;
                                float volume = (node.Attributes["volume"] != null) ? Mathf.Clamp(float.Parse(node.Attributes["volume"].Value), 0, 1) : 1f;

                                if (!SoundManager.AudioList.ContainsKey(trigger))
                                {
                                    switch (node.Name)
                                    {
                                        case "OneShot":
                                            {
                                                GameObject Obj = GameObject.Instantiate(ResourceManager.instance.Load("Prefabs/Audio/Audio Source SFX", typeof(GameObject))) as GameObject;
                                                GameObject.DontDestroyOnLoad(Obj);
                                                SoundManager.AudioObject obj = new SoundManager.AudiObjectOneshot(clip, Obj);
                                                obj.SetVolume(volume);
                                                SoundManager.AudioList.Add(trigger, obj);
                                            }
                                            break;
                                        case "Loop":
                                        case "Music":
                                            {
                                                GameObject Obj = GameObject.Instantiate(ResourceManager.instance.Load("Prefabs/Audio/Audio Source Music", typeof(GameObject))) as GameObject;
                                                GameObject.DontDestroyOnLoad(Obj);
                                                SoundManager.AudioObject obj = new SoundManager.AudiObjectLoop(clip, Obj);
                                                obj.SetVolume(volume);
                                                SoundManager.AudioList.Add(trigger, obj);
                                            }
                                            break;
                                    }

                                }

                            }
                        }
                    }
                }

            }


            public class SoundLoader : CSingletonMonoBehaviour<SoundLoader>
            {

                public TextAsset[] XMLs;

                public override void OnAwake()
                {
                    foreach (TextAsset xml in XMLs)
                    {
                        if (!isPackageLoaded(xml.name))
                            LoadAudio(xml);
                    }
                }

                public bool isPackageLoaded(string packageName)
                {
                    foreach (string str in SoundManager.LoadedPackages)
                    {
                        if (str == packageName)
                            return true;
                    }
                    SoundManager.LoadedPackages.Add(packageName);
                    return false;
                }

                public void LoadAudio(string _xml, bool clear = false)
                {
                    TextAsset XML = (TextAsset)ResourceManager.instance.Load(_xml, typeof(TextAsset));
                    LoadAudio(XML, clear);
                }

                public void LoadAudio(TextAsset _xml, bool clear = false)
                {
                    SoundManager.LoadAudio(_xml, clear);
                }

            }
        }
    }
}