﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace LHS.Framework
{

    public interface IRegisterManager<ToRegister> where ToRegister : Behaviour
    {
        void Register(ToRegister register);
    }
    
    public class CRegisterBehaviour<ToRegister, RegisterTo> : MonoBehaviour 
        where ToRegister : Behaviour
        where RegisterTo : IRegisterManager<ToRegister>
    {
        public void Awake()
        {
            ToRegister component = this.GetComponent<ToRegister>();
            if (component)
            {
                RegisterFunc(component);
            }
            else
                LHSDebug.LogWarningFormat("{0} - Does not contain a {1} - Pleae ensure that which you register contains a {1}", this.name, typeof(ToRegister).Name);
        }

        public virtual void RegisterFunc(ToRegister register)
        {
            // there is a way to use reflection to call a static function via RegisterTo type
            // I madfe an attempt at this, but did not get it to work
            // we can revisit this.
            // This solution allows us to overload this function in the particular register class
            // in which case we can call the correspionding
        }

        public void OnDestroy()
        {
            ToRegister component = this.GetComponent<ToRegister>();
            if (component)
            {
                DeRegisterFunc(component);
            }
        }

        public virtual void DeRegisterFunc(ToRegister register)
        {
            // there is a way to use reflection to call a static function via RegisterTo type
            // I madfe an attempt at this, but did not get it to work
            // we can revisit this.
            // This solution allows us to overload this function in the particular register class
            // in which case we can call the correspionding
        }
    }

    public class CRegisterManager<ToRegister, RegisterTo> : CSingleton<RegisterTo>, IRegisterManager<ToRegister>
        where ToRegister : Behaviour
        where RegisterTo : new()
    {

        public Dictionary<string, ToRegister> Registers;

        public CRegisterManager()
        {
            Registers = new Dictionary<string, ToRegister>();
        }

        public void Register(ToRegister register)
        {
            if (!Registers.ContainsKey(register.name))
            {
                Registers.Add(register.name, register);
            }
        }
        public void DeRegister(string key)
        {
            Registers.Remove(key);
        }

        public ToRegister Get(string key)
        {
            ToRegister toGet;
            Registers.TryGetValue(key, out toGet);
            return toGet;
        }
        public ToRegister GetSafe(string key)
        {
            ToRegister toGet;
            if (Registers.TryGetValue(key, out toGet))
                return toGet;
            return default(ToRegister);
        }
        public bool Get(string key, out ToRegister toGet)
        {
            if (Registers.TryGetValue(key, out toGet))
                return true;
            return false;
        }
    }
}
