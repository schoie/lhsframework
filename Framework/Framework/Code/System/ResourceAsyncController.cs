﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework
{
    public class ResourceAsyncController : MonoBehaviour
    {
        public interface ILoadResourceRequest
        {
            bool Process();
        }

        public static int uniqueID = 0;
        public class LoadResourceRequest<Type> : ILoadResourceRequest where Type : UnityEngine.Object
        {
            ResourceRequest Request;
            IHoldResources Holder;
            int amount;
            string path;

            public LoadResourceRequest(ResourceRequest _request, IHoldResources _holder, int _amount, string _path)
            {
                Request = _request;
                Holder = _holder;
                amount = _amount;
                path = _path;
            }

            public bool Process()
            {
                if (Request.isDone)
                {
                    if (Request.asset == null)
                    {
                        LHSDebug.LogErrorFormat("ResourceAsyncController.LoadResourceRequest - FAILED!! - Due to NULL load from path: {0}", path);
                        return true;
                    }
                    LHSDebug.LogFormat("ResourceAsyncController.LoadResourceRequest - SUCCEEDED!! - loaded {0} of path: {1}", amount, path);
                    Holder.SetAsync<Type>(Request.asset as Type, amount);
                    return true;
                }
                return false;
            }
        }

        Dictionary<int, ILoadResourceRequest> AsyncTasks = new Dictionary<int, ILoadResourceRequest>();
        Coroutine RequestTaskHandlerCo = null;

        public void CreateAsyncTask<Type>(ResourceRequest request, IHoldResources holder, int amount, string path) where Type : UnityEngine.Object
        {
            AsyncTasks.Add(uniqueID++, new LoadResourceRequest<Type>(request, holder, amount, path));
            StartRequestTaskHandlerCo();
        }

        private void StartRequestTaskHandlerCo()
        {
            if (RequestTaskHandlerCo == null)
                RequestTaskHandlerCo = StartCoroutine(SelectionAnimationCoroutine());
        }

        IEnumerator SelectionAnimationCoroutine()
        {
            //List<int> FinishedRequests = new List<int>();
            while (AsyncTasks.Count > 0)
            {
                foreach (var s in AsyncTasks.Where(p => p.Value.Process()).ToList())
                {
                    AsyncTasks.Remove(s.Key);
                }
                
                yield return null;
            }

            GameObject.DestroyObject(this.gameObject);

        }

        public bool isComplete()
        {
            return AsyncTasks.Count > 0;
        }

    }
}
