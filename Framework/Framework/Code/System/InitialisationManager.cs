﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework
{

    public interface IInitialise
    {
        void OnInitialise();
        void OnAlreadyInitialised();
    }

    public class MonoBehaviourInitialiser : MonoBehaviour, IInitialise
    {
        
        public bool CheckInitialiser(MonoBehaviour obj, bool setDoNotDestroy = true, bool doDestroy = true)
        {
            LHSDebug.Log("CheckInitialiser - Register = " + obj.name);
            if (!InitialisationManager.instance.Register(obj.name, this))
            {
                LHSDebug.Log("CheckInitialiser - doDestroy = " + obj.name);
                if (doDestroy)
                {
                    DestroyObject(this);
                }
                return false;
            }

            LHSDebug.Log("CheckInitialiser - setDoNotDestroy = " + obj.name);
            if (setDoNotDestroy)
                DontDestroyOnLoad(this);

            return true;
        }

        public virtual void OnInitialise()
        {

        }
        public virtual void OnAlreadyInitialised()
        {

        }
    }

    public abstract class CSingletonMonoBehaviourInitialiser<T> : MonoBehaviourInitialiser
    {
        public static T instance { get; private set; }

        public void Awake()
        {
            instance = (T)(object)this;
            OnAwake();
        }

        public abstract void OnAwake();
    }


    public class InitialisationManager : CSingleton<InitialisationManager>
    {

        // we want to register classes that have already been processed
        Dictionary<int, IInitialise> initialisers;

        // Register singletons so we can call functions on all of them
        Dictionary<int, ICSingleton> singletons;

        public InitialisationManager()
        {
            initialisers = new Dictionary<int, IInitialise>();
            singletons = new Dictionary<int, ICSingleton>();
        }

        //-------------------------------------------------------------
        // IInitialise
        //-------------------------------------------------------------
        public bool Register(string name, IInitialise data)
        {
            int code = name.GetHashCode();
            IInitialise check;
            if (!initialisers.TryGetValue(code, out check))
            {
                data.OnInitialise();
                initialisers.Add(code, data);

                return true;
            }
            check.OnAlreadyInitialised();
            return false;
        }
        //-------------------------------------------------------------


        //-------------------------------------------------------------
        // ICSingleton
        //-------------------------------------------------------------
        public bool Register(string name, ICSingleton singleton)
        {
            int code = name.GetHashCode();
            ICSingleton check;
            if (!singletons.TryGetValue(code, out check))
            {
                singletons.Add(code, singleton);

                return true;
            }
            return false;
        }

        public void InitialiseSingletons()
        {
            foreach (KeyValuePair<int, ICSingleton> kvp in singletons)
            {
                kvp.Value.Init();
            }
        }
        public void DeinitialiseSingletons()
        {
            foreach(KeyValuePair<int, ICSingleton> kvp in singletons)
            {
                kvp.Value.Deinit();
            }
        }
        //-------------------------------------------------------------

    }
}
