﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Project
{
    public class HierarchyController : MonoBehaviour
    {

        [Header("These objects will be disabled when the game starts")]
        public GameObject[] EditorViewObjects;

        [Header("These objects will be enabled when the game starts")]
        public GameObject[] GameObjects;


        public void Awake()
        {
            setObjects(ref EditorViewObjects, false);
            setObjects(ref GameObjects, true);
        }

        [ContextMenu("ResetHierarchy")]
        public void ResetHierarchy()
        {
            setObjects(ref EditorViewObjects, true);
            setObjects(ref GameObjects, false);
        }

        // private
        private void setObjects(ref GameObject[] array, bool set)
        {
            foreach (GameObject obj in array)
                obj.SetActive(set);
        }

    }
}
