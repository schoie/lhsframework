﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace LHS
{
    namespace Framework
    {
        public static class MyExtensions
        {
            public static void AddToFront<T>(this List<T> list, T item)
            {
                // omits validation, etc.
                list.Insert(0, item);
            }

            public delegate void iterateCollection(Object collection);
            public static void IterateWithDelegate<T>(this List<T> list, iterateCollection function)
            {
                // omits validation, etc.
                function(list);
            }
        }
        /*
                public class testClass
                {

                    public List<Object> Objects = new List<Object>();

                    public Dictionary<string, int> testD = new Dictionary<string, int>();
                    public List<int> testL = new List<int>();

                    public testClass()
                    {
                        Objects.Add(testD);
                        Objects.Add(testL);

                        for(int i = 0; i < Objects.Count; ++i)
                        {
                            Objects[i].GetType().
                        }

                    }

                }

            }

            */
    }
   }
