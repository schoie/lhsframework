﻿using LHS.Framework.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;

namespace LHS.Framework
{
    public class LoadManager : CSingleton<LoadManager>
    {
        List<GameObject> LoadedPrefabs;

        public LoadManager()
        {
            LoadedPrefabs = new List<GameObject>();
        }

        // Loaded prefab functions
        public void ClearLoadedPrefabs()
        {
            foreach (GameObject obj in LoadedPrefabs)
                GameObject.DestroyObject(obj);

            LoadedPrefabs.Clear();
        }

        // load functions
        public void Load(string file)
        {
            Load(ResourceManager.instance.Get<TextAsset>(file));
        }
        public void Load(TextAsset _xml)
        {
            if(_xml != null)
            {
                XmlDocument m_xmlDoc = new XmlDocument();
                m_xmlDoc.LoadXml(_xml.text);

                XmlNode nodeObjects = m_xmlDoc.SelectSingleNode("/Load");
                for (XmlNode node = nodeObjects.FirstChild; node != null; node = node.NextSibling)
                {
                    if (node != null && node.Attributes != null)
                    {
                        switch (node.Name)
                        {
                            case "Prefab":
                                {
                                    string path = node.Attributes["path"].Value;
                                    GameObject obj = ResourceManager.instance.Get<GameObject>(path);
                                    if (obj != null)
                                        LoadedPrefabs.Add(obj);
                                }
                                break;
                            case "UI_Prefab":
                                {
                                    string path = node.Attributes["path"].Value;
                                    string canvas_name = node.Attributes["canvas"].Value;
                                    Canvas canvas = UICanvasManager.instance.Get(canvas_name);
                                    if (canvas != null)
                                    {
                                        GameObject obj = ResourceManager.instance.Get<GameObject>(path);
                                        if (obj != null)
                                        {
                                            LoadedPrefabs.Add(obj);
                                            bool worldPositionStays = (node.Attributes["worldPositionStays"] != null) ? bool.Parse(node.Attributes["worldPositionStays"].Value) : false;
                                            obj.transform.SetParent(canvas.transform, worldPositionStays);
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }
            }
        }


    }
}
