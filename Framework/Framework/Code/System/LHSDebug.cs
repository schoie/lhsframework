﻿
//----------------------------------------------------
// Defines
//----------------------------------------------------
//#define ENABLE_BASIC_DEBUG
#define ENABLE_ERROR_DEBUG
//#define ENABLE_ASSERTION_DEBUG
//#define ENABLE_EXCEPTIONS_DEBUG
//#define ENABLE_WARNING_DEBUG

// special
#define ENABLE_METEOR_BASIC_DEBUG
#define ENABLE_METEOR_ERROR_DEBUG
#define ENABLE_METEOR_WARNING_DEBUG

//----------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace LHS.Framework
{



    public abstract class LHSDebugSettings
    {

    }

    public abstract class LHSDebug
    {

        static bool ErrorLogHandleFunctionSet = false;
        static List<UnityAction<string, string>>[] Actions = new List<UnityAction<string, string>>[(int)(LogType.Exception + 1)];

        //----------------------------------------------------------------------------------
        // Error catching
        //----------------------------------------------------------------------------------
        public static void RegisterApplicationErrorLog(UnityAction<string, string> callback, LogType type = LogType.Error)
        {
            if (Actions[(int)type] == null)
                Actions[(int)type] = new List<UnityAction<string, string>>();

            if (!ErrorLogHandleFunctionSet)
            {
                Application.logMessageReceived += ErrorLogHandleFunction;
                ErrorLogHandleFunctionSet = true;
            }
            Actions[(int)type].Add(callback);
        }

        public static void DeregisterApplicationErrorLog(UnityAction<string, string> callback, LogType type = LogType.Error)
        {
            if (Actions[(int)type] == null)
                return;

            if (Actions[(int)type].Contains(callback))
                Actions[(int)type].Remove(callback);
            
        }

        private static void ErrorLogHandleFunction(string message, string stackTrace, LogType type)
        {
            if (Actions[(int)type] != null)
                foreach (UnityAction<string, string> action in Actions[(int)type])
                    action.Invoke(message, stackTrace);
        }

        //----------------------------------------------------------------------------------


        //----------------------------------------------------------------------------------
        // Basic
        //----------------------------------------------------------------------------------
        public static void Log(object message)
        {
#if ENABLE_BASIC_DEBUG
            Debug.Log(message);
#endif
        }
        public static void Log(object message, Object context)
        {
#if ENABLE_BASIC_DEBUG
            Debug.Log(message, context);
#endif
        }
        public static void LogFormat(string format, params object[] args)
        {
#if ENABLE_BASIC_DEBUG
            Debug.LogFormat(format, args);
#endif
        }
        public static void LogFormat(Object context, string format, params object[] args)
        {
#if ENABLE_BASIC_DEBUG
            Debug.LogFormat(context, format, args);
#endif
        }
        //----------------------------------------------------------------------------------


        //----------------------------------------------------------------------------------
        // Error
        //----------------------------------------------------------------------------------
        public static void LogError(object message)
        {
#if ENABLE_ERROR_DEBUG
            Debug.LogError(message);
#endif
        }
        public static void LogError(object message, Object context)
        {
#if ENABLE_ERROR_DEBUG
            Debug.LogError(message, context);
#endif
        }
        public static void LogErrorFormat(string format, params object[] args)
        {
#if ENABLE_ERROR_DEBUG
            Debug.LogErrorFormat(format, args);
#endif
        }
        public static void LogErrorFormat(Object context, string format, params object[] args)
        {
#if ENABLE_ERROR_DEBUG
            Debug.LogErrorFormat(context, format, args);
#endif
        }
        //----------------------------------------------------------------------------------

        

        //----------------------------------------------------------------------------------
        // Assertion
        //----------------------------------------------------------------------------------
        public static void LogAssertion(object message)
        {
#if ENABLE_ASSERTION_DEBUG
            Debug.LogAssertion(message);
#endif
        }
        public static void LogAssertion(object message, Object context)
        {
#if ENABLE_ASSERTION_DEBUG
            Debug.LogAssertion(message, context);
#endif
        }
        public static void LogAssertionFormat(string format, params object[] args)
        {
#if ENABLE_ASSERTION_DEBUG
            Debug.LogAssertionFormat(format, args);
#endif
        }
        public static void LogAssertionFormat(Object context, string format, params object[] args)
        {
#if ENABLE_ASSERTION_DEBUG
            Debug.LogAssertionFormat(context, format, args);
#endif
        }
        //----------------------------------------------------------------------------------


        //----------------------------------------------------------------------------------
        // Exceptions
        //----------------------------------------------------------------------------------
        public static void LogException(System.Exception exception)
        {
#if ENABLE_EXCEPTIONS_DEBUG
            Debug.LogException(exception);
#endif
        }
        public static void LogException(System.Exception exception, Object context)
        {
#if ENABLE_EXCEPTIONS_DEBUG
            Debug.LogException(exception, context);
#endif
        }
        //----------------------------------------------------------------------------------


        //----------------------------------------------------------------------------------
        // Warnings
        //----------------------------------------------------------------------------------
        public static void LogWarning(object message)
        {
#if ENABLE_WARNING_DEBUG
            Debug.LogWarning(message);
#endif
        }
        public static void LogWarning(object message, Object context)
        {
#if ENABLE_WARNING_DEBUG
            Debug.LogWarning(message, context);
#endif
        }
        public static void LogWarningFormat(string format, params object[] args)
        {
#if ENABLE_WARNING_DEBUG
            Debug.LogWarningFormat(format, args);
#endif
        }
        public static void LogWarningFormat(Object context, string format, params object[] args)
        {
#if ENABLE_WARNING_DEBUG
            Debug.LogWarningFormat(context, format, args);
#endif
        }
        //----------------------------------------------------------------------------------


        #region Networked
        //----------------------------------------------------------------------------------
        // Network Basic
        //----------------------------------------------------------------------------------
        public static void LogNetwork(object message)
        {
#if ENABLE_METEOR_BASIC_DEBUG
            Debug.Log(message);
#endif
        }
        public static void LogNetwork(object message, Object context)
        {
#if ENABLE_METEOR_BASIC_DEBUG
            Debug.Log(message, context);
#endif
        }
        public static void LogNetworkFormat(string format, params object[] args)
        {
#if ENABLE_METEOR_BASIC_DEBUG
            Debug.LogFormat(format, args);
#endif
        }
        public static void LogNetworkFormat(Object context, string format, params object[] args)
        {
#if ENABLE_METEOR_BASIC_DEBUG
            Debug.LogFormat(context, format, args);
#endif
        }
        //----------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------
        // Network Errors
        //----------------------------------------------------------------------------------
        public static void LogNetworkError(object message)
        {
#if ENABLE_METEOR_ERROR_DEBUG
            Debug.LogError(message);
#endif
        }
        public static void LogNetworkError(object message, Object context)
        {
#if ENABLE_METEOR_ERROR_DEBUG
            Debug.LogError(message, context);
#endif
        }
        public static void LogNetworkErrorFormat(string format, params object[] args)
        {
#if ENABLE_METEOR_ERROR_DEBUG
            Debug.LogErrorFormat(format, args);
#endif
        }
        public static void LogNetworkErrorFormat(Object context, string format, params object[] args)
        {
#if ENABLE_METEOR_ERROR_DEBUG
            Debug.LogErrorFormat(context, format, args);
#endif
        }
        //----------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------
        // Warnings
        //----------------------------------------------------------------------------------
        public static void LogNetworkWarning(object message)
        {
#if ENABLE_METEOR_WARNING_DEBUG
            Debug.LogWarning(message);
#endif
        }
        public static void LogNetworkWarning(object message, Object context)
        {
#if ENABLE_METEOR_WARNING_DEBUG
            Debug.LogWarning(message, context);
#endif
        }
        public static void LogNetworkWarningFormat(string format, params object[] args)
        {
#if ENABLE_METEOR_WARNING_DEBUG
            Debug.LogWarningFormat(format, args);
#endif
        }
        public static void LogNetworkWarningFormat(Object context, string format, params object[] args)
        {
#if ENABLE_METEOR_WARNING_DEBUG
            Debug.LogWarningFormat(context, format, args);
#endif
        }
        //----------------------------------------------------------------------------------
        #endregion

    }

    public class LHSDebugWindow : MonoBehaviour
    {
        public void Awake()
        {
            GUIAtive = true;
        }

        static bool GUIAtive = false;
        static string pDocument = "";
        public static void showWaitingOn(string _text)
        {
            if(GUIAtive)
                pDocument += "\n" + _text;
        }

        Rect windowRect = new Rect(20, 20, 300, 600);

        private void OnGUI()
        {
            windowRect = GUI.Window(0, windowRect, OnWindowGUI, "LHS Debug Window");
        }
        private void OnWindowGUI(int windowId)
        {
            GUILayout.Label(pDocument);
            GUILayout.FlexibleSpace();
        }

    }
}
