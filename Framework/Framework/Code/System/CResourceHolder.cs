﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework
{
    public interface IHoldResourcesInternal<T>
    {
        T GetInternal();
        void SetInternal(T _res);
        void Clear();
    }
    public interface IHoldMultiResourcesInternal<T>
    {
        T[] GetInternal();
        void SetInternal(T[] _res);
        void Clear();
    }

    public class SingleHolder<T> : IHoldResourcesInternal<T> where T : UnityEngine.Object
    {
        public T single = null;

        public SingleHolder(T _res)
        {
            single = _res;
        }

        public virtual T GetInternal()
        {
            return single;
        }
        public virtual void SetInternal(T _res)
        {
            single = _res;
        }

        public void Clear()
        {
            single = null;
        }
    }

    public class MultiHolder<T> : IHoldMultiResourcesInternal<T> where T : UnityEngine.Object
    {
        public T[] array = null;

        public MultiHolder(T[] _array)
        {
            array = _array;
        }

        public virtual T[] GetInternal()
        {
            return array;
        }
        public virtual void SetInternal(T[] _res)
        {
            array = _res;
        }

        public void Clear()
        {
            array = null;
        }
    }

    public class QueueHolder<T> : IHoldResourcesInternal<T> where T : UnityEngine.Object
    {
        protected T defaultResource;
        protected Queue<T> pool;
        protected bool canGrow;

        public QueueHolder(T _res, int _amount, bool _canGrow)
        {
            pool = new Queue<T>();

            defaultResource = _res;
            canGrow = _canGrow;
            
            for (int i = 0; i < _amount; ++i)
            {
                Add(defaultResource);
            }
        }

        public void SetDefault(T _res, int _amount)
        {
            defaultResource = _res;

            for (int i = 0; i < _amount; ++i)
            {
                Add(defaultResource);
            }

        }
        
        protected T Add(T _res)
        {
            T forPool = Instantiate(_res);
            pool.Enqueue(forPool);
            return forPool;
        }
        virtual public T Instantiate(T _res)
        {
            LHSDebug.LogWarning("QueueHolder - Instantiate is returning NULL");
            return null;
        }

        virtual public void Clear()
        {
            int loopAmount = pool.Count;
            for (int i = 0; i < loopAmount; ++i)
            {
                if (pool.Count <= 0)
                    break;

                pool.Dequeue();
            }
        }

        virtual public T GetInternal()
        {
            T fromPool = null;
            if (pool.Count > 0)
                fromPool = pool.Dequeue();

            if (fromPool == null && canGrow)
            {
                fromPool = Add(defaultResource);
                return fromPool;
            }

            return fromPool;
        }

        virtual public void SetInternal(T _res)
        {
            pool.Enqueue(_res);
        }

    }

    public interface IHoldResources
    {
        T Get<T>() where T : UnityEngine.Object;
        T[] GetAll<T>() where T : UnityEngine.Object;
        void Set<T>(T _res) where T : UnityEngine.Object;
        void ClearAll();
        void SetAsync<T>(T _res, int _amount) where T : UnityEngine.Object;
    }

    public class GameObjectHolder : QueueHolder<GameObject>, IHoldResources
    {
        public GameObjectHolder(bool _canGrow) : base(null, 0, _canGrow)
        {

        }
        public GameObjectHolder(GameObject _res, int _amount, bool _canGrow) : base(_res, _amount, _canGrow)
        {

        }

        public void SetAsync<T>(T _res, int _amount) where T : UnityEngine.Object
        {
            SetDefault((GameObject)(UnityEngine.Object)_res, _amount);
        }

        public override void Clear()
        {
            int loopAmount = pool.Count;
            for (int i = 0; i < loopAmount; ++i)
            {
                if (pool.Count <= 0)
                    break;

                GameObject toDestroy = pool.Dequeue();
                if (toDestroy != null)
                    GameObject.DestroyObject(toDestroy);
            }
        }
        public void ClearAll()
        {
            Clear();
        }

        override public GameObject Instantiate(GameObject _res)
        {
            GameObject instantiated = GameObject.Instantiate(_res);
            instantiated.SetActive(false);
            instantiated.transform.SetParent(ResourceManager.instance.ResourcesTransform, false);
            return instantiated;
        }

        public T Get<T>() where T : UnityEngine.Object
        {
            return (T)(UnityEngine.Object)this.GetInternal();
        }
        public T[] GetAll<T>() where T : UnityEngine.Object
        {
            T[] array = new T[1];
            array[0] = Get<T>();
            return array;
        }
        public void Set<T>(T _res) where T : UnityEngine.Object
        {
            this.SetInternal((GameObject)(UnityEngine.Object)_res);
        }

        public GameObject GetRes()
        {
            return this.GetInternal();
        }
        public void SetRes(GameObject _res)
        {
            _res.transform.SetParent(ResourceManager.instance.ResourcesTransform, false);
            this.SetInternal(_res);
        }

        override public GameObject GetInternal()
        {
            GameObject fromPool = null;
            if (pool.Count > 0)
                fromPool = pool.Dequeue();

            if (fromPool == null && canGrow)
            {
                fromPool = Add(defaultResource);
                return fromPool;
            }
            fromPool.SetActive(true);
            return fromPool;
        }

        override public void SetInternal(GameObject _res)
        {
            _res.transform.SetParent(ResourceManager.instance.ResourcesTransform, false);
            _res.SetActive(false);
            pool.Enqueue(_res);
        }

        static public IHoldResources CreateGameObjectHolder(string path, int amount = 1, bool cangrow = true, bool async = false)
        {
            if (async)
            {
                ResourceRequest resReq = ResourceManager.instance.LoadAsync<GameObject>(path);
                GameObjectHolder holder = new GameObjectHolder(cangrow);
                ResourceManager.instance.TryCreateAsyncController();
                ResourceManager.instance.AsyncController.CreateAsyncTask<GameObject>(resReq, holder, amount, path);
                return holder;
            }

            // not async
            GameObject _res = ResourceManager.instance.Load<GameObject>(path);
            if (_res == null)
            {
                LHSDebug.LogErrorFormat("CreateGameObjectHolder - FAILED!! - Due to NULL load from path: {0}", path);
            }
            return new GameObjectHolder(_res, amount, cangrow);
        }
    }

    public class TypeHolder<Type> : SingleHolder<Type>, IHoldResources where Type : UnityEngine.Object
    {
        public void ClearAll()
        {
            Clear();
        }

        public TypeHolder(Type _res) : base(_res) { }
        
        public void SetAsync<T>(T _res, int _amount) where T : UnityEngine.Object
        {
            Set<T>(_res);
        }

        public T Get<T>() where T : UnityEngine.Object
        {
            return (T)(UnityEngine.Object)this.GetInternal();
        }
        public T[] GetAll<T>() where T : UnityEngine.Object
        {
            T[] array = new T[1];
            array[0] = Get<T>();
            return array;
        }
        public void Set<T>(T _res) where T : UnityEngine.Object
        {
            this.SetInternal((Type)(UnityEngine.Object)_res);
        }

        public Type GetRes()
        {
            return this.GetInternal();
        }
        public void SetRes(Type _res)
        {
            this.SetInternal(_res);
        }

        static public IHoldResources CreateTypeHolder<T>(string path, bool async = false) where T : UnityEngine.Object
        {
            if (async)
            {
                ResourceRequest resReq = ResourceManager.instance.LoadAsync<T>(path);
                TypeHolder<T> holder = new TypeHolder<T>(null);
                ResourceManager.instance.TryCreateAsyncController();
                ResourceManager.instance.AsyncController.CreateAsyncTask<T>(resReq, holder, 1, path);
                return holder;
            }

            T _res = ResourceManager.instance.Load<T>(path);
            if (_res == null)
            {
                LHSDebug.LogErrorFormat("CreateTypeHolder<{0}> - FAILED!! - Due to NULL load from path: {1}", typeof(T).Name, path);
            }
            return new TypeHolder<T>(_res);
        }
    }

    public class TypeHolderMulti<Type> : MultiHolder<Type>, IHoldResources where Type : UnityEngine.Object
    {
        public void ClearAll()
        {
            Clear();
        }

        public TypeHolderMulti(Type[] _res) : base(_res) { }
        
        public void SetAsync<T>(T _res, int _amount) where T : UnityEngine.Object
        {
            Set<T>(_res);
        }

        public T Get<T>() where T : UnityEngine.Object
        {
            T[] array = (T[])(UnityEngine.Object[])this.GetInternal();
            return array[0];
        }
        public T[] GetAll<T>() where T : UnityEngine.Object
        {
            return (T[])(UnityEngine.Object[])this.GetInternal();
        }
        public void Set<T>(T _res) where T : UnityEngine.Object
        {
            LHSDebug.LogWarningFormat("Set<{0}> is not availble with Holder type TypeHolderMulti<{1}>!!", typeof(T).Name, typeof(Type).Name);
        }

        public Type GetRes()
        {
            Type[] array = this.GetInternal();
            return array[0];
        }
        public void SetRes(Type _res)
        {
            LHSDebug.LogWarningFormat("SetRes()is not availble with Holder type TypeHolderMulti<{0}>!!", typeof(Type).Name);
        }

        static public IHoldResources CreateTypeHolderMulti<T>(string path, bool async = false) where T : UnityEngine.Object
        {
            /*if (async)
            {
                ResourceRequest resReq = ResourceManager.instance.LoadAsync<T>(path);
                TypeHolderMulti<T> holder = new TypeHolderMulti<T>(null);
                ResourceManager.instance.TryCreateAsyncController();
                ResourceManager.instance.AsyncController.CreateAsyncTask<T>(resReq, holder, 1, path);
                return holder;
            }*/

            T[] _res = ResourceManager.instance.LoadAll<T>(path);
            if (_res == null)
            {
                LHSDebug.LogErrorFormat("CreateTypeHolderMulti<{0}> - FAILED!! - Due to NULL load from path: {1}", typeof(T).Name, path);
            }
            return new TypeHolderMulti<T>(_res);
        }
    }

    public class InstantiatorHolder : SingleHolder<GameObject>, IHoldResources
    {
        public void ClearAll()
        {
            Clear();
        }

        public InstantiatorHolder(GameObject _res) : base(_res) { }
        
        public void SetAsync<T>(T _res, int _amount) where T : UnityEngine.Object
        {
            Set<T>(_res);
        }

        public T Get<T>() where T : UnityEngine.Object
        {
            GameObject returnObj = GameObject.Instantiate(GetInternal());
            return (T)(UnityEngine.Object)returnObj;
        }
        public T[] GetAll<T>() where T : UnityEngine.Object
        {
            T[] array = new T[1];
            array[0] = Get<T>();
            return array;
        }
        public void Set<T>(T _res) where T : UnityEngine.Object
        {
            this.SetInternal((GameObject)(UnityEngine.Object)_res);
        }

        public GameObject GetRes()
        {
            return this.GetInternal();
        }
        public void SetRes(GameObject _res)
        {
            this.SetInternal(_res);
        }

        static public IHoldResources CreateInstantiatorHolder(string path, bool async = false)
        {
            if (async)
            {
                ResourceRequest resReq = ResourceManager.instance.LoadAsync<GameObject>(path);
                InstantiatorHolder holder = new InstantiatorHolder(null);
                ResourceManager.instance.TryCreateAsyncController();
                ResourceManager.instance.AsyncController.CreateAsyncTask<GameObject>(resReq, holder, 1, path);
                return holder;
            }

            GameObject _res = ResourceManager.instance.Load<GameObject>(path);
            if (_res == null)
            {
                LHSDebug.LogErrorFormat("InstantiatorHolder - FAILED!! - Due to NULL load from path: {0}", path);
            }
            return new InstantiatorHolder(_res);
        }
    }

}
