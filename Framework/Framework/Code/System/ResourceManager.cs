﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace LHS.Framework
{
    public enum eResourceLoaderTypes
    {
        Default,
        Logger,

        Factory,
        FactoryLogger
    }

    public class ResourceController : MonoBehaviourInitialiser
    {
        public eResourceLoaderTypes Type;
        public TextAsset PreLoad;

        public void Awake()
        {
            if (this.CheckInitialiser(this))
            {
                ResourceManager.instance.SetResourceLoaderType(Type);

                if (PreLoad != null)
                {
                    ResourceManager.instance.SetResourcesTransform(this.gameObject);
                    ResourceManager.instance.PreLoad(PreLoad);
                }
            }
        }

        public override void OnInitialise()
        {
            LHSDebug.Log("OnInitialise ResourceController");
        }

        public override void OnAlreadyInitialised()
        {
            LHSDebug.Log("OnAlreadyInitialised ResourceController");
        }

    }

    public class ResourceDebugButton : MonoBehaviour
    {
        public string ExportPath;
        
        public void Awake()
        {
            Button button = this.GetComponent<Button>();
            if(button == null)
            {
                LHSDebug.LogError("ResourceDebugButton - Must be placed on a gameobject that has a Button!!");
                enabled = false;
                return;
            }
            
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(ExportFunction);
        }

        public void ExportFunction()
        {
            ResourceManager.instance.Log();
            ResourceManager.instance.Export(ExportPath);
        }

    }


    public class ResourceManager : CSingleton<ResourceManager>
    {

        Dictionary<eResourceLoaderTypes, ILoadResources> ResourceLoaders;
        ILoadResources CurResourceLoader;

        public ResourceAsyncController AsyncController { get; private set; }

        // transforms for storing resources
        public Transform ResourcesTransform { get; private set; }

        public ResourceManager()
        {
            // Create resource container, default ILoadResources class and add it
            ResourceLoaders = new Dictionary<eResourceLoaderTypes, ILoadResources>();
            SetResourceLoaderType(eResourceLoaderTypes.FactoryLogger);

            AsyncController = null;
            ResourcesTransform = null;

        }

        //--------------------------------------------------------------------------
        // Async functions
        //--------------------------------------------------------------------------
        public void TryCreateAsyncController()
        {
            if(AsyncController == null)
            {
                GameObject obj = new GameObject();
                AsyncController = obj.AddComponent<ResourceAsyncController>();
                UnityEngine.Object.DontDestroyOnLoad(obj);
            }
        }

        public bool IsAsyncLoadingComplete()
        {
            if (AsyncController == null)
                return true;

            return AsyncController.isComplete();
        }

        //--------------------------------------------------------------------------
        // Manager functions
        //--------------------------------------------------------------------------
        public void SetResourceLoaderType(eResourceLoaderTypes type)
        {
            ILoadResources loader;
            if (GetResourceLoader(type, out loader))
            {
                CurResourceLoader = loader;
            }
            else
            {
                switch (type)
                {
                    case eResourceLoaderTypes.Logger:
                        CurResourceLoader = new LogResourceLoader();
                        break;
                    case eResourceLoaderTypes.Factory:
                        CurResourceLoader = new FactoryResourceLoader();
                        break;
                    case eResourceLoaderTypes.FactoryLogger:
                        CurResourceLoader = new LogFactoryResourceLoader();
                        break;
                    case eResourceLoaderTypes.Default:
                        CurResourceLoader = new DefaultResourceLoader();
                        break;
                }
                ResourceLoaders.Add(type, CurResourceLoader);
            }
        }
        public ILoadResources GetResourceLoader(eResourceLoaderTypes type)
        {
            ILoadResources loader;
            if (!ResourceLoaders.TryGetValue(type, out loader))
            {
                return default(ILoadResources);
            }
            return loader;
        }
        public bool GetResourceLoader(eResourceLoaderTypes type, out ILoadResources loader)
        {
            if (!ResourceLoaders.TryGetValue(type, out loader))
            {
                return false;
            }
            return true;
        }
        public void SetResourcesTransform(GameObject resourceLocation)
        {
            if (resourceLocation != null) {
                GameObject.DontDestroyOnLoad(resourceLocation);
                ResourcesTransform = resourceLocation.transform;
            }
        }
        private void CreateResourcesTransform()
        {
            // create resources parent
            GameObject rParent = new GameObject("LHSResourceManager");
            GameObject.DontDestroyOnLoad(rParent);
            ResourcesTransform = rParent.transform;
        }
        public void ClearResourceLoaders()
        {
            foreach (KeyValuePair<eResourceLoaderTypes, ILoadResources> loader in ResourceLoaders)
            {
                loader.Value.Clear();
            }
        }

        //--------------------------------------------------------------------------

        //--------------------------------------------------------------------------
        // ILoadResources functions
        //--------------------------------------------------------------------------
        // PreLoad
        public void PreLoad(TextAsset asset)
        {
            if (ResourcesTransform == null)
                CreateResourcesTransform();
            CurResourceLoader.PreLoad(asset);
        }
        public T Get<T>(string pathOrId) where T : UnityEngine.Object
        {
            return CurResourceLoader.Get<T>(pathOrId);
        }
        public T[] GetAll<T>(string path) where T : UnityEngine.Object
        {
            return CurResourceLoader.GetAll<T>(path);
        }
        public void Set<T>(string pathOrId, T Resource) where T : UnityEngine.Object
        {
            CurResourceLoader.Set<T>(pathOrId, Resource);
        }
        //--------------------------------------------------------------------------

        //--------------------------------------------------------------------------
        // Debug
        //--------------------------------------------------------------------------
        public void Log()
        {
            CurResourceLoader.Log();
        }
        public void Export(string path)
        {
            CurResourceLoader.Export(path);
        }
        public void ExportAll(string path)
        {
            foreach(KeyValuePair<eResourceLoaderTypes, ILoadResources> kvp in ResourceLoaders)
            {
                kvp.Value.Export(path + "/" + kvp.Key.ToString());
            }
        }
        //--------------------------------------------------------------------------



        //--------------------------------------------------------------------------
        // Unity Engine Resources functions
        //--------------------------------------------------------------------------
        // Load
        public UnityEngine.Object Load(string path)
        {
            return CurResourceLoader.Load(path);
        }
        public UnityEngine.Object Load(string path, Type systemTypeInstance)
        {
            return CurResourceLoader.Load(path, systemTypeInstance);
        }
        public T Load<T>(string path) where T : UnityEngine.Object
        {
            return CurResourceLoader.Load<T>(path);
        }

        // LoadAll
        public UnityEngine.Object[] LoadAll(string path)
        {
            return CurResourceLoader.LoadAll(path);
        }
        public UnityEngine.Object[] LoadAll(string path, Type systemTypeInstance)
        {
            return CurResourceLoader.LoadAll(path, systemTypeInstance);
        }
        public T[] LoadAll<T>(string path) where T : UnityEngine.Object
        {
            return CurResourceLoader.LoadAll<T>(path);
        }

        // LoadAsync
        public UnityEngine.ResourceRequest LoadAsync(string path)
        {
            return CurResourceLoader.LoadAsync(path);
        }
        public UnityEngine.ResourceRequest LoadAsync(string path, Type type)
        {
            return CurResourceLoader.LoadAsync(path, type);
        }
        public UnityEngine.ResourceRequest LoadAsync<T>(string path) where T : UnityEngine.Object
        {
            return CurResourceLoader.LoadAsync<T>(path);
        }

        //--------------------------------------------------------------------------

    }
}
