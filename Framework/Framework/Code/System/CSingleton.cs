﻿/*
    CSingleton.cs

    Name: Steven Ogden
    Date: Unknown

    Provided by Steven Ogden copyright free as part of the GNU license.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS
{
    namespace Framework
    {

        public interface ICSingleton
        {
            void Init();
            void Deinit();
        }

        public abstract class CSingleton<T> where T : new()
        {
            private static T _instance;
            public static T instance { get { return (_instance != null) ? _instance : (_instance = new T()); } }

        }

        public abstract class CSingletonMonoBehaviour<T> : MonoBehaviour
        {
            public static T instance { get; private set; }

            public void Awake()
            {
                instance = (T)(object)this;
                OnAwake();
            }

            public abstract void OnAwake();
        }
    }
}

