﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;

namespace LHS.Framework
{

    public interface ILoadResources
    {
        // PreLoad
        void PreLoad(TextAsset asset);

        // Get/Set
        T Get<T>(string path) where T : UnityEngine.Object;
        T[] GetAll<T>(string path) where T : UnityEngine.Object;
        void Set<T>(string path, T Resource) where T : UnityEngine.Object;

        // Debug
        void Log();
        void Export(string path);

        // Load
        UnityEngine.Object Load(string path);
        UnityEngine.Object Load(string path, Type systemTypeInstance);
        T Load<T>(string path) where T : UnityEngine.Object;

        // LoadAll
        UnityEngine.Object[] LoadAll(string path);
        UnityEngine.Object[] LoadAll(string path, Type systemTypeInstance);
        T[] LoadAll<T>(string path) where T : UnityEngine.Object;

        // LoadAsync
        UnityEngine.ResourceRequest LoadAsync(string path);
        UnityEngine.ResourceRequest LoadAsync(string path, Type type);
        UnityEngine.ResourceRequest LoadAsync<T>(string path) where T : UnityEngine.Object;

        // Clear
        void Clear();
    }

    public class CResourceLoader : ILoadResources
    {

        // PreLoad
        virtual public void PreLoad(TextAsset asset)
        {

        }

        virtual public T Get<T>(string path) where T : UnityEngine.Object
        {
            return Load<T>(path);
        }
        virtual public T[] GetAll<T>(string path) where T : UnityEngine.Object
        {
            return LoadAll<T>(path);
        }
        virtual public void Set<T>(string path, T Resource) where T : UnityEngine.Object
        {
            // return CurResourceLoader.Load<T>(path);
        }

        // Debug
        public void Log()
        {
            LHSDebug.Log("CResourceLoader: \n" + this.ToString());
        }
        virtual public void Export(string path)
        {

        }

        // Load
        virtual public UnityEngine.Object Load(string path)
        {
            return Resources.Load(path);
        }
        virtual public UnityEngine.Object Load(string path, Type systemTypeInstance)
        {
            return Resources.Load(path, systemTypeInstance);
        }
        virtual public T Load<T>(string path) where T : UnityEngine.Object
        {
            if (typeof(T) == typeof(Sprite))
            {
                AtlasedSpriteRef atlasedRef = Resources.Load<AtlasedSpriteRef>(path);
                if (atlasedRef != null)
                    return atlasedRef.sprites[0] as T;
            }

            return Resources.Load<T>(path);
        }

        // LoadAll
        virtual public UnityEngine.Object[] LoadAll(string path)
        {
            return Resources.LoadAll(path);
        }
        virtual public UnityEngine.Object[] LoadAll(string path, Type systemTypeInstance)
        {
            return Resources.LoadAll(path, systemTypeInstance);
        }
        virtual public T[] LoadAll<T>(string path) where T : UnityEngine.Object
        {
            if(typeof(T) == typeof(Sprite))
            {
                AtlasedSpriteRef atlasedRef = Resources.Load<AtlasedSpriteRef>(path);
                if (atlasedRef != null)
                    return atlasedRef.sprites as T[];
            }

            return Resources.LoadAll<T>(path);
        }

        // LoadAsync
        virtual public UnityEngine.ResourceRequest LoadAsync(string path)
        {
            return Resources.LoadAsync(path);
        }
        virtual public UnityEngine.ResourceRequest LoadAsync(string path, Type type)
        {
            return Resources.LoadAsync(path, type);
        }
        virtual public UnityEngine.ResourceRequest LoadAsync<T>(string path) where T : UnityEngine.Object
        {
            return Resources.LoadAsync<T>(path);
        }

        // Clear
        virtual public void Clear()
        {

        }

    }

    // resource loaders
    public class DefaultResourceLoader : CResourceLoader
    {

    }

    public class LogResourceLoader : CResourceLoader
    {
        List<string> LogBook;
        Dictionary<string, int> LogCounts;

        public LogResourceLoader() : base()
        {
            LogBook = new List<string>();
            LogCounts = new Dictionary<string, int>();
        }

        // Debug
        override public void Export(string path)
        {
            File.WriteAllText(Application.dataPath + "/" + path + ".txt", this.GetExportData());
        }

        // Load
        override public UnityEngine.Object Load(string path)
        {
            AddLog("Load: ", path);
            return base.Load(path);
        }
        override public UnityEngine.Object Load(string path, Type systemTypeInstance)
        {
            AddLog("Load: ", path, systemTypeInstance);
            return base.Load(path, systemTypeInstance);
        }
        override public T Load<T>(string path)
        {
            AddLog("Load<T>: ", path, typeof(T));
            return base.Load<T>(path);
        }

        // LoadAll
        override public UnityEngine.Object[] LoadAll(string path)
        {
            AddLog("LoadAll: ", path);
            return base.LoadAll(path);
        }
        override public UnityEngine.Object[] LoadAll(string path, Type systemTypeInstance)
        {
            AddLog("LoadAll: ", path, systemTypeInstance);
            return base.LoadAll(path, systemTypeInstance);
        }
        override public T[] LoadAll<T>(string path)
        {
            AddLog("LoadAll<T>: ", path, typeof(T));
            return base.LoadAll<T>(path);
        }

        // logging
        private void AddLog(string text, string path)
        {
            LogBook.Add(text + path + " - " + Time.realtimeSinceStartup.ToString());
            CountLog(path);
        }
        private void AddLog(string text, string path, Type type)
        {
            LogBook.Add(text + path + " - " + Time.realtimeSinceStartup.ToString() + " - " + type.Name);
            CountLog(path);
        }
        private void CountLog(string path)
        {
            if (LogCounts.ContainsKey(path))
            {
                LogCounts[path]++;
            }
            else
            {
                LogCounts.Add(path, 1);
            }
        }
        private string GetExportData()
        {
            string s = "Log Book \n";
            foreach (string str in LogBook)
            {
                s += (str + "\n");
            }

            s += "\n\nLog Counts \n";
            int total = 0;
            foreach (KeyValuePair<string, int> kvp in LogCounts)
            {
                s += (kvp.Value + ": " + kvp.Key + "\n");
                total += kvp.Value;
            }

            s += ("\n\nTotal Loads: " + total);

            s += "\n\nEnd of Log";

            return s;
        }

        // Clear
        override public void Clear()
        {
            LogBook.Clear();
            LogCounts.Clear();
        }
    }

    // The Factory will generate and store 
    public class FactoryResourceLoader : CResourceLoader
    {
        protected Dictionary<int, IHoldResources> Holders;

        public FactoryResourceLoader() : base()
        {
            Holders = new Dictionary<int, IHoldResources>();
        }

        
        /*override public T Load<T>(string path)
        {
            int code = path.GetHashCode();
            IHoldResources Holder;
            if (!Holders.TryGetValue(code, out Holder))
            {
                Holder = CreateLoadHolder<T>(typeof(T), path);
                Holders.Add(code, Holder);
            }

            return Holder.Get<T>();
        }*/

        override public T Get<T>(string path)
        {
            int code = path.GetHashCode();
            IHoldResources Holder;
            if (!Holders.TryGetValue(code, out Holder))
            {
                Holder = CreateHolder<T>(typeof(T), path);
                Holders.Add(code, Holder);
            }

            return Holder.Get<T>();
        }
        override public T[] GetAll<T>(string path)
        {
            int code = path.GetHashCode();
            IHoldResources Holder;
            if (!Holders.TryGetValue(code, out Holder))
            {
                Holder = CreateHolderAll<T>(typeof(T), path);
                Holders.Add(code, Holder);
            }
            
            return Holder.GetAll<T>();
        }
        override public void Set<T>(string path, T Resource)
        {
            int code = path.GetHashCode();
            IHoldResources Holder;
            if (Holders.TryGetValue(code, out Holder))
            {
                Holder.Set<T>(Resource);
            }
        }

        virtual protected IHoldResources CreateHolder<T>(Type type, string path) where T : UnityEngine.Object
        {
            switch (type.Name)
            {
                case "GameObject":
                    return GameObjectHolder.CreateGameObjectHolder(path);
            }
            return TypeHolder<T>.CreateTypeHolder<T>(path);

        }

        virtual protected IHoldResources CreateLoadHolder<T>(Type type, string path) where T : UnityEngine.Object
        {
            switch (type.Name)
            {
                case "GameObject":
                    return InstantiatorHolder.CreateInstantiatorHolder(path);
            }
            return TypeHolder<T>.CreateTypeHolder<T>(path);

        }

        virtual protected IHoldResources CreateHolderAll<T>(Type type, string path) where T : UnityEngine.Object
        {
            return TypeHolderMulti<T>.CreateTypeHolderMulti<T>(path);
        }

        // PreLoad
        override public void PreLoad(TextAsset asset)
        {
            XmlDocument m_xmlDoc = new XmlDocument();
            m_xmlDoc.LoadXml(asset.text);

            XmlNode nodeGameObjects = m_xmlDoc.SelectSingleNode("/Resources");
            for (XmlNode node = nodeGameObjects.FirstChild; node != null; node = node.NextSibling)
            {

                switch (node.Name) {
                    case "GameObjects":
                        for (XmlNode nodeItem = node.FirstChild; nodeItem != null; nodeItem = nodeItem.NextSibling)
                        {
                            if (node != null && node.Attributes != null)
                            {

                                // load each GaemObject
                                //<item id="BUILDING_MENU" path="UI/BuildingInteractMenu" amount="2" cangrow="True"/>

                                bool idFound = (nodeItem.Attributes["id"] != null);
                                bool pathFound = (nodeItem.Attributes["path"] != null);

                                if (pathFound)
                                {
                                    string id = (idFound) ? nodeItem.Attributes["id"].Value : "NOT_FOUND";
                                    string path = (pathFound) ? nodeItem.Attributes["path"].Value : "NOT_FOUND";
                                    int amount = (nodeItem.Attributes["amount"] != null) ? int.Parse(nodeItem.Attributes["amount"].Value) : 1;
                                    bool cangrow = (nodeItem.Attributes["cangrow"] != null) ? bool.Parse(nodeItem.Attributes["cangrow"].Value) : true;
                                    bool async = (nodeItem.Attributes["async"] != null) ? bool.Parse(nodeItem.Attributes["async"].Value) : false;

                                    string strToUse = (idFound) ? id : path;

                                    IHoldResources Holder;
                                    int code = strToUse.GetHashCode();
                                    if (!Holders.TryGetValue(code, out Holder))
                                    {
                                        Holder = GameObjectHolder.CreateGameObjectHolder(path, amount, cangrow, async);
                                        Holders.Add(code, Holder);
                                    }

                                }
                            }
                        }

                        break;

                    case "Objects":

                        for (XmlNode nodeItem = node.FirstChild; nodeItem != null; nodeItem = nodeItem.NextSibling)
                        {
                            if (node != null && node.Attributes != null)
                            {
                                // load each Object
                                //< item type = "Sprite" path = "TimeLine/HUD_event_sheet" array = "True" />

                                bool typeFound = (nodeItem.Attributes["type"] != null);
                                bool pathFound = (nodeItem.Attributes["path"] != null);

                                if (typeFound && pathFound)
                                {
                                    string path = nodeItem.Attributes["path"].Value;
                                    string type = nodeItem.Attributes["type"].Value;
                                    bool array = (nodeItem.Attributes["array"] != null) ? bool.Parse(nodeItem.Attributes["array"].Value) : false;
                                    bool async = (nodeItem.Attributes["async"] != null) ? bool.Parse(nodeItem.Attributes["async"].Value) : false;


                                    IHoldResources Holder;
                                    int code = path.GetHashCode();
                                    if (!Holders.TryGetValue(code, out Holder))
                                    {
                                        switch (type)
                                        {
                                            case "Sprite":
                                                if (array)
                                                    Holder = TypeHolderMulti<Sprite>.CreateTypeHolderMulti<Sprite>(path, async);
                                                else
                                                    Holder = TypeHolder<Sprite>.CreateTypeHolder<Sprite>(path, async);
                                                Holders.Add(code, Holder);
                                                break;
                                            case "TextAsset":
                                                Holder = TypeHolder<TextAsset>.CreateTypeHolder<TextAsset>(path, async);
                                                Holders.Add(code, Holder);
                                                break;
                                                /*case "GameObject":
                                                    Holder = InstantiatorHolder.CreateInstantiatorHolder(path, async);
                                                    Holders.Add(code, Holder);
                                                    break;*/
                                        }

                                    }
                                }
                            }
                        }

                        break;
                    
                }
            }
        }

        // Clear
        override public void Clear()
        {
            foreach (KeyValuePair<int, IHoldResources> holder in Holders)
            {
                holder.Value.ClearAll();
            }
            Holders.Clear();
        }

    }

    public class LogFactoryResourceLoader : FactoryResourceLoader
    {
        List<string> LogBook;
        Dictionary<string, int> LogCounts;
        List<string> LogLoads;
        Dictionary<string, int> LogGets;
        Dictionary<string, int> LogSets;

        public LogFactoryResourceLoader() : base()
        {
            LogBook = new List<string>();
            LogLoads = new List<string>();
            LogCounts = new Dictionary<string, int>();
            LogSets = new Dictionary<string, int>();
            LogGets = new Dictionary<string, int>();
        }

        // Debug
        override public void Export(string path)
        {
            File.WriteAllText(Application.dataPath + "/" + path + ".txt", this.GetExportData());
        }

        override public T Get<T>(string path)
        {
            AddLog(LogBook, LogGets, "Get<T>: ", path, typeof(T));
            return base.Get<T>(path);
        }
        override public T[] GetAll<T>(string path)
        {
            AddLog(LogBook, LogGets, "GetAll<T>: ", path, typeof(T));
            return base.GetAll<T>(path);
        }
        override public void Set<T>(string path, T Resource)
        {
            AddLog(LogBook, LogSets, "Set<T>: ", path, typeof(T));
            base.Set<T>(path, Resource);
        }

        // Load
        override public UnityEngine.Object Load(string path)
        {
            AddLog(LogBook, LogCounts, "Load: ", path);
            return base.Load(path);
        }
        override public UnityEngine.Object Load(string path, Type systemTypeInstance)
        {
            AddLog(LogBook, LogCounts, "Load: ", path, systemTypeInstance);
            return base.Load(path, systemTypeInstance);
        }
        override public T Load<T>(string path)
        {
            AddLog(LogBook, LogCounts, "Load<T>: ", path, typeof(T));
            return base.Load<T>(path);
        }

        // LoadAll
        override public UnityEngine.Object[] LoadAll(string path)
        {
            AddLog(LogBook, LogCounts, "LoadAll: ", path);
            return base.LoadAll(path);
        }
        override public UnityEngine.Object[] LoadAll(string path, Type systemTypeInstance)
        {
            AddLog(LogBook, LogCounts, "LoadAll: ", path, systemTypeInstance);
            return base.LoadAll(path, systemTypeInstance);
        }
        override public T[] LoadAll<T>(string path)
        {
            AddLog(LogBook, LogCounts, "LoadAll<T>: ", path, typeof(T));
            return base.LoadAll<T>(path);
        }

        // logging
        private void AddLog(List<string> list, Dictionary<string, int> dic, string text, string path)
        {
            list.Add(text + path + " - " + Time.realtimeSinceStartup.ToString());
            CountLog(dic, path);
        }
        private void AddLog(List<string> list, Dictionary<string, int> dic, string text, string path, Type type)
        {
            list.Add(text + path + " - " + Time.realtimeSinceStartup.ToString() + " - " + type.Name);
            CountLog(dic, path);
        }
        private void CountLog(Dictionary<string, int> dic, string path)
        {
            if (dic.ContainsKey(path))
            {
                dic[path]++;
            }
            else
            {
                dic.Add(path, 1);
            }
        }
        private string GetExportData()
        {
            string s = "Log Book \n";
            foreach (string str in LogBook)
            {
                s += (str + "\n");
            }

            s += "\n\nLog Loads - Holders \n";
            foreach (string str in LogLoads)
            {
                s += (str + "\n");
            }

            s += "\n\nLog Counts \n";
            int total = 0;
            foreach (KeyValuePair<string, int> kvp in LogCounts)
            {
                s += (kvp.Value + ": " + kvp.Key + "\n");
                total += kvp.Value;
            }

            s += ("\n\nTotal Loads: " + total);

            s += "\n\nLog Gets \n";
            total = 0;
            foreach (KeyValuePair<string, int> kvp in LogGets)
            {
                s += (kvp.Value + ": " + kvp.Key + "\n");
                total += kvp.Value;
            }

            s += ("\n\nTotal Gets: " + total);

            s += "\n\nLog Sets \n";
            total = 0;
            foreach (KeyValuePair<string, int> kvp in LogSets)
            {
                s += (kvp.Value + ": " + kvp.Key + "\n");
                total += kvp.Value;
            }

            s += ("\n\nTotal Sets: " + total);

            s += "\n\nEnd of Log";

            return s;
        }

        override public void PreLoad(TextAsset asset)
        {
            base.PreLoad(asset);

            // loop over holders and record them
            LogLoads.Add("Holders Size: " + Holders.Count);
            foreach (KeyValuePair<int, IHoldResources> kvp in this.Holders)
            {
                LogLoads.Add("KeyCode: " + kvp.Key + " - " + kvp.Value.ToString());
            }
        }

    }
}
