﻿/*
    WeightedObject.cs -- Weighted probability system used for more efficient random allocation in a game

    Name: Daniel Schofiled
    Date: 28/11/2016
    
    Copyright Lionsheart Studios. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LHS.Framework.Game.Probability
{
    public class WeightedObject
    {
        float defaultWeight;

        string value;
        float weight;

        public WeightedObject(string input, float defaultVal)
        {
            value = input;
            defaultWeight = defaultVal;

            Reset();
        }

        public float GetWeight()
        {
            return weight;
        }

        public void Reset()
        {
            weight = defaultWeight;
        }

        public void Increase(float factor)
        {
            weight += factor;
        }

        public string GetValue()
        {
            return value;
        }
    }

    public class WeightedRandomisation
    {
        float incrementFactor;


        System.Random rand = new System.Random();
        List<WeightedObject> Objects;

        public WeightedRandomisation(float increment)
        {
            incrementFactor = increment;

            Objects = new List<WeightedObject>();
        }

        public void Register(WeightedObject weightedObj)
        {
            Objects.Add(weightedObj);
        }

        public WeightedObject Choose()
        {
            float totalWeight = Objects.Sum(obj => obj.GetWeight());
            float itemWeightIndex = (float)rand.NextDouble() * totalWeight;
            float currentWeightIndex = 0f;
            int index = 0;

            foreach (WeightedObject wObj in Objects)
            {
                currentWeightIndex += wObj.GetWeight();

                if (currentWeightIndex >= itemWeightIndex)
                {
                    IncrementByFactor(index);
                    return wObj;
                }

                index++;
            }

            return null;
        }

        public void IncrementByFactor(int index)
        {
            int count = 0;

            foreach (WeightedObject wObj in Objects)
            {
                if (count != index)
                    wObj.Increase(incrementFactor);
                else
                    wObj.Reset();

                count++;
            }
        }
    }
}
