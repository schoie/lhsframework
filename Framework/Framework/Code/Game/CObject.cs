﻿/*
    CObject.cs -- Part of the object system, designed for a generic editor

    Name: Steven Ogden
    Date: 23/03/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Game
{

    // This class is used with scripts that will place objects in menus
    // it allows us to define some spacing data and have it visible in teh editor
    [Serializable]
    public class ObjectSpacing
    {
        public float header;
        public float BetweenElements;
        public float TitleIndent;
        public float Indent;
    }

    public class CObject : ScriptableObject, IObject
    {

        [Header("Attributes")]
        public string Name = "Object Name";

        public virtual void BuildPrefab(ref GameObject obj)
        {
            // Will be override by Object class
            LHSDebug.LogFormat("BuildPrefab - {0} - {1}", this.GetType().Name, this.Name);
        }
        public virtual void BuildAttributeMenu(ref GameObject obj, GameObject[] resources, ObjectSpacing spacing)
        {
            // Will be override by Object class
            LHSDebug.LogFormat("BuildAttributeMenu - {0} - {1}", this.GetType().Name, this.Name);
        }
        public virtual void BuildExtrasMenu(ref GameObject obj, GameObject[] resources, ObjectSpacing spacing)
        {
            // Will be override by Object class
            LHSDebug.LogFormat("BuildExtrasMenu - {0} - {1}", this.GetType().Name, this.Name);
        }

        //-------------------------------------------------------------------------------------
        // Interface
        //-------------------------------------------------------------------------------------
        public virtual void OnCreate()
        {
            LHSDebug.LogFormat("OnCreate - {0} - {1}", this.GetType().Name, this.Name);
        }
        public virtual void OnEnable()
        {
        }
        public virtual void OnReload()
        {
            LHSDebug.LogFormat("OnCreate - {0} - {1}", this.GetType().Name, this.Name);
        }
        public void OnUpdate()
        {
            LHSDebug.LogFormat("OnUpdate - {0} - {1}", this.GetType().Name, this.Name);
        }
        public void OnEditorChange()
        {
            this.name = Name;
        }
        //-------------------------------------------------------------------------------------

    }
    
}
