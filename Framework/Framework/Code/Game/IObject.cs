﻿/*
    IObject.cs -- Part of the object system, designed for a generic editor

    Name: Steven Ogden
    Date: 23/03/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Game
{
    public interface IObject
    {

        void OnCreate();
        void OnUpdate();

        // OnCreate
        // OnDestroy

        void OnEditorChange();

    }
}
