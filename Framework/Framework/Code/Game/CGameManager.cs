﻿/*
    CGameManager.cs -- Part of the object system, designed for a generic editor

    Name: Steven Ogden
    Date: 23/03/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Game
{
    public class CGameManager<T> : CSingleton<T> where T : new()
    {

        public List<IObject> Objects;

        public CGameManager()
        {
            Objects = new List<IObject>();
        }

    }
    
    public class CGameManagerMonoBehaviour<T> : CSingletonMonoBehaviour<T>
    {
        public List<IObject> Objects;

        public override void OnAwake()
        {
            Objects = new List<IObject>();
        }

    }

    /*
    public abstract class LHSObjects
    {
        public static void BuildScriptableObjects<T>(ref T[] objectArray) where T : CObject
        {
            for (int i = 0; i < objectArray.Length; ++i)
            {
                if (objectArray[i] == null)
                {
                    objectArray[i] = (i == 0) ? ScriptableObject.CreateInstance<T>() : ScriptableObject.Instantiate<T>(objectArray[0]);
                }
            }
        }
    }
    */
}
