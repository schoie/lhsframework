﻿using LHS.Framework;
using LHS.Framework.Audio;
using LHS.Framework.Events;
using LHS.Framework.GameData;
using LHS.Framework.Tools;
using LHS.Framework.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;
using UnityEngine.Events;

namespace LHS.Framework.Game
{
    public class CharacterPopUpController : CSingletonMonoBehaviour<CharacterPopUpController>
    {

        public TextAsset[] XMLs;
        public UIPopUpCharacter PopUpUI;
        public bool DisablePopups = false;

        public override void OnAwake()
        {
            if (PopUpUI == null)
            {
                enabled = false;
                return;
            }

            CharacterPopUpManager PopupManager = CharacterPopUpManager.instance;
            PopupManager.Initialise(PopUpUI, DisablePopups);

            bool clear = true;
            foreach (TextAsset xml in XMLs)
            {
                PopupManager.LoadPopups(xml, true);
                clear = false;
            }

        }
    }

    public class CharacterPopUpManager : CSingleton<CharacterPopUpManager>
    {

        public static string EVENTS_CATEGORY = "CHARACTERPOPUPS";

        public class CharacterMultiData
        {
            public string label;
            public string message;

            public CharacterMultiData(string _label, string _message)
            {
                label = _label;
                message = _message;
            }

            public void TriggerEvent()
            {
                LHSDebug.Log("CharacterMultiData - TriggerEvent - TimelineTutorial: " + label);
                EventsManager.instance.OnEvent(EVENTS_CATEGORY, label, null);
            }
        }

        public class CharacterPopUp
        {
            public string message;
            public Sprite image;
            public string sound = null;
            public string multi = null;
            public bool single_trigger;
            public bool triggered;
            public bool localised;

            public void InitUI(UIPopUpCharacter PopUpUI)
            {
                // check if popup is active
                // if so lets disable it
                if (PopUpUI.gameObject.activeSelf)
                    PopUpUI.gameObject.SetActive(false);

                // set UI settings for the image
                PopUpUI.SetHelperImage(image);
            }

            public string GetMessage(out List<CharacterMultiData> _list)
            {
                _list = null;
                if (localised)
                {
                    if(multi != null)
                    {
                        _list = new List<CharacterMultiData>();
                        int count = 0;
                        while (true)
                        {
                            string label = message + string.Format(multi, count);
                            string msg = LocalisationReader.instance.GetLocalisedString(label);
                            if (msg == null || msg == "")
                                break;
                            _list.Add(new CharacterMultiData(label, msg));
                            count++;
                        }
                        return (_list.Count > 0) ? _list[0].message : "Multi Message Failed to LOAD!!";
                    }

                    return LocalisationReader.instance.GetLocalisedString(message);
                }

                return message;
            }

            public void TriggerMessage(UIPopUpCharacter PopUpUI, string _message)
            {
                PopUpUI.ShowHelperMessage(_message);
                HandleTrigger();
            }

            public void TriggerMessage(UIPopUpCharacter PopUpUI, List<CharacterMultiData> _messages)
            {
                PopUpUI.ShowHelperMessage(_messages);
                HandleTrigger();
            }

            private void HandleTrigger()
            {
                triggered = true;
                if (sound != null)
                    SoundManager.instance.Play(sound);
            }

        }
        
        public UIPopUpCharacter PopUpUI;
        public bool DisablePopups = false;

        Dictionary<string, CharacterPopUp> Popups;

        public CharacterPopUpManager()
        {
            Popups = new Dictionary<string, CharacterPopUp>();

            EventsManager.instance.CreateCategory(EVENTS_CATEGORY);

        }

        public void Initialise(UIPopUpCharacter _PopUpUI, bool _DisablePopups)
        {
            PopUpUI = _PopUpUI;
            DisablePopups = _DisablePopups;
        }

        public void TriggerPopUp(string trigger)
        {
            if (DisablePopups)
                return;

            CharacterPopUp Popup;
            if (GetCharacterPopUp(trigger, out Popup))
            {
                // prepare
                Popup.InitUI(PopUpUI);

                // then trigger the next message
                List<CharacterMultiData> MultiMessages;
                string message = Popup.GetMessage(out MultiMessages);

                // Call OnEvent for this Popup - we do this before so that we can tell if it has been triggered before
                EventsManager.instance.OnEvent(EVENTS_CATEGORY, trigger, new EventHolderVar<CharacterPopUp>(Popup));

                // trigger
                if (MultiMessages == null)
                    Popup.TriggerMessage(PopUpUI, message);
                else
                    Popup.TriggerMessage(PopUpUI, MultiMessages);
            }

        }

        public string GetTriggerMessage(string trigger)
        {
            CharacterPopUp Popup;
            if (GetCharacterPopUp(trigger, out Popup))
            {
                // then trigger the next message
                List<CharacterMultiData> MultiMessages;
                return Popup.GetMessage(out MultiMessages);
            }

            return null;
        }

        public List<CharacterMultiData> GetTriggerMessageList(string trigger)
        {
            CharacterPopUp Popup;
            if (GetCharacterPopUp(trigger, out Popup))
            {
                // then trigger the next message
                List<CharacterMultiData> MultiMessages;
                Popup.GetMessage(out MultiMessages);
                return MultiMessages; 
            }

            return null;
        }


        public void TriggerPopUp<ReflectionClass>(string trigger, ReflectionClass Data) where ReflectionClass : new()
        {
            if (DisablePopups)
                return;

            CharacterPopUp Popup;
            if (GetCharacterPopUp(trigger, out Popup))
            {
                // prepare
                Popup.InitUI(PopUpUI);

                // then trigger the next message
                List<CharacterMultiData> MultiMessages;
                string message = Popup.GetMessage(out MultiMessages);

                // Call OnEvent for this Popup - we do this before so that we can tell if it has been triggered before
                EventsManager.instance.OnEvent(EVENTS_CATEGORY, trigger, new EventHolderVar<CharacterPopUp>(Popup));

                // trigger
                if (MultiMessages == null)
                {
                    message = ReflectionFunctions.SetString<ReflectionClass>(message, Data);
                    Popup.TriggerMessage(PopUpUI, message);
                }
                else
                {
                    for(int i = 0; i < MultiMessages.Count; ++i)
                        MultiMessages[i].message = ReflectionFunctions.SetString<ReflectionClass>(MultiMessages[i].message, Data);
                    Popup.TriggerMessage(PopUpUI, MultiMessages);
                }

            }

        }

        public void PreparePopupButton(string buttonName, UnityAction function, string sprite)
        {
            PopUpUI.SetPopUpButton(buttonName, function, sprite);
        }

        private bool GetCharacterPopUp(string trigger, out CharacterPopUp Popup)
        {
            if(Popups.TryGetValue(trigger, out Popup))
            {
                // if the single flag is set get out of here, do not trigger
                if (Popup.single_trigger && Popup.triggered)
                    return false;

                return true;
            }
            return false;
        }

        public void LoadPopups(TextAsset _xml, bool clear = false)
        {
            if (_xml == null)
                return;

            if (clear)
            {
                Popups.Clear();
            }

            XmlDocument m_xmlDoc = new XmlDocument();
            m_xmlDoc.LoadXml(_xml.text);

            XmlNode nodeObjects = m_xmlDoc.SelectSingleNode("/CharacterPopups");
            for (XmlNode node = nodeObjects.FirstChild; node != null; node = node.NextSibling)
            {
                if (node != null && node.Attributes != null)
                {
                    if (node.Attributes["trigger"].Value != null && node.Attributes["message"].Value != null && node.Attributes["image"].Value != null)
                    {

                        CharacterPopUp popup = new CharacterPopUp();

                        string trigger = node.Attributes["trigger"].Value;
                        popup.message = node.Attributes["message"].Value;
                        popup.single_trigger = (node.Attributes["single"] != null) ? node.Attributes["single"].Value == "True" : false;
                        popup.localised = (node.Attributes["localised"] != null) ? node.Attributes["localised"].Value == "True" : false;
                        popup.triggered = false;
                        popup.sound = (node.Attributes["sound"] != null) ? node.Attributes["sound"].Value : null;
                        popup.multi = (node.Attributes["multi"] != null) ? node.Attributes["multi"].Value : null;
                        popup.image = (Sprite)Resources.Load(node.Attributes["image"].Value, typeof(Sprite));

                        LHSDebug.LogFormat("CharacterPopUp - Trigger: {0}, Message: {1}, Single: {2}, Localised: {3}, image: {4}",
                            trigger, popup.message, popup.single_trigger, popup.localised, popup.image
                            );

                        // add to popups
                        Popups.Add(trigger, popup);

                    }
                }
            }


        }
    }
}
