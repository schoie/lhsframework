﻿/*
    IDataManager.cs -- Part of the object system, designed for a generic editor

    Name: Steven Ogden
    Date: 23/03/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LHS.Framework.Game
{
    public interface IDataManager<DataKey>
    {

        O Get<O>(DataKey key) where O : new();
        void Set(DataKey key, IObject value);

    }
}

