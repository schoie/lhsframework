﻿/*
    CDataManager.cs -- Part of the object system, designed for a generic editor

    Name: Steven Ogden
    Date: 23/03/2016

    Copyright Lionsheart Studios. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Game
{

    public class CDataManager<T, DataKey> : CSingleton<T>, IDataManager<DataKey> where T : new()
    {

        public Dictionary<DataKey, IObject> Objects;

        public CDataManager()
        {
            Objects = new Dictionary<DataKey, IObject>();
        }

        // Tempalted Get and Normal Set functions
        public O Get<O>(DataKey key) where O : new()
        {
            IObject value = null;
            Objects.TryGetValue(key, out value);
            return (O)value;
        }

        public void Set(DataKey key, IObject value)
        {
            Objects.Add(key, value);
        }

    }

    public class CDataManagerMonoBehaviour<T, DataKey> : CSingletonMonoBehaviour<T>, IDataManager<DataKey>
    {

        public Dictionary<DataKey, IObject> Objects;

        public override void OnAwake()
        {
            Objects = new Dictionary<DataKey, IObject>();
        }

        // Templated Get and Normal Set functions
        public O Get<O>(DataKey key) where O : new()
        {
            IObject value = null;
            Objects.TryGetValue(key, out value);
            return (O)value;
        }

        public void Set(DataKey key, IObject value)
        {
            Objects.Add(key, value);
        }

    }
    
    public class CDataManagersController<T, DataKey> : CSingleton<T> where T : new()
    {
        public Dictionary<int, IDataManager<DataKey>> Managers;

        public CDataManagersController()
        {
            Managers = new Dictionary<int, IDataManager<DataKey>>();
            LinkManagers();
        }

        protected virtual void LinkManagers()
        {
            // game will override this
        }
    }

    public class CDataManagersControllerMonoBehaviour<T, DataKey> : CSingletonMonoBehaviour<T>
    {
        public Dictionary<int, IDataManager<DataKey>> Managers;

        public override void OnAwake()
        {
            Managers = new Dictionary<int, IDataManager<DataKey>>();
            LinkManagers();
        }

        protected virtual void LinkManagers()
        {
            // game will override this
        }
    }

    public abstract class LHSObjects
    {
        public static void BuildScriptableObjects<T>(ref T[] objectArray) where T : CObject
        {
            for (int i = 0; i < objectArray.Length; ++i)
            {
                if (objectArray[i] == null)
                {
                    objectArray[i] = (i == 0) ? ScriptableObject.CreateInstance<T>() : ScriptableObject.Instantiate<T>(objectArray[0]);
                }
            }
        }

        public static void BuildScriptableObjects<T>(ref List<T> objectList) where T : CObject
        {
            for (int i = 0; i < objectList.Count; ++i)
            {
                if (objectList[i] == null)
                {
                    objectList[i] = (i == 0) ? ScriptableObject.CreateInstance<T>() : ScriptableObject.Instantiate<T>(objectList[0]);
                }
            }
        }

        public static T CreateScriptableObject<T>(ref List<T> objectList) where T : CObject
        {
            T ReturnObject;
            if (objectList.Count > 0)
                ReturnObject = ScriptableObject.Instantiate<T>(objectList[0]);
            else
                ReturnObject = ScriptableObject.CreateInstance<T>();

            objectList.Add(ReturnObject);
            return ReturnObject;
        }
    }
}
