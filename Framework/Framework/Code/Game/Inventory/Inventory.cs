﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LHS.Framework.Game.Inventory
{
    public class Content
    {
        BitArray data;
        public Content(uint _size)
        {
            data = new BitArray((int)_size);
            data.SetAll(false);
            
        }

        public void Set(int _index, bool _set)
        {
            data.Set(_index, _set);
        }

        public bool IsSet(int _index)
        {
            return data.Get(_index);
        }

        public int GetInt()
        {
            if (data.Length > 32)
                throw new ArgumentException("data length shall be at most 32 bits - in order to use get int. As aresult you're nto getting all the bits");

            int[] array = new int[1];
            data.CopyTo(array, 0);
            return array[0];
        }

        public int[] GetIntArray()
        {
            int[] array = new int[1];
            data.CopyTo(array, 0);
            return array;
        }

        public void SetInt(int _data)
        {
            BitArray toSet = new BitArray(System.BitConverter.GetBytes(_data));
            for (int i = 0; i < data.Count; ++i)
            {
                if (i >= toSet.Count)
                    break;

                data.Set(i, toSet.Get(i));
            }
        }

        public void SetInt(int[] array)
        {
            List<byte> bytes = new List<byte>();
            for(int i = 0; i < array.Length; ++i)
            {
                byte[] newBytes = System.BitConverter.GetBytes(array[i]);
                foreach (byte b in newBytes)
                    bytes.Add(b);
            }
            
            if(bytes.Count > 0)
            {
                BitArray toSet = new BitArray(bytes.ToArray());
                for(int i = 0; i < data.Count; ++i)
                {
                    if (i >= toSet.Count)
                        break;

                    data.Set(i, toSet.Get(i));
                }
            } 
        }
        
        public void GetData(out int unlocked, out int size)
        {
            unlocked = 0;
            size = data.Length;
            for(int i = 0; i < size; ++i)
            {
                unlocked += (IsSet(i)) ? 1 : 0;
            }
        }
    }

    public class Inventory
    {

        // public


        // private
        private Content Content;

        public Inventory(uint _slotSize)
        {
            if (_slotSize <= 0)
                throw new ArgumentException("Cannot create an Inventory with 0 Slots specified");

            Content = new Content(_slotSize);
        }


    }
}
