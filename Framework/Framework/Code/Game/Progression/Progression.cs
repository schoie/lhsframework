﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;
using UnityEngine.Events;

namespace LHS.Framework.Game.Progression
{
    //----------------------------------------------------------------------------------------------------
    // Conditions
    //----------------------------------------------------------------------------------------------------
    public enum eConditionTypes
    {
        Equals,
        LessThan,
        GreaterThan,

        LessThanEquals,
        GreaterThanEquals
    }

    public class Condition<DataType> where DataType : IComparable<DataType>
    {
        public bool Passed { get; protected set; }
        public DataType amount;

        public Condition(DataType _value)
        {
            Passed = false;
            amount = _value;
        }

        public void Reset()
        {
            Passed = false;
        }

        public virtual bool CheckPassed(DataType data)
        {
            return Passed;
        }
    }

    public class ConditionEquals<DataType> : Condition<DataType> where DataType : IComparable<DataType>
    {
        public ConditionEquals(DataType _value) : base(_value){}

        public override bool CheckPassed(DataType data)
        {
            Passed = data.CompareTo(amount) == 0;
            return Passed;
        }
    }
    public class ConditionLessThan<DataType> : Condition<DataType> where DataType : IComparable<DataType>
    {
        public ConditionLessThan(DataType _value) : base(_value) { }

        public override bool CheckPassed(DataType data)
        {
            Passed = data.CompareTo(amount) < 0;
            return Passed;
        }
    }
    public class ConditionGreaterThan<DataType> : Condition<DataType> where DataType : IComparable<DataType>
    {
        public ConditionGreaterThan(DataType _value) : base(_value) { }

        public override bool CheckPassed(DataType data)
        {
            Passed = data.CompareTo(amount) > 0;
            return Passed;
        }
    }
    public class ConditionLessThanEquals<DataType> : Condition<DataType> where DataType : IComparable<DataType>
    {
        public ConditionLessThanEquals(DataType _value) : base(_value) { }

        public override bool CheckPassed(DataType data)
        {
            Passed = data.CompareTo(amount) <= 0;
            return Passed;
        }
    }
    public class ConditionGreaterThanEquals<DataType> : Condition<DataType> where DataType : IComparable<DataType>
    {
        public ConditionGreaterThanEquals(DataType _value) : base(_value) { }

        public override bool CheckPassed(DataType data)
        {
            Passed = data.CompareTo(amount) >= 0;
            return Passed;
        }
    }
    //----------------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------------
    // Conditional Event
    //----------------------------------------------------------------------------------------------------
    public enum eConditionalEventTypes
    {
        Persistent,
        Contextual
    }

    public class ConditionalEvent<DataType> where DataType : IComparable<DataType>
    {
        bool repeats;
        bool triggerred;

        public UnityEvent Event;
        public UnityEvent ConstEvent;
        protected Dictionary<int, Condition<DataType>> Conditions;

        public ConditionalEvent(bool _repeats)
        {
            Event = new UnityEvent();
            ConstEvent = new UnityEvent();
            Conditions = new Dictionary<int, Condition<DataType>>();

            repeats = _repeats;
            triggerred = false;
        }

        public void AddCondition(string _name, DataType _value, eConditionTypes _type = eConditionTypes.GreaterThan)
        {
            int code = _name.GetHashCode();
            if (!Conditions.ContainsKey(code))
            {
                Conditions.Add(code, CreateCondition(_type, _value));
            }
        }

        protected Condition<DataType> CreateCondition(eConditionTypes _type, DataType _value)
        {
            switch (_type)
            {
                case eConditionTypes.Equals:
                    return new ConditionEquals<DataType>(_value);
                case eConditionTypes.LessThan:
                    return new ConditionLessThan<DataType>(_value);
                case eConditionTypes.GreaterThan:
                    return new ConditionGreaterThan<DataType>(_value);
                case eConditionTypes.LessThanEquals:
                    return new ConditionLessThanEquals<DataType>(_value);
                case eConditionTypes.GreaterThanEquals:
                    return new ConditionGreaterThanEquals<DataType>(_value);
            }

            return new ConditionGreaterThan<DataType>(_value);
        }

        public virtual void Check(int code, DataType data) { ; }


        public bool ProcessPassed()
        {
            return (repeats) ? ProcessPassedRepeat() : ProcessPassedNormal();
        }

        public bool ProcessPassedNormal()
        {
            foreach (KeyValuePair<int, Condition<DataType>> kvp in Conditions)
            {
                if (!kvp.Value.Passed)
                {
                    return false;
                }
            }
            return true;
        }

        public bool ProcessPassedRepeat()
        {
            foreach (KeyValuePair<int, Condition<DataType>> kvp in Conditions)
            {
                if (!kvp.Value.Passed)
                {
                    triggerred = false;
                    return false;
                }
            }
            return true;
        }

        public bool Handle()
        {
            return (repeats) ? HandleRepeat() : HandleNormal();
        }

        public bool HandleNormal()
        {
            if (!ProcessPassed())
                return false;

            // trigger event
            TriggerEvents();
            // for security we clear this event now
            RemoveEvents();

            return true;
        }

        public bool HandleRepeat()
        {
            if (!ProcessPassed())
                return false;

            if (!triggerred)
            {
                TriggerEvents();
                triggerred = true;

                foreach (KeyValuePair<int, Condition<DataType>> kvp in Conditions)
                {
                    kvp.Value.Reset();
                }
            }

            return false;
        }

        private void TriggerEvents()
        {
            Event.Invoke();
            TriggerConstEvents();
        }
        private void TriggerConstEvents()
        {
            ConstEvent.Invoke();
        }
        private void RemoveEvents()
        {
            Event.RemoveAllListeners();
            ConstEvent.RemoveAllListeners();
        }

        public void Clear()
        {
            Conditions.Clear();
        }

        public bool HandleEventsWithOptions(bool _events, bool _constEvents)
        {
            if (repeats)
            {
                return false;
            }

            if (!ProcessPassed())
                return false;

            if (_events)
            {
                TriggerEvents();
            }
            if (_constEvents)
            {
                TriggerConstEvents();
            }

            RemoveEvents();

            return true;
        }

    }

    public class ConditionalEventPersistent<DataType> : ConditionalEvent<DataType> where DataType : IComparable<DataType>
    {
        public ConditionalEventPersistent(bool _repeats) : base(_repeats) {}

        public override void Check(int code, DataType data)
        {
            foreach (KeyValuePair<int, Condition<DataType>> kvp in Conditions)
            {
                if (kvp.Key == code && !kvp.Value.Passed)
                    kvp.Value.CheckPassed(data);
            }
        }
    }

    public class ConditionalEventContextual<DataType> : ConditionalEvent<DataType> where DataType : IComparable<DataType>
    {
        public ConditionalEventContextual(bool _repeats) : base(_repeats) { }

        public override void Check(int code, DataType data)
        {
            foreach (KeyValuePair<int, Condition<DataType>> kvp in Conditions)
            {
                if (kvp.Key == code)
                {
                    kvp.Value.Reset();
                    kvp.Value.CheckPassed(data);
                }
            }
        }
    }

    //----------------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------------
    // Progression Object
    //----------------------------------------------------------------------------------------------------
    public class Progression<DataType> where DataType : IComparable<DataType>
    {

        public List<ConditionalEvent<DataType>> Events;
        Dictionary<int, DataType> ProgressionData;

        public Progression()
        {
            Events = new List<ConditionalEvent<DataType>>();
            ProgressionData = new Dictionary<int, DataType>();
        }

        public void SetData(string _name, DataType _data)
        {
            int code = _name.GetHashCode();
            if (!ProgressionData.ContainsKey(code))
            {
                ProgressionData.Add(code, _data);
            }
        }

        public void UpdateData(string _name, DataType newVal)
        {
            DataType data;
            int code = _name.GetHashCode();
            // we have opted for a contains check and array access
            // the genric type used against the TryGet method perhaps has a scope issue - it appeared to be the case (not clear) - OGY 30/11/2016
            if (ProgressionData.ContainsKey(code))
            {
                ProgressionData[code] = newVal;

                CheckConditions(code, ProgressionData[code]);
            }
        }

        public DataType GetData(string _name)
        {
            DataType data = default(DataType);
            int code = _name.GetHashCode();
            ProgressionData.TryGetValue(code, out data);
            return data;
        }

        // add conditional event
        public void SetConditionalEvent(ConditionalEvent<DataType> CEvent)
        {
            if(!Events.Contains(CEvent))
                Events.Add(CEvent);
        }

        // process conditional events
        private void CheckConditions(int code, DataType data)
        {
            foreach(ConditionalEvent<DataType> CEvent in Events)
            {
                CEvent.Check(code, data);
                CEvent.ProcessPassed();
            }
        }

        public void Init()
        {
            Events.RemoveAll(delegate (ConditionalEvent<DataType> _event) {
                return _event.HandleEventsWithOptions(false, true);
            });
        }

        public void Process()
        {
            Events.RemoveAll(delegate (ConditionalEvent<DataType> _event) {
                return _event.Handle();
            });
        }

        public void Clear()
        {
            Events.RemoveAll(delegate (ConditionalEvent<DataType> _event) {
                _event.Clear();
                return true;
            });

            Events.Clear();
            ProgressionData.Clear();
        }

        // XML loader
        public delegate void MyConditionalEventDelegator<EventTypes>(UnityEvent _event, EventTypes _type, XmlNode _condition);

        public void LoadXML<EventTypes>(TextAsset _xml, MyConditionalEventDelegator<EventTypes> delegator)
        {
            if (_xml == null)
                return;

            XmlDocument m_xmlDoc = new XmlDocument();
            m_xmlDoc.LoadXml(_xml.text);

            XmlNode nodeProgression = m_xmlDoc.SelectSingleNode("/Progression");
            for (XmlNode nodeGroups = nodeProgression.FirstChild; nodeGroups != null; nodeGroups = nodeGroups.NextSibling)
            {
                switch (nodeGroups.Name)
                {
                    case "Data":

                        for (XmlNode node = nodeGroups.FirstChild; node != null; node = node.NextSibling)
                        {
                            SetData(node.Attributes["name"].Value, (DataType)Convert.ChangeType(node.Attributes["value"].Value, typeof(DataType)));
                        }

                        break;
                    case "ConditionalEvents":

                        eConditionalEventTypes ConditionalType;
                        eConditionTypes ConditionType;
                        EventTypes EventType;
                        ConditionalEvent<DataType> newCEvent;

                        for (XmlNode node = nodeGroups.FirstChild; node != null; node = node.NextSibling)
                        {
                            if (node != null && node.Attributes != null)
                            {
                                // determine the conditional event type
                                if (node.Attributes["type"] == null)
                                    ConditionalType = eConditionalEventTypes.Persistent;
                                else
                                    ConditionalType = (eConditionalEventTypes)Enum.Parse(typeof(eConditionalEventTypes), node.Attributes["type"].Value);

                                bool repeats = (node.Attributes["repeats"] != null) ? node.Attributes["repeats"].Value == "True" : false;

                                // create conditional event
                                switch (ConditionalType)
                                {
                                    case eConditionalEventTypes.Contextual:
                                        newCEvent = new ConditionalEventContextual<DataType>(repeats);
                                        break;
                                    case eConditionalEventTypes.Persistent:
                                    default:
                                        newCEvent = new ConditionalEventPersistent<DataType>(repeats);
                                        break;
                                }

                                // load conditions into conditional event
                                for (XmlNode condition = node.FirstChild; condition != null; condition = condition.NextSibling)
                                {

                                    switch (condition.Name)
                                    {
                                        case "Event":

                                            EventType = (EventTypes)Enum.Parse(typeof(EventTypes), condition.Attributes["type"].Value);
                                            delegator(newCEvent.Event, EventType, condition);

                                            break;
                                        case "ConstEvent":

                                            EventType = (EventTypes)Enum.Parse(typeof(EventTypes), condition.Attributes["type"].Value);
                                            delegator(newCEvent.ConstEvent, EventType, condition);

                                            break;
                                        case "Condition":

                                            ConditionType = (eConditionTypes)Enum.Parse(typeof(eConditionTypes), condition.Attributes["type"].Value);
                                            if (condition.Attributes["amount"].Value != null && condition.Attributes["data"].Value != null)
                                            {
                                                newCEvent.AddCondition(
                                                    condition.Attributes["data"].Value,
                                                    (DataType)Convert.ChangeType(condition.Attributes["amount"].Value, typeof(DataType)),
                                                    ConditionType);
                                            }

                                            break;
                                    }
                                }

                                SetConditionalEvent(newCEvent);
                            }
                        }
                        break;
                }
            }

        }

    }
    //----------------------------------------------------------------------------------------------------
}
