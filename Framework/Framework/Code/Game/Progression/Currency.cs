﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LHS.Framework.Game.Progression
{
    class My
    {
        public static My operator +(My x, My y)
        {
            return x;
        }
        public static My operator -(My x, My y)
        {
            return x;
        }
    }

    public static class PrimiteImpl
    {
        // Define operations here for all primitive classes
        public static int Add(int x, int y)
        {
            if ((int.MaxValue - y) < x)
                return int.MaxValue;
            return x + y;
        }
        public static int Sub(int x, int y)
        {
            if ((x - y) < 0)
                return 0;
            return x - y;
        }
        public static float Add(float x, float y)
        {
            if ((float.MaxValue - y) < x)
                return float.MaxValue;
            return x + y;
        }
        public static float Sub(float x, float y)
        {
            if ((x - y) < 0)
                return 0;
            return x - y;
        }
        public static double Add(double x, double y)
        {
            if ((double.MaxValue - y) < x)
                return double.MaxValue;
            return x + y;
        }
        public static double Sub(double x, double y)
        {
            if ((x - y) < 0)
                return 0;
            return x - y;
        }

        public static long Add(long x, long y)
        {
            if ((long.MaxValue - y) < x)
                return long.MaxValue;
            return x + y;
        }
        public static long Sub(long x, long y)
        {
            if ((x - y) < 0)
                return 0;
            return x - y;
        }
    }
    public static class MathOps<T>
    {

        delegate T BinaryOp(T a, T b);
        static readonly BinaryOp add;
        static readonly BinaryOp sub;
        static MathOps()
        {
            // For primitive types we need to emulate this functionality manualy
            if (typeof(T).IsPrimitive)
            {
                add = (BinaryOp)Delegate.CreateDelegate(typeof(BinaryOp), typeof(PrimiteImpl), "Add", false, false);
                sub = (BinaryOp)Delegate.CreateDelegate(typeof(BinaryOp), typeof(PrimiteImpl), "Sub", false, false);
            }
            else
            {
                // For user-defined custom types - use type from user
                add = (BinaryOp)Delegate.CreateDelegate(typeof(BinaryOp), typeof(T), "op_Addition", false, false);
            }
            // If not found do not leave our delegate as empty to avoid useless null-checks 
            // but assign it to method that will throw some valuable information to user
            // It's up to you to use this hack or throw some custom exception as early as possible
            if (add == null)
            {
                add = new BinaryOp(NotSupported);
            }
            if (sub == null)
            {
                sub = new BinaryOp(NotSupported);
            }
        }
        // Requered to avoid TypeInitializationException then no operation can be found
        private static T NotSupported(T dummy1, T dummy2)
        {
            throw new NotSupportedException(string.Format("I'm realy sorry. {0} does not support some requered operations.", typeof(T).FullName));
        }
        public static T Add(T x, T y)
        {
            return add(x, y);
        }
        public static T Sub(T x, T y)
        {
            return sub(x, y);
        }
    }

    public class Currency<DataType> where DataType : IComparable<DataType>
    {

        // this class will house a currency
        // it will allow us to increment and will handle a max value
        // we will be able to spend teh currency with checks and events triggers based on responces
        
        public DataType Amount { get; private set; }

        public delegate bool MyCheck(DataType _amount); // amount being checked for this spend
        public delegate void MyConfirmation(DataType _amount); // amount remaining after a spend/deposit

        public Currency()
        {
            Amount = default(DataType);
        }

        // Functions
        public bool Spend(DataType _amount)
        {
            if(Amount.CompareTo(_amount) >= 0)
            {
                Amount = MathOps<DataType>.Sub(Amount, _amount);
                return true;
            }
            return false;
        }
        public bool Spend(DataType _amount, MyCheck OnCheck)
        {
            if (!OnCheck(_amount))
                return false;

            return Spend(_amount);
        }
        public bool Spend(DataType _amount, MyConfirmation OnSuccess, MyConfirmation OnFailure)
        {
            bool returnVal = true;
            if (Spend(_amount))
            {
                OnSuccess(Amount);
            }
            else
            {
                OnFailure(Amount);
                returnVal = false;
            }
            return returnVal;
        }
        public bool Spend(DataType _amount, MyCheck OnCheck, MyConfirmation OnSuccess, MyConfirmation OnFailure)
        {
            if (!OnCheck(_amount))
                return false;

            return Spend(_amount, OnSuccess, OnFailure);
        }

        public void Deposit(DataType _amount)
        {
            Amount = MathOps<DataType>.Add(Amount, _amount);
        }
        public void Deposit(DataType _amount, MyConfirmation OnDeposit)
        {
            Amount = MathOps<DataType>.Add(Amount, _amount);
            OnDeposit(Amount);
        }

        public bool CanAfford(DataType _amount)
        {
            if (Amount.CompareTo(_amount) >= 0)
            {
                return true;
            }
            return false;
        }
    }
}
