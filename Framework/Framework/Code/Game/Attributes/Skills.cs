﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using LHS.Framework.Game.Inventory;
using System.Xml;

namespace LHS.Framework.Game.Attributes
{
    public class Skills
    {
        private Content Content;

        public Skills(uint _size)
        {
            if (_size <= 0)
                throw new ArgumentException("Cannot create a skill set with 0 Size specified");

            Content = new Content(_size);
        }

        public void Unlock(int _id)
        {
            Content.Set(_id, true);
        }

        public bool IsUnlocked(int _id)
        {
            return Content.IsSet(_id);
        }

        public void SetData(int data)
        {
            Content.SetInt(data);
        }
        public void SetData(int[] array)
        {
            Content.SetInt(array);
        }
        public int[] GetData()
        {
            return Content.GetIntArray();
        }

        public void GetSkillsData(out int unlocked, out int size)
        {
            Content.GetData(out unlocked, out size);
        }
    }

    public class SkillElement
    {
        public string type = "default";
        public string name = "default";
        public float size;
        public bool utilised = false;
        public bool[] connections;

        public SkillElement()
        {
            connections = new bool[4];
            for (int i = 0; i < connections.Length; ++i)
                connections[i] = true;
        }
    }

    public class SkillTree
    {
        public Skills Skills { get; private set; }
        public SkillElement[,] Elements { get; private set; }

        public SkillTree(int _rows, int _columns)
        {
            Skills = new Skills((uint)(_rows * _columns));

            Elements = new SkillElement[_rows, _columns];
            for(int r = 0; r < _rows; ++r)
            {
                for (int c = 0; c < _columns; ++c)
                {
                    Elements[r, c] = new SkillElement();
                }
            }
        }

        public void Load(XmlNode node)
        {
            // load elements
            for (XmlNode nodeData = node.FirstChild; nodeData != null; nodeData = nodeData.NextSibling)
            {
                string type = nodeData.Attributes["type"].Value;
                string name = nodeData.Attributes["name"].Value;
                int row = int.Parse(nodeData.Attributes["row"].Value);
                int col = int.Parse(nodeData.Attributes["column"].Value);

                SkillElement element = Elements[row, col];
                element.connections[0] = (nodeData.Attributes["right"] != null) ? bool.Parse(nodeData.Attributes["right"].Value) : false;
                element.connections[1] = (nodeData.Attributes["left"] != null) ? bool.Parse(nodeData.Attributes["left"].Value) : false;
                element.connections[2] = (nodeData.Attributes["up"] != null) ? bool.Parse(nodeData.Attributes["up"].Value) : false;
                element.connections[3] = (nodeData.Attributes["down"] != null) ? bool.Parse(nodeData.Attributes["down"].Value) : false;

                element.size = (nodeData.Attributes["size"] != null) ? float.Parse(nodeData.Attributes["size"].Value) : 1;

                element.type = type;
                element.name = name;
                element.utilised = true;
            }

        }

    }
}
