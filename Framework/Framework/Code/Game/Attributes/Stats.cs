﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Game.Attributes
{
    public interface IStatsClass
    {
        void ProcessStats();
    }

    public class Stats<ClassStats, effectsEnum> : Attributes<ClassStats, effectsEnum> 
        where effectsEnum : IConvertible 
        where ClassStats : IStatsClass, new()
    {
        int MaxLevel;
        AnimationCurve Curve;
        ClassStats MaxStats;
        effectsEnum LevelEnum;

        public Stats(int maxLevel, ClassStats maxStats, AnimationCurve curve, effectsEnum levelEnum) : base()
        {
            MaxLevel = maxLevel;
            MaxStats = maxStats;
            Curve = curve;
            LevelEnum = levelEnum;
            SetLevel(1);
        }
        
        private float GetLevelPercentage(int level)
        {
            //return Curve.Evaluate((float)level / (float)MaxLevel);
            return level / (float)MaxLevel;
        }

        // This will set the base state for the level youa re on
        public void SetLevel(int level)
        {
            
            int NewLevel = Mathf.Min(level, MaxLevel);
            float LevelPercentage = GetLevelPercentage(NewLevel);
          //  Debug.LogError("LevelPercentage: " + LevelPercentage);

            Type T = typeof(ClassStats);
            FieldInfo[] fields = T.GetFields();

            foreach (FieldInfo field in fields)
            {
                switch (field.FieldType.Name)
                {
                    case "Int16":
                    case "UInt16":
                    case "Int32":
                    case "UInt32":
                    case "Int64":
                    case "UInt64":
                        this.Set<int>(LevelEnum, Mathf.RoundToInt((int)field.GetValue(MaxStats) * LevelPercentage));
                       // Debug.LogErrorFormat("SetValue:: {0} = {1} ", field.Name, this.GetInt(field.Name));
                        break;
                    case "Single":
                    case "Double":
                        this.Set<float>(LevelEnum, Mathf.RoundToInt((float)field.GetValue(MaxStats) * LevelPercentage));
                       // Debug.LogErrorFormat("SetValue:: {0} = {1} ", field.Name, this.GetInt(field.Name));
                        break;
                    default:
                        break;
                }
            }

        }

        // This will get you the base stats at any level
        public ClassStats GetBaseLevelStatsAt(int level)
        {
            ClassStats stats = new ClassStats();

            int GetLevel = Mathf.Min(level, MaxLevel);
            float LevelPercentage = GetLevelPercentage(GetLevel);

            Type T = typeof(ClassStats);
            FieldInfo[] fields = T.GetFields();

            foreach (FieldInfo field in fields)
            {
                switch (field.FieldType.Name)
                {
                    case "Int16":
                    case "UInt16":
                    case "Int32":
                    case "UInt32":
                    case "Int64":
                    case "UInt64":
                        field.SetValue(stats, Mathf.RoundToInt((int)field.GetValue(MaxStats) * LevelPercentage));
                        break;
                    case "Single":
                    case "Double":
                        field.SetValue(stats, Mathf.RoundToInt((float)field.GetValue(MaxStats) * LevelPercentage));
                        break;
                    default:
                        break;
                }
            }

            stats.ProcessStats();
            return stats;
        }

        // This will get you the base stats at any level
        public ClassStats GetCurrentStats()
        {
            ClassStats stats = new ClassStats();

            Type T = typeof(ClassStats);
            FieldInfo[] fields = T.GetFields();

            foreach (FieldInfo field in fields)
            {
                switch (field.FieldType.Name)
                {
                    case "Int16":
                    case "UInt16":
                    case "Int32":
                    case "UInt32":
                    case "Int64":
                    case "UInt64":
                        field.SetValue(stats, this.GetInt(field.Name));
                        break;
                    case "Single":
                    case "Double":
                        field.SetValue(stats, this.GetFloat(field.Name));
                        break;
                    default:
                        break;
                }
            }

            stats.ProcessStats();
            return stats;
        }

        public ClassStats GetCurrentStats(effectsEnum effect)
        {
            ClassStats stats = new ClassStats();

            Type T = typeof(ClassStats);
            FieldInfo[] fields = T.GetFields();

            foreach (FieldInfo field in fields)
            {
                switch (field.FieldType.Name)
                {
                    case "Int16":
                    case "UInt16":
                    case "Int32":
                    case "UInt32":
                    case "Int64":
                    case "UInt64":
                        field.SetValue(stats, this.GetInt(field.Name, effect));
                        break;
                    case "Single":
                    case "Double":
                        field.SetValue(stats, this.GetFloat(field.Name, effect));
                        break;
                    default:
                        break;
                }
            }

            stats.ProcessStats();
            return stats;
        }
    }
}
