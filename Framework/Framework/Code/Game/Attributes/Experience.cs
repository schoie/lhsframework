﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Game.Attributes
{
    public class Experience
    {
        int MaxLevel;
        int MaxExperience;
        AnimationCurve Curve;

        public Experience(int maxLevel, int maxExperience, AnimationCurve curve) : base()
        {
            MaxLevel = maxLevel;
            MaxExperience = maxExperience;
            Curve = curve;
        }

        public bool IncrementExperience(int level, int experience, int increment, out int newLevel, out int newExperience)
        {
            newLevel = level;
            newExperience = 0;

            if (level == MaxLevel)
            {
                // move towards current experience target and max out
            }
            else
            {
                bool levelUp = false;
                newExperience = Mathf.Min(experience + increment, MaxExperience);
                int xpTarget = GetExperienceRequiredAt(newLevel + 1);
                while (newExperience >= xpTarget)
                {
                    newLevel++;
                    xpTarget = GetExperienceRequiredAt(newLevel + 1);
                    levelUp = true;
                }

                return levelUp;
            }
            return false;
        }

        public int GetExperienceRequiredAt(int level)
        {
            int testLevel = Mathf.Min(level, MaxLevel);
            return Mathf.RoundToInt(MaxExperience * Curve.Evaluate(testLevel / MaxLevel));
        }

        public int GetLevelViaExperience(int experience)
        {
            int level = 1;
            int xp = GetExperienceRequiredAt(level);
            while (experience >= xp)
            {
                level++;
                xp = GetExperienceRequiredAt(level);
            }
            return level;
        }

        public void GetCurrentExperienceLevelData(int experience, out int currentXP, out int targetXP)
        {
            int level = GetLevelViaExperience(experience);

            int targetCur = GetExperienceRequiredAt(level);
            int targetNext = GetExperienceRequiredAt(level + 1);

            currentXP = experience - targetCur;
            targetXP = targetNext - targetCur;
        }

    }
}
