﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace LHS.Framework.Game.Attributes
{

    public interface ITypedAttribute<T>
    {
        T[] Get();
        T Get(int DataIndex);
        void Set(int DataIndex, T data);
    }

    public interface IAttribute{}

    public class Attribute<type> : IAttribute, ITypedAttribute<type>
    {
        public type[] Data;
        public Attribute(int size)
        {
            Data = new type[size];
        }

        public type[] Get()
        {
            return Data;
        }
        public type Get(int DataIndex)
        {
            return Data[DataIndex];
        }
        public void Set(int DataIndex, type data)
        {
            Data[DataIndex] = data;
        }
    }
    
    public class Attributes<ClassAttributes, effectsEnum> where effectsEnum : IConvertible
    {

        public Dictionary<string, IAttribute> MyAttributes;
        
        public Attributes()
        {

            Type EffectsType = typeof(effectsEnum);
            if (!EffectsType.IsEnum)
            {
                LHSDebug.LogWarning("Attributes<ClassAttributes, effectsEnum> -- effectsEnum must be an enum type!!");
                return;
            }

            int effectSize = GetEnumMaxValue(EffectsType) + 1;

            MyAttributes = new Dictionary<string, IAttribute>();

            Type T = typeof(ClassAttributes);
            FieldInfo[] fields = T.GetFields();
            
            foreach (FieldInfo field in fields)
            {
                switch (field.FieldType.Name)
                {
                    case "Int16":
                    case "UInt16":
                    case "Int32":
                    case "UInt32":
                    case "Int64":
                    case "UInt64":

                        // int
                        MyAttributes.Add(field.Name, new Attribute<int>(effectSize));
                        break;
                    case "Single":
                    case "Double":

                        // float
                        MyAttributes.Add(field.Name, new Attribute<float>(effectSize));
                        break;
                    default:

                        // Do not support these file types in the Attribute class
                        LHSDebug.LogWarningFormat("Do not support file type ({0}) in the Attribute class", field.FieldType.Name);

                        break;
                }
            }
        }

        public float GetFloat(string name)
        {
            IAttribute DataToGet;
            Attribute<float> Data;
            if (MyAttributes.TryGetValue(name, out DataToGet))
            {
                Data = DataToGet as Attribute<float>;
                float[] array = Data.Get();
                float rf = 0;
                foreach (float f in array) rf += f;
                return rf;
            }
            return 0;
        }

        public int GetInt(string name)
        {
            IAttribute DataToGet;
            Attribute<int> Data;
            if (MyAttributes.TryGetValue(name, out DataToGet))
            {
                Data = DataToGet as Attribute<int>;
                int[] array = Data.Get();
                int ri = 0;
                foreach (int i in array) ri += i;
                return ri;
            }
            return 0;
        }

        public int GetInt(string name, effectsEnum effect)
        {
            IAttribute DataToGet;
            Attribute<int> Data;
            if (MyAttributes.TryGetValue(name, out DataToGet))
            {
                Data = DataToGet as Attribute<int>;
                return Data.Get(Convert.ToInt32(effect));
            }
            return 0;
        }

        public float GetFloat(string name, effectsEnum effect)
        {
            IAttribute DataToGet;
            Attribute<float> Data;
            if (MyAttributes.TryGetValue(name, out DataToGet))
            {
                Data = DataToGet as Attribute<float>;
                return Data.Get(Convert.ToInt32(effect));
            }
            return 0;
        }

        public void Set(string name, effectsEnum effect, float data)
        {
            IAttribute DataToGet;
            Attribute<float> Data;
            if (MyAttributes.TryGetValue(name, out DataToGet))
            {
                Data = DataToGet as Attribute<float>;
                Data.Set(Convert.ToInt32(effect), data);
            }
        }

        public void Set(string name, effectsEnum effect, int data)
        {
            IAttribute DataToGet;
            Attribute<int> Data;
            if (MyAttributes.TryGetValue(name, out DataToGet))
            {
                Data = DataToGet as Attribute<int>;
                Data.Set(Convert.ToInt32(effect), data);
            }
        }

        public void SetSafe(string name, effectsEnum effect, float data)
        {
            IAttribute DataToGet;
            Attribute<float> Data;
            if (MyAttributes.TryGetValue(name, out DataToGet))
            {
                Data = DataToGet as Attribute<float>;
                if (Data != null)
                    Data.Set(Convert.ToInt32(effect), data);
            }
        }

        public void SetSafe(string name, effectsEnum effect, int data)
        {
            IAttribute DataToGet;
            Attribute<int> Data;
            if (MyAttributes.TryGetValue(name, out DataToGet))
            {
                Data = DataToGet as Attribute<int>;
                if(Data != null)
                    Data.Set(Convert.ToInt32(effect), data);
            }
        }

        public void SetSafe<T>(string name, effectsEnum effect, T data)
        {
            IAttribute DataToGet;
            Attribute<T> Data;
            if (MyAttributes.TryGetValue(name, out DataToGet))
            {
                Data = DataToGet as Attribute<T>;
                if (Data != null)
                    Data.Set(Convert.ToInt32(effect), data);
            }
        }

        public void Set<T>(effectsEnum effect, T data)
        {

            Type EffectsType = typeof(effectsEnum);
            if (!EffectsType.IsEnum)
            {
                LHSDebug.LogWarning("Attributes<ClassAttributes, effectsEnum> -- effectsEnum must be an enum type!!");
                return;
            }

            int effectSize = GetEnumMaxValue(EffectsType) + 1;
            
            foreach (KeyValuePair<string, IAttribute> kvp in MyAttributes)
            {
                SetSafe<T>(kvp.Key, effect, data);

                /*Attribute<T> ToSet;
                if (CheckAttType<T>(kvp.Value, out ToSet))
                {
                    ToSet.Set(Convert.ToInt32(effect), data);
                }*/
            }
        }

        public bool isInt(string name)
        {
            IAttribute DataToGet;
            if (MyAttributes.TryGetValue(name, out DataToGet))
            {
                return (DataToGet as Attribute<int>) != null;
            }
            return false;
        }

        public bool isFloat(string name)
        {
            IAttribute DataToGet;
            if (MyAttributes.TryGetValue(name, out DataToGet))
            {
                return (DataToGet as Attribute<float>) != null;
            }
            return false;
        }

        static public int GetEnumMaxValue(Type t_type)
        {
            int curMax = 0;

            // Enumerate the Enum's fields.
            FieldInfo[] field_infos = t_type.GetFields();

            // Loop over the fields.
            foreach (FieldInfo field_info in field_infos)
            {
                // See if this is a literal value (set at compile time).
                if (field_info.IsLiteral)
                {
                    // Add it.
                    curMax = Mathf.Max((int)field_info.GetValue(null), curMax);
                }
            }

            return curMax;
        }

        public override string ToString()
        {
            Attribute<int> DataInt;
            Attribute<float> DataFloat;

            Type EffectsType = typeof(effectsEnum);
            if (!EffectsType.IsEnum)
            {
                return "Attributes<ClassAttributes, effectsEnum> -- effectsEnum must be an enum type!!";
            }

            int effectSize = GetEnumMaxValue(EffectsType) + 1;

            string str = "Attributes<" + typeof(ClassAttributes).Name + ", " + typeof(effectsEnum).Name + "> " + "- (" + MyAttributes.Count + ", " + effectSize + ")\n\n";

            for (int i = 0; i < effectSize; ++i)
            {
                str += (" - " + Enum.GetName(typeof(effectsEnum), i) + "\n\n");

                foreach (KeyValuePair<string, IAttribute> kvp in MyAttributes)
                {
                    Attribute<int> LogInt;
                    Attribute<float> LogFloat;
                    if (CheckAttType<int>(kvp.Value, out LogInt))
                    {
                        str += (LogInt.Get(i) + "\n");
                    }
                    else if (CheckAttType<float>(kvp.Value, out LogFloat))
                    {
                        str += (LogFloat.Get(i) + "\n");
                    }
                }
            }

            str += ("\n" + "Total" + "\n\n");
            foreach (KeyValuePair<string, IAttribute> kvp in MyAttributes)
            {
                Attribute<int> LogInt;
                Attribute<float> LogFloat;
                if (CheckAttType<int>(kvp.Value, out LogInt))
                {
                    int[] array = LogInt.Get();
                    int ri = 0;
                    foreach (int i in array) ri += i;
                    str += (" - " + kvp.Key + " = " + ri + "\n");
                }
                else if (CheckAttType<float>(kvp.Value, out LogFloat))
                {
                    float[] array = LogFloat.Get();
                    float ri = 0;
                    foreach (float i in array) ri += i;
                    str += (" - " + kvp.Key + " = " + ri + "\n");
                }
            }

            str += "\n End";

            return str;

        }

        public void Copy(Attributes<ClassAttributes, effectsEnum> toCopy)
        {
            Attribute<int> DataInt;
            Attribute<float> DataFloat;

            Type EffectsType = typeof(effectsEnum);
            if (!EffectsType.IsEnum)
            {
                LHSDebug.LogWarning("Attributes<ClassAttributes, effectsEnum> -- effectsEnum must be an enum type!!");
                return;
            }

            int effectSize = GetEnumMaxValue(EffectsType) + 1;

            IAttribute toCopyFrom;
            foreach (KeyValuePair<string, IAttribute> kvp in MyAttributes)
            {
                if (toCopy.MyAttributes.TryGetValue(kvp.Key, out toCopyFrom))
                {
                    Attribute<int> CopyInt;
                    Attribute<float> CopyFloat;
                    if(CheckAttType<int>(toCopyFrom, out CopyInt))
                    {
                        DoCopy<int>(kvp.Value, CopyInt, effectSize);
                    }
                    else if (CheckAttType<float>(toCopyFrom, out CopyFloat))
                    {
                        DoCopy<float>(kvp.Value, CopyFloat, effectSize);
                    }
                }
            }
        }

        public void Add(Attributes<ClassAttributes, effectsEnum> toCopy)
        {
            Attribute<int> DataInt;
            Attribute<float> DataFloat;

            Type EffectsType = typeof(effectsEnum);
            if (!EffectsType.IsEnum)
            {
                LHSDebug.LogWarning("Attributes<ClassAttributes, effectsEnum> -- effectsEnum must be an enum type!!");
                return;
            }

            int effectSize = GetEnumMaxValue(EffectsType) + 1;

            IAttribute toCopyFrom;
            foreach (KeyValuePair<string, IAttribute> kvp in MyAttributes)
            {
                if (toCopy.MyAttributes.TryGetValue(kvp.Key, out toCopyFrom))
                {
                    Attribute<int> CopyInt;
                    Attribute<float> CopyFloat;
                    if (CheckAttType<int>(toCopyFrom, out CopyInt))
                    {
                        DoAddInt(kvp.Value, CopyInt, effectSize);
                    }
                    else if (CheckAttType<float>(toCopyFrom, out CopyFloat))
                    {
                        DoAddFloat(kvp.Value, CopyFloat, effectSize);
                    }
                }
            }
        }

        public void Sub(Attributes<ClassAttributes, effectsEnum> toCopy)
        {
            Attribute<int> DataInt;
            Attribute<float> DataFloat;

            Type EffectsType = typeof(effectsEnum);
            if (!EffectsType.IsEnum)
            {
                LHSDebug.LogWarning("Attributes<ClassAttributes, effectsEnum> -- effectsEnum must be an enum type!!");
                return;
            }

            int effectSize = GetEnumMaxValue(EffectsType) + 1;

            IAttribute toCopyFrom;
            foreach (KeyValuePair<string, IAttribute> kvp in MyAttributes)
            {
                if (toCopy.MyAttributes.TryGetValue(kvp.Key, out toCopyFrom))
                {
                    Attribute<int> CopyInt;
                    Attribute<float> CopyFloat;
                    if (CheckAttType<int>(toCopyFrom, out CopyInt))
                    {
                        DoSubInt(kvp.Value, CopyInt, effectSize);
                    }
                    else if (CheckAttType<float>(toCopyFrom, out CopyFloat))
                    {
                        DoSubFloat(kvp.Value, CopyFloat, effectSize);
                    }
                }
            }
        }
        public void Add(Attributes<ClassAttributes, effectsEnum> toCopy, string effector)
        {
            Type EffectsType = typeof(effectsEnum);
            if (!EffectsType.IsEnum)
            {
                LHSDebug.LogWarning("Attributes<ClassAttributes, effectsEnum> -- effectsEnum must be an enum type!!");
                return;
            }

            int effectSize = GetEnumMaxValue(EffectsType) + 1;

            for (int i = 0; i < effectSize; ++i)
            {
                if (effector == Enum.GetName(typeof(effectsEnum), i))
                {
                    Add(toCopy, i);
                    break;
                }
            }
        }

        public void Add(Attributes<ClassAttributes, effectsEnum> toCopy, int index)
        {
            IAttribute toCopyFrom;
            foreach (KeyValuePair<string, IAttribute> kvp in MyAttributes)
            {
                if (toCopy.MyAttributes.TryGetValue(kvp.Key, out toCopyFrom))
                {
                    Attribute<int> CopyInt;
                    Attribute<float> CopyFloat;
                    if (CheckAttType<int>(toCopyFrom, out CopyInt))
                    {
                        DoAddIntAt(kvp.Value, CopyInt, index);
                    }
                    else if (CheckAttType<float>(toCopyFrom, out CopyFloat))
                    {
                        DoAddFloatAt(kvp.Value, CopyFloat, index);
                    }
                }
            }
        }


        public void Sub(Attributes<ClassAttributes, effectsEnum> toCopy, string effector)
        {
            Type EffectsType = typeof(effectsEnum);
            if (!EffectsType.IsEnum)
            {
                LHSDebug.LogWarning("Attributes<ClassAttributes, effectsEnum> -- effectsEnum must be an enum type!!");
                return;
            }

            int effectSize = GetEnumMaxValue(EffectsType) + 1;

            for (int i = 0; i < effectSize; ++i)
            {
                if (effector == Enum.GetName(typeof(effectsEnum), i))
                {
                    Sub(toCopy, i);
                    break;
                }
            }
        }
        public void Sub(Attributes<ClassAttributes, effectsEnum> toCopy, int index)
        {
            IAttribute toCopyFrom;
            foreach (KeyValuePair<string, IAttribute> kvp in MyAttributes)
            {
                if (toCopy.MyAttributes.TryGetValue(kvp.Key, out toCopyFrom))
                {
                    Attribute<int> CopyInt;
                    Attribute<float> CopyFloat;
                    if (CheckAttType<int>(toCopyFrom, out CopyInt))
                    {
                        DoSubIntAt(kvp.Value, CopyInt, index);
                    }
                    else if (CheckAttType<float>(toCopyFrom, out CopyFloat))
                    {
                        DoSubFloatAt(kvp.Value, CopyFloat, index);
                    }
                }
            }
        }

        private bool CheckAttType<T>(IAttribute toCheck, out Attribute<T> Data)
        {
            Data = toCheck as Attribute<T>;
            return Data != null;
        }

        private void DoCopy<T>(IAttribute to, Attribute<T> from, int max)
        {
            Attribute<T> Data;
            if (CheckAttType<T>(to, out Data))
            {
                for (int i = 0; i < max; ++i)
                    Data.Set(i, from.Get(i));
            }
        }

        private void DoAddFloat(IAttribute to, Attribute<float> from, int max)
        {
            Attribute<float> Data;
            if (CheckAttType<float>(to, out Data))
            {
                for (int i = 0; i < max; ++i)
                    Data.Set(i, Data.Get(i) + from.Get(i));
            }
        }
        private void DoAddInt(IAttribute to, Attribute<int> from, int max)
        {
            Attribute<int> Data;
            if (CheckAttType<int>(to, out Data))
            {
                for (int i = 0; i < max; ++i)
                    Data.Set(i, Data.Get(i) + from.Get(i));
            }
        }

        private void DoSubFloat(IAttribute to, Attribute<float> from, int max)
        {
            Attribute<float> Data;
            if (CheckAttType<float>(to, out Data))
            {
                for (int i = 0; i < max; ++i)
                    Data.Set(i, Data.Get(i) - from.Get(i));
            }
        }
        private void DoSubInt(IAttribute to, Attribute<int> from, int max)
        {
            Attribute<int> Data;
            if (CheckAttType<int>(to, out Data))
            {
                for (int i = 0; i < max; ++i)
                    Data.Set(i, Data.Get(i) - from.Get(i));
            }
        }

        private void DoAddFloatAt(IAttribute to, Attribute<float> from, int index)
        {
            Attribute<float> Data;
            if (CheckAttType<float>(to, out Data))
            {
                Data.Set(index, Data.Get(index) + from.Get(index));
            }
        }
        private void DoAddIntAt(IAttribute to, Attribute<int> from, int index)
        {
            Attribute<int> Data;
            if (CheckAttType<int>(to, out Data))
            {
                Data.Set(index, Data.Get(index) + from.Get(index));
            }
        }

        private void DoSubFloatAt(IAttribute to, Attribute<float> from, int index)
        {
            Attribute<float> Data;
            if (CheckAttType<float>(to, out Data))
            {
                Data.Set(index, Data.Get(index) - from.Get(index));
            }
        }
        private void DoSubIntAt(IAttribute to, Attribute<int> from, int index)
        {
            Attribute<int> Data;
            if (CheckAttType<int>(to, out Data))
            {
                Data.Set(index, Data.Get(index) - from.Get(index));
            }
        }
    }
}
