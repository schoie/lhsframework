﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;

namespace LHS.Framework.Game.Attributes
{
    public class Upgrades<ClassAttributes, effectsEnum, attributeTypes> 
        where effectsEnum : IConvertible 
        where attributeTypes : IConvertible
    {

        public Dictionary<int, Upgrade> UpgradeData = new Dictionary<int, Upgrade>();

        public class Upgrade
        {
            public Dictionary<string, string> Data;
            public Attributes<ClassAttributes, effectsEnum>[] Attributes;

            public Upgrade()
            {
                Data = new Dictionary<string, string>();

                Type attributesType = typeof(attributeTypes);
                if (!attributesType.IsEnum)
                {
                    LHSDebug.LogWarning("Upgrades<ClassAttributes, effectsEnum, attributeTypes> -- attributeTypes must be an enum type!!");
                    return;
                }
                int attsSize = Attributes<ClassAttributes, effectsEnum>.GetEnumMaxValue(attributesType) + 1;

                Attributes = new Attributes<ClassAttributes, effectsEnum>[attsSize];
            }
        }

        public bool Get(int level, out Upgrade upgrade)
        {
            if (UpgradeData.TryGetValue(level, out upgrade))
            {
                return true;
            }
            return false;
        }

        public delegate void XMLLoadAttributes(XmlNode _node, Attributes<ClassAttributes, effectsEnum>[] Attributes);

        public void LoadUpgrades(XmlNode _node, XMLLoadAttributes attributeLoader)
        {
            int level = 0;
            for (XmlNode nodeUpgrade = _node.FirstChild; nodeUpgrade != null; nodeUpgrade = nodeUpgrade.NextSibling)
            {

                if (nodeUpgrade.Attributes["level"] == null)
                {
                    break;
                }

                level = int.Parse(nodeUpgrade.Attributes["level"].Value);
                if (!UpgradeData.ContainsKey(level))
                {
                    Upgrade upg = new Upgrade();
                    for (XmlNode node = nodeUpgrade.FirstChild; node != null; node = node.NextSibling)
                    {
                        if (node.Name == "Attributes")
                        {
                            if (attributeLoader != null)
                                attributeLoader(node, upg.Attributes);
                        }
                        else
                        {
                            if (node.Attributes["name"] != null && node.Attributes["value"] != null)
                            {
                                string name = node.Attributes["name"].Value;
                                if (!upg.Data.ContainsKey(name))
                                {
                                    upg.Data.Add(name, node.Attributes["value"].Value);
                                }
                            }
                        }
                    }

                    UpgradeData.Add(level, upg);
                }
            }
        }

    }
}
