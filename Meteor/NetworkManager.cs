﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Meteor.Extensions;
using System;
using LHS.Framework;
using Meteor;

namespace LHS.UnityMeteor {

    public class NetworkManager : CSingletonMonoBehaviour<NetworkManager>
    {

        // vars
        public string Username = "test";
        public string Password = "123456";

        // properties
        public bool Connected { get; private set; }

        // Delegates
        public delegate void ConnectionDelegate();
        public delegate void ReturnDelegate<T>(T data);

        private ConnectionDelegate DefaultConnectionDelegate;

        public override void OnAwake()
        {
            Connected = false;
            DefaultConnectionDelegate = new ConnectionDelegate(ConnectedCallback);
            StartCoroutine(MeteorConnect(Username, Password));

        }

        //------------------------------------------------------------------------------------
        // Callbacks
        //------------------------------------------------------------------------------------
        private void ConnectedCallback()
        {
            Debug.Log("Meteor Network Connected");

            Connected = true;

            Call("data.Clear");
        }
        private void ReturnCallback<T>(T data)
        {
            Debug.Log("Data: " + data.ToString());
        }
        //------------------------------------------------------------------------------------

        //------------------------------------------------------------------------------------
        // Call Function
        //------------------------------------------------------------------------------------
        public void Call(string func, params object[] args)
        {
            StartCoroutine(CallFunction(func, args));
        }

        IEnumerator CallFunction(string func, params object[] args)
        {
            var Call = Method.Call(func, args);
            yield return (Coroutine)Call;
        }
        //------------------------------------------------------------------------------------

        //------------------------------------------------------------------------------------
        // Get Function
        //------------------------------------------------------------------------------------
        public void Get<T>(string func, ReturnDelegate<T> callback, params object[] args)
        {
            StartCoroutine(CallGetFunction<T>(func, callback, args));
        }

        IEnumerator CallGetFunction<T>(string func, ReturnDelegate<T> callback, params object[] args)
        {
            var getCall = Method<T>.Call(func, args);
            yield return (Coroutine)getCall;

            if (getCall != null && getCall.Response != null)
                callback(getCall.Response);
        }
        //------------------------------------------------------------------------------------

        //------------------------------------------------------------------------------------
        // Connect Function
        //------------------------------------------------------------------------------------
        IEnumerator MeteorConnect(string username, string password, bool production = false)
        {

            // Connect to the meteor server. Yields when you're connected
            if (production)
            {
                yield return Connection.Connect("wss://productionserver.com/websocket");
            }
            else
            {
                Debug.Log("attempting local connection");
                yield return Connection.Connect("ws://localhost:3000/websocket");
            }

            yield return (Coroutine)Accounts.LoginWith(username, password);

            DefaultConnectionDelegate();
        }

    }
}
