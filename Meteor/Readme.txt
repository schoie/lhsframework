
---------------------
Meteor Unity
---------------------

Meteor is a Networking program that allows us to easily connect to MongoDB and alter server data.
This set of classes provided here "Meteor-Unity" is a free library we have come by that integrates meteor into unity. hurray!!


Use:

Take the "Meteor-Unity" folder and place it in your project. 
It contains the necessary files plus any helpful scripts that LHS has written.

Scripts
- NetworkManager
	- Provides a connection function that accepts a callback to signify the attemp is complete
	- A function call 
	- And a get which also uses a Callback to understand it is completed and to obtain the data from the server


NOTE:
We did attempt to get this to compile in a DLL. We did not get this to work at the Unity level as we got errors.

ERRORS:
- SpritePacker failed to get types from System.ServiceModel, Version=3.0.0.0, Culture=neutral, PublicKeyToken=xxxxxxxxxx. Error: The classes in the module cannot be loaded. UnityEditor.Sprites.Packer:GetSelectedPolicyId()
- Could not load type 'System.Runtime.Versioning.TargetFrameworkAttribute'