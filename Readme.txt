

------------------------------------------
Readme - LHS Modules
------------------------------------------

We build modules using Visual Studio, that said it can be completed in any editor including Monodevelop.
The modules are esentially Class Libraries that Unity refers to as Managed Plugins.



Batch File

- COPY_MODULES_TO_PROJECT.bat

This is a batch file that you reference from within your project folder.
It will grab all of the modules and copy them over to where you call the this batch file from.
It should be used in conjuction with the provided GET_MODUELS_FOR_PROJECT.bat

- GET_MODUELS_FOR_PROJECT.bat 

We have provided this batch as an example or resource that you can copy straight into you project 
You can then run this batch and it will go grab the modules (DLLs) and copy them to the location in which this 
batch file was run. You can edit this default batch to ensure it is finding the Modules Directory correctly
and also select an additional output directory.

Essentially it is calling 

call [Environment Variable]COPY_MODULES_TO_PROJECT.bat [NA][ModuleName][BuildType]

Environment Variables
This can be enhanced with Environment variables
To do this go to "Control Panel -> System and Security -> System" and select Advanced system settings
Can also be achived by right clicking "Computer" and selecting properties
Then select Environmetn Variables.

Under system variables add an environment variable (via New) for both:

- LHS_MODUELS 
	- Location of the lhs modules sourcetree clone


So to clarify

- To use this we need to copy and paste GET_MODULES_FOR_PROJECT.bat into the directory in theproject in which we want the dll to reside
- we then setup this batch file witht he module name and the build type
- and double click on the batch file